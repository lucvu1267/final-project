const CracoLessPlugin = require("craco-less");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": "#0d656d",
              "@btn-primary-color": "#0d656d",
              "@border-radius-base": "6px",
              "@btn-border-radius-base": "9px",
              // "@btn-padding-horizontal-base": "5rem",
              // "@btn-padding-vertical-base": "2rem",
              "@input-padding-horizontal-base": "1rem",
              "@input-padding-vertical-base": "0.8rem",
              "@font-size-base": "14px",
              "@btn-default-color": "#fbfbfb",
              "@select-border-color": "rgb(90, 146, 153)",
              // "@text-color": "#285158",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
