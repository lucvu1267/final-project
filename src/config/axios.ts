import axios from "axios";
import { JWT_AUTH } from "constants/index";
import { useNavigate } from "react-router";
import { toastCustom } from "./toast";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  // timeout: 1000,
  headers: { "X-Requested-With": "XMLHttpRequest" },
  responseType: "json",
});

// Add a request interceptor
axiosInstance.interceptors.request.use(
  function (config: any) {
    const token = localStorage.getItem(JWT_AUTH);
    if (token) {
      config.headers.Authorization = "Bearer " + token;
    }
    // Do something before request is sent
    return config;
  },
  function (error) {
    // toastCustom("error", "Lỗi hệ thống!");
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosInstance.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response.data;
  },
  function (error) {
const navigate = useNavigate();
    console.log(error);
    if (error?.response?.data.status === 500) {
      toastCustom("error", "Lỗi hệ thống!");
      // navigate("/error-page")
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default axiosInstance;
