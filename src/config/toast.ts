import { toast } from "react-toastify";

export declare type TypeToast = "error" | "success";

export const toastCustom = (type: TypeToast, mess: any) => {
  toast[type](mess, {
    position: "top-right",
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme:"colored"
  });
};
