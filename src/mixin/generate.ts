import axios from "axios";
import { toastCustom } from "config/toast";
import { ROLE_EVENT } from "constants/event";

export const uploadImgCloudinary = async (file: any) => {
  if (isEmpty(file)) return null;
  let formData = new FormData();
  formData.append("upload_preset", "ems-fpt");
  formData.append("file", file);
  try {
    const data: any = await axios.post(
      "https://api.cloudinary.com/v1_1/dcgjui0yd/image/upload",
      formData
    );
    return data.data.url;
  } catch (error) {
    toastCustom("error", "Lỗi upload ảnh!");
  }
  return null;
};

export const isEmpty = (value: any) => {
  return (
    value === null ||
    value === undefined ||
    value === "" ||
    value === "undefined"
  );
};

export const getRoleMemberEvent = (role: any) => {
  switch (role) {
    case ROLE_EVENT.HEADER:
      return "Trưởng ban tổ chức";
    case ROLE_EVENT.SUB_HEADER:
      return "Quản trị viên";
    case ROLE_EVENT.MEMBER:
      return "Thành viên";
  }
};

export const numberFormat = (value: any) =>
{ 
   try {
    return new Intl.NumberFormat("vi-VN", {
    // style: "currency",
    currency: "VND",
  }).format(value);
  } catch (error) {
    return value?value.substring(value.length-1,value.length):""
  }
}
 

export const getMessStatus = (status: any) => {
  if (status === "DONE") {
    return "Sự kiện đã hoàn thành, bạn không thể thực hiện thao tác này!";
  } else if (status === "CANCEL") {
    return "Sự kiện đã bị hủy, bạn không thể thực hiện thao tác này!";
  }
};
