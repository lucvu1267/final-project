import React, { useEffect } from "react";
import "./auth_layout.scss";
import companycasel1 from "assets/images/company-casel-1.jpg";
import companycasel2 from "assets/images/company-casel-2.jpg";
import companycasel3 from "assets/images/company-casel-3.jpg";
import logo from "assets/images/logo.png";
import { Carousel, Typography } from "antd";
import { useNavigate } from "react-router-dom";
import { JWT_AUTH } from "constants/index";

const { Title } = Typography;
function AuthLayout({ children }: any) {
  const onChange = (currentSlide: number) => {};
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem(JWT_AUTH)) {
      navigate("/");
    }
  }, []);
  return (
    <div className="container-auth">
      <div className="wrap-carousel">
        <Carousel className="carousel" afterChange={onChange} autoplay>
          <img src={companycasel1} alt="" className="carousel-item" />
          <img src={companycasel2} alt="" className="carousel-item" />
          <img src={companycasel3} alt="" className="carousel-item" />
        </Carousel>
      </div>
      <div className="wrap-content">
        <div className="logo-container">
          <img
            onClick={() => navigate("/")}
            className="logo-auth"
            src={logo}
            alt=""
          />
          <Title className="logo-title" level={4}>
            {/* Chào mừng bạn đến với <span>EMS</span> */}
          </Title>
        </div>
        {children}
      </div>
    </div>
  );
}

export default AuthLayout;
