import { useEffect, useState } from "react";
import "./default-layout.scss";
import { Col, Row, Dropdown, Avatar, BackTop, Button } from "antd";
import logo from "assets/images/logo.png";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  UpCircleOutlined,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { JWT_AUTH } from "constants/index";
import { useDispatch, useSelector } from "react-redux";
import { setIsAuth } from "redux/slice";
import { getInfoUser, setUser } from "features/contact/contactSlice";
import BreadCrumb from "components/common/BreadCrumb";
import {
  propsEvent,
  setUserEventPops,
} from "features/eventManage/eventManageSlice";

export const DefaultLayout = ({ children }: any) => {
  const { idEvent } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const isAuthen = useSelector((state: any) => state.slice.isAuth);
  const isLoading = useSelector((state: any) => state.loadingSlice.isLoading);
  const user = useSelector((state: any) => state.contactSlice.user);
  const [isRender, setIsRender] = useState(false);
  const handleLogout = (event: any) => {
    localStorage.removeItem(JWT_AUTH);
    dispatch(setIsAuth(false));
    navigate("/login");
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={() => navigate("/profile")}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <UserOutlined
            className="icon"
            style={{ fontSize: "16px", marginRight: "5px" }}
          />{" "}
          Thông tin cá nhân
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          onClick={handleLogout}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <LogoutOutlined
            className="icon"
            style={{
              fontSize: "16px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Đăng xuất
        </div>
      ),
    },
  ];

  useEffect(() => {
    if (Object.keys(user).length === 0 && localStorage.getItem(JWT_AUTH)) {
      handleGetInfoUser();
    }
  }, []);

  useEffect(() => {
    if (idEvent) {
      getPropsEvent();
    } else {
      setIsRender(true);
      dispatch(setUserEventPops({}));
    }
  }, [idEvent]);

  const getPropsEvent = async () => {
    setIsRender(false);
    try {
      const data = await propsEvent(idEvent);
      dispatch(setUserEventPops(data.data));
    } catch (error) {
    } finally {
      setIsRender(true);
    }
  };

  const handleGetInfoUser = async () => {
    try {
      const data = await getInfoUser();
      dispatch(setUser(data.data));
    } catch (error) {
    } finally {
    }
  };

  return (
    <>
      {isRender && (
        <Col className={`container-default-layout ${isLoading && "loading"}`}>
          <Row style={{ marginBottom: "4rem", fontSize: "16px" }}>
            <Col span={9}>
              <img
                className="logo"
                src={logo}
                alt=""
                onClick={() => navigate("/")}
              />
            </Col>
            <Col span={9} className="wraper-link">
              <Row justify="space-between">
                {/* <Col></Col> */}
                <Link
                  className={location.pathname === "/" ? "link-active" : ""}
                  to={"/"}
                >
                  Trang chủ
                </Link>
                <Link
                  className={
                    location.pathname.includes("/event-manage")
                      ? "link-active"
                      : ""
                  }
                  to={"/event-manage"}
                >
                  Quản lý sự kiện
                </Link>
                <Link
                  className={
                    location.pathname.includes("/task") ? "link-active" : ""
                  }
                  to={"/task"}
                >
                  Quản lý công việc
                </Link>
                <Link
                  className={
                    location.pathname.includes("/contact") ? "link-active" : ""
                  }
                  to={"/contact"}
                >
                  Danh bạ
                </Link>
              </Row>
            </Col>
            <Col span={6} className="wraper-button">
              {isAuthen ? (
                <Row>
                  <Dropdown menu={{ items }} placement="bottom" arrow>
                    {user && (
                      <div className="wrapper-avatar">
                        <Avatar src={user.imagePath} />{" "}
                        <div className="name-profile">{user.firstName}</div>{" "}
                      </div>
                    )}
                  </Dropdown>
                </Row>
              ) : (
                <Row justify="end">
                  <Link
                    className={
                      location.pathname.includes("/login") ? "link-active" : ""
                    }
                    to={"/login"}
                  >
                    <Button className="btn btn-login">Đăng nhập</Button>
                  </Link>

                  <Link
                    className={
                      location.pathname.includes("/register")
                        ? "link-active"
                        : ""
                    }
                    to={"/register"}
                  >
                    <Button className="btn btn-register">Đăng ký</Button>
                  </Link>
                </Row>
              )}
            </Col>
          </Row>
          <BreadCrumb />
          <Row className="wrapper-body">{children}</Row>
          <Row></Row>
        </Col>
      )}
      <BackTop>
        <UpCircleOutlined className="icon" />
      </BackTop>
      {/* <Loading /> */}
    </>
  );
};
