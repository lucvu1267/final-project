import { Routes, Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

function RouterList({ listRoute, authComponent }: any) {
  return (
    <Routes>
      <Route element={<PrivateRoute />}>
        {listRoute.map((item: any, idx: number) => {
          return (
            item.private && (
              <Route
                key={idx}
                path={item.path}
                element={
                  <item.layout>
                    <item.component />
                  </item.layout>
                }
              />
            )
          );
        })}
      </Route>
      {listRoute.map((item: any, idx: number) => {
        return (
          !item.private && (
            <Route
              key={idx}
              path={item.path}
              element={
                <item.layout>
                  <item.component />
                </item.layout>
              }
            />
          )
        );
      })}
    </Routes>
  );
}

export default RouterList;
