import ErrorPage from "components/common/403/ErrorPage";
import ErrorPermission from "components/common/403/ErrorPermission";
import AcceptJoin from "components/common/joinEvent/AcceptJoin";
import RejectJoin from "components/common/joinEvent/RejectJoin";
import AddEvent from "components/event/addEvent/AddEvent";
import EventDetail from "components/event/eventDetail/EventDetail";
import ActiveConfirm from "features/auth/activeConfirm/ActiveConfirm";
import ForgotPassword from "features/auth/forgotPassword/ForgotPassword";
import ResetPassword from "features/auth/forgotPassword/ResetPassword";
import Login from "features/auth/login/Login";
import Register from "features/auth/register/Register";
import { Contacts } from "features/contact/Contacts";
import ContactDetailed from "features/contact/detailContact/[contactId]";
import Agenda from "features/eventManage/agenda/Agenda";
import Booth from "features/eventManage/booth";
import RegisterBooths from "features/eventManage/booth/RegisterBooths";
import CheckAttenDance from "features/eventManage/checkAttendance";
import { ListEvent } from "features/eventManage/ListEvent";
import ListParticipants from "features/eventManage/participants/ListParticipants";
import AddPost from "features/eventManage/post/addPost/AddPost";
import PostDetailHomePage from "features/eventManage/post/postDetailHomePage/PostDetailHomePage";
import PostsByEvent from "features/eventManage/post/PostsByEvent";
import Sponsor from "features/eventManage/sponsor";
import TeamManageIndex from "features/eventManage/team/index";
import TeamManage from "features/eventManage/team/TeamManage";
import ListTicket from "features/eventManage/ticket/ListTicket";
import { Homepage } from "features/home/Homepage";
import ChangePassword from "features/profile/changePassword/ChangePassword";
import DetailProfile from "features/profile/detailProfile/DetailProfile";
import EditProfile from "features/profile/editProfile/EditProfile";
import Tasks from "features/task/Tasks";
import AuthLayout from "layouts/auth/AuthLayout";
import { DefaultLayout } from "layouts/default/DefaultLayout";
import { Fragment } from "react";

export const authRoutes = [
  { path: "/login", component: Login, layout: AuthLayout, private: false },
  {
    path: "/register",
    component: Register,
    layout: AuthLayout,
    private: false,
  },
  {
    path: "/forgot-password",
    component: ForgotPassword,
    layout: AuthLayout,
    private: false,
  },
  {
    path: "/profile/change-password",
    component: ChangePassword,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/profile/edit",
    component: EditProfile,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/profile",
    component: DetailProfile,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/reset-password",
    component: ResetPassword,
    layout: AuthLayout,
    private: false,
  },
  { path: "/", component: Homepage, layout: DefaultLayout, private: false },
  {
    path: "/event-manage",
    component: ListEvent,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/participant",
    component: ListParticipants,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/add",
    component: AddEvent,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/detail/:idEvent",
    component: EventDetail,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/edit/:idEvent",
    component: AddEvent,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/teams",
    component: TeamManageIndex,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/team/:idTeam",
    component: TeamManage,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/post",
    component: PostsByEvent,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/ticket",
    component: ListTicket,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/post/add",
    component: AddPost,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/post/:idPost/update",
    component: AddPost,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/post-detail/:idPost",
    component: PostDetailHomePage,
    layout: DefaultLayout,
    private: false,
  },

  { path: "/task", component: Tasks, layout: DefaultLayout, private: true },
  {
    path: "/contact",
    component: Contacts,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/contact/detail/:contactId",
    component: ContactDetailed,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/agenda",
    component: Agenda,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/booths",
    component: Booth,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/event-manage/:idEvent/sponsors",
    component: Sponsor,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/activated",
    component: ActiveConfirm,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/error-page",
    component: ErrorPage,
    layout: Fragment,
    private: false,
  },
  {
    path: "/not-permission",
    component: ErrorPermission,
    layout: Fragment,
    private: false,
  },
  {
    path: "/booth/:idEventBooth",
    component: RegisterBooths,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/attendance/:idEventBooth",
    component: CheckAttenDance,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/web/:idEventWeb",
    component: Homepage,
    layout: DefaultLayout,
    private: true,
  },
  {
    path: "/accept-join-event",
    component: AcceptJoin,
    layout: DefaultLayout,
    private: false,
  },
  {
    path: "/reject-join-event",
    component: RejectJoin,
    layout: DefaultLayout,
    private: true,
  },
];

export const publicRoutes = [
  { path: "/", component: Homepage, layout: DefaultLayout },
];
