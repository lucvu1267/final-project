import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

function PrivateRoute(props: any) {
  const isAuthen = useSelector((state: any) => state.slice.isAuth);
  return isAuthen ? <Outlet /> : <Navigate to="/login" />;
}

export default PrivateRoute;
