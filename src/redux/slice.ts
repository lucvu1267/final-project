import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
  name: "loading",
  initialState: {
    isAuth: false,
  },
  reducers: {
    setIsAuth: (state, action) => {
      state.isAuth = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setIsAuth } = slice.actions;

export default slice.reducer;
