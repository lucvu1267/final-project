import loadingSlice from "components/common/loading/loadingSlice";
import contactSlice from "features/contact/contactSlice";
import eventManageSlice from "features/eventManage/eventManageSlice";
import homeSlice from "features/home/homeSlice";
import taskSlice from "features/task/taskSlice";
import { combineReducers } from "redux";
import slice from "./slice";

const rootReducer = combineReducers({
  slice: slice,
  homeSlice: homeSlice,
  loadingSlice: loadingSlice,
  eventManageSlice: eventManageSlice,
  contactSlice: contactSlice,
  taskSlice: taskSlice,
});

export default rootReducer;
