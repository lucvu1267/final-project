import "./event-item.scss";
import logo from "assets/images/logo.png";
import { Card } from "antd";
import EventOverview from "./EventOverview";
import { useNavigate } from "react-router-dom";

function EnventItem(props: any) {
  const { eventDetail } = props;
  const navigate = useNavigate();

  return (
    <Card
      onClick={() => {
        navigate(`/event-manage/detail/${eventDetail.id}`);
      }}
      className="container-event-item"
      hoverable
      cover={
        <img
          className="img-event-overview"
          width={100}
          alt="example"
          src={eventDetail.logoPath ? eventDetail.logoPath : logo}
        />
      }
    >
      <EventOverview eventDetail={eventDetail} />
    </Card>
  );
}

export default EnventItem;
