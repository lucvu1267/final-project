import { CleaningServices } from "@mui/icons-material";
import { Col, Input, Row, Card, Select, Tooltip, Form } from "antd";
import TextArea from "antd/lib/input/TextArea";
import HeaderFeature from "components/common/HeaderFeature";
import { setLoading } from "components/common/loading/loadingSlice";
import MessError from "components/common/MessError";
import UploadImage from "components/common/uploadImage/UploadImage";
import { toastCustom } from "config/toast";
import { TYPE_POST } from "constants/event";
import { createPost, updatePost } from "features/eventManage/eventManageSlice";
import { isEmpty, uploadImgCloudinary } from "mixin/generate";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import "./step-one.scss";

const { Option } = Select;

function StepOne(props: any) {
  const {
    handleNextStep,
    isCheckNextStep,
    setIsCheckNextStep,
    isCreatePost,
    content,
    setCurrent,
    postDetail,
    setidPostAfterAddOrUpdate,
  } = props;
  const dispatch = useDispatch();
  const { idPost, idEvent } = useParams();
  const [form] = Form.useForm();

  const [file, setFile] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);
  const [dataOverview, setDataOverview] = useState<any>({});

  const handleSubmitStepOne = (value: any) => {
    if (value.title) {
      handleNextStep();
      setDataOverview(value);
    }
  };
  const handleCreatePost = async () => {
    try {
      dispatch(setLoading(true));
      const urlImage = await uploadImgCloudinary(file);
      console.log(idEvent);
      const payload = {
        content: content,
        eventId: parseInt(idEvent + ""),
        overviewDescription: dataOverview.description,
        overviewImagePath: urlImage,
        postType: TYPE_POST.EVENT,
        subject: dataOverview.title,
        tags: null,
      };
      const payloadUpdate = {
        content: content,
        eventId: parseInt(idEvent + ""),
        id: idPost ? parseInt(idPost) : null,
        overviewDescription: dataOverview.description,
        overviewImagePath: isEmpty(file)
          ? postDetail.overviewImagePath
          : urlImage,
        subject: dataOverview.title,
        status: "ACTIVE",
      };
      if (idPost) {
        const data = await updatePost(payloadUpdate);
        setidPostAfterAddOrUpdate(data.data.id);
      } else {
        const data = await createPost(payload);
        setidPostAfterAddOrUpdate(data.data.id);
      }

      setCurrent(2);
      // toastCustom("success", "Tạo bài viết thành công");
    } catch (e) {
      toastCustom("error", "Tạo bài viết thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  useEffect(() => {
    if (isCheckNextStep) {
      form.submit();
      setIsCheckNextStep(false);
    }
  }, [isCheckNextStep]);

  useEffect(() => {
    if (postDetail) {
      form.setFieldsValue({
        title: postDetail?.subject,
        description: postDetail?.overviewDescription,
      });
    }
  }, [postDetail]);

  useEffect(() => {
    if (isCreatePost) {
      handleCreatePost();
    }
  }, [isCreatePost]);

  return (
    <div className="container-step-one">
      <HeaderFeature content={"Tổng quan bài viết"} />
      <Card className="wrapper-card">
        <Row justify="center">
          <Tooltip placement="top" title="Thêm ảnh bài viết" arrowPointAtCenter>
            <div>
              <UploadImage
                imgUrlInit={postDetail?.overviewImagePath}
                setFile={setFile}
                isResetImageUrl={isResetImageUrl}
                setDefaultReset={() => setIsResetImageUrl(false)}
              />
            </div>
          </Tooltip>
        </Row>
        {/* {postDetail?.subject && ( */}
        <Form
          form={form}
          onFinish={(value: any) => handleSubmitStepOne(value)}
          autoComplete="off"
        >
          <Row justify="center">
            <Col className="wrapper-field" span={24}>
              <div className="field-add">
                <div className="label-field">
                  Tiêu đề bài viết<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="title"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập tiêu đề!"} />,
                    },
                    {
                      max: 255,
                      message: (
                        <MessError
                          message={"Tiêu đề không được quá 255 kí tự!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Tiêu đề" />
                </Form.Item>
              </div>
            </Col>
            <Col className="wrapper-field" span={24}>
              <div className="field-add">
                <div className="label-field">
                  Mô tả<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="description"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập mô tả!"} />,
                    },
                  ]}
                >
                  <TextArea
                    placeholder="Mô tả"
                    className="description"
                    // onChange={getDataPost}
                  ></TextArea>
                </Form.Item>
              </div>
            </Col>
          </Row>
        </Form>
      </Card>
    </div>
  );
}

export default StepOne;
