import React from "react";
import luc_vu from "assets/images/luc_vu.jpeg";
import "./event-overview.scss";
import { Avatar, Card, Row, Tooltip } from "antd";
import { MAX_LENGTH_DESCRIPTION, MAX_LENGTH_TITLE } from "constants/event";
import {
  TeamOutlined,
  ReconciliationOutlined,
  UserOutlined,
  ControlOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { FORMAT_DATE } from "constants/index";
import { isEmpty } from "mixin/generate";
const { Meta } = Card;

function EventOverview(props: any) {
  const { eventDetail } = props;
  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  const getContentStatus = (status: any) => {
    if (status === "INPROGRESS" || isEmpty(status)) {
      return {
        className: "attendence",
        text: "Đang hoạt động",
      };
    } else if (status === "DONE") {
      return {
        className: "accept",
        text: "Đã hoàn thành",
      };
    } else if (status === "CANCEL") {
      return {
        className: "absent",
        text: "Đã bị hủy",
      };
    }
  };

  return (
    <div className="container-event-overview">
      <Meta />
      <div className="wrapper-content">
        <div className="title label-grayest">
          {subTitle(eventDetail.name, MAX_LENGTH_TITLE)}
        </div>
        <div className="description">
          {subTitle(eventDetail.description, MAX_LENGTH_DESCRIPTION)}
        </div>
        <div className="wrapper-more-content">
          <div className="content">
            <Tooltip
              placement="top"
              title="Trưởng ban tổ chức"
              arrowPointAtCenter
            >
              <UserOutlined className="icon-gray" />
            </Tooltip>
            <span className="label-grayest">
              {eventDetail.creator.firstName}
            </span>
          </div>
          <div className="content">
            <Tooltip
              placement="top"
              title="Thời gian chạy sự kiện"
              arrowPointAtCenter
            >
              <ReconciliationOutlined className="icon-gray" />
            </Tooltip>
            <span className="label-grayest">
              {moment(eventDetail.beginDate).format(FORMAT_DATE)} -{" "}
              {moment(eventDetail.endDate).format(FORMAT_DATE)}
            </span>
          </div>
          <div className="content">
            <Tooltip
              placement="top"
              title="Số lượng thành viên tham gia"
              arrowPointAtCenter
            >
              <TeamOutlined className="icon-gray" />
            </Tooltip>
            <span className="label-grayest">{eventDetail.noMembers}</span>
          </div>
          <div className="content">
            <Tooltip placement="top" title="Trạng thái" arrowPointAtCenter>
              <ControlOutlined className="icon-gray" />
            </Tooltip>
            <span
              className={`label--${
                getContentStatus(eventDetail.status)?.className
              }`}
              // style={{ display: "flex", justifyContent: "end" }}
            >
              <span style={{ fontWeight: "600" }}>
                {" "}
                {getContentStatus(eventDetail.status)?.text}
              </span>
            </span>
          </div>
        </div>{" "}
      </div>
    </div>
  );
}

export default EventOverview;
