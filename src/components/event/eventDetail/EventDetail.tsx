import {
  Badge,
  Button,
  Col,
  Dropdown,
  message,
  Modal,
  Row,
  Tooltip,
} from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import "./event-detail.scss";
import {
  TagsOutlined,
  ContactsOutlined,
  ShopOutlined,
  FileTextOutlined,
  AuditOutlined,
  TeamOutlined,
  GlobalOutlined,
  ReconciliationOutlined,
  UserOutlined,
  FileProtectOutlined,
  EditOutlined,
  EyeOutlined,
  SettingFilled,
  MailOutlined,
  CloseCircleOutlined,
  CheckCircleOutlined,
  ControlOutlined,
  ArrowRightOutlined,
  WarningOutlined,
  ClockCircleOutlined,
  ExclamationCircleOutlined,
  FrownOutlined,
} from "@ant-design/icons";
import { useNavigate, useParams } from "react-router-dom";
import Note from "./Note";
import {
  changeStatusEvent,
  getAgenda,
  getEventById,
  getLogEvent,
  updateEvent,
} from "features/eventManage/eventManageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import Agenda from "features/eventManage/agenda/Agenda";
import { FORMAT_DATE, FORMAT_DATE_FULL } from "constants/index";
import moment from "moment";
import { parseISO } from "date-fns";
import { ROLE_MEMBER } from "constants/event";
import ModalConfigMail from "components/common/modalConfigMail/ModalConfigMail";
import { TYPE_CONFIG_MAIL } from "components/common/modalConfigMail/configMailDefault";
import { toastCustom } from "config/toast";
import ModalUpdateStatusEvent from "components/common/modalUpdateStatusEvent/ModalUpdateStatusEvent";
import { getMessStatus, isEmpty } from "mixin/generate";

const TYPE_MODAL_STATUS = {
  DONE: 1,
  CANCEL: 2,
};

function EventDetail(props: any) {
  const [messageApi, contextHolder] = message.useMessage();
  const { idEvent } = useParams();
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isOpenModalWarnTask, setIsOpenModalWarnTask] = useState(false);
  const [isOpenModalConfigMail, setIsOpenModalConfigMail] = useState(false);
  const [isOpenModalUpdateStatus, setIsOpenModalUpdateStatus] = useState(false);
  const [isOpenModalLogStatus, setIsOpenModalLogStatus] = useState(false);
  const [eventDetail, setEventDetail] = useState<any>({});
  const [listAgenda, setListAgenda] = useState<any>([]);
  const [noteContent, setNoteContent] = useState<any>(null);
  const [typeModal, setTypeModal] = useState<any>(null);
  const [logEvent, setLogEvent] = useState<any>(null);
  const [typeModalUpdateSatus, setTypeModalUpdateSatus] = useState<any>(null);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };
  const handleCloseModalWarnTask = () => {
    setIsOpenModalWarnTask(false);
  };

  useEffect(() => {
    dispatch(setLoading(true));
    Promise.all([
      handleGetEventById(),
      handleGetListAgenda(),
      handleGetLogEvent(),
    ]).finally(() => dispatch(setLoading(false)));
  }, []);

  const handleGetEventById = async () => {
    try {
      const data = await getEventById(idEvent);
      setEventDetail(data.data);
      setNoteContent(data.data.note);
    } catch (error) {
    } finally {
    }
  };

  const handleGetLogEvent = async () => {
    try {
      const data = await getLogEvent(idEvent);
      setLogEvent(data.data[0]);
      console.log(data.data[0]);
    } catch (error) {
    } finally {
    }
  };

  const handleGetListAgenda = async () => {
    try {
      const data = await getAgenda(idEvent);
      setListAgenda(
        data.data.map((item: any) => ({
          event_id: item.id,
          title: item.name,
          start: parseISO(item.startDate),
          end: parseISO(item.endDate),
          admin_id: item.event.id,
          editable: true,
          deletable: true,
          color: "rgb(174 235 187)",
          createDate: item.createDate,
          description: item.description,
          startDate: item.startDate,
          endDate: item.endDate,
        }))
      );
    } catch (error) {
      dispatch(setLoading(false));
    }
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            }
            e.stopPropagation();
            navigate(`/event-manage/edit/${idEvent}`);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <EditOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Chỉnh sửa thông tin sự kiện
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <span
          onClick={(e) => {
            // e.stopPropagation();
            // setTypeModal(TYPE_CONFIG_MAIL.PARTICIPAN);
            // setIsOpenModalConfigMail(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <MailOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Mẫu gửi email
        </span>
      ),

      children: [
        {
          key: "2-1",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setTypeModal(TYPE_CONFIG_MAIL.PARTICIPAN);
                setIsOpenModalConfigMail(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <EyeOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Người tham dự
            </div>
          ),
        },
        {
          key: "2-2",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setTypeModal(TYPE_CONFIG_MAIL.INVITED_PARTICIPAN);
                setIsOpenModalConfigMail(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <EyeOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Khách mời
            </div>
          ),
        },
        {
          key: "2-3",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setTypeModal(TYPE_CONFIG_MAIL.TICKET_ORDER);
                setIsOpenModalConfigMail(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <EyeOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Người mua vé
            </div>
          ),
        },
      ],
    },
    {
      type: "divider",
    },
    {
      key: "3",
      label: eventDetail?.status === "INPROGRESS" && (
        <span
          onClick={(e) => {
            // e.stopPropagation();
            // setTypeModal(TYPE_CONFIG_MAIL.INVITED_PARTICIPAN);
            // setIsOpenModalConfigMail(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <ControlOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Trạng thái
        </span>
      ),
      children: [
        {
          key: "3-1",
          label: (
            <div
              onClick={(e) => {
                setTypeModalUpdateSatus(TYPE_MODAL_STATUS.CANCEL);
                setIsOpenModalUpdateStatus(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <CloseCircleOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Hủy sự kiện
            </div>
          ),
        },
        {
          key: "3-2",
          label: (
            <div
              onClick={(e) => {
                handleChangeStatusTask();
                // setTypeModalUpdateSatus(TYPE_MODAL_STATUS.DONE);
                // setIsOpenModalUpdateStatus(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <CheckCircleOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Hoàn thành sự kiện
            </div>
          ),
        },
      ],
    },
  ];

  const handleUpdateNote = async () => {
    if (user.id !== eventDetail?.creator?.id) {
      return messageApi.open({
        type: "error",
        content: "Bạn không có quyền!",
      });
    } else if (
      roleUser?.event?.status === "DONE" ||
      roleUser?.event?.status === "CANCEL"
    ) {
      return message.open({
        type: "error",
        content: getMessStatus(roleUser?.event?.status),
      });
    }
    dispatch(setLoading(true));
    const dataBodyUpdate = {
      id: idEvent,
      autoConfirm: eventDetail.autoconfirm,
      beginDate: eventDetail.beginDate,
      endDate: eventDetail.endDate,
      code: eventDetail.code,
      description: eventDetail.description,
      logoPath: eventDetail.logoPath,
      name: eventDetail.name,
      note: noteContent,
      tags: null,
    };
    await updateEvent(dataBodyUpdate);
    await handleGetEventById();
    dispatch(setLoading(false));
    handleCloseModal();
    toastCustom("success", "Cập nhật thành công");
  };

  const getContentStatus = (status: any) => {
    if (status === "INPROGRESS" || isEmpty(status)) {
      return {
        className: "attendence",
        text: "Đang hoạt động",
      };
    } else if (status === "DONE") {
      return {
        className: "accept",
        text: "Đã hoàn thành",
      };
    } else if (status === "CANCEL") {
      return {
        className: "absent",
        text: "Đã bị hủy",
      };
    }
  };

  const handleChangeStatusTask = async () => {
    // dispatch(setLoading(true));
    const payload = {
      checkTaskDone: true,
      contentMail: null,
      eventId: idEvent,
      note: "",
      status: "DONE",
    };
    const data: any = await changeStatusEvent(payload);
    if (data.errorCode === "EVENT_HAS_NOT_COMPLETED_TASK") {
      setIsOpenModalWarnTask(true);
    } else {
      setTypeModalUpdateSatus(TYPE_MODAL_STATUS.DONE);
      setIsOpenModalUpdateStatus(true);
    }
  };

  return (
    eventDetail && (
      <>
        {contextHolder}
        {user.id === eventDetail?.creator?.id && (
          <Dropdown menu={{ items }} placement="bottomLeft" arrow>
            <SettingFilled className="icon" style={{ fontSize: "22px" }} />
          </Dropdown>
        )}
        <Row className="wrapper-title-agenda">
          <div className="title">Thông tin sự kiện</div>
        </Row>
        <div className="event-detail-container">
          <Row className="wrapper-feature" justify="end">
            <Col span={2}>
              <Tooltip placement="top" title="Người tham dự" arrowPointAtCenter>
                {" "}
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.participantAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/participant`);
                  }}
                >
                  <Badge count={eventDetail.noParticipants}>
                    {" "}
                    {/* <SolutionOutlined /> */}
                    <ContactsOutlined className="icon-feature" />
                  </Badge>
                </div>{" "}
              </Tooltip>
              {/* 
            Người tham dự
          </div> */}
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Vé sự kiện" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.ticketAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/ticket`);
                  }}
                >
                  <TagsOutlined className="icon-feature" />
                </div>
              </Tooltip>
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Gian hàng" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.boothAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/booths`);
                  }}
                >
                  <ShopOutlined className="icon-feature" />
                </div>
              </Tooltip>
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Bài viết" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.postAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/post`);
                  }}
                >
                  <FileTextOutlined className="icon-feature" />
                </div>
              </Tooltip>
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Nhà tài trợ" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.sponsorAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/sponsors`);
                  }}
                >
                  <AuditOutlined className="icon-feature" />
                </div>
              </Tooltip>
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Ban Tổ Chức" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    if (roleUser.organizationAccess === ROLE_MEMBER.NONE) {
                      return messageApi.open({
                        type: "error",
                        content: "Bạn không có quyền!",
                      });
                    }
                    navigate(`/event-manage/${idEvent}/teams`);
                  }}
                >
                  <Badge count={eventDetail.noMembers}>
                    <TeamOutlined className="icon-feature" />
                  </Badge>
                </div>
              </Tooltip>
            </Col>
            <Col span={2}>
              <Tooltip placement="top" title="Web sự kiện" arrowPointAtCenter>
                <div
                  className="item-feature"
                  onClick={() => {
                    navigate(`/web/${idEvent}`);
                  }}
                >
                  <GlobalOutlined className="icon-feature" />
                </div>
              </Tooltip>
            </Col>
          </Row>

          <Row className="wrapper-detail">
            <Row>
              <Col className="title-event" span={24}>
                {`[${eventDetail.code}] ${eventDetail.name}`}
              </Col>
              <Col span={24}>
                <p style={{ marginTop: "2rem" }} className="description-event">
                  {eventDetail.description}
                </p>
              </Col>
              <Row
                style={{ marginTop: "2rem" }}
                className="wrapper-more-content"
              >
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  className="content"
                >
                  <Tooltip
                    placement="top"
                    title="Trưởng ban tổ chức"
                    arrowPointAtCenter
                  >
                    <UserOutlined className="icon-gray" />
                  </Tooltip>
                  <span className="label-grayest">
                    {" "}
                    {eventDetail?.creator?.firstName}
                  </span>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  className="content"
                >
                  <Tooltip
                    placement="top"
                    title="Thời gian chạy sự kiện"
                    arrowPointAtCenter
                  >
                    <ReconciliationOutlined className="icon-gray" />
                  </Tooltip>
                  <span className="label-grayest">{`${moment(
                    eventDetail?.beginDate
                  ).format(FORMAT_DATE)} - ${moment(
                    eventDetail?.endDate
                  ).format(FORMAT_DATE)}`}</span>
                </Col>{" "}
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  className="content"
                >
                  <Tooltip placement="top" title="Ghi chú" arrowPointAtCenter>
                    <FileProtectOutlined className="icon-gray" />
                  </Tooltip>
                  {/* {user.id === eventDetail?.creator?.id && ( */}
                  <span
                    onClick={() => setIsOpenModal(true)}
                    className="label-grayest feild-link"
                    style={{ textDecoration: "underline" }}
                  >
                    Ghi chú
                  </span>
                  {/* )} */}
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  className="content"
                >
                  <Tooltip
                    placement="top"
                    title="Trạng thái"
                    arrowPointAtCenter
                  >
                    <ControlOutlined className="icon-gray" />
                  </Tooltip>
                  {/* {user.id === eventDetail?.creator?.id && ( */}
                  <span
                    onClick={() => {
                      eventDetail.status !== "INPROGRESS" &&
                        setIsOpenModalLogStatus(true);
                    }}
                    className={`label--${
                      getContentStatus(eventDetail.status)?.className
                    }`}
                    style={
                      eventDetail.status === "INPROGRESS"
                        ? { fontWeight: "600" }
                        : {
                            fontWeight: "600",
                            textDecoration: "underline",
                            cursor: "pointer",
                          }
                    }
                  >
                    {getContentStatus(eventDetail.status)?.text}
                  </span>
                  {eventDetail.status !== "INPROGRESS" && (
                    <ExclamationCircleOutlined
                      style={{ fontSize: "17px", cursor: "pointer" }}
                      onClick={() => {
                        setIsOpenModalLogStatus(true);
                      }}
                    />
                  )}
                  {/* )} */}
                </Col>
              </Row>
            </Row>
          </Row>

          <Modal
            width={1000}
            title="Ghi chú"
            open={isOpenModal}
            onCancel={handleCloseModal}
            centered={true}
            footer={[
              <Button
                className="btn"
                style={{ fontWeight: "bold" }}
                type="primary"
                onClick={() => {
                  handleUpdateNote();
                }}
              >
                Cập nhật
              </Button>,
            ]}
          >
            <Note noteContent={noteContent} setNoteContent={setNoteContent} />
          </Modal>
          <Modal
            width={500}
            title="Hoàn thành sự kiện"
            open={isOpenModalWarnTask}
            onCancel={handleCloseModalWarnTask}
            centered={true}
            footer={[
              <Tooltip placement="top" title={"Tiếp tục"} arrowPointAtCenter>
                {/* <ArrowRightOutlined /> */}
                <ArrowRightOutlined
                  style={{ fontSize: "22px" }}
                  onClick={() => {
                    handleCloseModalWarnTask();
                    setTypeModalUpdateSatus(TYPE_MODAL_STATUS.DONE);
                    setIsOpenModalUpdateStatus(true);
                  }}
                  className="icon"
                />
              </Tooltip>,
            ]}
          >
            <div
              className="container-buy-ticket-success "
              style={{ padding: "2rem" }}
            >
              <WarningOutlined
                className="icon-success"
                size={200}
                style={{ marginBottom: "1rem" }}
              />
              <p>
                Vẫn còn những công việc trong sự kiện chưa hoàn thành, hãy kiểm
                tra lại các công việc. Nếu bạn vẫn muốn đổi trạng thái, ấn icon
                "Tiếp tục".
              </p>
            </div>
          </Modal>
          <Modal
            width={500}
            title={getContentStatus(eventDetail.status)?.text}
            open={isOpenModalLogStatus}
            onCancel={() => setIsOpenModalLogStatus(false)}
            centered={true}
            footer={null}
          >
            <div
              // className="container-buy-ticket-success "
              style={{ padding: "2rem" }}
            >
              {/* <WarningOutlined
                className="icon-success"
                size={200}
                style={{ marginBottom: "1rem" }}
              /> */}
              <div
                className="container-buy-ticket-success "
                style={{ padding: "2rem" }}
              >
                {/* WarningOutlined */}
                {eventDetail.status === "CANCEL" && (
                  <CloseCircleOutlined
                    className="icon-success"
                    size={200}
                    style={{ marginBottom: "1rem" }}
                  />
                )}
                {eventDetail.status === "DONE" && (
                  <CheckCircleOutlined
                    className="icon-success"
                    size={200}
                    style={{ marginBottom: "1rem" }}
                  />
                )}
              </div>
              <div
                className="description-gray"
                style={{ marginBottom: "12px", fontSize: "12px" }}
              >
                <Tooltip
                  placement="top"
                  title="Thời gian hoàn thành sự kiện"
                  arrowPointAtCenter
                >
                  <span>
                    <ClockCircleOutlined />{" "}
                    {logEvent?.createDate &&
                      moment(logEvent?.createDate).format(FORMAT_DATE_FULL)}
                  </span>
                </Tooltip>
              </div>
              <p>{logEvent?.note}</p>
            </div>
          </Modal>
        </div>{" "}
        {roleUser.agendaAccess !== ROLE_MEMBER.NONE && (
          <Agenda
            listAgenda={listAgenda}
            handleGetListAgenda={handleGetListAgenda}
          />
        )}
        <ModalConfigMail
          eventDetail={eventDetail}
          typeModal={typeModal}
          isOpenModal={isOpenModalConfigMail}
          setIsOpenModal={setIsOpenModalConfigMail}
          handleGetEventById={handleGetEventById}
        />
        <ModalUpdateStatusEvent
          eventDetail={eventDetail}
          typeModal={typeModalUpdateSatus}
          isOpenModal={isOpenModalUpdateStatus}
          setIsOpenModal={setIsOpenModalUpdateStatus}
          handleGetEventById={handleGetEventById}
        />
      </>
    )
  );
}

export default EventDetail;
