import TextArea from "antd/lib/input/TextArea";
import React, { useState } from "react";
import "./note.scss";
function Note(props: any) {
  const { noteContent, setNoteContent } = props;

  const getDataPost = (e: any) => {
    setNoteContent(e.target.value);
  };

  return (
    <div className="wrapper-note">
      <TextArea
        placeholder="Ghi chú"
        className="note"
        onChange={getDataPost}
        defaultValue={noteContent}
      ></TextArea>
    </div>
  );
}
export default Note;
