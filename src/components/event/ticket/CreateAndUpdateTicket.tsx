import {
  Col,
  Row,
  Form,
  Input,
  Select,
  InputNumber,
  Space,
  DatePicker,
  ConfigProvider,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import { setLoading } from "components/common/loading/loadingSlice";
import MessError from "components/common/MessError";
import { toastCustom } from "config/toast";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import { LIST_FEATURE_EVENT } from "constants/event";
import {
  createTicketManager,
  updateTicket,
} from "features/eventManage/eventManageSlice";
import { numberFormat } from "mixin/generate";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import "./create-update-ticket.scss";
import moment from "moment";
import locale from "antd/es/locale/vi_VN";
const { Option } = Select;
const { RangePicker } = DatePicker;

function CreateAndUpdateTicket(props: any) {
  const {
    isCreateUpdateTicket,
    setIsCreateUpdateTicket,
    setIsUpdateDataTable,
    setOpenModalAddTicket,
    typeModal,
    dataDetal,
  } = props;

  const { idEvent } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const roleUser: any = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  useEffect(() => {
    if (isCreateUpdateTicket) {
      form.submit();
      setIsCreateUpdateTicket(false);
    }
  }, [isCreateUpdateTicket]);

  const handleSubmitForm = async (value: any) => {
    if (value.name) {
      const payload = {
        eventId: idEvent,
        description: value.description,
        limited: value.numberTicket,
        name: value.name,
        price: parseInt((value.cost + "").split(".").join("")),
        saleBeginDate: value.datetime[0]._d,
        saleEndDate: value.datetime[1]._d,
      };
      const paloadUpdate = {
        eventId: idEvent,
        id: dataDetal.id,
        description: value.description,
        limited: value.numberTicket,
        name: value.name,
        price: parseInt((value.cost + "").split(".").join("")),
        saleBeginDate: value.datetime[0]._d,
        saleEndDate: value.datetime[1]._d,
        status: "ACTIVE",
      };
      try {
        dispatch(setLoading(true));
        if (typeModal === TYPE_FORM.CREATE) {
          await createTicketManager(payload);
        } else {
          await updateTicket(paloadUpdate);
        }
        setIsUpdateDataTable(true);
        setOpenModalAddTicket(false);
        toastCustom(
          "success",
          typeModal === TYPE_FORM.CREATE
            ? "Tạo vé thành công"
            : "Cập nhật thành công"
        );
      } catch (error) {
        toastCustom(
          "error",
          typeModal === TYPE_FORM.CREATE
            ? "Tạo vé thất bại!"
            : "Cập nhật thất bại!"
        );
      } finally {
        dispatch(setLoading(false));
      }
    }
  };

  return (
    <div className="container-add-update-ticket">
      {" "}
      <Form
        form={form}
        onFinish={(value) => handleSubmitForm(value)}
        initialValues={
          typeModal === TYPE_FORM.UPDATE
            ? {
                name: dataDetal.name,
                numberTicket: dataDetal.limited,
                cost: dataDetal.price,
                description: dataDetal.description,
                datetime: [
                  moment(dataDetal?.saleBeginDate),
                  moment(dataDetal?.saleEndDate),
                ],
              }
            : {}
        }
        autoComplete="off"
        style={{ width: "100%" }}
      >
        <Row justify="center">
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="field-add">
              <div className="label-field">
                Tên<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="name"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: <MessError message={"Vui lòng nhập tên vé!"} />,
                  },
                  {
                    max: 255,
                    message: (
                      <MessError message={"Tên vé không được quá 255 kí tự!"} />
                    ),
                  },
                ]}
              >
                <Input className="input" placeholder="Tên vé" />
              </Form.Item>
            </div>
          </Col>
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="field-add">
              <div className="label-field">
                Số lượng vé<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="numberTicket"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: (
                      <MessError message={"Vui lòng nhập số lượng vé!"} />
                    ),
                  },
                  {
                    pattern: /^[0-9]+$/,
                    message: (
                      <MessError message={"Số lượng phải bao gồm các chữ số"} />
                    ),
                  },
                ]}
              >
                <Input className="input" placeholder="Số lượng vé" />
              </Form.Item>
            </div>
          </Col>
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="field-add">
              <div className="label-field">
                Giá vé
                {typeModal === TYPE_FORM.CREATE && (
                  <span style={{ color: "red" }}> *</span>
                )}
              </div>
              <Form.Item
                name="cost"
                className="text-field"
                rules={
                  typeModal === TYPE_FORM.CREATE
                    ? [
                        {
                          required: true,
                          message: (
                            <MessError message={"Vui lòng nhập giá vé!"} />
                          ),
                        },
                        {
                          pattern: /^[0-9.,]+$/,
                          message: (
                            <MessError
                              message={"Giá vé phải bao gồm các chữ số"}
                            />
                          ),
                        },
                      ]
                    : []
                }
              >
                <Input
                  disabled={typeModal !== TYPE_FORM.CREATE}
                  onInput={(e: any) => {
                    if (
                      e.target.value &&
                      !isNaN(e.target.value.split(".").join(""))
                    ) {
                      e.target.value =
                        e.target.value &&
                        !isNaN(e.target.value.split(".").join("")) &&
                        numberFormat(e.target.value.split(".").join(""));
                    } else {
                      e.target.value = e.target.value.substring(
                        0, e.target.value.length - 1
                      );
                    }

                    // (e.target.value =
                    //   e.target.value && !isNaN(e.target.value.split(".").join(""))&&
                    //   numberFormat(e.target.value.split(".").join("")))
                  }}
                  className="input"
                  placeholder="Giá vé"
                />
              </Form.Item>
            </div>
          </Col>
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
          >
            <div className="field-add">
              <div className="label-field">
                Thời gian mở bán vé<span style={{ color: "red" }}> *</span>
              </div>
              <Space direction="vertical" size={12}>
                <ConfigProvider locale={locale}>
                  <Form.Item
                    name="datetime"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng nhập thời gian sự kiện!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <RangePicker
                      showTime={{ format: "HH:mm" }}
                      format={"DD/MM/YYYY HH:mm"}
                      // disabledDate={(current) => {
                      //   let customDate = moment().format("DD/MM/YYYY HH:mm");
                      //   return (
                      //     current &&
                      //     current < moment(customDate, "DD/MM/YYYY HH:mm")
                      //   );
                      // }}
                      // disabledTime
                    />
                  </Form.Item>
                </ConfigProvider>
              </Space>
            </div>
          </Col>{" "}
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={24}
            lg={24}
            xl={24}
          >
            <div className="field-add" style={{ width: "90%" }}>
              <div className="label-field">
                Mô tả
                <span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="description"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: <MessError message={"Vui lòng nhập mô tả!"} />,
                  },
                  {
                    max: 1000,
                    message: (
                      <MessError message={"Mô tả không được quá 1000 kí tự!"} />
                    ),
                  },
                ]}
              >
                <TextArea
                  placeholder="Mô tả"
                  className="description"
                  // onChange={getDataPost}
                ></TextArea>
              </Form.Item>
            </div>
          </Col>
        </Row>
        <Row></Row>
      </Form>
    </div>
  );
}

export default CreateAndUpdateTicket;
