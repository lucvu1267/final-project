import {
  Input,
  Select,
  Space,
  DatePicker,
  Checkbox,
  Row,
  Col,
  Button,
  Form,
  Tooltip,
  Avatar,
  message,
} from "antd";

import React, { useState, useEffect } from "react";
import "./add-event.scss";
import TextArea from "antd/lib/input/TextArea";
import HeaderFeature from "components/common/HeaderFeature";
import MessError from "components/common/MessError";
import UploadImage from "components/common/uploadImage/UploadImage";
import {
  createEvent,
  getEventById,
  updateEvent,
} from "features/eventManage/eventManageSlice";
import { getMessStatus, isEmpty, uploadImgCloudinary } from "mixin/generate";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { toastCustom } from "config/toast";
import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";
const { RangePicker } = DatePicker;

function AddEvent(props: any) {
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const [file, setFile] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);
  const [eventDetail, setEventDetail] = useState<any>({});

  const handleCreateEvent = async (value: any) => {
    if (user.id !== eventDetail?.creator?.id && idEvent) {
      return message.open({
        type: "error",
        content: "Bạn không có quyền!",
      });
    } else if (
      roleUser?.event?.status === "DONE" ||
      roleUser?.event?.status === "CANCEL"
    ) {
      return message.open({
        type: "error",
        content: getMessStatus(roleUser?.event?.status),
      });
    }
    dispatch(setLoading(true));
    const urlImage = await uploadImgCloudinary(file);
    const dataBody = {
      autoConfirm: value.autoconfirm,
      beginDate: value.datetime[0]._d,
      endDate: value.datetime[1]._d,
      code: value.code,
      description: value.description,
      logoPath: urlImage,
      name: value.name,
      note: value.note,
      inchargeId: null, // value.assign
    };
    const dataBodyUpdate = {
      id: idEvent,
      autoConfirm: value.autoconfirm,
      beginDate: value.datetime[0]._d,
      endDate: value.datetime[1]._d,
      code: eventDetail.code,
      description: value.description,
      logoPath: isEmpty(file) ? eventDetail?.logoPath : urlImage,
      name: value.name,
      note: value.note,
      tags: null,
    };
    try {
      let data: any;
      if (idEvent) {
        data = await updateEvent(dataBodyUpdate);
      } else {
        data = await createEvent(dataBody);
      }
      navigate(`/event-manage/detail/${data.data.id}`);
      dispatch(setLoading(false));
      form.resetFields();
      setIsResetImageUrl(true);
      toastCustom(
        "success",
        idEvent ? "Cập nhật sự kiện thành công" : "Tạo sự kiện thành công"
      );
    } catch (error) {
      dispatch(setLoading(false));
      toastCustom(
        "error",
        idEvent ? "Cập nhật sự kiện thất bại!" : "Tạo sự kiện thất bại!"
      );
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetEventDetail = async () => {
    try {
      const data = await getEventById(idEvent);
      setEventDetail(data.data);
      form.setFieldsValue({
        name: data.data.name,
        datetime: [moment(data.data?.beginDate), moment(data.data?.endDate)],
        autoconfirm: data.data.autoConfirm,
        description: data.data.description,
        note: data.data.note,
      });
    } catch (error) {
    } finally {
    }
  };

  useEffect(() => {
    if (idEvent) handleGetEventDetail();
  }, []);

  return (
    <>
      <HeaderFeature
        content={idEvent ? "Chỉnh sửa thông tin sự kiện" : "Thêm sự kiện"}
      ></HeaderFeature>
      <Form
        form={form}
        onFinish={(value) => handleCreateEvent(value)}
        autoComplete="off"
        className="add-event-container"
      >
        <Row justify="center" style={{ marginBottom: "1rem" }}>
          <Tooltip
            placement="top"
            title="Thêm ảnh cho sự kiện"
            arrowPointAtCenter
          >
            <div>
              <UploadImage
                imgUrlInit={eventDetail?.logoPath}
                setFile={setFile}
                isResetImageUrl={isResetImageUrl}
                setIsResetImageUrl={setIsResetImageUrl}
              />
            </div>
          </Tooltip>
        </Row>
        {!idEvent && (
          <Row justify="space-between">
            <Col xs={24} sm={24} md={8} lg={8} xl={8}>
              <div className="field-add">
                <div className="label-field ">
                  Mã sự kiện<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="code"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError message={"Vui lòng nhập mã sự kiện!"} />
                      ),
                    },
                    {
                      min: 5,
                      message: (
                        <MessError
                          message={"Mã sự kiện phải từ [5-10] kí tự!"}
                        />
                      ),
                    },
                    {
                      max: 10,
                      message: (
                        <MessError
                          message={"Mã sự kiện phải từ [5-10] kí tự!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Input
                    onInput={(e: any) =>
                      (e.target.value = e.target.value.toUpperCase())
                    }
                    className="input"
                    placeholder="Mã sự kiện"
                  />
                </Form.Item>
              </div>
            </Col>
          </Row>
        )}
        <Row justify="space-between">
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <div className="field-add field-row">
              <div className="label-field">
                Tên sự kiện<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="name"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: (
                      <MessError message={"Vui lòng nhập tên sự kiện!"} />
                    ),
                  },
                  {
                    max: 255,
                    message: (
                      <MessError
                        message={"Tên sự kiện không được quá 255 kí tự!"}
                      />
                    ),
                  },
                ]}
              >
                <Input className="input" placeholder="Tên sự kiện" />
              </Form.Item>
            </div>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <div className="field-add ">
              <div className="label-field">
                Thời gian<span style={{ color: "red" }}> *</span>
              </div>

              <Space direction="vertical" size={12}>
                {" "}
                <Form.Item
                  name="datetime"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError
                          message={"Vui lòng nhập thời gian sự kiện!"}
                        />
                      ),
                    },
                  ]}
                >
                  <RangePicker showTime format={"DD/MM/YYYY HH:mm"} 
                    // disabledDate={(current) => {
                    //   let customDate = moment().format("DD/MM/YYYY HH:mm");
                    //   return (
                    //     current &&
                    //     current < moment(customDate, "DD/MM/YYYY HH:mm")
                    //   );
                    // }}
                  />
                </Form.Item>
              </Space>
            </div>
          </Col>
        </Row>
        {/* <Row justify="space-between">
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <div className="field-add">
              <div className="label-field">
                Thời gian<span style={{ color: "red" }}> *</span>
              </div>

              <Space direction="vertical" size={12}>
                {" "}
                <Form.Item
                  name="datetime"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError
                          message={"Vui lòng nhập thời gian sự kiện!"}
                        />
                      ),
                    },
                  ]}
                >
                  <RangePicker showTime format={"DD/MM/YYYY HH:mm"} />
                </Form.Item>
              </Space>
            </div>
          </Col>
        </Row> */}
        <Row justify="space-between">
          <div className="field-add">
            <div className="label-field"> </div>
            <Form.Item
              name="autoconfirm"
              className="text-field"
              valuePropName="checked"
            >
              <Checkbox>Tự động xác nhận tham dự</Checkbox>
            </Form.Item>
          </div>
        </Row>
        <div className="field-add">
          <div className="label-field">
            Mô tả sự kiện<span style={{ color: "red" }}> *</span>
          </div>
          <Form.Item
            name="description"
            className="text-field"
            rules={[
              {
                required: true,
                message: <MessError message={"Vui lòng nhập mô tả sự kiện!"} />,
              },
            ]}
          >
            <TextArea rows={8} size="large" placeholder="Chi tiết công việc" />
          </Form.Item>
        </div>
        <div className="field-add">
          <div className="label-field">Ghi chú</div>
          <Form.Item name="note" className="text-field">
            <TextArea rows={8} size="large" placeholder="Ghi chú" />
          </Form.Item>
        </div>
        <Row style={{ marginTop: "1rem" }} justify="end">
          <Col>
            <Button type="primary" htmlType="submit" className="btn-primary">
              {!idEvent ? "Thêm sự kiện" : "Cập nhật"}
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
}

export default AddEvent;
