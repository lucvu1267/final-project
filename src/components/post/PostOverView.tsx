import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "assets/images/logo.png";
import "./post-over-view.scss";
import { ClockCircleOutlined } from "@ant-design/icons";
import { Card, Row, Tooltip } from "antd";
import { MAX_LENGTH_DESCRIPTION, MAX_LENGTH_TITLE } from "constants/event";
import { FORMAT_DATE_FULL } from "constants/index";
import moment from "moment";
const { Meta } = Card;

function PostOverView(props: any) {
  const { postOverView } = props;
  const navigate = useNavigate();

  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  return (
    <Card
      className="container-post-item"
      hoverable
      cover={
        <img
          className="img-post-overview"
          width={100}
          alt="example"
          src={
            postOverView.overviewImagePath
              ? postOverView.overviewImagePath
              : logo
          }
        />
      }
    >
      <div className="container-post-overview">
        <Meta />
        <div className="wrapper-content">
          <div
            className="description"
            style={{ marginBottom: "12px", fontSize: "12px" }}
          >
            <span>
              <ClockCircleOutlined />{" "}
              {moment(postOverView.createDate).format(FORMAT_DATE_FULL)}
            </span>
          </div>
          <div
            className="title label-grayest"
            onClick={() => {
              navigate(`/post-detail/${postOverView.id}`);
            }}
          >
            {subTitle(postOverView.subject, MAX_LENGTH_TITLE)}
          </div>

          <Tooltip
            placement="top"
            title={postOverView.overviewDescription}
            arrowPointAtCenter
          >
            <div className="description" style={{ marginTop: "0.7rem" }}>
              {subTitle(
                postOverView.overviewDescription,
                MAX_LENGTH_DESCRIPTION
              )}
            </div>
          </Tooltip>
        </div>
      </div>
    </Card>
  );
}

export default PostOverView;
