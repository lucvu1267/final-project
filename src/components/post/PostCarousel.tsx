import React from "react";
import "./post-carousel.scss";
import { useNavigate } from "react-router-dom";
import logo from "assets/images/logo.png";
import { MAX_LENGTH_DESCRIPTION, MAX_LENGTH_TITLE } from "constants/event";

function PostCarousel(props: any) {
  const { postOverView } = props;
  const navigate = useNavigate();

  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  return (
    <div className="container-post-carousel">
      <img
        src={
          postOverView.overviewImagePath ? postOverView.overviewImagePath : logo
        }
        alt=""
        className="carousel-item"
        style={{ borderRadius: "10px" }}
      />
      <div
        className="wrapper-title"
        style={{
          width: "100%",
          borderBottomRightRadius: "10px",
          borderBottomLeftRadius: "10px",
        }}
      >
        <p
          className="title"
          style={{ color: "white" }}
          onClick={() => {
            navigate(`/post-detail/${postOverView.id}`);
          }}
        >
          {subTitle(postOverView.subject, MAX_LENGTH_TITLE)}
        </p>
        <span style={{ color: "white" }}>
          {subTitle(postOverView.overviewDescription, MAX_LENGTH_DESCRIPTION)}
        </span>
      </div>
    </div>
  );
}

export default PostCarousel;
