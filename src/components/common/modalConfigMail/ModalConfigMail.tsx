import { Button, message, Modal, Row, Tooltip } from "antd";
import React, { useRef, useState, useEffect } from "react";
import { MAIL_TICKET, TABLE, TYPE_CONFIG_MAIL } from "./configMailDefault";
import { CloseOutlined } from "@ant-design/icons";
import qrcode_default from "assets/images/qrcode-default.png";
import JoditEditor from "jodit-react";
import "./config-mail.scss";
import { useParams } from "react-router-dom";
import {
  configMailBuyTicket,
  configMailInviteParticipant,
  configMailTicket,
} from "features/eventManage/eventManageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "../loading/loadingSlice";
import { toastCustom } from "config/toast";
import { getMessStatus } from "mixin/generate";
const config: any = {
  readonly: false,
  allowResizeX: true,
  allowResizeY: true,
  height: 500,
  buttons: [
    "bold",
    "underline",
    "italic",
    "eraser",
    "|",
    "ul",
    "ol",
    "|",
    "outdent",
    "indent",
    "|",
    "font",
    "fontsize",
    "brush",
    "paragraph",
    "|",
    "image",
    "table",
    "link",
    "|",
    "align",
    "|",
    "hr",
    "|",
    "fullsize",
    "|",
  ],
  uploader: {
    url: "https://api.cloudinary.com/v1_1/dcgjui0yd/image/upload",
    imagesExtensions: ["jpg", "png", "jpeg", "gif"],
    //headers: {"token":`${db.token}`},
    filesVariableName: function (t: any) {
      return "file";
    }, //"files",
    // withCredentials: false,
    // pathVariableName: "path",
    format: "json",
    method: "POST",
    prepareData: function (formdata: any) {
      formdata.append("upload_preset", "ems-fpt");
      return formdata;
    },
    isSuccess: function (this: any, e: any) {
      console.log(e);
      const j = this;
      const tagName = "img";
      const elm = j.jodit.createInside.element(tagName);
      elm.setAttribute("src", e.url);

      j.jodit.s.insertImage(
        elm as HTMLImageElement,
        null,
        j.jodit.o.imageDefaultWidth
      );
      return e.success;
    },
    getMessage: function (e: any) {
      return void 0 !== e.data?.messages && Array.isArray(e.data?.messages)
        ? e.data?.messages.join("")
        : "";
    },
    process: function (resp: any) {
      //success callback transfrom data to defaultHandlerSuccess use.it's up to you.
      let files = [];
      files.unshift(resp.data);
      return {
        files: resp.data,
        error: resp.msg,
        msg: resp.msg,
      };
    },
    // error: function (this: any, e: Error) {
    //   this.j.e.fire("errorMessage", e?.message, "error", 4000);
    // },

    defaultHandlerError: function (this: any, e: any) {
      this.j.e.fire("errorMessage", e.message);
    },
  },
};

function ModalConfigMail(props: any) {
  const {
    typeModal,
    isOpenModal,
    setIsOpenModal,
    eventDetail,
    handleGetEventById,
  } = props;
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const editor = useRef(null);
  const [content, setContent] = useState("");
  const [isShowEdit, setIsShowEdit] = useState(false);

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setIsShowEdit(false);
  };

  const handleUpdateConfigMail = async () => {
    if (user.id !== eventDetail?.creator?.id) {
      return message.open({
        type: "error",
        content: "Bạn không có quyền!",
      });
    } else if (
      roleUser?.event?.status === "DONE" ||
      roleUser?.event?.status === "CANCEL"
    ) {
      return message.open({
        type: "error",
        content: getMessStatus(roleUser?.event?.status),
      });
    }
    dispatch(setLoading(true));
    const payload = {
      contentMail: content,
      eventId: idEvent,
    };
    if (typeModal === TYPE_CONFIG_MAIL.INVITED_PARTICIPAN) {
      await configMailInviteParticipant(payload);
    } else if (typeModal === TYPE_CONFIG_MAIL.PARTICIPAN) {
      await configMailTicket(payload);
    } else if (typeModal === TYPE_CONFIG_MAIL.TICKET_ORDER) {
      await configMailBuyTicket(payload);
    }
    await handleGetEventById();
    dispatch(setLoading(false));
    setIsShowEdit(false);
    toastCustom("success", "Cập nhật thành công");
  };

  const handleSetContent = () => {
    if (typeModal === TYPE_CONFIG_MAIL.INVITED_PARTICIPAN) {
      setContent(eventDetail.contentInviteMail);
    } else if (typeModal === TYPE_CONFIG_MAIL.PARTICIPAN) {
      setContent(eventDetail.contentTicketMail);
    } else if (typeModal === TYPE_CONFIG_MAIL.TICKET_ORDER) {
      setContent(eventDetail.contentBuyTicketMail);
    }
  };

  const getTitleModal = () => {
    if (typeModal === TYPE_CONFIG_MAIL.INVITED_PARTICIPAN) {
      return "Khách mời";
    } else if (typeModal === TYPE_CONFIG_MAIL.PARTICIPAN) {
      return "Người tham dự";
    } else if (typeModal === TYPE_CONFIG_MAIL.TICKET_ORDER) {
      return "Người mua vé";
    }
  };

  useEffect(() => {
    handleSetContent();
  }, [typeModal]);

  return (
    <Modal
      width={1000}
      open={isOpenModal}
      destroyOnClose={true}
      centered={true}
      title={getTitleModal()}
      onCancel={handleCloseModal}
      footer={null}
      // closeIcon={
      //   <CloseOutlined className="icon" style={{ fontSize: "18px" }} />
      // }
    >
      <div style={{ padding: "2rem" }}>
        {!isShowEdit && (
          <Tooltip
            placement="top"
            title="Nhấn để chỉnh sửa"
            arrowPointAtCenter
            open={true}
          >
            <div
              style={{ marginTop: "1rem", cursor: "pointer" }}
              onClick={() => setIsShowEdit(true)}
              dangerouslySetInnerHTML={{
                __html: content,
              }}
            />
          </Tooltip>
        )}
        {isShowEdit && (
          <div>
            <div style={{ marginTop: "0.5rem" }}>
              <JoditEditor
                ref={editor}
                value={content}
                config={config}
                // tabIndex={1} // tabIndex of textarea
                onBlur={(newContent: any) => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={(newContent: any) => {}}
              />
            </div>
            <div
              style={{
                marginTop: "0.5rem",
                display: "flex",
                justifyContent: "end",
              }}
            >
              <Button
                onClick={() => setIsShowEdit(false)}
                type="primary"
                ghost
                style={{
                  fontWeight: "600",
                }}
                className="feild-link"
              >
                Quay lại
              </Button>
              <Button
                onClick={() => {
                  handleUpdateConfigMail();
                }}
                type="primary"
                ghost
                style={{
                  marginLeft: "1rem",
                  fontWeight: "600",
                }}
                className="feild-link"
              >
                Cập nhật
              </Button>
            </div>
          </div>
        )}
        {typeModal === TYPE_CONFIG_MAIL.TICKET_ORDER && (
          <div className="table-custom" style={{ marginTop: "2rem" }}>
            <div className="label-grayest">Thông tin chi tiết đơn hàng:</div>

            <div
              style={{ marginTop: "1rem" }}
              //   onClick={() => setIsShowEdit(true)}
              dangerouslySetInnerHTML={{
                __html: TABLE,
              }}
            />
          </div>
        )}
        {typeModal !== TYPE_CONFIG_MAIL.TICKET_ORDER && (
          <div>
            <div>
              Loại vé: <span className="label-grayest">Vé Vip</span>
            </div>
            <div>
              Sự kiện:{" "}
              <span className="label-grayest">Event Manage System</span>
            </div>
          </div>
        )}
      </div>
      {typeModal !== TYPE_CONFIG_MAIL.TICKET_ORDER && (
        <div>
          <img
            src={qrcode_default}
            alt=""
            style={{ width: "300px", height: "300px" }}
          />
        </div>
      )}
    </Modal>
  );
}

export default ModalConfigMail;
