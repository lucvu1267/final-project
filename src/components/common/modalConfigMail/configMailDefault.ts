export const MAIL_TICKET = `<p><span><img src="https://res.cloudinary.com/dcgjui0yd/image/upload/v1671031958/logo_ixfins.png" alt="" width="120" style="display: block; margin-left: auto; margin-right: auto;" height="98"></span></p><p><span >Chúc mừng bạn đã đăng ký vé thành công! Sự kiện được diễn ra vào 19 giờ 30 phút tại trường Đại Học FPT. Với sự góp mặt của ca sĩ Sơn Tùng MTP hứa hẹn sẽ mang đến sự bùng nổ cho những người tham gia sự. Bạn vui lòng đến sớm trước 30 phút và checkin bằng mã QR dưới đây. Xin chân thành cảm ơn!</span></p>
<p><span >Dress code: Black and White</span></p>`;

export const MAIL_TICKET_ORDER = `<p><span><img src="https://res.cloudinary.com/dcgjui0yd/image/upload/v1671031958/logo_ixfins.png" alt="" width="120" style="display: block; margin-left: auto; margin-right: auto;" height="98"></span></p><p><span >Mua vé thành công!  Sự kiện được diễn ra vào 19 giờ 30 phút tại trường Đại Học FPT. Với sự góp mặt của ca sĩ Sơn Tùng MTP hứa hẹn sẽ mang đến sự bùng nổ cho những người tham gia sự. Bạn vui lòng đến sớm trước 30 phút và checkin bằng mã QR dưới đây. Xin chân thành cảm ơn!</span></p>
<p><span >Dress code: Black and White</span></p>`;

export const TABLE = `<table style="width: 60%;">
<tbody>
    <tr>
        <td style="width: 33.3333%; background-color: rgb(229, 197, 179); text-align: center;"><strong>Loại vé</strong></td>
        <td style="width: 33.3333%; background-color: rgb(229, 197, 179); text-align: center;"><strong>Giá vé</strong></td>
        <td style="width: 33.3333%; background-color: rgb(229, 197, 179); text-align: center;"><strong>Số lượng</strong></td>
    </tr>
    <tr>
        <td style="width: 33.3333%; text-align: center;"><strong><br></strong></td>
        <td style="width: 33.3333%; text-align: center;"><strong><br></strong></td>
        <td style="width: 33.3333%; text-align: center;"><strong><br></strong></td>
    </tr>
</tbody>
</table>
<table style="border-collapse:collapse;width: 60%; margin-top: 1rem">
<tbody>
    <tr>
        <td style="width: 50%; background-color: rgb(229, 197, 179); text-align: center;"><strong>Tổng số vé</strong></td>
        <td style="width: 50%; background-color: rgb(229, 197, 179); text-align: center;"><strong>Tổng tiền</strong></td>
    </tr>
    <tr>
        <td style="width: 50%;"><strong><br></strong></td>
        <td style="width: 50%;"><strong><br></strong></td>
    </tr>
</tbody>
</table>`;

export const TYPE_CONFIG_MAIL = {
  TICKET_ORDER: 1,
  INVITED_PARTICIPAN: 2,
  PARTICIPAN: 3,
};
