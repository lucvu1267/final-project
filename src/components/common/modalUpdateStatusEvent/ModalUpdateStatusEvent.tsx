import { Button, message, Modal, Row, Tooltip } from "antd";
import React, { useRef, useState, useEffect } from "react";
import {
  CloseOutlined,
  QuestionCircleOutlined,
  ArrowRightOutlined,
  WarningOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import qrcode_default from "assets/images/qrcode-default.png";
import JoditEditor from "jodit-react";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setLoading } from "../loading/loadingSlice";
import { toastCustom } from "config/toast";
import TextArea from "antd/lib/input/TextArea";
import { isEmpty } from "mixin/generate";
import { changeStatusEvent, propsEvent, setUserEventPops } from "features/eventManage/eventManageSlice";
const config: any = {
  readonly: false,
  allowResizeX: true,
  allowResizeY: true,
  height: 300,
  buttons: [
    "bold",
    "underline",
    "italic",
    "eraser",
    "|",
    "ul",
    "ol",
    "|",
    "outdent",
    "indent",
    "|",
    "font",
    "fontsize",
    "brush",
    "paragraph",
    "|",
    "image",
    "table",
    "link",
    "|",
    "align",
    "|",
    "hr",
    "|",
    "fullsize",
    "|",
  ],
  uploader: {
    url: "https://api.cloudinary.com/v1_1/dcgjui0yd/image/upload",
    imagesExtensions: ["jpg", "png", "jpeg", "gif"],
    //headers: {"token":`${db.token}`},
    filesVariableName: function (t: any) {
      return "file";
    }, //"files",
    // withCredentials: false,
    // pathVariableName: "path",
    format: "json",
    method: "POST",
    prepareData: function (formdata: any) {
      formdata.append("upload_preset", "ems-fpt");
      return formdata;
    },
    isSuccess: function (this: any, e: any) {
      console.log(e);
      const j = this;
      const tagName = "img";
      const elm = j.jodit.createInside.element(tagName);
      elm.setAttribute("src", e.url);

      j.jodit.s.insertImage(
        elm as HTMLImageElement,
        null,
        j.jodit.o.imageDefaultWidth
      );
      return e.success;
    },
    getMessage: function (e: any) {
      return void 0 !== e.data?.messages && Array.isArray(e.data?.messages)
        ? e.data?.messages.join("")
        : "";
    },
    process: function (resp: any) {
      //success callback transfrom data to defaultHandlerSuccess use.it's up to you.
      let files = [];
      files.unshift(resp.data);
      return {
        files: resp.data,
        error: resp.msg,
        msg: resp.msg,
      };
    },
    // error: function (this: any, e: Error) {
    //   this.j.e.fire("errorMessage", e?.message, "error", 4000);
    // },

    defaultHandlerError: function (this: any, e: any) {
      this.j.e.fire("errorMessage", e.message);
    },
  },
};

const TYPE_MODAL_STATUS = {
  DONE: 1,
  CANCEL: 2,
};

function ModalUpdateStatusEvent(props: any) {
  const {
    typeModal,
    isOpenModal,
    setIsOpenModal,
    eventDetail,
    handleGetEventById,
  } = props;
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const editor = useRef(null);
  const [content, setContent] = useState("");
  const [note, setNote] = useState("");
  const [isShowEdit, setIsShowEdit] = useState(false);
  const [step, setStep] = useState(1);

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setIsShowEdit(false);
    setContent("")
    setNote("")
    setTimeout(() => {
      setStep(1);
    }, 300);
  };

  const handleNextStep = () => {
    if (isEmpty(content) || content === "<p><br></p>") {
      message.error({
        type: "error",
        content: "Vui lòng nhập nội dung email!",
      });
      return;
    } else if (content.length < 50) {
      message.error({
        type: "error",
        content: "Nội dung email quá ngắn!",
      });
      return;
    } else if (isEmpty(note)) {
      message.error({
        type: "error",
        content: "Vui lòng nhập ghi chú!",
      });
      return;
    }
    setStep(2);
  };

  const getPropsEvent = async () => {
    try {
      const data = await propsEvent(idEvent);
      dispatch(setUserEventPops(data.data));
    } catch (error) {
    } 
  };

  const handleChangeStatusEvent = async () => {
    dispatch(setLoading(true));
    const payload = {
      checkTaskDone: false,
      contentMail: content,
      eventId: idEvent,
      note: note,
      status: typeModal === TYPE_MODAL_STATUS.DONE ? "DONE" : "CANCEL",
    };
    await changeStatusEvent(payload);
    await handleGetEventById(idEvent);
    await getPropsEvent();
    handleCloseModal();
    dispatch(setLoading(false));
    toastCustom("success", "Cập nhật trạng thái thành công");
  };

  useEffect(() => {}, [typeModal]);

  return (
    <Modal
      width={step == 1 ? 1000 : 500}
      open={isOpenModal}
      destroyOnClose={true}
      centered={true}
      title={
        typeModal === TYPE_MODAL_STATUS.DONE
          ? "Hoàn thành sự kiện"
          : "Hủy sự kiện"
      }
      onCancel={handleCloseModal}
      footer={[
        <Tooltip
          placement="top"
          title={step === 1 ? "Tiếp tục" : "Đồng ý"}
          arrowPointAtCenter
        >
          {/* <ArrowRightOutlined /> */}
          {step === 1 ? (
            <ArrowRightOutlined
              style={{ fontSize: "22px" }}
              onClick={() => handleNextStep()}
              className="icon"
            />
          ) : (
            <CheckCircleOutlined
              style={{ fontSize: "30px" }}
              onClick={() => handleChangeStatusEvent()}
              className="icon"
            />
          )}
        </Tooltip>,
      ]}
    >
      {step === 1 ? (
        <div style={{ padding: "2rem" }}>
          <div style={{ marginBottom: "1rem" }}>
            <div
              className="label-field "
              style={{
                fontWeight: "bold",
                color: "#707373",
                marginBottom: "0.5rem",
              }}
            >
              Nội dung email<span style={{ color: "red" }}> *</span>{" "}
              <Tooltip
                placement="top"
                title="Email này sẽ được gửi đến người tham dự, người thuê gian hàng, các thành viên trong nhóm."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "15px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
            <JoditEditor
              ref={editor}
              value={content}
              config={config}
              // tabIndex={1} // tabIndex of textarea
              onBlur={(newContent: any) => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
              onChange={(newContent: any) => {}}
            />
          </div>
          <div>
            <div
              className="label-field "
              style={{
                fontWeight: "bold",
                color: "#707373",
                marginBottom: "0.5rem",
              }}
            >
              Ghi chú<span style={{ color: "red" }}> *</span>{" "}
            </div>
            <TextArea
              placeholder="Ghi chú"
              className="note"
              onChange={(e) => setNote(e.target.value)}
            ></TextArea>
          </div>
        </div>
      ) : (
        <div
          className="container-buy-ticket-success "
          style={{ padding: "2rem" }}
        >
          <WarningOutlined
            className="icon-success"
            size={200}
            style={{ marginBottom: "1rem" }}
          />
          <p>
            Sau khi chuyển trạng thái sự kiện bạn chỉ được phép xem những dữ
            liệu đã có trong sự kiện và sẽ không thể thực hiện tính năng thêm,
            sửa, xóa.
          </p>
        </div>
      )}
    </Modal>
  );
}

export default ModalUpdateStatusEvent;
