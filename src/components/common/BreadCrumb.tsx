import { Breadcrumb, Row } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { JWT_AUTH, routes } from "constants/index";
function BreadCrumb(props: any) {
  const location = useLocation();

  const getBreadcrumb = () => {
    return (
      <Breadcrumb>
        {location.pathname !== "/" &&
          location.pathname.split("/")?.map(
            (value: string, idx: number) =>
            idx===2&&/^-?\d+$/.test(value)&&location.pathname.includes("event-manage")?
            <Breadcrumb.Item>
            <Link to={`/event-manage/detail/${value}`}>Thông tin sự kiện</Link>
          </Breadcrumb.Item>:
              !/^-?\d+$/.test(value) && (
                <Breadcrumb.Item key={idx}>
                  <Link
                    to={`${
                      idx === 0
                        ? "/"
                        : location.pathname
                            .split("/")
                            .splice(0, idx + 1)
                            .join("/")
                    }`}
                  >
                    {" "}
                    {routes.find((item) => item.path === value)?.breadcrumbName}
                  </Link>
                </Breadcrumb.Item>
              )
          )}
        {location.pathname === "/" && (
          <Breadcrumb.Item>
            <Link to={`/`}> Trang chủ /</Link>
          </Breadcrumb.Item>
        )}
      </Breadcrumb>
    );
  };

  return <Row style={{ marginBottom: "2rem" }}>{getBreadcrumb()}</Row>;
}

export default BreadCrumb;
