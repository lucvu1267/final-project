import React from "react";
import "./loading.scss";
import logo from "assets/images/logo.png";
import { useSelector } from "react-redux";

function Loading(props: any) {
  const isLoading = useSelector((state: any) => state.loadingSlice.isLoading);

  return (
    <>
      <div
        className="container-loading"
        style={{ display: isLoading ? "" : "none" }}
      >
        <div className="wrapper-loading"></div>
        <div className="wrapper-load-animation">
          <div className="load-animation"></div>
        </div>{" "}
        <img
          // onClick={() => navigate("/")}
          className="logo-loading"
          src={logo}
          alt=""
        />
      </div>
    </>
  );
}

export default Loading;
