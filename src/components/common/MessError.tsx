import React from "react";

function MessError(props: any) {
  const { message } = props;
  return (
    <div>
      <i
        className="fas fa-exclamation-circle"
        style={{ marginRight: "5px" }}
      ></i>
      {message}
    </div>
  );
}

export default MessError;
