import React from "react";
import { FrownOutlined } from "@ant-design/icons";
function RejectJoin(props: any) {
  return (
    <div className="container-buy-ticket-success">
      <FrownOutlined className="icon-success" size={200} />
      <div className="mess-success">
        <p
          style={{
            fontSize: "17px",
            fontWeight: "bold",
            color: "#285158",
          }}
        >
          Bạn đã từ chối vào sự kiện!
        </p>
        {/* <p className="description-gray"> Số lượng vé hiện tại không đủ với.</p> */}
      </div>
    </div>
  );
}

export default RejectJoin;
