import React from "react";
import { CheckCircleOutlined } from "@ant-design/icons";
function AcceptJoin(props: any) {
  return (
    <div>
      <div className="container-buy-ticket-success">
        <CheckCircleOutlined className="icon-success" size={200} />
        <div className="mess-success">
          <p
            style={{
              fontSize: "17px",
              fontWeight: "bold",
              color: "#285158",
            }}
          >
            Bạn đã xác nhận tham gia sự kiện
          </p>
          {/* <p className="description-gray">
            {" "}
            Chúng tôi sẽ gửi thông tin vé về email bạn đã đăng ký.
          </p> */}
        </div>
      </div>
    </div>
  );
}

export default AcceptJoin;
