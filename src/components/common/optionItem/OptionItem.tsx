import { Avatar, Col, Tooltip } from "antd";
import "./option-item.scss";
import profileDefault from "assets/images/profileDefault.png";
import { isEmpty } from "mixin/generate";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";

function OptionItem(props: any) {
  const { mail, name, image, status } = props;

  return (
    <>
      <div className="aaaaa">
        <Col>
          <div className="wrapper-option-item">
            <Avatar src={isEmpty(image) ? profileDefault : image} />
            <div className="info">
              <div> {mail}</div>
              <div className="description-gray"> {name}</div>
            </div>
          </div>
        </Col>
        <Col>
          {/* {status && ( */}
          {/* <span
            className={`status--accept`}
            // style={{ marginRight: "0px" }}
          > */}

          {status && (
            <Tooltip
              placement="top"
              title={
                status === 1
                  ? "Đã gửi"
                  : "Đã bị từ chối, bạn có muốn gửi lại không?"
              }
              arrowPointAtCenter
            >
              {status === 1 && (
                <CheckCircleOutlined
                  style={{ fontSize: "18px", color: "rgb(6, 173, 34)" }}
                />
              )}
              {status === 2 && (
                <CloseCircleOutlined
                  style={{ fontSize: "18px", color: "rgb(173, 6, 6)" }}
                />
              )}
            </Tooltip>
          )}
          {/* <CloseCircleOutlined />*/}
          {/* </span> */}
          {/* )} */}
        </Col>
      </div>
    </>
  );
}

export default OptionItem;
