import { Avatar, Tooltip } from "antd";
import React from "react";
import logo from "assets/images/logo.png";
import { isEmpty } from "mixin/generate";
function OptionEventItem(props: any) {
  const { code, image, name, isShowAvt = true } = props;
  return (
    <Tooltip
      placement="top"
      title={name}
      arrowPointAtCenter
      popupVisible={!!name}
    >
      <div className="wrapper-option-item">
        {isShowAvt && (
          <Avatar
            shape="square"
            src={isEmpty(image) ? logo : image}
            size={20}
          />
        )}
        <div className="info" style={isShowAvt ? { marginLeft: "15px" } : {}}>
          <span className="label-grayest"> {code}</span>
        </div>
      </div>
    </Tooltip>
  );
}

export default OptionEventItem;
