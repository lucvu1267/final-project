import { Divider } from "antd";
import React from "react";
import "./header-feature.scss";

function HeaderFeature(props: any) {
  const { content } = props;
  return (
    <div className="wrapper-header-feature">
      <div className="content">{content}</div>
    </div>
    // <Divider className="divider" plain>
    //   {content}
    // </Divider>
  );
}

export default HeaderFeature;
