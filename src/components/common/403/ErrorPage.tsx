import { Button, Result } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";

function ErrorPage(props: any) {
  const navigate = useNavigate();
  return (
    <Result
      status="404"
      title="404"
      subTitle="Không tìm thấy trang truy cập!."
      extra={
        <Button onClick={() => navigate("/")} type="primary">
          Trang chủ
        </Button>
      }
    />
  );
}

export default ErrorPage;
