import { Button, Result } from 'antd';
import React from 'react';
import { useNavigate } from 'react-router-dom';

function ErrorPermission(props:any) {
    const navigate = useNavigate();

    return (
        <Result
        status="403"
        title="403"
        subTitle="Xin lỗi, bạn không  truy cập!."
        extra={<Button onClick={()=>navigate("/")} type="primary">Trang chủ</Button>}
      />
    );
}

export default ErrorPermission;