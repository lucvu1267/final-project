import { Avatar, Card, Tooltip, Popover, Modal, Button, message } from "antd";
import profileDefault from "assets/images/profileDefault.png";
import "./list-member.scss";
import {
  CloseCircleOutlined,
  CheckCircleOutlined,
  UserAddOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { addContact } from "features/contact/contactSlice";
import { toastCustom } from "config/toast";
import {
  grantRoleMember,
  removeMemberFromTeam,
} from "features/eventManage/eventManageSlice";
import { getMessStatus, isEmpty } from "mixin/generate";

const { Meta } = Card;

function ListMember(props: any) {
  const { teamDetail, handleGetTeam } = props;
  const { idTeam } = useParams();
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const [isOpenModal, setIsOpenModal] = useState(false);
  const navigate = useNavigate();
  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const handleAddContact = async (idUser: any) => {
    await addContact(idUser);
    handleGetTeam();
    toastCustom("success", "Đã thêm liên hệ");
  };

  const handleGrantRole = async (idUser: any, nameUser: any) => {
    await grantRoleMember(idTeam, idUser);
    handleGetTeam();
    toastCustom("success", `${nameUser} đã trở thành quản trị viên`);
  };

  const handleRemoveMember = async (idUser: any, nameUser: any) => {
    await removeMemberFromTeam(idTeam, idUser);
    handleGetTeam();
    toastCustom("success", `${nameUser} đã bị xóa khỏi nhóm`);
  };

  const checkRoleUserAuthen = () => {
    return teamDetail?.members?.find((item: any) => item.member.id === user.id)
      ?.teamRole;
  };

  return (
    <>
      <div className="wrapper-member">
        {teamDetail?.members?.map((item: any, idx: any) => (
          <Popover
            key={idx}
            className="popover-teams"
            placement="topRight"
            content={
              user.id === item?.member?.id ? (
                <span className="description-gray">Bạn</span>
              ) : (
                <div>
                  <span className="description-gray">
                    <span
                      className="label-grayest"
                      style={{ color: "#285158" }}
                    >
                      Email:{" "}
                    </span>
                    {item.member.emailAccount}
                  </span>
                  {item.member.isAdded === false &&
                    user.id !== item.member.id && (
                      <div
                        className="action-member"
                        style={{ marginTop: "0.7rem" }}
                        onClick={() => handleAddContact(item.member.id)}
                      >
                        <UserAddOutlined
                          className="icon"
                          style={{ fontSize: "16px", marginRight: "6px" }}
                        />{" "}
                        <span
                          className="description-gray"
                          style={{ color: "#285158", fontWeight: "600" }}
                        >
                          Thêm liên hệ
                        </span>
                      </div>
                    )}
                  {item.teamRole !== "LEADER" &&
                    checkRoleUserAuthen() === "LEADER" && (
                      <div
                        className="action-member"
                        style={{ marginTop: "0.3rem" }}
                        onClick={() => {
                          if (
                            roleUser?.event?.status === "DONE" ||
                            roleUser?.event?.status === "CANCEL"
                          ) {
                            return message.open({
                              type: "error",
                              content: getMessStatus(roleUser?.event?.status),
                            });
                          }
                          handleGrantRole(
                            item.member.id,
                            item.member.firstName
                          );
                        }}
                      >
                        <UserOutlined
                          className="icon"
                          style={{ fontSize: "16px", marginRight: "10px" }}
                        />
                        <span
                          className="description-gray"
                          style={{ color: "#285158", fontWeight: "600" }}
                        >
                          Cho quyền quản trị viên
                        </span>
                      </div>
                    )}
                  {teamDetail?.creator?.id !== item.member.id &&
                    checkRoleUserAuthen() === "LEADER" && (
                      <div
                        className="action-member"
                        style={{ marginTop: "0.3rem" }}
                        onClick={() => {
                          if (
                            roleUser?.event?.status === "DONE" ||
                            roleUser?.event?.status === "CANCEL"
                          ) {
                            return message.open({
                              type: "error",
                              content: getMessStatus(roleUser?.event?.status),
                            });
                          }
                          handleRemoveMember(
                            item.member.id,
                            item.member.firstName
                          );
                        }}
                      >
                        <CloseCircleOutlined
                          className="icon"
                          style={{ fontSize: "16px", marginRight: "10px" }}
                        />
                        <span
                          className="description-gray"
                          style={{ color: "#285158", fontWeight: "600" }}
                        >
                          Kích khỏi nhóm
                        </span>
                      </div>
                    )}

                  <div></div>
                </div>
              )
            }
          >
            <div className="wrapper-animation">
              <Meta
                className="member-item"
                avatar={
                  <Avatar
                    src={
                      isEmpty(item.member.imagePath)
                        ? profileDefault
                        : item.member.imagePath
                    }
                  />
                }
                title={<div> {item.member.firstName}</div>}
                description={
                  teamDetail?.creator?.id === item.member.id
                    ? "Trưởng nhóm"
                    : item.teamRole === "LEADER"
                    ? "Quản trị viên"
                    : "Thành viên"
                }
              />
              {/* <Tooltip
                placement="top"
                title={"Xóa khỏi nhóm"}
                arrowPointAtCenter
              >
                <CloseCircleOutlined
                  onClick={() => {
                    setIsOpenModal(true);
                  }}
                  className="block-icon icon"
                />
              </Tooltip> */}
            </div>
          </Popover>
        ))}
      </div>
      <Modal
        width={500}
        title="Xóa khỏi nhóm"
        open={isOpenModal}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Chắc chắn" arrowPointAtCenter>
            <CheckCircleOutlined
              // onClick={handleCreateContact}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <div className="mess-confirn">
          Bạn có chắc muốn xóa <strong>Vũ Xuân Lực</strong> khỏi nhóm?
        </div>
      </Modal>
    </>
  );
}

export default ListMember;
