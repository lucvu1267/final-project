import React, { useState, useRef, useEffect } from "react";
import JoditEditor from "jodit-react";
import "./add-post-cmt.scss";
import {
  createComment,
  createPostTeam,
  updateCmtTeam,
  updatePostTeam,
} from "features/eventManage/eventManageSlice";
import { useParams } from "react-router-dom";
import { toastCustom } from "config/toast";
import { isEmpty } from "mixin/generate";

const TYPE_FORM_POST = {
  POST_UPDATE: "POST_UPDATE",
  POST_CREATE: "POST_CREATE",
  CMT_UPDATE: "CMT_UPDATE",
  CMT_CREATE: "CMT_CREATE",
};

const config: any = {
  readonly: false,
  allowResizeX: true,
  allowResizeY: true,
  height: 500,
  buttons: [
    "bold",
    "underline",
    "italic",
    "eraser",
    "|",
    "ul",
    "ol",
    "|",
    "outdent",
    "indent",
    "|",
    "font",
    "fontsize",
    "brush",
    "paragraph",
    "|",
    "image",
    "table",
    "link",
    "|",
    "align",
    "|",
    "hr",
    "|",
    "fullsize",
    "|",
  ],
  uploader: {
    url: "https://api.cloudinary.com/v1_1/dcgjui0yd/image/upload",
    imagesExtensions: ["jpg", "png", "jpeg", "gif"],
    //headers: {"token":`${db.token}`},
    filesVariableName: function (t: any) {
      return "file";
    }, //"files",
    // withCredentials: false,
    // pathVariableName: "path",
    format: "json",
    method: "POST",
    prepareData: function (formdata: any) {
      formdata.append("upload_preset", "ems-fpt");
      return formdata;
    },
    isSuccess: function (this: any, e: any) {
      console.log(e);
      const j = this;
      const tagName = "img";
      const elm = j.jodit.createInside.element(tagName);
      elm.setAttribute("src", e.url);

      j.jodit.s.insertImage(
        elm as HTMLImageElement,
        null,
        j.jodit.o.imageDefaultWidth
      );
      return e.success;
    },
    getMessage: function (e: any) {
      return void 0 !== e.data?.messages && Array.isArray(e.data?.messages)
        ? e.data?.messages.join("")
        : "";
    },
    process: function (resp: any) {
      //success callback transfrom data to defaultHandlerSuccess use.it's up to you.
      let files = [];
      files.unshift(resp.data);
      return {
        files: resp.data,
        error: resp.msg,
        msg: resp.msg,
      };
    },
    // error: function (this: any, e: Error) {
    //   this.j.e.fire("errorMessage", e?.message, "error", 4000);
    // },

    defaultHandlerError: function (this: any, e: any) {
      this.j.e.fire("errorMessage", e.message);
    },
  },
};

function AddPostAndCmt(props: any) {
  const {
    isCreatePost,
    setIsLoadingPost,
    setIsCreatePost,
    setIsOpenModal,
    setIsCallBackApi,
    typeModal,
    postIdCurrent,
    idxCmt,
    handleShowComment,
    commentDetail,
    postDetail,
  } = props;
  const { idEvent, idTeam } = useParams();
  const editor = useRef(null);
  const [content, setContent] = useState("");

  const handleCreatePost = async () => {
    try {
      if (isEmpty(content) || content === "<p><br></p>") {
        toastCustom("error", "Vui lòng nhập nội dung bài viết!");
        setIsCreatePost(false);
        return;
      }
      setIsLoadingPost(true);
      const payload = {
        content: content,
        overviewDescription: null,
        overviewImagePath: null,
        subject: null,
        tags: null,
        teamId: idTeam,
      };
      const payloadUpdate = {
        content: content,
        overviewDescription: null,
        overviewImagePath: null,
        subject: null,
        status: "ACTIVE",
        id: postDetail.id && parseInt(postDetail.id),
        eventId: idEvent && parseInt(idEvent),
      };
      const payloadCreateCmt = {
        content: content,
        postId: postIdCurrent,
      };

      const payloadUpdateCmt = {
        content: content,
        postId: postIdCurrent,
        id: commentDetail.id && parseInt(commentDetail.id + ""),
      };
      if (typeModal === TYPE_FORM_POST.POST_CREATE) {
        await createPostTeam(payload);
        setIsCallBackApi();
      } else if (typeModal === TYPE_FORM_POST.CMT_CREATE) {
        await createComment(payloadCreateCmt);
        handleShowComment(idxCmt, postIdCurrent, true);
      } else if (typeModal === TYPE_FORM_POST.POST_UPDATE) {
        await updatePostTeam(payloadUpdate);
        setIsCallBackApi();
      } else if (typeModal === TYPE_FORM_POST.CMT_UPDATE) {
        await updateCmtTeam(payloadUpdateCmt);
        handleShowComment(idxCmt, postIdCurrent, true);
      }
      setIsOpenModal(false);
      setContent("");
    } catch (error) {
    } finally {
      setIsLoadingPost(false);
    }
  };

  useEffect(() => {
    if (isCreatePost) {
      handleCreatePost();
    }
    setIsCreatePost(false);
  }, [isCreatePost]);

  useEffect(() => {
    if (Object.keys(commentDetail).length > 0) {
      setContent(commentDetail.content);
    }
    if (Object.keys(postDetail).length > 0) {
      setContent(postDetail.content);
    }
  }, [commentDetail, postDetail]);

  return (
    <div className="wrapper-add-post">
      <JoditEditor
        ref={editor}
        value={content}
        config={config}
        // tabIndex={1} // tabIndex of textarea
        onBlur={(newContent: any) => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
        onChange={(newContent: any) => {}}
      />
    </div>
  );
}

export default AddPostAndCmt;
