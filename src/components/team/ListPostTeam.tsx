import {
  Avatar,
  Card,
  Col,
  Divider,
  Input,
  Row,
  Skeleton,
  Modal,
  Tooltip,
  Dropdown,
  message,
} from "antd";
import React from "react";
import { useState, useEffect } from "react";
import "./list-post-team.scss";
import {
  CheckCircleOutlined,
  MessageOutlined,
  LikeOutlined,
  EditOutlined,
  SettingOutlined,
  DeleteOutlined,
  LikeFilled,
} from "@ant-design/icons";
import profileDefault from "assets/images/profileDefault.png";
import AddPostAndCmt from "./addPostAndCmt/AddPostAndCmt";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { MAX_LENGTH_TITLE, TYPE_POST } from "constants/event";
import { setLoading } from "components/common/loading/loadingSlice";
import {
  deleteComment,
  deletePostTeam,
  getListComment,
  getListPostAuthen,
  likePost,
} from "features/eventManage/eventManageSlice";
import moment from "moment";
import { getMessStatus } from "mixin/generate";

const { Meta } = Card;

const TYPE_FORM_POST = {
  POST_UPDATE: "POST_UPDATE",
  POST_CREATE: "POST_CREATE",
  CMT_UPDATE: "CMT_UPDATE",
  CMT_CREATE: "CMT_CREATE",
};
function ListPostTeam(props: any) {
  const { idEvent, idTeam } = useParams();
  const { teamDetail } = props;
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isCreatePost, setIsCreatePost] = useState(false);
  const [isLoadingPost, setIsLoadingPost] = useState(false);
  const [isCallBackApi, setIsCallBackApi] = useState(false);
  const [typeModal, setTypeModal] = useState(TYPE_FORM_POST.POST_UPDATE);
  const [listShowComment, setListShowComment] = useState<any>([]);
  const [listPost, setListPost] = useState<any>([]);
  const [postIdCurrent, setPostIdCurrent] = useState<any>(null);
  const [idxCmt, setIdxCmt] = useState<any>(null);
  const [commentDetail, setCommentDetail] = useState({});
  const [postDetail, setPostDetail] = useState({});
  const [dataLikeCmt, setDataLikeCmt] = useState<any>([]);

  const handleCloseModal = () => {
    setCommentDetail({});
    setPostDetail({});
    setIsOpenModal(false);
  };

  const handleShowComment = async (idx: any, idPost: any, isSaveOpen: any) => {
    setPostIdCurrent(idPost);
    const payload = {
      orders: [
        {
          direction: "DESC",
          ignoreCase: true,
          nullHandling: "NATIVE",
          property: "createDate",
        },
      ],
      page: 0,
      size: 100,
    };
    const data = await getListComment(idPost, payload);
    const dataListPost = await handleCallBackListPost();
    const listPostTemp = dataListPost.data.postPage.content;
    setListShowComment((current: any) =>
      current.map((obj: any, elm: any) => {
        if (elm === idx) {
          return {
            ...obj,
            noLike: listPostTemp[elm].noLike,
            noComment: listPostTemp[elm].noComment,
            isHide: isSaveOpen ? obj.isHide : !obj.isHide,
            listComment: data.data.postPage.content,
          };
        } else {
          return obj;
        }
      })
    );
  };

  const getTitleModal = () => {
    switch (typeModal) {
      case TYPE_FORM_POST.CMT_CREATE:
        return "Thêm bình luận";
      case TYPE_FORM_POST.CMT_UPDATE:
        return "Chỉnh sửa bình luận";
      case TYPE_FORM_POST.POST_CREATE:
        return "Thêm bài đăng";
      case TYPE_FORM_POST.POST_UPDATE:
        return "Chỉnh sửa bài đăng";
    }
  };

  const handleGetListPost = async () => {
    try {
      dispatch(setLoading(true));
      const payload = {
        // eventId: parseInt(idEvent + ""),
        orders: [
          {
            direction: "DESC",
            ignoreCase: true,
            nullHandling: "NATIVE",
            property: "createDate",
          },
        ],
        page: 0,
        searchKey: null,
        size: 100,
        postType: TYPE_POST.TEAM,
        teamId: parseInt(idTeam + ""),
      };
      const data = await getListPostAuthen(payload);
      const listPostTemp = data.data.postPage.content;
      setListPost(listPostTemp);
      const listShowCommentTemp: any = [];
      listPostTemp?.map((value: any, idx: any) => {
        listShowCommentTemp.push({
          isHide: true,
          listComment: [],
          noLike: value.noLike,
          noComment: value.noComment,
          isLiked: value.isLiked,
        });
      });
      setListShowComment(listShowCommentTemp);
      // new Array(listPostTemp.length).fill({
      //   isHide: true,
      //   listComment: [],
      // }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleCallBackListPost = async () => {
    const payload = {
      // eventId: parseInt(idEvent + ""),
      orders: [
        {
          direction: "DESC",
          ignoreCase: true,
          nullHandling: "NATIVE",
          property: "createDate",
        },
      ],
      page: 0,
      searchKey: null,
      size: 100,
      postType: TYPE_POST.TEAM,
      teamId: parseInt(idTeam + ""),
    };
    return await getListPostAuthen(payload);
    // const listPostTemp = data.data.postPage.content;
  };

  const handleLike = async (idPost: any) => {
    await likePost(idPost);
    const data = await handleCallBackListPost();
    const listPostTemp = data.data.postPage.content;
    const listShowCommentTemp: any = [];
    listPostTemp?.map((value: any, idx: any) => {
      listShowCommentTemp.push({
        ...listShowComment[idx],
        noLike: value.noLike,
        isLiked: value.isLiked,
      });
    });
    setListShowComment(listShowCommentTemp);
  };

  const handleDeletePost = async (postId: any) => {
    await deletePostTeam(postId);
    handleGetListPost();
  };

  const handleDeleteComment = async (cmtId: any) => {
    await deleteComment(cmtId);
    handleShowComment(idxCmt, postIdCurrent, true);
  };

  const itemComment = (idx: any, cmtDetail: any) => {
    return {
      items: [
        {
          key: "1",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleDeleteComment(cmtDetail.id);
              }}
              style={{ fontWeight: "600", color: "#285158", fontSize: "12px" }}
            >
              <DeleteOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Xóa bình luận
            </div>
          ),
        },
        {
          key: "2",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setCommentDetail(cmtDetail);
                setIdxCmt(idx);
                setIsOpenModal(true);
                setTypeModal(TYPE_FORM_POST.CMT_UPDATE);
              }}
              style={{ fontWeight: "600", color: "#285158", fontSize: "12px" }}
            >
              <EditOutlined
                className="icon"
                style={{
                  fontSize: "14px",
                  marginRight: "5px",
                  fontWeight: "bolder",
                }}
              />{" "}
              Chỉnh sửa
            </div>
          ),
        },
      ],
    };
  };

  const items = (idx: any, postDetail: any) => {
    return {
      items: [
        {
          key: "1",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleDeletePost(postDetail.id);
                // handleDeleteContact();
              }}
              style={{ fontWeight: "600", color: "#285158", fontSize: "12px" }}
            >
              <DeleteOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Xóa bài viết
            </div>
          ),
        },
        {
          key: "2",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setPostDetail(postDetail);
                setIdxCmt(idx);
                setIsOpenModal(true);
                setTypeModal(TYPE_FORM_POST.POST_UPDATE);
              }}
              style={{ fontWeight: "600", color: "#285158", fontSize: "12px" }}
            >
              <EditOutlined
                className="icon"
                style={{
                  fontSize: "14px",
                  marginRight: "5px",
                  fontWeight: "bolder",
                }}
              />{" "}
              Chỉnh sửa
            </div>
          ),
        },
      ],
    };
  };

  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  useEffect(() => {
    handleGetListPost();
    setIsCallBackApi(false);
  }, [isCallBackApi]);

  return (
    <>
      <Row className="wrapper-post-team">
        {/* <Row justify={"center"}> */}
        {/* <div className="container-team-overview-header">
          <img
            className="img-post-overview"
            alt="example"
            src="http://res.cloudinary.com/dcgjui0yd/image/upload/v1671732704/na7pkotvuhqy2qor3wze.jpg"
          />
        </div> */}
        <div
          className="container-post-carousel"
          style={{ height: "300px", marginBottom: "1.5rem" }}
        >
          <img
            src={teamDetail.imagePath}
            alt=""
            className="carousel-item"
            style={{ borderRadius: "10px" }}
          />
          <div
            className="wrapper-title"
            style={{
              width: "100%",
              borderBottomRightRadius: "10px",
              borderBottomLeftRadius: "10px",
            }}
          >
            <p
              className="title"
              style={{ color: "white" }}
              // onClick={() => {
              //   navigate(`/post-detail/${postOverView.id}`);
              // }}
            >
              {subTitle(teamDetail.name, MAX_LENGTH_TITLE)}
            </p>
            <span style={{ color: "white" }}>
              {" "}
              {subTitle(teamDetail.description, MAX_LENGTH_TITLE)}
            </span>
          </div>
        </div>
        <div className="add-post">
          <Col span={2}>
            <Avatar size={45} src={user?.imagePath} />
          </Col>
          <Col span={16}>
            <Input
              onClick={() => {
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setIsOpenModal(true);
                setTypeModal(TYPE_FORM_POST.POST_CREATE);
              }}
              readOnly
              className="input-post"
              placeholder="Đăng bài viết lên nhóm"
            />
          </Col>
        </div>
        <Divider></Divider>
        <div className="list-post">
          {isLoadingPost && (
            <Skeleton className="post-detail" loading={true} avatar active>
              <Meta
                className="post-detail"
                avatar={<Avatar src={profileDefault} />}
                title={
                  <div className="info-post">
                    <div className="username ">Vũ Xuân lực </div>{" "}
                    <span className="time-post">11/04/2022 13:30</span>
                  </div>
                }
                description="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officia molestiae atque sequi aperiam magni quaerat quidem quisquam nesciunt alias fugiat. Illum sunt atque eveniet sint rem accusamus earum minima provident."
              />
            </Skeleton>
          )}
          {listPost &&
            listPost.map((item: any, idx: any) => (
              <div key={idx} className="post-detail">
                <Meta
                  key={idx}
                  // className="post-detail"
                  avatar={<Avatar src={item.creator.imagePath} />}
                  title={
                    <div className="info-post">
                      <div className="username ">{item.creator.firstName}</div>
                      <span
                        className="time-post"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        {moment(item.createDate).format(FORMAT_DATE_FULL)}
                        {user.id === item.creator.id && (
                          <Dropdown
                            menu={items(idx, item)}
                            placement="topLeft"
                            arrow
                          >
                            <SettingOutlined
                              className="icon"
                              style={{ fontSize: "12px", marginLeft: "7px" }}
                              onClick={(e: any) => e.stopPropagation()}
                            />
                          </Dropdown>
                        )}
                      </span>
                    </div>
                  }
                  description={
                    <div
                      dangerouslySetInnerHTML={{
                        __html: `${item.content}`,
                      }}
                    />
                  }
                />
                <div className="action-cmt">
                  <span
                    className="btn-like"
                    style={{
                      cursor: "pointer",
                    }}
                    onClick={() => handleLike(item.id)}
                  >
                    <Tooltip
                      placement="top"
                      title="Lượt thích"
                      arrowPointAtCenter
                    >
                      {listShowComment[idx].isLiked && (
                        <LikeFilled
                          className="icon"
                          style={{
                            fontSize: "20px",
                            marginRight: "5px",
                          }}
                        />
                      )}
                      {!listShowComment[idx].isLiked && (
                        <LikeOutlined
                          className="icon"
                          style={{ fontSize: "20px", marginRight: "5px" }}
                        />
                      )}
                      <span className="label">
                        {listShowComment[idx].noLike} Lượt thích
                      </span>
                    </Tooltip>
                  </span>
                  <span
                    className="btn-comment"
                    onClick={() => handleShowComment(idx, item.id, false)}
                  >
                    <Tooltip
                      placement="top"
                      title="Bình luận"
                      arrowPointAtCenter
                    >
                      <MessageOutlined
                        className="icon"
                        style={{ fontSize: "20px", marginRight: "5px" }}
                      />
                      <span style={{ cursor: "pointer" }} className="label">
                        {listShowComment[idx].noComment} Bình luận
                      </span>
                    </Tooltip>
                  </span>
                </div>
                <div
                  hidden={listShowComment[idx]?.isHide}
                  className="container-comment"
                >
                  <div className="add-cmt">
                    <Col span={1}>
                      <Avatar src={user.imagePath ?? profileDefault} />
                    </Col>
                    <Col span={16}>
                      <Input
                        onClick={() => {
                          if (
                            roleUser?.event?.status === "DONE" ||
                            roleUser?.event?.status === "CANCEL"
                          ) {
                            return message.open({
                              type: "error",
                              content: getMessStatus(roleUser?.event?.status),
                            });
                          }
                          setIdxCmt(idx);
                          setIsOpenModal(true);
                          setTypeModal(TYPE_FORM_POST.CMT_CREATE);
                        }}
                        readOnly
                        className="input-post"
                        placeholder="Viết bình luận"
                      />
                    </Col>
                  </div>
                  {/* <Spin /> */}
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "start",
                    }}
                  >
                    {listShowComment[idx]?.listComment?.map(
                      (item: any, idxComment: any) => (
                        <Meta
                          key={idxComment}
                          className="wrapper-comment"
                          avatar={<Avatar src={item.creator.imagePath} />}
                          title={
                            <div className="info-post" style={{ margin: "0" }}>
                              <div className="username ">
                                {item.creator.firstName}
                              </div>
                              <span
                                className="time-post"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                {moment(item.createDate).format(
                                  FORMAT_DATE_FULL
                                )}
                                {user.id === item.creator.id && (
                                  <Dropdown
                                    menu={itemComment(idx, item)}
                                    placement="topLeft"
                                    arrow
                                  >
                                    <SettingOutlined
                                      className="icon"
                                      style={{
                                        fontSize: "12px",
                                        marginLeft: "7px",
                                      }}
                                      onClick={(e: any) => e.stopPropagation()}
                                    />
                                  </Dropdown>
                                )}
                              </span>
                            </div>
                          }
                          description={
                            <div
                              className="desciption"
                              dangerouslySetInnerHTML={{
                                __html: `${item.content}`,
                              }}
                            />
                          }
                        />
                      )
                    )}
                  </div>
                </div>
              </div>
            ))}
        </div>

        {/* </Row> */}
      </Row>
      <Modal
        width={
          typeModal === TYPE_FORM_POST.POST_CREATE ||
          typeModal === TYPE_FORM_POST.POST_UPDATE
            ? 1000
            : 600
        }
        title={getTitleModal()}
        open={isOpenModal}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              className="icon"
              onClick={() => setIsCreatePost(true)}
            />
          </Tooltip>,
        ]}
      >
        <AddPostAndCmt
          typeModal={typeModal}
          isCreatePost={isCreatePost}
          postIdCurrent={postIdCurrent}
          idxCmt={idxCmt}
          postDetail={postDetail}
          commentDetail={commentDetail}
          setIsOpenModal={setIsOpenModal}
          setIsLoadingPost={setIsLoadingPost}
          setIsCreatePost={setIsCreatePost}
          setIsCallBackApi={setIsCallBackApi}
          handleShowComment={handleShowComment}
        />
      </Modal>
    </>
  );
}

export default ListPostTeam;
