import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Radio,
  Row,
  Select,
  Tooltip,
} from "antd";
import "./add-contact.scss";
import React, { useEffect, useState } from "react";
import { CheckCircleOutlined } from "@ant-design/icons";
import MessError from "components/common/MessError";
import { addContact, createContact } from "features/contact/contactSlice";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { toastCustom } from "config/toast";
import useDebounce from "hook/useDebounce";
import { getUserInSystem } from "features/eventManage/eventManageSlice";
import OptionItem from "components/common/optionItem/OptionItem";
const { Option } = Select;

function AddContact(props: any) {
  const dispatch = useDispatch();
  const { setIsOpenModalAddContact, setIsCallBackApi } = props;

  const [form] = Form.useForm();
  const [keySearchUser, setKeySearchUser] = useState(null);
  const [isLoadingDropdown, setIsLoadingDropdown] = useState(false);
  const [listUser, setListUser] = useState([]);
  const debouncedSearch = useDebounce(keySearchUser, 1000);

  const handleAddContact = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const data = await addContact(value.user);
      setIsCallBackApi(true);
      setIsOpenModalAddContact(false);
      if (data.data.errorCode === null) {
        toastCustom("success", "Thêm liên hệ thành công");
      }
    } catch (e) {
      toastCustom("error", "Thêm liên hệ thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleDebounceSearchUser = async () => {
    const payload = {
      eventId: null,
      // isUser: true,
      keyword: keySearchUser,
      findAddedUser: false,
      orders: [],
      page: 0,
      size: 100,
      teamId: null,
    };
    try {
      setIsLoadingDropdown(true);
      const data = await getUserInSystem(payload);
      setListUser(data.data.contactPage.content);
    } catch (error) {
    } finally {
      setIsLoadingDropdown(false);
    }
  };

  useEffect(() => {
    // search the api
    if (debouncedSearch) handleDebounceSearchUser();
  }, [debouncedSearch]);

  return (
    <div className="wrapper-add-contact">
      <Row justify="center" className="action-add">
        <Card className="wrapper-card">
          <Form
            form={form}
            onFinish={(value) => handleAddContact(value)}
            autoComplete="off"
            style={{ width: "100%" }}
          >
            <Row justify="center" style={{ marginBottom: "2rem" }}>
              <Col span={16} style={{ marginRight: "1rem" }}>
                <Form.Item
                  name="user"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng chọn liên hệ!"} />,
                    },
                  ]}
                >
                  <Select
                    className="select"
                    showSearch
                    placeholder="Tìm kiếm theo tên tài khoản"
                    notFoundContent="Không có dữ liệu"
                    optionLabelProp="label"
                    onSearch={(value: any) => {
                      setKeySearchUser(value?.trim());
                    }}
                    loading={isLoadingDropdown}
                    filterOption={false}
                  >
                    {listUser &&
                      listUser.map(
                        (item: any, idx) =>
                          (item.isAdded === false || item.isAdded === null) && (
                            <Option
                              key={idx}
                              value={item.id}
                              label={item.email}
                            >
                              <OptionItem
                                mail={item.email}
                                name={item.firstName}
                                image={item.imagePath}
                              />
                            </Option>
                          )
                      )}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={4}>
                <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
                  <CheckCircleOutlined
                    className="icon"
                    onClick={() => form.submit()}
                  />
                </Tooltip>
              </Col>
            </Row>
          </Form>
        </Card>
      </Row>
    </div>
  );
}

export default AddContact;
