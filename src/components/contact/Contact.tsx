import {
  Avatar,
  Card,
  Drawer,
  Tooltip,
  Row,
  Divider,
  Dropdown,
  Modal,
} from "antd";
import profileDefault from "assets/images/profileDefault.png";
import "./contact.scss";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import {
  EllipsisOutlined,
  UserAddOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { isEmpty } from "mixin/generate";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { deleteContact, getContactId } from "features/contact/contactSlice";
import AddMemberToEvent from "features/eventManage/team/modal/AddMemberToEvent";
import { TYPE_MODAL_MEMBER_EVENT } from "constants/event";
import { toastCustom } from "config/toast";
const { Meta } = Card;
function Contact(props: any) {
  const { contact, setIsCallBackApi } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [contactDetail, setContactDetail] = useState<any>({});
  const [isOpenModal, setIsOpenModal] = useState(false);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const showDrawer = (contactDetail: any) => {
    setContactDetail(contactDetail);
    // handleGetContactById(idContact);
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleGetContactById = async (idContact: any) => {
    try {
      const data = await getContactId(idContact);
      setContactDetail(data.data);
    } catch (error) {
    } finally {
    }
  };

  const handleDeleteContact = async () => {
    try {
      const data: any = await deleteContact(contact.id);
      if (data?.errorCode === null) {
        toastCustom("success", "Xóa thành công");
      }
      setIsCallBackApi(true);
    } catch (error) {
    } finally {
    }
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            handleDeleteContact();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <DeleteOutlined
            className="icon"
            style={{ fontSize: "16px", marginRight: "5px" }}
          />{" "}
          Xóa liên hệ
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setIsOpenModal(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <UserAddOutlined
            className="icon"
            style={{
              fontSize: "16px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Mời vào sự kiện
        </div>
      ),
    },
  ];

  const getEventCommon = (listEventCommon: any) => {
    let eventCommon = "";
    listEventCommon?.map((item: any) => {
      eventCommon += item.name + ", ";
    });
    return eventCommon?.trim().substring(0, eventCommon.length - 2);
  };

  return (
    <>
      <Card
        onClick={() => showDrawer(contact)}
        className="card-item"
        title={<div>{contact.firstName}</div>}
        extra={
          <Dropdown menu={{ items }} placement="top" arrow>
            <EllipsisOutlined
              className="icon"
              style={{ fontSize: "22px" }}
              onClick={(e) => e.stopPropagation()}
            />
          </Dropdown>
        }
        hoverable
      >
        {/* <Skeleton loading={true} avatar active> */}
        <Meta
          avatar={
            <Avatar
              src={
                isEmpty(contact.imagePath) ? profileDefault : contact.imagePath
              }
            />
          }
          title={<div>{contact.email}</div>}
          description={
            <Tooltip placement="top" title="Số điện thoại" arrowPointAtCenter>
              <span>{contact.phone}</span>
            </Tooltip>
          }
        />
        {/* </Skeleton> */}
      </Card>
      <Drawer
        title={contact.firstName}
        placement="right"
        onClose={onClose}
        open={open}
      >
        <div className="drawer-contact-detail">
          <Row justify={"center"} className="wrapper-avatar">
            <Avatar
              className="avatar"
              src={
                isEmpty(contact.imagePath) ? profileDefault : contact.imagePath
              }
            />
          </Row>
          <div className="wrapper-detail">
            <div className="row-info field">
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.firstName}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Email: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.email}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Số điện thoại: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.phone}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Sự kiện tham gia: </span>
            </div>
            <div className="row-info infor">
              <span style={{ overflow: "auto" }}>
                {getEventCommon(contactDetail.eventsInCommon)}
              </span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Nhóm: </span>
            </div>
            <div className="row-info infor">
              <span style={{ overflow: "auto" }}>
                {" "}
                {getEventCommon(contactDetail.teamsInCommon)}
              </span>
            </div>
          </div>
        </div>
      </Drawer>
      <Modal
        width={1000}
        title={"Mời vào sự kiện"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddMemberToEvent
          typeModal={TYPE_MODAL_MEMBER_EVENT.ADD_CONTACT}
          userContactId={contact.id}
          setIsOpenModal={setIsOpenModal}
        />
      </Modal>
    </>
  );
}

export default Contact;
