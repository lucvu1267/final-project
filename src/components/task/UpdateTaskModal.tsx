import { Button, Col, Input, Modal, Row, Select, Tooltip } from "antd";
import "./add-task.scss";
import { useState, useEffect } from "react";
import { DatePicker, Space, Form } from "antd";
import React from "react";
import locale from "antd/es/locale/vi_VN";
import { CheckCircleOutlined } from "@ant-design/icons";
import ConfigProvider from "antd/es/config-provider";
import { getListTaskById, updateTask } from "features/task/taskSlice";
import MessError from "components/common/MessError";
import OptionItem from "components/common/optionItem/OptionItem";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { toastCustom } from "config/toast";
import { isEmpty } from "mixin/generate";
import { FORMAT_DATE_FULL } from "constants/index";
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

const lIST_PERCENT = [
  { value: 0, label: "0" },
  { value: 10, label: "10" },
  { value: 20, label: "20" },
  { value: 30, label: "30" },
  { value: 40, label: "40" },
  { value: 50, label: "50" },
  { value: 60, label: "60" },
  { value: 70, label: "70" },
  { value: 80, label: "80" },
  { value: 90, label: "90" },
  { value: 100, label: "100" },
];

const LIST_STATUS = [
  { value: "OPEN", label: "Đang mở" },
  { value: "INPROGRESS", label: "Đang thực hiện" },
  { value: "DONE", label: "Hoàn thành" },
  { value: "CANCEL", label: "Đã đóng" },
];

function UpdateTaskModal(props: any) {
  const {
    scheduler,
    setIsOpenModalUpdate,
    isOpenModal,
    taskDetail,
    listMember,
    listTaskByEvent,
    setIsCallbackApi,
  } = props;

  const user = useSelector((state: any) => state.contactSlice.user);

  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [stastusData, setStatusData] = useState(null);
  const [progressData, setProgressData] = useState(null);
  const handleCloseModal = () => {
    setIsOpenModalUpdate(false);
    scheduler && scheduler.close();
  };

  const isDifference = (value: any) => {
    return (
      taskDetail.name !== value.name ||
      (taskDetail?.parentTask ? taskDetail?.parentTask.id : null) !==
        (isEmpty(value.taskParent) ? null : parseInt(value.taskParent + "")) ||
      taskDetail?.assignee?.id !==
        (isEmpty(value.assignee) ? null : parseInt(value.assignee + "")) ||
      taskDetail?.description !== value?.description ||
      taskDetail?.progress !==
        (isEmpty(value.progress) ? null : parseInt(value.progress + "")) ||
      !(
        moment(value.deadline[0]._d).format(FORMAT_DATE_FULL) ===
        moment(taskDetail?.startDate).format(FORMAT_DATE_FULL)
      ) ||
      !(
        moment(value.deadline[1]._d).format(FORMAT_DATE_FULL) ===
        moment(taskDetail?.endDate).format(FORMAT_DATE_FULL)
      ) ||
      !isEmpty(value.note)
    );
  };

  const handleSubmitUpdateTask = async (value: any) => {
    if (!isDifference(value)) {
      return toastCustom("error", "Thông tin chưa được thay đổi");
    }
    const payload = {
      assigneeId: isEmpty(value.assignee)
        ? null
        : parseInt(value.assignee + ""),
      description: value?.description,
      id: taskDetail.id,
      name: value.name,
      note: value.note,
      parentTaskId: isEmpty(value.taskParent)
        ? null
        : parseInt(value.taskParent + ""),
      progress:
        value.status === "OPEN" || value.status === "CANCEL"
          ? 0
          : value.status === "DONE"
          ? 100
          : isEmpty(value.progress)
          ? null
          : parseInt(value.progress + ""),
      startDate: moment(value.deadline[0]._d, "YYYY/MM/DD HH:mm"),
      endDate: moment(value.deadline[1]._d, "YYYY/MM/DD HH:mm"),
      status: value.status,
    };
    try {
      dispatch(setLoading(true));
      const data: any = await updateTask(payload);
      if (data.errorCode === null) {
        setIsOpenModalUpdate(false);
        setIsCallbackApi(true);
        toastCustom("success", "Cập nhật thành công");
      }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  useEffect(() => {
    setStatusData(taskDetail?.status);
    setProgressData(taskDetail?.progress);
  }, [taskDetail]);

  return (
    <div>
      <Modal
        width={1300}
        title="Cập nhật công việc"
        open={isOpenModal}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <Form
          form={form}
          onFinish={(value) => handleSubmitUpdateTask(value)}
          autoComplete="off"
          style={{ width: "100%" }}
          initialValues={{
            name: taskDetail?.name,
            taskParent: taskDetail?.parentTask?.id,
            event: taskDetail?.event?.name,
            assignee: taskDetail?.assignee?.id,
            description: taskDetail?.description,
            progress: taskDetail?.progress,
            deadline: [
              moment(taskDetail?.startDate),
              moment(taskDetail?.endDate),
            ],
            status: taskDetail.status,
          }}
        >
          <div className="container-add-task">
            <Row className="wrapper-top">
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Tên công việc<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng nhập tên công việc!"} />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={"Tên công việc không được quá 255 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên công việc" />
                  </Form.Item>
                </div>
                <div className="field-add">
                  <div className="label-field">
                    Thời gian hoàn thành
                    <span style={{ color: "red" }}> *</span>
                  </div>
                  <Space direction="vertical" size={12}>
                    <ConfigProvider locale={locale}>
                      <Form.Item
                        name="deadline"
                        className="text-field"
                        rules={[
                          {
                            required: true,
                            message: (
                              <MessError
                                message={"Vui lòng chọn thời gian hoàn thành!"}
                              />
                            ),
                          },
                        ]}
                      >
                        <RangePicker
                          showTime={{ format: "HH:mm" }}
                          format={"DD/MM/YYYY HH:mm"}
                          onChange={(e: any) =>
                            console.log(moment(e[0]._d, "YYYY/MM/DD HH:mm"))
                          }
                        />
                      </Form.Item>
                    </ConfigProvider>
                  </Space>
                </div>
                <div className="field-add">
                  <div className="label-field">Công việc cha</div>
                  <Form.Item name="taskParent" className="text-field">
                    <Select
                      className="select"
                      showSearch
                      placeholder="Tìm kiếm theo tên"
                      notFoundContent="Không có dữ liệu"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      onChange={() => {}}
                      onSearch={() => {}}
                      // filterOption={(input, option) =>
                      //   (option!.children as unknown as string)
                      //     .toLowerCase()
                      //     .includes(input.toLowerCase())
                      // }
                    >
                      {listTaskByEvent &&
                        listTaskByEvent.map(
                          (item: any, idx: any) =>
                            item.id !== taskDetail.id && (
                              <Option
                                key={idx}
                                value={item.id}
                                label={item.name}
                              >
                                {item.name}
                              </Option>
                            )
                        )}
                    </Select>
                  </Form.Item>
                </div>{" "}
                {stastusData === "INPROGRESS" && (
                  <div className="field-add">
                    <div className="label-field">Tiến độ</div>
                    <Form.Item name="progress" className="text-field">
                      <Select
                        className="select"
                        placeholder="Tìm kiếm theo tên"
                        optionFilterProp="children"
                        onChange={(e) => setProgressData(e)}
                      >
                        {lIST_PERCENT?.map((item: any, idx: any) => (
                          <Option key={idx} value={item.value}>
                            {`${item.label}%`}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </div>
                )}
              </Col>
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Sự kiện<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item name="event" className="text-field">
                    <Input
                      className="input"
                      placeholder="Sự kiện"
                      readOnly
                      value={"Ngày nhà giáo Việt Nam - 20/11"}
                    />
                  </Form.Item>
                </div>

                <div className="field-add">
                  <div className="label-field">
                    Người nhận việc<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="assignee"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng chọn người nhận việc!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Select
                      className="select"
                      showSearch
                      placeholder="Tìm kiếm theo tên"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      notFoundContent="Không có dữ liệu"
                    >
                      {listMember &&
                        listMember.map((item: any, idx: any) => (
                          <Option
                            key={idx}
                            value={item.member.id}
                            label={item.member.emailAccount}
                          >
                            <OptionItem
                              name={item.member.firstName}
                              mail={item.member.emailAccount}
                              image={item.member.imagePath}
                            />
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </div>
                <div className="field-add">
                  <div className="label-field">Trạng thái</div>
                  <Form.Item name="status" className="text-field">
                    <Select
                      className="select"
                      placeholder="Tìm kiếm theo tên"
                      notFoundContent="Không có dữ liệu"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      onChange={(e) => setStatusData(e)}
                      onSearch={() => {}}
                      // filterOption={(input, option) =>
                      //   (option!.children as unknown as string)
                      //     .toLowerCase()
                      //     .includes(input.toLowerCase())
                      // }
                    >
                      {LIST_STATUS.map((item: any, idx: any) => (
                        <Option
                          key={idx}
                          value={item.value}
                          label={item.label}
                          disabled={
                            // (item.value === "INPROGRESS" &&
                            //   progressData === 0) ||
                            (taskDetail.status === "DONE" &&
                              (item.value === "CANCEL" ||
                                item.value === "INPROGRESS")) ||
                            (taskDetail.status === "CANCEL" &&
                              (item.value === "DONE" ||
                                item.value === "INPROGRESS"))
                          }
                        >
                          {item.label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </div>
              </Col>
            </Row>
            {/* {user.emailAccount === taskDetail.assigner.emailAccount && ( */}
            <Row className="wrapper-bottom" style={{ width: "100%" }}>
              <div className="label-field">
                Chi tiết công việc<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="description"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: (
                      <MessError
                        message={"Vui lòng nhập chi tiết công việc!"}
                      />
                    ),
                  },
                  {
                    max: 500,
                    message: (
                      <MessError
                        message={"Chi tiết công việc không được quá 500 kí tự!"}
                      />
                    ),
                  },
                ]}
              >
                <TextArea
                  rows={3}
                  size="large"
                  placeholder="Chi tiết công việc"
                />
              </Form.Item>{" "}
            </Row>
            {/* )} */}
            <Row className="wrapper-bottom" style={{ width: "100%" }}>
              <div className="label-field">Ghi chú</div>
              {((stastusData === "DONE" && taskDetail.status !== "DONE") ||
                (stastusData === "CANCEL" &&
                  taskDetail.status !== "CANCEL")) && (
                <span style={{ color: "red" }}> *</span>
              )}
              <Form.Item
                name="note"
                className="text-field"
                rules={[
                  {
                    required:
                      (stastusData === "DONE" &&
                        taskDetail.status !== "DONE") ||
                      (stastusData === "CANCEL" &&
                        taskDetail.status !== "CANCEL"),
                    message: <MessError message={"Vui lòng nhập ghi chú!"} />,
                  },
                ]}
              >
                <TextArea rows={3} size="large" placeholder="Ghi chú" />
              </Form.Item>
            </Row>
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default UpdateTaskModal;
