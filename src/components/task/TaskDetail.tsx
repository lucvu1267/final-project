import { Col, Collapse, Divider, List, Progress, Row, Typography } from "antd";
import luc_vu from "assets/images/luc_vu.jpeg";
import { Button, Modal } from "antd";
import { EditOutlined } from "@ant-design/icons";
import "./task-detail.scss";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { getActivitiLog, getListTaskById } from "features/task/taskSlice";
import moment from "moment";
import { FORMAT_DATE_FULL } from "constants/index";
import { isEmpty } from "mixin/generate";
const { Panel } = Collapse;
const { Item } = List;
function TaskDetail(props: any) {
  const {
    event,
    fields,
    setIsOpenModalView,
    taskId,
    handleOpenModalUpdateTask,
  } = props;
  const dispatch = useDispatch();

  const [isOpenModal, setIsOpenModal] = useState(true);
  const [taskDetail, setTaskDetail] = useState<any>(null);
  const [activitiLog, setActivitiLog] = useState<any>(null);
  const [maxWidth, setMaxWidth] = useState(false);

  const handleCloseModal = () => {
    document.getElementById("btn")?.click();
    setIsOpenModal(false);
    setIsOpenModalView(false);
  };

  const handleGetTask = async () => {
    dispatch(setLoading(true));
    try {
      Promise.all([
        getListTaskById(event ? event.event_id : taskId),
        getActivitiLog(event ? event.event_id : taskId),
      ]).then((value: any) => {
        setTaskDetail(value[0].data);
        setActivitiLog(value[1].data);
      });
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const getContentLog = (nameFile: any, oldValue: any, newValue: any) => {
    switch (nameFile) {
      case "name":
        return {
          label: "Tên công việc  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {oldValue}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {newValue}
              </Typography.Text>
            </span>
          ),
        };

      case "startDate":
        return {
          label: "Ngày bắt đầu  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {moment(new Date(parseInt(oldValue))).format(FORMAT_DATE_FULL)}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {moment(new Date(parseInt(newValue))).format(FORMAT_DATE_FULL)}
              </Typography.Text>
            </span>
          ),
        };

      case "endDate":
        return {
          label: "Ngày kết thúc",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                underline
                style={{ fontWeight: 600, color: "#285158" }}
              >
                {" "}
                {moment(new Date(parseInt(oldValue))).format(FORMAT_DATE_FULL)}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                underline
                style={{ fontWeight: 600, color: "#285158" }}
              >
                {" "}
                {moment(new Date(parseInt(newValue))).format(FORMAT_DATE_FULL)}
              </Typography.Text>
            </span>
          ),
        };

      case "description":
        return {
          label: "Mô tả  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {oldValue}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {newValue}
              </Typography.Text>
            </span>
          ),
        };

      case "parentTaskCode":
        return {
          label: "Công việc liên quan  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {!isEmpty(oldValue) && (
                <span>
                  {" "}
                  <Typography.Text
                    style={{ fontWeight: 600, color: "#285158" }}
                    underline
                  >
                    [{oldValue}]
                  </Typography.Text>{" "}
                  đổi sang{" "}
                </span>
              )}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                [{newValue}]
              </Typography.Text>
            </span>
          ),
        };

      case "progress":
        return {
          label: "Tiến độ  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {oldValue}%
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {newValue}%
              </Typography.Text>
            </span>
          ),
        };
      case "assigneeName":
        return {
          label: "Người thực hiện  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {oldValue}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {newValue}
              </Typography.Text>
            </span>
          ),
        };
      case "status":
        return {
          label: "Trạng thái  ",
          content: (
            <span className="description-gray" style={{ fontWeight: 600 }}>
              {" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {handleGetNameStatus(oldValue)}
              </Typography.Text>{" "}
              đổi sang{" "}
              <Typography.Text
                style={{ fontWeight: 600, color: "#285158" }}
                underline
              >
                {handleGetNameStatus(newValue)}
              </Typography.Text>
            </span>
          ),
        };

      // default:
      //   return "";
    }
  };

  const handleGetNameStatus = (status: any) => {
    switch (status) {
      case "OPEN":
        return "Đang mở";
      case "INPROGRESS":
        return "Đang thực hiện";
      case "DONE":
        return "Hoàn thành";
      case "CANCEL":
        return "Đã đóng";
      default:
        break;
    }
  };

  useEffect(() => {
    const elm = document.querySelectorAll('[data-testid="ClearRoundedIcon"]');
    elm[0]?.parentElement?.setAttribute("id", "btn");
  }, []);

  useEffect(() => {
    handleGetTask();
  }, [taskId]);

  return (
    <Modal
      width={1500}
      title={taskDetail?.code}
      open={isOpenModal}
      centered={true}
      onCancel={handleCloseModal}
      footer={[
        <Button
          id="btn2"
          className="btn"
          style={{ fontWeight: "bold" }}
          key="submit"
          type="primary"
          onClick={() => {
            handleCloseModal();
            handleOpenModalUpdateTask(taskDetail.id);
          }}
        >
          <EditOutlined />
        </Button>,
      ]}
    >
      {taskDetail !== null && (
        <div className="container-task" id="test">
          <div className="wrapper-left">
            <Col>
              <Row>
                <div className="title">{taskDetail.name}</div>
              </Row>
              <Row className="detail">
                <Col span={15}>
                  <Row key={1} style={{ marginBottom: "0.5rem" }}>
                    <Col span={8} className="wrapper-field">
                      <span className="field-detail">Sự kiện:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      {taskDetail.event.name}
                    </Col>
                  </Row>
                  <Row key={2} style={{ marginBottom: "0.5rem" }}>
                    <Col span={8} className="wrapper-field">
                      <span className="field-detail">Người tạo:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      {taskDetail.assigner.firstName}
                    </Col>
                  </Row>
                  <Row key={3} style={{ marginBottom: "0.5rem" }}>
                    <Col span={8} className="wrapper-field">
                      <span className="field-detail">Người thực hiện:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      {taskDetail.assignee.firstName}
                    </Col>
                  </Row>
                  <Row key={4} style={{ marginBottom: "0.5rem" }}>
                    <Col span={8} className="wrapper-field">
                      <span className="field-detail">Công việc liên quan:</span>
                    </Col>
                    <Col span={14} className="wrapper-field field-task-parent">
                      {taskDetail.parentTask?.name}
                    </Col>
                  </Row>
                </Col>
                <Col span={9}>
                  <Row key={4} style={{ marginBottom: "0.5rem" }}>
                    <Col span={10} className="wrapper-field">
                      <span className="field-detail">Ngày bắt đầu:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      {moment(taskDetail.startDate).format(FORMAT_DATE_FULL)}
                    </Col>
                  </Row>
                  <Row key={5} style={{ marginBottom: "0.5rem" }}>
                    <Col span={10} className="wrapper-field">
                      <span className="field-detail">Ngày hoàn đổi sang:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      {moment(taskDetail.endDate).format(FORMAT_DATE_FULL)}
                    </Col>
                  </Row>
                  <Row key={6} style={{ marginBottom: "0.5rem" }}>
                    <Col span={10} className="wrapper-field">
                      <span className="field-detail">Tiến độ:</span>
                    </Col>
                    <Col span={14} className="wrapper-field">
                      <Progress
                        style={{ width: "10rem" }}
                        percent={taskDetail.progress}
                        size="small"
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="description">
                <Collapse
                  style={maxWidth ? { width: "100%" } : {}}
                  defaultActiveKey={0}
                  onChange={(e) => {
                    setMaxWidth(e.length > 1);
                  }}
                >
                  {/* {taskDetail?.childrenTask.length > 0 && ( */}
                  <Panel
                    style={{ width: "100%" }}
                    header="Các việc cần làm"
                    key="1"
                  >
                    <List
                      style={{
                        maxHeight: "300px",
                        width: "100%",
                        overflowY: "auto",
                      }}
                      header={null}
                      footer={null}
                      bordered
                      dataSource={taskDetail.childrenTask}
                      renderItem={(item: any) => (
                        <List.Item key={item.id}>
                          <Typography.Text strong mark>
                            [{item.code}]
                          </Typography.Text>{" "}
                          {item.name}
                          <div className="subtask-content">
                            <div className="content">
                              {item.assignee.firstName}
                            </div>
                            <div className="content">
                              {moment(item.startDate).format(FORMAT_DATE_FULL)}{" "}
                              - {moment(item.endDate).format(FORMAT_DATE_FULL)}
                            </div>
                            <Progress
                              style={{ width: "10rem" }}
                              percent={item.progress}
                              size="small"
                            />
                          </div>
                        </List.Item>
                      )}
                    >
                      {/* {taskDetail?.childrenTask.map((item: any, idx: any) => (
                          <List.Item key={item.id}>
                            <Typography.Text strong mark>
                              [{item.code}]
                            </Typography.Text>{" "}
                            {item.name}
                            <div className="subtask-content">
                              <div className="content">
                                {item.assignee.firstName}
                              </div>
                              <div className="content">
                                {moment(item.startDate).format(
                                  FORMAT_DATE_FULL
                                )}{" "}
                                -{" "}
                                {moment(item.endDate).format(FORMAT_DATE_FULL)}
                              </div>
                              <Progress
                                style={{ width: "10rem" }}
                                percent={item.progress}
                                size="small"
                              />
                            </div>
                          </List.Item>
                        ))} */}
                    </List>
                  </Panel>
                  {/* )} */}
                </Collapse>
                <Divider style={{ marginBottom: "2rem" }} plain>
                  Mô tả công việc
                </Divider>
                <span>{taskDetail.description}</span>
              </Row>
            </Col>
          </div>
          <div className="wrapper-right">
            {activitiLog?.map((item: any, index: any) => {
              return (
                <div key={index} className="wrapper-update">
                  <Divider className="divider" plain>
                    {moment(item.createDate).format(FORMAT_DATE_FULL)}
                  </Divider>

                  <div className="content-account">
                    <img
                      className="img-profile"
                      src={item.actor.imagePath}
                      alt=""
                    />
                    <div className="name-profile">
                      <div style={{ fontWeight: "bold", marginRight: "5px" }}>
                        {item.actor.firstName}
                      </div>
                      <div className="description-gray">đã cập nhật:</div>
                    </div>
                  </div>
                  <div className="content-update">
                    <ul>
                      {item.activityLogDetails.map((log: any, idx: any) => (
                        <li>
                          <span
                            style={{
                              fontWeight: "bold",
                              color: "#707373",
                              marginRight: "10px",
                            }}
                          >
                            {
                              getContentLog(
                                log.property,
                                log.oldValue,
                                log.newValue
                              )?.label
                            }
                            :{"    "}
                          </span>{" "}
                          {
                            getContentLog(
                              log.property,
                              log.oldValue,
                              log.newValue
                            )?.content
                          }
                        </li>
                      ))}
                    </ul>
                  </div>
                  {item.note && (
                    <div
                      style={{
                        backgroundColor: "white",
                        padding: "0.7rem 1rem",
                        borderRadius: "5px",
                      }}
                    >
                      <span style={{ whiteSpace: "break-spaces" }}>
                        {item.note}
                      </span>
                    </div>
                  )}
                </div>
              );
            })}
          </div>
        </div>
      )}
    </Modal>
  );
}

export default TaskDetail;
