import { Button, Col, Form, Input, Modal, Row, Select, Tooltip } from "antd";
import "./add-task.scss";
import { useState } from "react";
import { DatePicker, Space } from "antd";
import React, { useEffect } from "react";
import locale from "antd/es/locale/vi_VN";
import { CheckCircleOutlined } from "@ant-design/icons";
import ConfigProvider from "antd/es/config-provider";
import moment from "moment";
import MessError from "components/common/MessError";
import {
  getAllMember,
  getListEventJoined,
  getTaskBySearch,
} from "features/eventManage/eventManageSlice";
import OptionEventItem from "components/common/optionItem/OptionEventItem";
import OptionItem from "components/common/optionItem/OptionItem";
import { createTask } from "features/task/taskSlice";
import { toastCustom } from "config/toast";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

function AddTaskModal(props: any) {
  const { scheduler, setIsOpenModalAdd, setIsCallbackApi } = props;
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(true);
  const [isDisableField, setIsDisableField] = useState(true);
  const [isLoadingDropdown, setIsLoadingDropdown] = useState(false);
  const [listEvent, setListEvent] = useState([]);
  const [listTask, setListTask] = useState([]);
  const [listMember, setListMember] = useState([]);
  const [idEventSelected, setIdEventSelected] = useState(null);

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setIsOpenModalAdd(false);
    scheduler.close();
  };

  const handlecCreateTask = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        assigneeId: parseInt(value.assignee + ""),
        description: value.description,
        startDate: value.deadline[0]._d,
        endDate: value.deadline[1]._d,
        eventId: idEventSelected,
        name: value.name,
        parenTaskId: value.taskParent ? parseInt(value.taskParent + "") : null,
      };
      await createTask(payload);
      setIsOpenModalAdd(false);
      if (scheduler) {
        handleCloseModal();
      }
      setIsCallbackApi(true);
      toastCustom("success", "Tạo công việc thành công");
    } catch (error) {
      toastCustom("error", "Tạo công việc thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetListEventJoined = async () => {
    const data = await getListEventJoined();
    setListEvent(data.data);
  };

  useEffect(() => {
    handleGetListEventJoined();
  }, []);

  useEffect(() => {
    if (idEventSelected) {
      getMemberByEvent();
      getTaskByEvent();
      form.resetFields(["assignee", "taskParent"]);
    }
  }, [idEventSelected]);

  const getTaskByEvent = async () => {
    const payload = {
      assigneeId: null,
      eventId: idEventSelected,
      orders: [],
      page: 0,
      searchKey: null,
      size: 100,
      teamId: null,
    };
    const data = await getTaskBySearch(payload);
    setListTask(data.data.taskPage.content);
  };

  const getMemberByEvent = async () => {
    const data = await getAllMember(idEventSelected);
    setListMember(data.data);
  };

  return (
    <div>
      <Modal
        width={1300}
        title="Tạo mới công việc"
        open={isOpenModal}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        {" "}
        <Form
          form={form}
          onFinish={(value) => handlecCreateTask(value)}
          autoComplete="off"
          style={{ width: "100%" }}
          initialValues={
            scheduler
              ? {
                  deadline: [
                    moment(scheduler.state.start.value),
                    moment(scheduler.state.end.value),
                  ],
                }
              : {}
          }
        >
          <div className="container-add-task">
            <Row className="wrapper-top">
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Tên công việc<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng nhập tên công việc!"} />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={"Tên công việc không được quá 255 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên công việc" />
                  </Form.Item>
                </div>
                <div className="field-add">
                  <div className="label-field">
                    Sự kiện<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="event"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng chọn sự kiện bạn đang tham gia!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Select
                      className="select"
                      showSearch
                      placeholder="Tìm kiếm theo tên"
                      notFoundContent="Không có dữ liệu"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      onChange={(value) => {
                        setIsDisableField(false);
                        setIdEventSelected(value);
                      }}
                      // onSearch={(value: any) => {
                      //   setKeySearchEvent(value?.trim());
                      // }}
                      loading={isLoadingDropdown}
                    >
                      {listEvent &&
                        listEvent.map((item: any, idx) => (
                          <Option key={idx} value={item.id} label={item.name}>
                            <OptionEventItem
                              code={item.code}
                              image={item.logoPath}
                              name={item.name}
                            />
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </div>
                <div className="field-add">
                  <div className="label-field">Công việc cha</div>
                  <Form.Item name="taskParent" className="text-field">
                    <Select
                      disabled={isDisableField}
                      className="select"
                      showSearch
                      placeholder="Tìm kiếm theo tên"
                      notFoundContent="Không có dữ liệu"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      onChange={() => {}}
                      onSearch={() => {}}
                      // filterOption={(input, option) =>
                      //   (option!.children as unknown as string)
                      //     .toLowerCase()
                      //     .includes(input.toLowerCase())
                      // }
                    >
                      {listTask &&
                        listTask.map((item: any, idx) => (
                          <Option key={idx} value={item.id} label={item.name}>
                            {item.name}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </div>
              </Col>
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Thời gian hoàn thành<span style={{ color: "red" }}> *</span>
                  </div>
                  <Space direction="vertical" size={12}>
                    <ConfigProvider locale={locale}>
                      <Form.Item
                        name="deadline"
                        className="text-field"
                        rules={[
                          {
                            required: true,
                            message: (
                              <MessError
                                message={"Vui lòng chọn thời gian hoàn thành!"}
                              />
                            ),
                          },
                        ]}
                      >
                        <RangePicker
                          showTime={{ format: "HH:mm" }}
                          format="DD/MM/YYYY HH:mm"
                        />
                      </Form.Item>
                    </ConfigProvider>
                  </Space>
                </div>
                <div className="field-add">
                  <div className="label-field">
                    Người nhận việc<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="assignee"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng chọn người nhận việc!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Select
                      disabled={isDisableField}
                      className="select"
                      showSearch
                      placeholder="Tìm kiếm theo tên"
                      optionFilterProp="label"
                      optionLabelProp="label"
                      notFoundContent="Không có dữ liệu"
                      // filterOption={(input, option) =>
                      //   (option!.children as unknown as string).includes(input)
                      // }
                      // filterSort={(optionA, optionB) =>
                      //   (optionA!.children as unknown as string)
                      //     .toLowerCase()
                      //     .localeCompare(
                      //       (
                      //         optionB!.children as unknown as string
                      //       ).toLowerCase()
                      //     )
                      // }
                    >
                      {listMember &&
                        listMember.map((item: any, idx) => (
                          <Option
                            key={idx}
                            value={item.member.id}
                            label={item.member.emailAccount}
                          >
                            <OptionItem
                              name={item.member.firstName}
                              mail={item.member.emailAccount}
                              image={item.member.imagePath}
                            />
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row className="wrapper-bottom" style={{ width: "100%" }}>
              <div className="label-field">
                Chi tiết công việc<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="description"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: (
                      <MessError message={"Vui lòng chi tiết công việc!"} />
                    ),
                  },
                  {
                    max: 500,
                    message: (
                      <MessError
                        message={"Chi tiết công việc không được quá 500 kí tự!"}
                      />
                    ),
                  },
                ]}
              >
                <TextArea
                  rows={8}
                  size="large"
                  placeholder="Chi tiết công việc"
                />
              </Form.Item>{" "}
            </Row>
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default AddTaskModal;
