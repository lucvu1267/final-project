import React from "react";
import { Button, Form, Input } from "antd";
import "./changePassword.scss";
import { useLocation, useNavigate } from "react-router-dom";
function ChangePassword(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  return (
    <div className="detail-profile-container">
      <div className="detail-profile-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className=""
          onClick={() => navigate("/profile")}
        >
          Quay lại
        </Button>
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="detail-profile-edit"
        >
          Chỉnh sửa
        </Button>
      </div>
      <Form
        name="basic"
        className="container-changePassword"
        // onFinish={onFinish}
        // onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          // label="Mật khẩu cũ"
          // name="old-password"
          className="text-field"
          rules={[{ required: false, message: "Hãy nhập mật khẩu cũ!" }]}
        >
          <Input.Password
            className="input input-password"
            placeholder="Nhập mật khẩu cũ"
          />
        </Form.Item>

        <Form.Item
          // label="Mật khẩu mới"
          // name="new-password"
          className="text-field"
          rules={[{ required: false, message: "Hãy nhập mật khẩu mới!" }]}
        >
          <Input.Password
            className="input input-password"
            placeholder="Nhập mật khẩu mới"
          />
        </Form.Item>

        <Form.Item
          // label="Xác nhận mật khẩu"
          // name="confirm-password"
          className="text-field"
          rules={[{ required: false, message: "Hãy nhập xác nhận mật khẩu!" }]}
        >
          <Input.Password
            className="input input-confirm-password"
            placeholder="Nhập xác nhận mật khẩu"
          />
        </Form.Item>
        <div className="wrap-action">
          <Button
            ant-click-animating-without-extra-node="true"
            type="primary"
            htmlType="submit"
            className="btn-border-primary"
          >
            Lưu
          </Button>
        </div>
      </Form>
    </div>
  );
}

export default ChangePassword;
