import { Avatar, Button, Card, Descriptions, Divider, Tag } from "antd";
import { useNavigate } from "react-router-dom";
import "./detailProfile.scss";
import { useSelector } from "react-redux";

function DetailProfile(props: any) {
  const navigate = useNavigate();

  const user = useSelector((state: any) => state.contactSlice.user);
  console.log(user);

  return (
    <div className="detail-profile-container" style={{ width: "100%" }}>
      <div className="detail-profile-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="detail-profile-change-pass"
          onClick={() => navigate("/profile/change-password")}
        >
          Đổi mật khẩu
        </Button>
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="detail-profile-edit"
          onClick={() => navigate("/profile/edit")}
        >
          Chỉnh sửa
        </Button>
      </div>
      <Divider />
      <div className="detail-profile-view">
        <div className="detail-profile-common">
          <Card
            title={
              <Avatar
                size={{
                  xs: 48,
                  sm: 64,
                  md: 80,
                  lg: 128,
                  xl: 160,
                  xxl: 200,
                }}
                src={
                  user.imagePath ??
                  "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
                }
              />
            }
            bordered={false}
            className="common-info"
          ></Card>
        </div>
        <div className="detail-profile-detail">
          <div>
            <Descriptions
              title={"Thông tin người dùng"}
              labelStyle={{ fontStyle: "italic", fontWeight: "bold" }}
              layout={"vertical"}
            >
              <Descriptions.Item label="Tên">
                {user.firstName ?? ""}
              </Descriptions.Item>
              {/* <Descriptions.Item label="Địa chỉ chung">
                {user.address &&
                user.address.province &&
                user.address.district &&
                user.address.commune
                  ? user.address.street2
                    ? user.address.street2 +
                      "," +
                      user.address.commune +
                      "," +
                      user.address.district +
                      "," +
                      user.address.province
                    : user.address.commune +
                      "," +
                      user.address.district +
                      "," +
                      user.address.province
                  : ""}
              </Descriptions.Item>
              <Descriptions.Item label="Địa chỉ chi tiết">
                {user.address && user.address.address1
                  ? user.address.address1
                  : ""}
              </Descriptions.Item> */}
            </Descriptions>
          </div>
          <Divider />
          <div>
            <Descriptions
              title="Liên lạc"
              labelStyle={{ fontStyle: "italic", fontWeight: "bold" }}
              layout={"vertical"}
            >
              <Descriptions.Item label="Email">
                {user.email ?? ""}
              </Descriptions.Item>
              {/* <Descriptions.Item label="Điện thoại di động">
                {user.mobile ?? ""}
              </Descriptions.Item> */}
              {user.phone !== "" ? (
                <Descriptions.Item label="Điện thoại di động">
                  {user.phone}
                </Descriptions.Item>
              ) : null}
            </Descriptions>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailProfile;
