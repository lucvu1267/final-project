import {
  Button,
  Select,
  Descriptions,
  Divider,
  Form,
  Input,
  Cascader,
} from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./editProfile.scss";
import { useDispatch, useSelector } from "react-redux";
import UploadImage from "../../../components/common/uploadImage/UploadImage";
import { isEmpty, uploadImgCloudinary } from "../../../mixin/generate";
import {
  getInfoUser,
  setUser,
  updateProfile,
} from "../../contact/contactSlice";
import { toastCustom } from "../../../config/toast";
import { setLoading } from "../../../components/common/loading/loadingSlice";
import axios from "axios";
import MessError from "components/common/MessError";

const { Option } = Select;

function EditProfile(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.contactSlice.user);
  const [form] = Form.useForm();
  const [file, setFile] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);

  const [commonAddress, setCommonAddress] = useState([]);

  const getAllProvinces = () => {
    axios
      .get("https://provinces.open-api.vn/api/?depth=3")
      .then((response) => {
        const address = response.data.map((province: any) => {
          return (
            {
              value: province.name,
              label: province.name,
              children: province.districts.map((district: any) => {
                return (
                  {
                    value: district.name,
                    label: district.name,
                    children: district.wards.map((ward: any) => {
                      return (
                        {
                          value: ward.name,
                          label: ward.name,
                        } ?? {}
                      );
                    }),
                  } ?? {}
                );
              }),
            } ?? {}
          );
        });
        setCommonAddress(address);
      })
      .catch((error) => {
        toastCustom("error", "Không lấy được thông tin tỉnh thành!");
      });
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 80,
        }}
      >
        <Option value="+84">+84</Option>
      </Select>
    </Form.Item>
  );

  useEffect(() => {
    form.setFieldsValue({
      imagePath: user.imagePath ?? "",
      firstName: user.firstName ?? "",
      address1:
        user.address &&
        user.address.province &&
        user.address.district &&
        user.address.commune
          ? user.address.street2
            ? [
                user.address.province,
                user.address.district,
                user.address.commune,
                user.address.street2,
              ]
            : [
                user.address.province,
                user.address.district,
                user.address.commune,
              ]
          : [],
      address2:
        user.address && user.address.street1 ? user.address.street1 : "",
      email: user.email ?? "",
      mobile: user.phone ?? "",
      phone: user.phone ?? "",
    });
    getAllProvinces();
  }, [user]);

  const saveProfile = async (values: any) => {
    try {
      dispatch(setLoading(true));
      let fileUpload;
      if (file) {
        fileUpload = await uploadImgCloudinary(file);
      }
      const payload = {
        id: user.id,
        imagePath: fileUpload ?? user.imagePath,
        firstName: values.firstName ?? user.firstName,
        middleName: null,
        lastName: null,
        address: null,
        email: user.email,
        mobile: null,
        phone: values.phone ?? user.phone,
        websiteLink: null,
      };
      await updateProfile(payload);
      toastCustom("success", "Cập nhật tài khoản thành công");
      const data = await getInfoUser();
      dispatch(setUser(data.data));
      navigate("/profile");
    } catch (error) {
      toastCustom("error", "Cập nhật tài khoản thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  return (
    <Form
      className="edit-profile-container"
      form={form}
      onFinish={saveProfile}
      initialValues={{
        prefix: "+84",
      }}
      scrollToFirstError
    >
      <div className="edit-profile-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="edit-profile-save"
          htmlType="submit"
        >
          Lưu
        </Button>
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="edit-profile-cancel"
          onClick={() => navigate("/profile")}
        >
          Huỷ
        </Button>
      </div>
      <Divider />
      <div className="edit-profile-view">
        <div className="edit-profile-common">
          <Form.Item name="imagePath">
            <UploadImage
              imgUrlInit={user?.imagePath}
              setFile={setFile}
              isResetImageUrl={isResetImageUrl}
              setDefaultReset={() =>
                setIsResetImageUrl(!isEmpty(user.imagePath))
              }
            />
          </Form.Item>
        </div>
        <div className="edit-profile-detail">
          <div>
            <Descriptions
              title={"Thông tin người dùng"}
              labelStyle={{ fontStyle: "italic", fontWeight: "bold" }}
              layout={"vertical"}
            >
              <Descriptions.Item label="Tên">
                <Form.Item
                  name="firstName"
                  rules={[{ required: true, message: "Vui lòng nhập tên!" }]}
                >
                  <Input />
                </Form.Item>
              </Descriptions.Item>
              <Descriptions.Item label="Số điện thoại">
                <Form.Item
                  name="phone"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError message={"Vui lòng nhập số điện thoại!"} />
                      ),
                    },
                    {
                      min: 9,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số!"}
                        />
                      ),
                    },
                    {
                      max: 11,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số"}
                        />
                      ),
                    },
                    {
                      pattern:
                        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                      message: (
                        <MessError
                          message={"Số điện thoại phải bao gồm các chữ số!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Descriptions.Item>
            </Descriptions>
          </div>
        </div>
      </div>
    </Form>
  );
}

export default EditProfile;
