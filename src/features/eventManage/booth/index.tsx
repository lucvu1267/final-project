import { Dropdown, message, Modal, Row, Switch, Tabs, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import "./index.scss";
import ListBooth from "./ListBooth";
import ListLocation from "./ListLocation";
import ListOrderBooth from "./ListOrderBooth";
import {
  SettingFilled,
  PlusCircleFilled,
  LinkOutlined,
  CloseOutlined,
  UploadOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { TYPE_FORM } from "constants/index";
import {
  getBoothOrder,
  getListBooths,
  getListLocations,
  getMap,
  getStatusRenBooth,
  updateRenBoothStatus,
  uploadMap,
} from "../boothSlice";
import { useNavigate, useParams } from "react-router-dom";
import { setLoading } from "components/common/loading/loadingSlice";
import { useDispatch, useSelector } from "react-redux";
import { ROLE_MEMBER } from "constants/event";
import UploadImage from "components/common/uploadImage/UploadImage";
import { getMessStatus, uploadImgCloudinary } from "mixin/generate";
import { toastCustom } from "config/toast";
function Booth(props: any) {
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const [isOpenModalLocation, setIsOpenModalLocation] = useState(false);
  const [isOpenModalBooths, setIsOpenModalBooths] = useState(false);
  const [typeForm, setTypeForm] = useState(TYPE_FORM.CREATE);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isOpenModalMap, setIsOpenModalMap] = useState(false);
  const [isCallBackApi, setIsCallBackApi] = useState(false);
  const [listLocations, setListLocations] = useState<any>([]);
  const [listBooth, setListBooth] = useState<any>([]);
  const [listBoothOrder, setListBoothOrder] = useState<any>([]);
  const [file, setFile] = useState(null);
  const [initImgMap, setInitImgMap] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);
  const [isOpenRenBooth, setIsOpenRenBooth] = useState(false);
  const handleCloseModal = () => {
    setIsOpenModal(false);
  };
  const handleCloseModalMap = () => {
    setIsOpenModalMap(false);
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
              return message.error({
                type: "error",
                content: "Bạn không có quyền!",
              });
            } else if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            }
            setTypeForm(TYPE_FORM.CREATE);
            setIsOpenModalLocation(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm vị trí gian hàng
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
              return message.error({
                type: "error",
                content: "Bạn không có quyền!",
              });
            } else if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            }
            setTypeForm(TYPE_FORM.CREATE);
            setIsOpenModalBooths(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{
              fontSize: "14px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Thêm gian hàng
        </div>
      ),
    },
    {
      key: "3",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setIsOpenModalMap(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <EyeOutlined
            className="icon"
            style={{
              fontSize: "14px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Xem bản đồ sự kiện
        </div>
      ),
    },
    {
      key: "4",
      label: (
        <div
          style={{
            fontWeight: "600",
            color: "#285158",
            display: "flex",
            alignItems: "center",
          }}
        >
          Mở đăng ký:{" "}
          <Switch
            onChange={() => handleUpdateRenBooth()}
            disabled={
              roleUser.boothAccess === ROLE_MEMBER.VIEW ||
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            }
            style={{ marginLeft: "7px" }}
            size="small"
            defaultChecked={isOpenRenBooth}
          />
        </div>
      ),
    },
  ];

  const handleGetListLocation = async () => {
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    return await getListLocations(payload);
    //  setListLocations(data.data)
  };

  const handleGetListBooths = async () => {
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    return await getListBooths(payload);
    //  setListLocations(data.data)
  };

  const handleGetListBoothOrder = async () => {
    const payload = {
      boothId: null,
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    const data = await getBoothOrder(payload);
    setListBoothOrder(data.data.page.content);
    //  setListLocations(data.data)
  };

  const handleGetMap = async () => {
    const data = await getMap(idEvent);
    setInitImgMap(data.data);
  };

  const handleUploadMap = async () => {
    dispatch(setLoading(true));
    let urlImage;
    if (file) {
      urlImage = await uploadImgCloudinary(file);
    }
    const payload = {
      eventId: idEvent,
      mapPath: urlImage,
    };
    await uploadMap(payload);
    await handleGetMap();
    dispatch(setLoading(false));
    toastCustom("success", "Cập nhật bản đồ sự kiện thành công!");
  };

  const handleGetStatusRenBooth = async () => {
    const data = await getStatusRenBooth(idEvent);
    setIsOpenRenBooth(data.data === "ENABLE");
  };

  const handleUpdateRenBooth = async () => {
    const data = await updateRenBoothStatus(idEvent);
  };

  useEffect(() => {
    if (file) {
      handleUploadMap();
    }
  }, [file]);

  useEffect(() => {
    if (roleUser.boothAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission");
    } else {
      dispatch(setLoading(true));
      Promise.all([
        handleGetListLocation(),
        handleGetListBooths(),
        handleGetListBoothOrder(),
        handleGetStatusRenBooth(),
      ])
        .then((value) => {
          setListLocations(value[0].data.page.content);
          setListBooth(value[1].data.page.content);
          dispatch(setLoading(false));
        })
        .catch(() => dispatch(setLoading(false)));
      setIsCallBackApi(false);
    }
  }, [isCallBackApi]);

  useEffect(() => {
    handleGetMap();
  }, []);

  return (
    <>
      <Row className="wrapper-action">
        <Dropdown menu={{ items }} placement="bottomLeft" arrow>
          <SettingFilled className="icon" style={{ fontSize: "22px" }} />
        </Dropdown>
      </Row>
      <Tabs
        defaultActiveKey="1"
        type="card"
        // onChange={onChange}
        items={[
          {
            label: `Danh sách gian hàng`,
            key: "1",
            children: (
              <ListBooth
                listBooth={listBooth}
                listLocations={listLocations}
                setIsCallBackApi={setIsCallBackApi}
                isOpenModalBooths={isOpenModalBooths}
                setIsOpenModalBooths={setIsOpenModalBooths}
                typeForm={typeForm}
                setTypeForm={setTypeForm}
              />
            ),
          },
          {
            label: `Danh sách đặt gian hàng`,
            key: "2",
            children: (
              <ListOrderBooth
                listBoothOrder={listBoothOrder}
                listBooth={listBooth}
                handleGetListBoothOrder={handleGetListBoothOrder}
              />
            ),
          },
        ]}
      />
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "start",
          marginTop: "4rem",
        }}
      >
        <div style={{ width: "50%" }}>
          <Tabs
            defaultActiveKey="1"
            type="card"
            items={[
              {
                label: `Danh sách vị trí gian hàng`,
                key: "1",
                children: (
                  <ListLocation
                    listLocations={listLocations}
                    setIsCallBackApi={setIsCallBackApi}
                    isOpenModalLocation={isOpenModalLocation}
                    setIsOpenModalLocation={setIsOpenModalLocation}
                    typeForm={typeForm}
                    setTypeForm={setTypeForm}
                  />
                ),
              },
            ]}
          />
        </div>
      </div>
      <Modal
        width={400}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
        closeIcon={
          <CloseOutlined className="icon" style={{ fontSize: "18px" }} />
        }
      >
        <div className="description-gray" style={{ padding: "3rem 2rem" }}>
          {window.location.hostname === "localhost"
            ? "http://localhost:3008/booth/5"
            : `${process.env.REACT_APP_BASE_URL_FE}booth/5`}
        </div>
      </Modal>
      <Modal
        width={1300}
        open={isOpenModalMap}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModalMap}
        footer={null}
        closeIcon={
          <CloseOutlined className="icon" style={{ fontSize: "18px" }} />
        }
      >
        <div
          className="center-common"
          style={{ width: "100%", height: "100%", padding: "50px" }}
        >
          <div
            className="wrapper-map "
            style={{ width: "70vw", height: "80vh" }}
          >
            <Tooltip
              placement="top"
              title="Tải lên bản đồ sự kiện"
              arrowPointAtCenter
            >
              <UploadImage
                isDisable={roleUser.boothAccess === ROLE_MEMBER.VIEW}
                imgUrlInit={initImgMap}
                setFile={setFile}
                isResetImageUrl={isResetImageUrl}
                setDefaultReset={() => setIsResetImageUrl(false)}
              />
            </Tooltip>{" "}
          </div>
        </div>
      </Modal>
    </>
  );
}

export default Booth;
