import React, { useState } from "react";
import "./list-order-booth.scss";
import {
  InfoCircleOutlined,
  CloseOutlined,
  CheckOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { Divider, Drawer, Dropdown, message, Table, Tooltip } from "antd";
import moment from "moment";
import { FORMAT_DATE_FULL } from "constants/index";
import { useDispatch, useSelector } from "react-redux";
import { ROLE_MEMBER } from "constants/event";
import { getMessStatus, numberFormat } from "mixin/generate";
import { useParams } from "react-router-dom";
import { acceptBoothOrder, rejectBoothOrder } from "../boothSlice";
import { toastCustom } from "config/toast";
import { setLoading } from "components/common/loading/loadingSlice";

const STATUS = {
  WAITING: "WAITING",
  ACCEPT: "ACCEPT",
  REJECT: "REJECT",
};
function ListOrderBooth(props: any) {
  const { listBoothOrder, listBooth, handleGetListBoothOrder } = props;
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const [boothDetail, setBoothDetail] = useState<any>({});

  const handleGetBoothDetail = (idBooth: any) => {
    setBoothDetail(listBooth.find((item: any) => item.id === idBooth));
    setOpen(true);
  };

  const handleAccept = async (idBoothOrder: any) => {
    dispatch(setLoading(true));
    const data: any = await acceptBoothOrder(idEvent, idBoothOrder);
    if (data.errorCode === null) {
      await handleGetListBoothOrder();
      toastCustom("success", "Đã xác nhận");
    } else {
      toastCustom("error", "Gian hàng này đã được cho thuê!");
    }
    dispatch(setLoading(false));
  };

  const handleReject = async (idBoothOrder: any) => {
    dispatch(setLoading(true));
    await rejectBoothOrder(idEvent, idBoothOrder);
    await handleGetListBoothOrder();
    dispatch(setLoading(false));
    toastCustom("success", "Đã từ chối");
  };

  const getItem = (value: any) => {
    return {
      items: [
        {
          key: "1",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                } else {
                  handleAccept(value.id);
                }
                // setIsOpenModal(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <CheckOutlined
                className="icon"
                style={{ fontSize: "14px", marginRight: "5px" }}
              />{" "}
              Xác nhận
            </div>
          ),
        },
        {
          key: "2",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                } else {
                  handleReject(value.id);
                }
                // setIsOpenModal(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <CloseOutlined
                className="icon"
                style={{
                  fontSize: "14px",
                  marginRight: "5px",
                  fontWeight: "bolder",
                }}
              />{" "}
              Từ chối
            </div>
          ),
        },
      ],
    };
  };

  const columnsOrderTicket: any = [
    {
      title: "Tên",
      render: (value: any) => <div className="label-grayest">{value.name}</div>,
    },
    {
      title: "Ngày tạo",
      align: "center",
      render: (value: any) => (
        <div>{moment(value.createDate).format(FORMAT_DATE_FULL)}</div>
      ),
    },
    {
      title: "Số điện thoại",
      align: "center",
      render: (value: any) => <div>{value.phone}</div>,
    },
    {
      title: "Email",
      align: "center",
      render: (value: any) => <div>{value.email}</div>,
    },
    {
      title: "Gian hàng",
      align: "center",
      render: (value: any) => (
        <div className="label-grayest center-common">
          {value.booth.name}{" "}
          <Tooltip placement="top" title="Thông tin" arrowPointAtCenter>
            <InfoCircleOutlined
              className="icon"
              style={{ fontSize: "18px", marginLeft: "6px" }}
              onClick={() => handleGetBoothDetail(value.booth.id)}
            />
          </Tooltip>
        </div>
      ),
    },

    {
      title: "Trạng thái",
      align: "center",
      render: (value: any) => (
        <div className={`status--${getContentStatus(value.status)?.class}`}>
          <span>{getContentStatus(value.status)?.text}</span>
        </div>
      ),
    },
    {
      title: "Thao tác",
      align: "center",
      render: (value: any) =>
        value.status === "WAITING" && (
          <Dropdown menu={getItem(value)} placement="top" arrow>
            <SettingOutlined
              className="icon"
              style={{ fontSize: "18px", marginLeft: "6px" }}
            />
          </Dropdown>
        ),
    },
  ];

  const getContentStatus = (status: any) => {
    switch (status) {
      case STATUS.ACCEPT:
        return {
          class: "accept",
          text: "Đã xác nhận",
        };
      case STATUS.REJECT:
        return {
          class: "absent",
          text: "Đã từ chối",
        };
      case STATUS.WAITING:
        return {
          class: "attendence",
          text: "Đợi xác nhận",
        };
    }
  };

  const [open, setOpen] = useState(false);
  const onClose = () => {
    setOpen(false);
  };
  const showDrawer = (idContact: any) => {
    setOpen(true);
    // handleGetContactById(idContact);
  };

  return (
    <div>
      <Table
        className="table-common"
        style={{ width: "100vw" }}
        rowKey={(record: any) => record.id}
        columns={columnsOrderTicket}
        dataSource={listBoothOrder}
        pagination={{ position: ["bottomCenter"] }}
      />
      <Drawer
        title={"Thông tin gian hàng"}
        placement="right"
        onClose={onClose}
        open={open}
      >
        <div className="drawer-contact-detail">
          <div className="wrapper-detail">
            <div className="row-info field">
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{boothDetail?.name}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Ngày tạo: </span>
            </div>
            <div className="row-info infor">
              <span>
                {moment(boothDetail?.createDate).format(FORMAT_DATE_FULL)}
              </span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Giá: </span>
            </div>
            <div className="row-info infor">
              <span>{numberFormat(boothDetail?.rentFee)} vnđ</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Vị trí: </span>
            </div>
            <div className="row-info infor">
              <span style={{ overflow: "auto" }}>
                {boothDetail?.location?.name}
              </span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Thời gian thuê: </span>
            </div>
            <div className="row-info infor">
              <span style={{ overflow: "auto" }}>
                {moment(boothDetail.startRentDate).format(FORMAT_DATE_FULL)}{" "}
                {" - "}
                {moment(boothDetail.endRentDate).format(FORMAT_DATE_FULL)}
              </span>
            </div>
            <Divider className="divider" plain></Divider>
          </div>
        </div>
      </Drawer>
    </div>
  );
}

export default ListOrderBooth;
