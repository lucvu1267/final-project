import React, { useEffect, useState } from "react";
import "./list-booth.scss";
import {
  CheckCircleOutlined,
  EyeOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import {
  Col,
  Modal,
  Row,
  Table,
  Tooltip,
  Form,
  Input,
  Select,
  Button,
  Space,
  ConfigProvider,
  DatePicker,
  message,
} from "antd";
import { numberFormat } from "mixin/generate";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import MessError from "components/common/MessError";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { useParams } from "react-router-dom";
import { getListBoothPublic, getMap, registerBooth } from "../boothSlice";
import locale from "antd/es/locale/vi_VN";
import { toastCustom } from "config/toast";

const { RangePicker } = DatePicker;

function RegisterBooths(props: any) {
  const [form] = Form.useForm();
  const { idEventBooth } = useParams();
  const [initImgMap, setInitImgMap] = useState<any>(null);
  const [boothId, setBoothId] = useState<any>(null);

  const dispatch = useDispatch();

  const columnsOrderTicket: any = [
    {
      title: "Tên",
      render: (value: any) => <div className="label-grayest">{value.name}</div>,
    },
    {
      title: "Giá",
      render: (value: any) => (
        <div className="label-grayest">{numberFormat(value.rentFee)} vnđ</div>
      ),
    },
    {
      title: "Vị trí",
      render: (value: any) => (
        <div style={{ display: "flex", alignItems: "center" }}>
          <AddLocationIcon
            className="icon"
            style={{ fontSize: "22px", marginRight: "6px" }}
          />
          {value.location.name}
        </div>
      ),
    },

    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value: any) => (
        <div className="operator-wrapper">
          <Button
            disabled={value.isRegister}
            type="primary"
            ghost
            onClick={() => {
              setIsOpenModal(true);
              setBoothId(value.id);
            }}
          >
            Đăng ký
          </Button>
        </div>
      ),
    },
  ];

  const [isOpenModal, setIsOpenModal] = useState(false);
  const [listBooth, setListBooth] = useState<any>([]);
  const [isOpenModalMap, setIsOpenModalMap] = useState(false);

  const handleCloseModalMap = () => {
    setIsOpenModalMap(false);
  };

  const handleGetListBooth = async () => {
    dispatch(setLoading(true));
    const paload = {
      eventId: idEventBooth,
      orders: [],
      page: 0,
      size: 100,
    };
    const data = await getListBoothPublic(paload);
    setListBooth(data.data.page.content);
    dispatch(setLoading(false));
  };

  const handleGetMap = async () => {
    const data = await getMap(idEventBooth);
    setInitImgMap(data.data);
  };

  const handleRegisterBooth = async (value: any) => {
    dispatch(setLoading(true));
    const payload = {
      boothId: boothId,
      email: value.email,
      endRentDate: null,
      name: value.name,
      phone: value.phone,
      startRentDate: null,
    };
    const data: any = await registerBooth(payload);
    handleCloseModal();
    dispatch(setLoading(false));
    if (data.errorCode === null) {
      toastCustom("success", "Đăng ký thành công");
    } else {
      message.error("Đăng ký thất bại, mỗi email chỉ được đăng ký một lần!");
    }
  };

  useEffect(() => {
    handleGetListBooth();
    handleGetMap();
  }, []);

  const handleCloseModal = () => {
    setIsOpenModal(false);
    form.resetFields();
  };

  return (
    <div>
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "end",
          marginBottom: "2rem",
        }}
      >
        <Button
          type="primary"
          // ghost
          className="btn-primary"
          icon={<EyeOutlined style={{ fontSize: "20px" }} />}
          onClick={() => setIsOpenModalMap(true)}
        >
          Xem bản đồ sự kiện
        </Button>
      </div>
      <Table
        className="table-common"
        style={{ width: "100vw" }}
        rowKey={(record: any) => record.id}
        columns={columnsOrderTicket}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>
              <span className="label-grayest">Mô tả: </span>{" "}
              {record.description}
            </p>
          ),
          rowExpandable: (record) => record.name !== "Not Expandable",
        }}
        dataSource={listBooth}
        pagination={{ position: ["bottomCenter"] }}
      />
      <Modal
        width={1000}
        title={"Đăng ký gian hàng"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <Form
          form={form}
          onFinish={(value) => handleRegisterBooth(value)}
          autoComplete="off"
          style={{ width: "100%" }}
        >
          <Row justify="center" style={{ padding: "2rem" }}>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Tên<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="name"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập tên!"} />,
                    },
                    {
                      max: 255,
                      message: (
                        <MessError message={"Tên không được quá 255 kí tự!"} />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Tên" />
                </Form.Item>
              </div>
            </Col>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Email
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name={`email`}
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập email!"} />,
                    },
                    {
                      type: "email",
                      message: (
                        <MessError message={"Email không đúng định dạng!"} />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Email" />
                </Form.Item>
              </div>
            </Col>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Số điện thoại
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name={`phone`}
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError message={"Vui lòng nhập số điện thoại!"} />
                      ),
                    },
                    {
                      min: 9,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số!"}
                        />
                      ),
                    },
                    {
                      max: 11,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số"}
                        />
                      ),
                    },
                    {
                      pattern:
                        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                      message: (
                        <MessError
                          message={"Số điện thoại phải bao gồm các chữ số!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Số điện thoại:" />
                </Form.Item>
              </div>
            </Col>{" "}
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              {/* <div className="field-add" style={{ width: "90%" }}>
                <div className="label-field">
                  Thời gian thuê
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Space direction="vertical" size={12}>
                  <ConfigProvider locale={locale}>
                    <Form.Item
                      name="dateRental"
                      className="text-field"
                      rules={[
                        {
                          required: true,
                          message: (
                            <MessError
                              message={"Vui lòng chọn thời gian thuê!"}
                            />
                          ),
                        },
                      ]}
                    >
                      <RangePicker
                        showTime={{ format: "HH:mm" }}
                        format="DD/MM/YYYY HH:mm"
                      />
                    </Form.Item>
                  </ConfigProvider>
                </Space>
              </div> */}
            </Col>
          </Row>
        </Form>
      </Modal>

      <Modal
        width={1300}
        open={isOpenModalMap}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModalMap}
        footer={null}
        closeIcon={
          <CloseOutlined className="icon" style={{ fontSize: "18px" }} />
        }
      >
        <div
          className="center-common"
          style={{ width: "100%", height: "100%", padding: "50px" }}
        >
          <div
            className="wrapper-map "
            style={{ width: "70vw", height: "80vh" }}
          >
            <img
              src={initImgMap}
              alt="avatar"
              style={{ width: "100%", objectFit: "cover", height: "100%" }}
            />
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default RegisterBooths;
