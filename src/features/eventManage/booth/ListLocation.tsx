import React, { useEffect, useState } from "react";
import {
  DeleteOutlined,
  EditOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import {
  Col,
  Dropdown,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Row,
  Switch,
  Table,
  Tooltip,
} from "antd";
import moment from "moment";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import MessError from "components/common/MessError";
import TextArea from "antd/lib/input/TextArea";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { useParams } from "react-router-dom";
import { addLocation, deleteLocationById, updateLocation } from "../boothSlice";
import { toastCustom } from "config/toast";
import { ROLE_MEMBER } from "constants/event";
import { getMessStatus } from "mixin/generate";

function ListLocation(props: any) {
  const {
    isOpenModalLocation,
    setIsOpenModalLocation,
    listLocations,
    setIsCallBackApi,
    typeForm,
    setTypeForm,
  } = props;

  const [idCurrent, setIdCurrent] = useState(null);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const dispatch = useDispatch();
  const { idEvent } = useParams();
  const [form] = Form.useForm();

  const columnsOrderTicket: any = [
    {
      title: "Tên",
      render: (value: any) => (
        <div
          className="label-grayest"
          style={{ display: "flex", alignItems: "center" }}
        >
          {" "}
          <AddLocationIcon
            className="icon"
            style={{ fontSize: "22px", marginRight: "6px" }}
          />
          {value.name}
        </div>
      ),
    },
    {
      title: "Ngày tạo",
      align: "center",
      render: (value: any) => (
        <div>{moment(value.createDate).format(FORMAT_DATE_FULL)}</div>
      ),
    },
    {
      title: "Thao tác",
      align: "center",
      render: (value: any) => (
        <div className="center-common ">
          <Tooltip placement="top" title="Xóa" arrowPointAtCenter>
            <Popconfirm
              title="Bạn có chắc muốn xóa?"
              onConfirm={() => {
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleDelete(value.id);
              }}
              // onCancel={null}
              okText="Xóa"
              showCancel={false}
            >
              <DeleteOutlined
                className="icon-table"
                style={{ marginRight: "10px" }}
                // onClick={() => getTicketDetail(value.id)}
              />
            </Popconfirm>
          </Tooltip>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              className="icon-table"
              onClick={() => {
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setTypeForm(TYPE_FORM.UPDATE);
                handleGetLocationById(value.id);
                setIdCurrent(value.id);
              }}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  const handleSubmitForm = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        description: value.description,
        eventId: parseInt(idEvent + ""),
        name: value.name,
      };
      const payloadUpdate = {
        description: value.description,
        eventId: parseInt(idEvent + ""),
        id: parseInt(idCurrent + ""),
        name: value.name,
      };
      const data: any =
        typeForm === TYPE_FORM.CREATE
          ? await addLocation(payload)
          : await updateLocation(payloadUpdate);
      if (data.errorCode === null) {
        setIsCallBackApi(true);
        setIsOpenModalLocation(false);
        form.resetFields();
        toastCustom(
          "success",
          typeForm === TYPE_FORM.CREATE
            ? "Tạo vị trí thành công"
            : "Cập nhật thành công"
        );
      }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetLocationById = async (idLocation: any) => {
    try {
      const locationDetail = listLocations.find(
        (item: any) => item.id === idLocation
      );
      form.setFieldsValue({
        name: locationDetail.name,
        description: locationDetail.description,
      });
      setIsOpenModalLocation(true);
    } catch (error) {}
  };

  const handleDelete = async (idLocation: any) => {
    try {
      const data: any = await deleteLocationById(idEvent, idLocation);
      if (data.errorCode === null) {
        setIsCallBackApi(true);
        toastCustom("success", "Xóa thành công");
      } else {
        toastCustom("error", "Không thể xóa vì vị trí đã có gian hàng!");
      }
    } catch (error) {
      toastCustom("error", "Không thể xóa vì vị trí đã có gian hàng!");
    }
  };

  // useEffect(() => {
  //   setIsOpenModalLocation(isOpenModalLocation);
  // }, [typeForm]);

  const handleCloseModal = () => {
    form.resetFields();
    setIsOpenModalLocation(false);
    setTypeForm(TYPE_FORM.CREATE);
  };

  return (
    <div>
      <Table
        className="table-common"
        rowKey={(record: any) => record.id}
        columns={columnsOrderTicket}
        dataSource={listLocations}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>
              <span className="label-grayest">Mô tả: </span>{" "}
              {record.description}
            </p>
          ),
          rowExpandable: (record) => record.name !== "Not Expandable",
        }}
        pagination={{ position: ["bottomCenter"] }}
      />
      <Modal
        width={700}
        title={
          typeForm === TYPE_FORM.CREATE
            ? "Thêm vị trí gian hàng"
            : "Chỉnh sửa vị trí gian hàng"
        }
        open={isOpenModalLocation}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <div className="container-add-update-ticket">
          {" "}
          <Form
            form={form}
            onFinish={(value: any) => handleSubmitForm(value)}
            autoComplete="off"
            style={{ width: "100%" }}
          >
            <Row justify="center">
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={24}
                xl={24}
              >
                <div className="field-add" style={{ width: "100%" }}>
                  <div className="label-field">
                    Tên<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng nhập tên vị trí!"} />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={"Tên vị trí không được quá 255 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên vị trí" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={24}
                xl={24}
              >
                <div className="field-add" style={{ width: "100%" }}>
                  <div className="label-field">
                    Mô tả
                    <span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="description"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: <MessError message={"Vui lòng nhập mô tả!"} />,
                      },
                      {
                        max: 1000,
                        message: (
                          <MessError
                            message={"Mô tả không được quá 1000 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <TextArea
                      placeholder="Mô tả"
                      className="description"
                      // onChange={getDataPost}
                    ></TextArea>
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row></Row>
          </Form>
        </div>
      </Modal>
    </div>
  );
}

export default ListLocation;
