import React, { useEffect, useState } from "react";
import "./list-booth.scss";
import {
  EditOutlined,
  CheckCircleOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import {
  Col,
  Modal,
  Row,
  Switch,
  Table,
  Tooltip,
  Form,
  Input,
  Select,
  Popconfirm,
  message,
  Space,
  ConfigProvider,
  DatePicker,
} from "antd";
import moment from "moment";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import { getMessStatus, numberFormat } from "mixin/generate";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import MessError from "components/common/MessError";
import TextArea from "antd/lib/input/TextArea";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { setLoading } from "components/common/loading/loadingSlice";
import { toastCustom } from "config/toast";
import { createBooth, deleteBooth, updateBooth } from "../boothSlice";
import { ROLE_MEMBER } from "constants/event";
import locale from "antd/es/locale/vi_VN";
const { Option } = Select;
const { RangePicker } = DatePicker;
function ListBooth(props: any) {
  const {
    isOpenModalBooths,
    setIsOpenModalBooths,
    listBooth,
    setIsCallBackApi,
    listLocations,
    typeForm,
    setTypeForm,
  } = props;
  const [form] = Form.useForm();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const dispatch = useDispatch();
  const { idEvent } = useParams();

  const columnsOrderTicket: any = [
    {
      title: "Tên",
      render: (value: any) => <div className="label-grayest">{value.name}</div>,
    },
    {
      title: "Ngày tạo",
      render: (value: any) => (
        <div>{moment(value.createDate).format(FORMAT_DATE_FULL)}</div>
      ),
    },
    {
      title: "Giá",
      render: (value: any) => (
        <div className="label-grayest">{numberFormat(value.rentFee)} vnđ</div>
      ),
    },
    {
      title: "Vị trí",
      render: (value: any) => (
        <div style={{ display: "flex", alignItems: "center" }}>
          <AddLocationIcon
            className="icon"
            style={{ fontSize: "22px", marginRight: "6px" }}
          />
          {value.location.name}
        </div>
      ),
    },
    {
      title: "Thời gian cho thuê",
      render: (value: any) => (
        <div>
          {moment(value.startRentDate).format(FORMAT_DATE_FULL)} {" - "}
          {moment(value.endRentDate).format(FORMAT_DATE_FULL)}
        </div>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "",
      key: "y",
      render: (value: any) => (
        <div>
          <Tooltip placement="top" title="Đang hoạt động" arrowPointAtCenter>
            <Switch
              disabled={
                roleUser.boothAccess === ROLE_MEMBER.VIEW ||
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL" ||
                value.isRented
              }
              defaultChecked={value.status === "ACTIVE"}
              onChange={(isActive) => handleUpdateStatus(value, isActive)}
            />
          </Tooltip>
        </div>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value: any) => (
        <div className="operator-wrapper">
          <Tooltip placement="top" title="Xóa" arrowPointAtCenter>
            <Popconfirm
              title="Bạn có chắc muốn xóa?"
              onConfirm={() => {
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                } else if (value.isRented) {
                  return message.open({
                    type: "error",
                    content: "Gian hàng này đã có người thuê!",
                  });
                }
                handleDelete(value.id);
              }}
              okText="Xóa"
              showCancel={false}
            >
              <DeleteOutlined
                className="icon-table"
                style={{ marginRight: "5px" }}
                // onClick={() => getTicketDetail(value.id)}
              />
            </Popconfirm>
          </Tooltip>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              className="icon-table"
              onClick={() => {
                if (roleUser.boothAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                } else if (value.isRented) {
                  return message.open({
                    type: "error",
                    content: "Gian hàng này đã có người thuê!",
                  });
                }
                setTypeForm(TYPE_FORM.UPDATE);
                handleGetBoothById(value.id);
                setIdCurrent(value.id);
              }}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  // const [isOpenModal, setIsOpenModal] = useState(isOpenModalBooths);
  const [idCurrent, setIdCurrent] = useState(null);

  const handleGetBoothById = async (idBooth: any) => {
    try {
      const boothDetail = listBooth.find((item: any) => item.id === idBooth);
      form.setFieldsValue({
        name: boothDetail.name,
        description: boothDetail.description,
        price: boothDetail.rentFee,
        location: boothDetail.location.id,
        time: [
          moment(boothDetail.startRentDate),
          moment(boothDetail.endRentDate),
        ],
      });
      setIsOpenModalBooths(true);
    } catch (error) {}
  };

  const handleUpdateStatus = async (value: any, isActive: any) => {
    const payloadUpdate = {
      description: value.description,
      eventId: parseInt(idEvent + ""),
      id: parseInt(value.id + ""),
      name: value.name,
      rentFee: parseInt((value.rentFee + "").split(".").join("")),
      locationId: value.location.id,
      status: isActive ? "ACTIVE" : "NOT_ACTIVE",
      startRentDate: value.startRentDate,
      endRentDate: value.endRentDate,
    };
    await updateBooth(payloadUpdate);
  };

  const handleSubmitForm = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        description: value.description,
        eventId: parseInt(idEvent + ""),
        name: value.name,
        rentFee: parseInt((value.price + "").split(".").join("")),
        locationId: value.location,
        startRentDate: value.time[0]._d,
        endRentDate: value.time[1]._d,
      };
      const payloadUpdate = {
        description: value.description,
        eventId: parseInt(idEvent + ""),
        id: parseInt(idCurrent + ""),
        name: value.name,
        rentFee: parseInt((value.price + "").split(".").join("")),
        locationId: value.location,
        status: "ACTIVE",
        startRentDate: value.time[0]._d,
        endRentDate: value.time[1]._d,
      };
      const data: any =
        typeForm === TYPE_FORM.CREATE
          ? await createBooth(payload)
          : await updateBooth(payloadUpdate);
      if (data.errorCode === null) {
        setIsCallBackApi(true);
        setIsOpenModalBooths(false);
        form.resetFields();
        toastCustom(
          "success",
          typeForm === TYPE_FORM.CREATE
            ? "Tạo gian hàng thành công"
            : "Cập nhật thành công"
        );
        setTypeForm(TYPE_FORM.CREATE);
      }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleDelete = async (idBooth: any) => {
    try {
      const data: any = await deleteBooth(idEvent, idBooth);
      if (data.errorCode === null) {
        setIsCallBackApi(true);
        toastCustom("success", "Xóa thành công");
      } else {
        toastCustom("error", "Gian hàng này đã được thuê nên không thể xóa!");
      }
    } catch (error) {
      toastCustom("error", "Gian hàng này đã được thuê nên không thể xóa!");
    }
  };
  // useEffect(() => {
  //   setIsOpenModal(isOpenModalBooths);
  // }, [isOpenModalBooths]);

  const handleCloseModal = () => {
    form.resetFields();
    setIsOpenModalBooths(false);
    setTypeForm(TYPE_FORM.CREATE);
  };

  return (
    <div>
      <Table
        className="table-common"
        style={{ width: "100vw" }}
        rowKey={(record: any) => record.id}
        columns={columnsOrderTicket}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>
              <span className="label-grayest">Mô tả: </span>{" "}
              {record.description}
            </p>
          ),
          rowExpandable: (record) => record.name !== "Not Expandable",
        }}
        dataSource={listBooth}
        pagination={{ position: ["bottomCenter"] }}
      />
      <Modal
        width={1000}
        title={
          typeForm === TYPE_FORM.CREATE
            ? "Thêm gian hàng"
            : "Chỉnh sửa thông tin gian hàng"
        }
        open={isOpenModalBooths}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => {
                form.submit();
              }}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <div className="container-add-update-ticket">
          {" "}
          <Form
            form={form}
            onFinish={(value) => handleSubmitForm(value)}
            autoComplete="off"
            style={{ width: "100%" }}
          >
            <Row justify="center">
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Tên<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng nhập tên gian hàng!"} />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={"Tên gian hàng không được quá 255 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên gian hàng" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Thời gian cho thuê
                    <span style={{ color: "red" }}> *</span>
                  </div>
                  <Space direction="vertical" size={12}>
                    <ConfigProvider locale={locale}>
                      <Form.Item
                        name="time"
                        className="text-field"
                        rules={[
                          {
                            required: true,
                            message: (
                              <MessError
                                message={"Vui lòng chọn thời gian hoàn thành!"}
                              />
                            ),
                          },
                        ]}
                      >
                        <RangePicker
                          showTime={{ format: "HH:mm" }}
                          format="DD/MM/YYYY HH:mm"
                        />
                      </Form.Item>
                    </ConfigProvider>
                  </Space>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Giá thuê gian hàng<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="price"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: <MessError message={"Vui lòng nhập giá!"} />,
                      },
                      {
                        pattern: /^[0-9.,]+$/,
                        message: (
                          <MessError message={"Giá phải bao gồm các chữ số"} />
                        ),
                      },
                    ]}
                  >
                    <Input
                      disabled={typeForm === TYPE_FORM.UPDATE}
                      onInput={(e: any) => {
                        if (
                          e.target.value &&
                          !isNaN(e.target.value.split(".").join(""))
                        ) {
                          e.target.value =
                            e.target.value &&
                            !isNaN(e.target.value.split(".").join("")) &&
                            numberFormat(e.target.value.split(".").join(""));
                        } else {
                          e.target.value = e.target.value.substring(
                            0, e.target.value.length - 1
                          );
                        }
                      }}
                      className="input"
                      placeholder="Giá thuê gian hàng"
                    />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Vị trí<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="location"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng chọn vị trí!"} />
                        ),
                      },
                    ]}
                  >
                    <Select
                      disabled={typeForm === TYPE_FORM.UPDATE}
                      className="select"
                      placeholder="Chọn vị trí"
                      // optionFilterProp="children"
                    >
                      {listLocations &&
                        listLocations.map(
                          (itemLocation: any, idxLocation: any) => (
                            <Option key={idxLocation} value={itemLocation.id}>
                              {itemLocation.name}
                            </Option>
                          )
                        )}
                    </Select>
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={24}
                xl={24}
              >
                <div className="field-add" style={{ width: "90%" }}>
                  <div className="label-field">
                    Mô tả
                    <span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="description"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: <MessError message={"Vui lòng nhập mô tả!"} />,
                      },
                      {
                        max: 1000,
                        message: (
                          <MessError
                            message={"Mô tả không được quá 1000 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <TextArea
                      placeholder="Mô tả"
                      className="description"
                      // onChange={getDataPost}
                    ></TextArea>
                  </Form.Item>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    </div>
  );
}

export default ListBooth;
