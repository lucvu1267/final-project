import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api

export const getListParticipant = async (payload: any) => {
  return await axiosInstance.post(`participant/search`, payload);
};

export const confirmJoinEvent = async (eventId: any, id: any) => {
  return await axiosInstance.post(
    `participant/confirm-joined/${eventId}/${id}`
  );
};

export const downloadListParticipant = async (eventId: any) => {
  return await axiosInstance.post(`participant/get-file/${eventId}`);
};

export const inviteParticipant = async (payload: any) => {
  return await axiosInstance.post(`participant/invite-participant`, payload);
};

export const paticipantSlice = createSlice({
  name: "loading",
  initialState: {},
  reducers: {
    // setLoading: (state, action) => {
    //   state.isLoading = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = paticipantSlice.actions;

export default paticipantSlice.reducer;
