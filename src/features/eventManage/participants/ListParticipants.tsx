import {
  Button,
  Cascader,
  Col,
  Dropdown,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Row,
  Select,
  Switch,
  Table,
  Tooltip,
} from "antd";
import React, { useEffect, useState } from "react";
import "./list-participants.scss";
import {
  CheckOutlined,
  DownloadOutlined,
  SettingFilled,
  TagsOutlined,
  PlusCircleFilled,
  CheckCircleOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import Search from "antd/lib/input/Search";
import MessError from "components/common/MessError";
import { useDispatch, useSelector } from "react-redux";
import { ROLE_MEMBER } from "constants/event";
import { useNavigate, useParams } from "react-router-dom";
import {
  confirmJoinEvent,
  getListParticipant,
  inviteParticipant,
} from "../paticipantSlice";
import { setLoading } from "components/common/loading/loadingSlice";
import { FORMAT_DATE_FULL, JWT_AUTH } from "constants/index";
import moment from "moment";
import axios from "axios";
import { getListTicketsByEvent } from "../eventManageSlice";
import { toastCustom } from "config/toast";
import { getMessStatus } from "mixin/generate";

const { Option } = Select;
const STATUS = {
  WAITING: "WAITING",
  ACCEPT: "ACCEPT",
};

function ListParticipants(props: any) {
  const [form] = Form.useForm();
  const [formFilter] = Form.useForm();
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [listTicket, setListTicket] = useState([]);
  const [listParticipants, setListParticipants] = useState([]);
  const [messageApi, contextHolder] = message.useMessage();
  const [keySearchPaticipant, setKeySearchPaticipant] = useState(null);
  const [ticketSelected, setTicketSelected] = useState(null);

  const navigate = useNavigate();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const handleResetFilter = async () => {
    formFilter.resetFields();
    setKeySearchPaticipant(null);
    setTicketSelected(null);
    handleGetListPaticipant();
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            handleResetFilter();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <FilterOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Xóa bộ lọc
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            if (roleUser.participantAccess === ROLE_MEMBER.VIEW) {
              return message.error({
                type: "error",
                content: "Bạn không có quyền!",
              });
            } else if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            } else {
              setIsOpenModal(true);
            }
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm người tham dự
        </div>
      ),
    },

    {
      key: "3",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            downloadXLSFile();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <DownloadOutlined
            className="icon"
            style={{
              fontSize: "14px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Tải danh sách người tham dự
        </div>
      ),
    },
  ];

  const columnsOrderTicket: any = [
    {
      title: "Ngày mở đăng ký",
      render: (value: any) => (
        <span>{moment(value.addedDate).format(FORMAT_DATE_FULL)}</span>
      ),
      sorter: (a: any, b: any) =>
        moment(a.addedDate).unix() - moment(b.addedDate).unix(),
    },
    {
      title: "Tên người tham dự",
      render: (value: any) => (
        <span className="label-grayest">{value.name}</span>
      ),
    },
    { title: "Email", dataIndex: "email", key: "email" },
    { title: "Số điện thoại", dataIndex: "phone", key: "phone" },
    {
      title: "Loại vé",
      render: (value: any) => (
        <>
          <TagsOutlined style={{ fontSize: "18px", marginRight: "10px" }} />
          <span className="label-grayest">{value.ticket.name}</span>
        </>
      ),
    },
    {
      title: "Trạng thái",
      key: "status",
      align: "center",
      render: (value: any) => (
        <div
          className={`label--${
            value.status === STATUS.ACCEPT ? "accept" : "attendence"
          }`}
        >
          <span>
            {" "}
            {value.status === STATUS.ACCEPT ? "Đã tham gia" : "Chưa tham gia"}
          </span>
        </div>
      ),
    },
    {
      title: "Thao tác",
      key: "x",
      align: "center",
      render: (value: any) => (
        <span
          hidden={value.status === STATUS.ACCEPT}
          style={{
            cursor: "pointer",
          }}
        >
          <Tooltip
            placement="top"
            title="Đánh dấu là đã tham gia"
            arrowPointAtCenter
          >
            <Popconfirm
              title="Xác nhận người này đã tham gia sự kiện?"
              onConfirm={() => {
                if (roleUser.participantAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                } else {
                  handleConfirnInvited(value.id);
                }
              }}
              okText="Xác nhận"
              showCancel={false}
            >
              <Button
                type="primary"
                ghost
                icon={
                  <CheckOutlined
                    className="icon-table"
                    style={{
                      fontSize: "13px",
                      color: "#0d656d",
                    }}
                  />
                }
                style={{ fontWeight: "600" }}
              >
                Đã tham gia
              </Button>
            </Popconfirm>
          </Tooltip>
        </span>
      ),
    },
  ];

  const downloadXLSFile = async () => {
    const req = await axios({
      method: "post",
      url: `${process.env.REACT_APP_BASE_URL}participant/get-file/${idEvent}`,
      responseType: "blob",
      headers: {
        Authorization: `Bearer ${localStorage.getItem(JWT_AUTH)}`,
      },
    });
    var blob = new Blob([req.data], {
      type: req.headers["content-type"],
    });
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.download = `EMS_${idEvent}_${new Date().getTime()}.xlsx`;
    link.click();
  };

  const handleInviteParticipant = async (value: any) => {
    try {
      const paload = {
        email: value.name,
        eventId: idEvent,
        name: value.email,
        phone: value.phone,
        ticketId: value.ticket,
      };
      await inviteParticipant(paload);

      handleGetListPaticipant();
      setIsOpenModal(false);
      form.resetFields();
      toastCustom("success", "Thêm người tham dự thành công");
    } catch (error) {
    } finally {
    }
  };

  const handleGetListPaticipant = async (
    keySearchPaticipant = null,
    ticketSelected = null
  ) => {
    dispatch(setLoading(true));
    try {
      const payload = {
        eventId: idEvent,
        ticketId: ticketSelected,
        keySearch: keySearchPaticipant,
        orders: [],
        page: 0,
        size: 100,
      };
      const data: any = await getListParticipant(payload);
      setListParticipants(data.data.page.content);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleConfirnInvited = async (idParticipant: any) => {
    dispatch(setLoading(true));
    await confirmJoinEvent(idEvent, idParticipant);
    handleGetListPaticipant();
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const handleGetListTicket = async () => {
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    const data = await getListTicketsByEvent(payload);
    setListTicket(data.data.ticketPage.content);
  };

  const handleSearchByTicket = async (ticketId: any) => {
    setTicketSelected(ticketId);
    handleGetListPaticipant(keySearchPaticipant, ticketId);
  };

  const handleSearchKey = async (value: any) => {
    await handleGetListPaticipant(keySearchPaticipant, ticketSelected);
  };

  useEffect(() => {
    if (roleUser.participantAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission");
    } else {
      handleGetListPaticipant();
      handleGetListTicket();
    }
  }, []);

  return (
    <>
      {contextHolder}
      <Form form={formFilter} autoComplete="off" className="wrapper-action">
        <Form.Item name="name">
          <Search
            className="search search-40px"
            placeholder="Tìm kiếm theo tên"
            enterButton
            allowClear={false}
            size="large"
            onChange={(e: any) => setKeySearchPaticipant(e.target.value)}
            onSearch={handleSearchKey}
          />
        </Form.Item>
        <Form.Item name="ticket">
          <Select
            className="select"
            showSearch={false}
            placeholder="Tìm kiếm theo vé"
            notFoundContent="Không có dữ liệu"
            optionFilterProp="label"
            optionLabelProp="label"
            onChange={(value) => handleSearchByTicket(value)}
            filterOption={false}
          >
            {listTicket.map((item: any, idx: any) => (
              <Option key={idx} label={item.name} value={item.id}>
                {" "}
                {item.name}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="name">
          <Dropdown menu={{ items }} placement="bottomLeft" arrow>
            <SettingFilled className="icon" style={{ fontSize: "22px" }} />
          </Dropdown>
        </Form.Item>
      </Form>
      <div className="container-list-participants">
        <Table
          className="table-post"
          columns={columnsOrderTicket}
          rowKey={(item: any) => item.id}
          // size="large"
          // expandable={{
          //   expandedRowRender: (record) => (
          //     <p style={{ margin: 0 }}>
          //       <span className="label-grayest">Địa chỉ: </span>{" "}
          //       {record.description}
          //     </p>
          //   ),
          //   rowExpandable: (record) => record.name !== "Not Expandable",
          // }}
          dataSource={listParticipants}
          pagination={{ position: ["bottomCenter"] }}
        />
      </div>
      <Modal
        width={1000}
        title={"Thêm người tham gia"}
        open={isOpenModal}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              className="icon"
              onClick={() => form.submit()}
            />
          </Tooltip>,
        ]}
      >
        <Form
          form={form}
          onFinish={(value) => handleInviteParticipant(value)}
          autoComplete="off"
          style={{ width: "100%" }}
        >
          <Row justify="center" style={{ padding: "2rem" }}>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Tên<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="name"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập tên!"} />,
                    },
                    {
                      max: 255,
                      message: (
                        <MessError message={"Tên không được quá 255 kí tự!"} />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Tên" />
                </Form.Item>
              </div>
            </Col>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Email
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name={`email`}
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng nhập email!"} />,
                    },
                    {
                      type: "email",
                      message: (
                        <MessError message={"Email không đúng định dạng!"} />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Email" />
                </Form.Item>
              </div>
            </Col>
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add">
                <div className="label-field">
                  Số điện thoại
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name={`phone`}
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError message={"Vui lòng nhập số điện thoại!"} />
                      ),
                    },
                    {
                      min: 9,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số!"}
                        />
                      ),
                    },
                    {
                      max: 11,
                      message: (
                        <MessError
                          message={"Số điện thoại phải từ 9 đến 11 số"}
                        />
                      ),
                    },
                    {
                      pattern:
                        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                      message: (
                        <MessError
                          message={"Số điện thoại phải bao gồm các chữ số!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Input className="input" placeholder="Số điện thoại:" />
                </Form.Item>
              </div>
            </Col>{" "}
            <Col
              className="wrapper-field-common"
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
            >
              <div className="field-add" style={{ width: "90%" }}>
                <div className="label-field">
                  Loại vé
                  <span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="ticket"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng chọn loại vé!"} />,
                    },
                  ]}
                >
                  <Select
                    className="select"
                    showSearch={false}
                    placeholder="Chọn vé"
                    notFoundContent="Không có dữ liệu"
                    optionFilterProp="label"
                    optionLabelProp="label"
                    filterOption={false}
                  >
                    {listTicket.map((item: any, idx: any) => (
                      <Option key={idx} label={item.name} value={item.id}>
                        {item.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
}

export default ListParticipants;
