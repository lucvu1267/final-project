import { Col, Form, message, Radio, Row, Select, Table, Tooltip } from "antd";
import MessError from "components/common/MessError";
import React, { useState, useEffect } from "react";
import "./add-member-to-event.scss";
import { QuestionCircleOutlined } from "@ant-design/icons";
import {
  ROLE_EVENT,
  ROLE_MEMBER,
  TYPE_MODAL_MEMBER_EVENT,
} from "constants/event";
import { CheckCircleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import {
  addMember,
  getAllMember,
  getAllMemberRequest,
  getListEventJoined,
  getMemberToAddEvent,
  updateRoleMember,
} from "features/eventManage/eventManageSlice";
import { toastCustom } from "config/toast";
import { useParams } from "react-router-dom";
import useDebounce from "hook/useDebounce";
import OptionItem from "components/common/optionItem/OptionItem";
import OptionEventItem from "components/common/optionItem/OptionEventItem";
import { getMessStatus } from "mixin/generate";
const { Option } = Select;

const ROLE = {
  SUB_HEADER: "SUB_HEADER",
  MEMBER: "MEMBER",
};

const TYPE_PERMISSION = {
  NONE: "NONE",
  VIEW: "VIEW",
  EDIT: "EDIT",
};

function AddMemberToEvent(props: any) {
  const {
    typeModal,
    setIsOpenModal,
    setIsCallBackApi,
    dataPermission,
    userContactId,
    isRequestAgain = false,
  } = props;
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Column, ColumnGroup } = Table;

  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const [keySearchUser, setKeySearchUser] = useState(null);
  const [role, setRole] = useState(ROLE.MEMBER);
  const [userId, setUserId] = useState(null);
  const [isLoadingDropdown, setIsLoadingDropdown] = useState(false);
  const [roleTicket, setRoleTicket] = useState(TYPE_PERMISSION.NONE);
  const [roleSponsor, setRoleSponsor] = useState(TYPE_PERMISSION.NONE);
  const [roleOrganizing, setRoleOrganizing] = useState(TYPE_PERMISSION.NONE);
  const [roleBooth, setRoleBooth] = useState(TYPE_PERMISSION.NONE);
  const [rolePost, setRolePost] = useState(TYPE_PERMISSION.NONE);
  const [rolePaticipan, setRolePaticipan] = useState(TYPE_PERMISSION.NONE);
  const [roleAgenda, setRoleAgenda] = useState(TYPE_PERMISSION.NONE);
  const [listUser, setListUser] = useState([]);
  const [listEvent, setListEvent] = useState([]);

  const debouncedSearch = useDebounce(keySearchUser, 1000);

  const data: any[] = [
    {
      key: "1",
      firstName: (
        <Radio.Group
          onChange={(e: any) => {
            setRoleTicket(e.target.value);
          }}
          value={roleTicket}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),
      age: "Vé",
    },
    {
      key: "2",
      firstName: (
        <Radio.Group
          onChange={(e) => {
            setRoleBooth(e.target.value);
          }}
          value={roleBooth}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),

      age: "Gian hàng",
    },
    // {
    //   key: "3",
    //   firstName: (
    //     <Radio.Group
    //       onChange={(e) => {
    //         setRoleOrganizing(e.target.value);
    //       }}
    //       value={roleOrganizing}
    //     >
    //       <Radio value={TYPE_PERMISSION.NONE}>
    //         {" "}
    //         <div style={{ fontWeight: "600", color: "#285158" }}>
    //           Không quyền{" "}
    //           <Tooltip
    //             placement="top"
    //             title="Chỉ có thể xem thông tin."
    //             arrowPointAtCenter
    //           >
    //             <QuestionCircleOutlined
    //               className="icon"
    //               style={{ fontSize: "13px", marginLeft: "5px" }}
    //             />
    //           </Tooltip>
    //         </div>
    //       </Radio>
    //       <Radio value={TYPE_PERMISSION.VIEW}>
    //         {" "}
    //         <div style={{ fontWeight: "600", color: "#285158" }}>
    //           Chỉ xem{" "}
    //           <Tooltip
    //             placement="top"
    //             title="Chỉ có thể xem thông tin."
    //             arrowPointAtCenter
    //           >
    //             <QuestionCircleOutlined
    //               className="icon"
    //               style={{ fontSize: "13px", marginLeft: "5px" }}
    //             />
    //           </Tooltip>
    //         </div>
    //       </Radio>
    //       <Radio value={TYPE_PERMISSION.EDIT}>
    //         {" "}
    //         <div style={{ fontWeight: "600", color: "#285158" }}>
    //           Tất cả quyền{" "}
    //           <Tooltip
    //             placement="top"
    //             title="Chỉ có thể xem thông tin."
    //             arrowPointAtCenter
    //           >
    //             <QuestionCircleOutlined
    //               className="icon"
    //               style={{ fontSize: "13px", marginLeft: "5px" }}
    //             />
    //           </Tooltip>
    //         </div>
    //       </Radio>
    //     </Radio.Group>
    //   ),
    //   age: "Ban tổ chức",
    // },
    {
      key: "4",
      firstName: (
        <Radio.Group
          onChange={(e) => {
            setRoleSponsor(e.target.value);
          }}
          value={roleSponsor}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),
      age: "Nhà tài trợ",
    },
    {
      key: "5",
      firstName: (
        <Radio.Group
          onChange={(e) => {
            setRolePost(e.target.value);
          }}
          value={rolePost}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),
      age: "Bài viết",
    },
    {
      key: "6",
      firstName: (
        <Radio.Group
          onChange={(e) => {
            setRolePaticipan(e.target.value);
          }}
          value={rolePaticipan}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),
      age: "Người tham dự",
    },
    {
      key: "7",
      firstName: (
        <Radio.Group
          onChange={(e) => {
            setRoleAgenda(e.target.value);
          }}
          value={roleAgenda}
        >
          <Radio value={TYPE_PERMISSION.NONE}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Không quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.VIEW}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Chỉ xem{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
          <Radio value={TYPE_PERMISSION.EDIT}>
            {" "}
            <div style={{ fontWeight: "600", color: "#285158" }}>
              Tất cả quyền{" "}
              <Tooltip
                placement="top"
                title="Chỉ có thể xem thông tin."
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "13px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          </Radio>
        </Radio.Group>
      ),
      age: "Lịch trình",
    },
  ];

  const onChangeSelectUser = (e: any) => {
    setUserId(e);
  };

  const handleAddMember = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        agendaAccess: roleAgenda,
        boothAccess: roleBooth,
        eventId: idEvent ? parseInt(idEvent) : value.event,
        isSubHeader: role === ROLE.SUB_HEADER,
        memberId: isRequestAgain
          ? dataPermission.member.id
          : userContactId
          ? userContactId
          : userId,
        organizationAccess:
          role === ROLE.MEMBER ? TYPE_PERMISSION.VIEW : TYPE_PERMISSION.EDIT,
        participantAccess: rolePaticipan,
        postAccess: rolePost,
        sponsorAccess: roleSponsor,
        ticketAccess: roleTicket,
      };
      const payloadUpdate = {
        agendaAccess: roleAgenda,
        boothAccess: roleBooth,
        eventId: idEvent ? parseInt(idEvent) : value.event,
        id: dataPermission?.id,
        isSubHeader: role === ROLE.SUB_HEADER,
        organizationAccess:
          role === ROLE.MEMBER ? TYPE_PERMISSION.VIEW : TYPE_PERMISSION.EDIT,
        participantAccess: rolePaticipan,
        postAccess: rolePost,
        sponsorAccess: roleSponsor,
        ticketAccess: roleTicket,
      };
      if (
        typeModal === TYPE_MODAL_MEMBER_EVENT.ADD ||
        typeModal === TYPE_MODAL_MEMBER_EVENT.ADD_CONTACT ||
        isRequestAgain
      ) {
        await addMember(payload);
      } else if (typeModal === TYPE_MODAL_MEMBER_EVENT.UPDATE) {
        await updateRoleMember(payloadUpdate);
      }
      setIsOpenModal(false);
      if (idEvent) {
        setIsCallBackApi(true);
      }
      if (
        typeModal === TYPE_MODAL_MEMBER_EVENT.ADD ||
        typeModal === TYPE_MODAL_MEMBER_EVENT.ADD_CONTACT
      ) {
        toastCustom(
          "success",
          idEvent ? "Thêm thành viên thành công" : "Mời thành công"
        );
      } else if (typeModal === TYPE_MODAL_MEMBER_EVENT.UPDATE) {
        toastCustom("success", "Cập nhật thành công");
      }
    } catch (error) {
      console.log(error);
      // toastCustom("error", "Thêm thành viên thất bại!");
    } finally {
      dispatch(setLoading(false));
      resetData();
    }
  };

  const handleDebounceSearchUser = async () => {
    const payload = {
      eventId: idEvent && parseInt(idEvent),
      keyword: keySearchUser,
    };
    try {
      setIsLoadingDropdown(true);
      const data = await getMemberToAddEvent(payload);
      setListUser(data.data);
    } catch (error) {
    } finally {
      setIsLoadingDropdown(false);
    }
  };

  const handleChooseRole = (e: any) => {
    setRole(e.target.value);
    if (e.target.value === ROLE.SUB_HEADER) {
      setRoleTicket(TYPE_PERMISSION.EDIT);
      setRoleSponsor(TYPE_PERMISSION.EDIT);
      setRoleOrganizing(TYPE_PERMISSION.EDIT);
      setRoleBooth(TYPE_PERMISSION.EDIT);
      setRolePost(TYPE_PERMISSION.EDIT);
      setRolePaticipan(TYPE_PERMISSION.EDIT);
      setRoleAgenda(TYPE_PERMISSION.EDIT);
    } else {
      setRoleTicket(TYPE_PERMISSION.NONE);
      setRoleSponsor(TYPE_PERMISSION.NONE);
      setRoleOrganizing(TYPE_PERMISSION.NONE);
      setRoleBooth(TYPE_PERMISSION.NONE);
      setRolePost(TYPE_PERMISSION.NONE);
      setRolePaticipan(TYPE_PERMISSION.NONE);
      setRoleAgenda(TYPE_PERMISSION.NONE);
    }
  };

  const resetData = () => {
    setKeySearchUser(null);
    setListUser([]);
    setListEvent([]);
  };

  useEffect(() => {
    // search the api
    if (debouncedSearch) handleDebounceSearchUser();
  }, [debouncedSearch]);

  useEffect(() => {
    // search the api
    if (dataPermission) {
      setRoleTicket(dataPermission.ticketAccess);
      setRoleSponsor(dataPermission.sponsorAccess);
      setRoleOrganizing(dataPermission.organizationAccess);
      setRoleBooth(dataPermission.boothAccess);
      setRolePost(dataPermission.postAccess);
      setRolePaticipan(dataPermission.participantAccess);
      setRoleAgenda(dataPermission.agendaAccess);
      setRole(dataPermission.eventRole);
    }
  }, [dataPermission]);

  const handleGetListEventJoined = async () => {
    const data = await getListEventJoined();
    setListEvent(data.data);
  };

  useEffect(() => {
    if (typeModal === TYPE_MODAL_MEMBER_EVENT.ADD_CONTACT) {
      handleGetListEventJoined();
    }
  }, []);

  useEffect(() => {
    if (
      roleTicket === TYPE_PERMISSION.EDIT &&
      roleSponsor === TYPE_PERMISSION.EDIT &&
      // roleOrganizing === TYPE_PERMISSION.EDIT &&
      roleBooth === TYPE_PERMISSION.EDIT &&
      rolePost === TYPE_PERMISSION.EDIT &&
      rolePaticipan === TYPE_PERMISSION.EDIT &&
      roleAgenda === TYPE_PERMISSION.EDIT
    ) {
      setRole(ROLE.SUB_HEADER);
    } else {
      setRole(ROLE.MEMBER);
    }
  }, [
    roleTicket,
    roleSponsor,
    // roleOrganizing,
    roleBooth,
    rolePost,
    rolePaticipan,
    roleAgenda,
  ]);

  return (
    <>
      <Row className="container-modal-add-member">
        <Col
          hidden={
            typeModal === TYPE_MODAL_MEMBER_EVENT.VIEW ||
            typeModal === TYPE_MODAL_MEMBER_EVENT.UPDATE
          }
          className="wrapper-field"
          span={24}
        >
          <Form
            form={form}
            onFinish={(value) => handleAddMember(value)}
            autoComplete="off"
            style={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <div className="field-add">
              <div className="label-field">
                {typeModal === TYPE_MODAL_MEMBER_EVENT.ADD
                  ? "Tên tài khoản"
                  : "Sự kiện"}
                <span style={{ color: "red" }}> *</span>
              </div>
              {typeModal === TYPE_MODAL_MEMBER_EVENT.ADD && (
                <Form.Item
                  name="user"
                  className="text-field"
                  rules={[
                    {
                      required: typeModal === TYPE_MODAL_MEMBER_EVENT.ADD,
                      message: (
                        <MessError message={"Vui lòng chọn tài khoản!"} />
                      ),
                    },
                  ]}
                >
                  <Select
                    className="select"
                    showSearch
                    placeholder="Tìm kiếm theo tên tài khoản"
                    notFoundContent="Không có dữ liệu"
                    optionLabelProp="label"
                    onChange={(e) => {
                      onChangeSelectUser(e);
                    }}
                    onSearch={(value: any) => {
                      setKeySearchUser(value?.trim());
                    }}
                    loading={isLoadingDropdown}
                    filterOption={false}
                  >
                    {listUser &&
                      listUser.map(
                        (item: any, idx) =>
                          (item.statusInEvent === null ||
                            item.statusInEvent === "REJECT") && (
                            <Option
                              key={idx}
                              value={item.id}
                              label={item.emailAccount}
                              // disabled={item.status === "WAITING"}
                            >
                              <OptionItem
                                status={
                                  item.statusInEvent === "REJECT" ? 2 : null
                                }
                                mail={item.emailAccount}
                                name={item.firstName}
                                image={item.imagePath}
                              />
                            </Option>
                          )
                      )}
                  </Select>
                </Form.Item>
              )}
              {typeModal === TYPE_MODAL_MEMBER_EVENT.ADD_CONTACT && (
                <Form.Item
                  name="event"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: <MessError message={"Vui lòng chọn sự kiện!"} />,
                    },
                    {
                      validator: async (_, value) => {
                        await Promise.all([
                          getAllMember(value),
                          getAllMemberRequest(value),
                        ]).then((value) => {
                          if (
                            value[0].data.some(
                              (item: any) => item.member.id === userContactId
                            )
                          ) {
                            return Promise.reject(
                              "Người này đã ở trong sự kiện!"
                            );
                          } else if (
                            value[1].data.some(
                              (item: any) => item.member.id === userContactId
                            )
                          ) {
                            return Promise.reject(
                              "Người dùng đã được mời vào sự kiện này!"
                            );
                          } else {
                            return Promise.resolve();
                          }
                        });
                        // const data = await getAllMember(value);
                        // if (
                        //   data.data.some(
                        //     (item: any) => item.member.id === userContactId
                        //   )
                        // ) {
                        //   // return Promise.resolve();
                        //   Promise.reject(
                        //     "Người này đã ở trong sự kiện này!"
                        //   )
                        // } else {
                        //   return Promise.reject(
                        //     "Người này đã ở trong sự kiện này!"
                        //   );
                        // }
                      },
                    },
                  ]}
                >
                  <Select
                    className="select"
                    showSearch
                    placeholder="Tìm kiếm theo tên sự kiện"
                    notFoundContent="Không có dữ liệu"
                    optionLabelProp="label"
                    optionFilterProp="label"
                  >
                    {listEvent &&
                      listEvent.map((item: any, idx) => (
                        <Option key={idx} value={item.id} label={item.name}>
                          <OptionEventItem
                            code={item.code}
                            image={item.logoPath}
                            name={item.name}
                          />
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              )}
            </div>
          </Form>
        </Col>
        <Col span={24}>
          <Radio.Group
            onChange={(e: any) => {
              handleChooseRole(e);
            }}
            value={role}
            className={`radio-group ${
              (typeModal === TYPE_MODAL_MEMBER_EVENT.VIEW ||
                roleUser.organizationAccess === ROLE_MEMBER.VIEW ||
                dataPermission?.member?.id === user.id) &&
              "table-disable"
            }`}
          >
            <Radio value={ROLE.MEMBER}>
              <span className="label-grayest">
                Thành viên{" "}
                <Tooltip
                  placement="top"
                  title="Có thể phân quyền cho thành viên"
                  arrowPointAtCenter
                >
                  <QuestionCircleOutlined
                    className="icon"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  />
                </Tooltip>
              </span>
            </Radio>
            <Radio value={ROLE.SUB_HEADER}>
              <span className="label-grayest">
                Quản trị viên{" "}
                <Tooltip
                  placement="top"
                  title="Quản trị viên sẽ có mọi quyền trong sự kiện"
                  arrowPointAtCenter
                >
                  <QuestionCircleOutlined
                    className="icon"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  />
                </Tooltip>
              </span>
            </Radio>
          </Radio.Group>
        </Col>
        <Col span={24}>
          <Table
            className={`${
              (typeModal === TYPE_MODAL_MEMBER_EVENT.VIEW ||
                roleUser.organizationAccess === ROLE_MEMBER.VIEW ||
                dataPermission?.member?.id === user.id) &&
              "table-disable"
            }`}
            dataSource={data}
            bordered={true}
            pagination={false}
            rowClassName={`table-row-color ${
              (typeModal === TYPE_MODAL_MEMBER_EVENT.VIEW ||
                roleUser.organizationAccess === ROLE_MEMBER.VIEW ||
                dataPermission?.member?.id === user.id) &&
              "table-disable"
            }`}
            rowKey={(record: any) => record.key}
          >
            <Column
              align="center"
              title="Chức năng"
              dataIndex="age"
              key="age"
            />
            <Column
              align="center"
              title="Phân quyền"
              dataIndex="firstName"
              key="firstName"
            />
            {/* <ColumnGroup title="Phân quyền">
              <Column
                align="center"
                title={
                  <div>
                    Chỉ xem{" "}
                    <Tooltip
                      placement="top"
                      title="Chỉ có thể xem thông tin."
                      arrowPointAtCenter
                    >
                      <QuestionCircleOutlined
                        className="icon"
                        style={{ fontSize: "15px", marginLeft: "5px" }}
                      />
                    </Tooltip>
                  </div>
                }
                dataIndex="firstName"
                key="firstName"
              />
              <Column
                align="center"
                title={
                  <div>
                    Tất cả{" "}
                    <Tooltip
                      placement="top"
                      title="Có mọi quyền bao gồm cả xem, thêm, sửa, xóa(nếu có)."
                      arrowPointAtCenter
                    >
                      <QuestionCircleOutlined
                        className="icon"
                        style={{ fontSize: "15px", marginLeft: "5px" }}
                      />
                    </Tooltip>
                  </div>
                }
                dataIndex="lastName"
                key="lastName"
              />
            </ColumnGroup> */}
          </Table>
        </Col>
      </Row>
      <Row
        hidden={
          typeModal === TYPE_MODAL_MEMBER_EVENT.VIEW ||
          roleUser.organizationAccess === ROLE_MEMBER.VIEW ||
          dataPermission?.member?.id === user.id
        }
        justify={"center"}
        style={{ paddingBottom: "2rem" }}
      >
        <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
          <CheckCircleOutlined
            onClick={() => {
              if (
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              ) {
                return message.open({
                  type: "error",
                  content: getMessStatus(roleUser?.event?.status),
                });
              }
              form.submit();
            }}
            className="icon"
          />
        </Tooltip>
      </Row>
    </>
  );
}

export default AddMemberToEvent;
