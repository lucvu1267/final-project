import "./list-team.scss";
import {
  Avatar,
  Button,
  Col,
  message,
  Modal,
  Popover,
  Row,
  Tooltip,
} from "antd";
import HeaderFeature from "components/common/HeaderFeature";
import {
  InfoCircleOutlined,
  PlusOutlined,
  FormOutlined,
} from "@ant-design/icons";
import {
  TeamOutlined,
  ReconciliationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { useState, useEffect } from "react";
import { TYPE_FORM } from "constants/index";
import AddAndUpdateTeam from "../addAndUpdate/AddAndUpdateTeam";
import { useNavigate, useParams } from "react-router-dom";
import { setLoading } from "components/common/loading/loadingSlice";
import { getTeamByEvent } from "features/eventManage/eventManageSlice";
import { useDispatch, useSelector } from "react-redux";
import EmptyData from "components/common/emptyData/EmptyData";
import { ROLE_MEMBER } from "constants/event";
import { getMessStatus } from "mixin/generate";

function ListTeam(props: any) {
  const { idEvent } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [typeForm, setTypeForm] = useState(TYPE_FORM.CREATE);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isSubmitForm, setIsSubmitForm] = useState(false);
  const [isCallBackApi, setIsCallBackApi] = useState(false);
  const [listTeam, setListTeam] = useState([]);
  const [teamDetail, setTeamDetail] = useState({});

  const user = useSelector((state: any) => state.contactSlice.user);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const handleOpenModal = (typeForm: any) => {
    setTypeForm(typeForm);
    setIsOpenModal(true);
  };

  const handleGetListteam = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getTeamByEvent(idEvent);
      setListTeam(data.data);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  useEffect(() => {
    handleGetListteam();
    setIsCallBackApi(false);
  }, [isCallBackApi]);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  return (
    <>
      <Row className="wrapper-action">
        <Tooltip placement="top" title="Tạo nhóm" arrowPointAtCenter>
          <Button
            type="primary"
            className="add-contact btn-primary"
            icon={<PlusOutlined />}
            onClick={() => {
              if (roleUser.organizationAccess === ROLE_MEMBER.VIEW) {
                return message.error({
                  type: "error",
                  content: "Bạn không có quyền!",
                });
              } else if (
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              ) {
                return message.open({
                  type: "error",
                  content: getMessStatus(roleUser?.event?.status),
                });
              } else {
                handleOpenModal(TYPE_FORM.CREATE);
              }
            }}
          ></Button>
        </Tooltip>
      </Row>
      <div className="container-list-team">
        <HeaderFeature content={"Danh sách nhóm"} />

        <div className="wrapper-list-team">
          <div className="team-over-view">
            {listTeam &&
              listTeam.map((item: any, idx) => (
                <div
                  key={idx}
                  className="wrapper-animation-team"
                  onClick={() =>
                    navigate(`/event-manage/${idEvent}/team/${item.id}`)
                  }
                >
                  <Row justify={"space-between"} className="team-item">
                    <Col className="avatar-team" span={5}>
                      <Avatar
                        style={{ width: "100px", height: "100px" }}
                        shape="square"
                        size="large"
                        src={item.imagePath}
                      />
                    </Col>
                    <Col className="info-team" span={18}>
                      <div className="title">{item.name}</div>
                      <p className="description">{item.description}</p>
                      <div className="wrapper-more-content">
                        <div className="content">
                          <Tooltip
                            placement="top"
                            title="Trưởng nhóm"
                            arrowPointAtCenter
                          >
                            <UserOutlined className="icon-gray" />
                          </Tooltip>
                          <span
                            className="label-grayest"
                            style={{ marginLeft: "0.7rem" }}
                          >
                            {" "}
                            {item.creator.firstName}
                          </span>
                        </div>
                        <div className="content">
                          <Tooltip
                            placement="top"
                            title="Thành viên"
                            arrowPointAtCenter
                          >
                            <TeamOutlined className="icon-gray" />
                          </Tooltip>
                          <span
                            className="label-grayest"
                            style={{ marginLeft: "0.7rem" }}
                          >
                            {item.noMembers}
                          </span>
                        </div>
                      </div>
                    </Col>
                    <Col span={1}>
                      <div className="wrapper-action">
                        {user.emailAccount === item.creator.emailAccount && (
                          <Tooltip
                            placement="top"
                            title="Chỉnh sửa"
                            arrowPointAtCenter
                          >
                            <FormOutlined
                              className="icon"
                              style={{ fontSize: "20px" }}
                              onClick={(e) => {
                                e.stopPropagation();
                                if (
                                  roleUser?.event?.status === "DONE" ||
                                  roleUser?.event?.status === "CANCEL"
                                ) {
                                  return message.open({
                                    type: "error",
                                    content: getMessStatus(
                                      roleUser?.event?.status
                                    ),
                                  });
                                }
                                setTypeForm(TYPE_FORM.UPDATE);
                                setTeamDetail(item);
                                setIsOpenModal(true);
                              }}
                            />
                          </Tooltip>
                        )}
                      </div>
                    </Col>
                  </Row>
                </div>
              ))}
          </div>{" "}
          {listTeam.length === 0 && <EmptyData />}
        </div>
      </div>
      {/* <ListMemberEvent /> */}
      <Modal
        width={1000}
        title={
          typeForm === TYPE_FORM.CREATE
            ? "Tạo nhóm mới"
            : "Chỉnh sửa thông tin nhóm"
        }
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddAndUpdateTeam
          dataDetail={teamDetail}
          typeForm={typeForm}
          setIsCallBackApi={setIsCallBackApi}
          setIsSubmitForm={setIsSubmitForm}
          isSubmitForm={isSubmitForm}
          setIsOpenModal={setIsOpenModal}
        />
      </Modal>
    </>
  );
}

export default ListTeam;
