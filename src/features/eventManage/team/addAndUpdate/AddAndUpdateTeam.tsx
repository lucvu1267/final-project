import React, { useEffect, useState } from "react";
import { Col, Form, Input, Row, Tooltip } from "antd";
import "./add-update-team.scss";
import MessError from "components/common/MessError";
import TextArea from "antd/lib/input/TextArea";
import UploadImage from "components/common/uploadImage/UploadImage";
import { addTeam, updateTeam } from "features/eventManage/eventManageSlice";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { toastCustom } from "config/toast";
import { CheckCircleOutlined } from "@ant-design/icons";
import { isEmpty, uploadImgCloudinary } from "mixin/generate";
import { TYPE_FORM } from "constants/index";

function AddAndUpdateTeam(props: any) {
  const { idEvent } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const {
    isSubmitForm,
    setIsSubmitForm,
    setIsOpenModal,
    dataDetail,
    typeForm,
    setIsCallBackApi,
  } = props;

  const [isResetImageUrl, setIsResetImageUrl] = useState(false);
  const [file, setFile] = useState(null);

  useEffect(() => {
    if (isSubmitForm) {
      form.submit();
      setIsSubmitForm(false);
    }
  }, [isSubmitForm]);

  const handleSubmitForm = async (value: any) => {
    try {
      dispatch(setLoading(true));
      let urlImage;
      if (file) {
        urlImage = await uploadImgCloudinary(file);
      }
      const payload = {
        contactType: "TEAM",
        description: value.description,
        email: null,
        eventId: idEvent,
        firstName: value.name,
        imagePath: urlImage,
        mobile: null,
        phone: null,
        websiteLink: null,
      };

      const payloadUpdate = {
        id: dataDetail.id,
        description: value.description,
        email: null,
        eventId: idEvent,
        firstName: value.name,
        imagePath: isEmpty(file) ? dataDetail.imagePath : urlImage,
        mobile: null,
        phone: null,
        websiteLink: null,
      };
      if (typeForm === TYPE_FORM.CREATE) {
        await addTeam(payload);
      } else {
        await updateTeam(payloadUpdate);
      }
      setIsCallBackApi(true);
      setIsOpenModal(false);
      toastCustom(
        "success",
        typeForm === TYPE_FORM.CREATE
          ? "Tạo nhóm thành công"
          : "Cập nhật thành công"
      );
    } catch (e) {
      toastCustom(
        "error",
        typeForm === TYPE_FORM.CREATE
          ? "Tạo nhóm thất bại!"
          : "Cập nhật thất bại!"
      );
    } finally {
      dispatch(setLoading(false));
    }
  };

  return (
    <div className="wrapper-modal-create-update-team">
      <Row justify="center" style={{ marginBottom: "1rem" }}>
        <Tooltip placement="top" title="Thêm ảnh cho nhóm" arrowPointAtCenter>
          <div>
            <UploadImage
              imgUrlInit={
                typeForm === TYPE_FORM.UPDATE && dataDetail?.imagePath
              }
              setFile={setFile}
              isResetImageUrl={isResetImageUrl}
              setDefaultReset={() => setIsResetImageUrl(false)}
            />
          </div>
        </Tooltip>
      </Row>
      <Form
        form={form}
        onFinish={(value) => handleSubmitForm(value)}
        autoComplete="off"
        style={{ width: "100%" }}
        initialValues={
          typeForm === TYPE_FORM.UPDATE
            ? {
                name: dataDetail?.name,
                description: dataDetail?.description,
              }
            : {}
        }
      >
        <Row justify="center">
          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={24}
            lg={24}
            xl={24}
          >
            <div className="field-add">
              <div className="label-field">
                Tên nhóm<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="name"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: <MessError message={"Vui lòng nhập tên nhóm!"} />,
                  },
                  {
                    max: 255,
                    message: (
                      <MessError
                        message={"Tên nhóm không được quá 255 kí tự!"}
                      />
                    ),
                  },
                ]}
              >
                <Input className="input" placeholder="Tên nhóm" />
              </Form.Item>
            </div>
          </Col>

          <Col
            className="wrapper-field"
            xs={24}
            sm={24}
            md={24}
            lg={24}
            xl={24}
          >
            <div className="field-add">
              <div className="label-field">
                Mô tả<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="description"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: <MessError message={"Vui lòng nhập mô tả!"} />,
                  },
                  {
                    max: 1000,
                    message: (
                      <MessError message={"Mô tả không được quá 1000 kí tự!"} />
                    ),
                  },
                ]}
              >
                <TextArea
                  placeholder="Mô tả"
                  className="description"
                  // onChange={getDataPost}
                ></TextArea>
              </Form.Item>
            </div>
          </Col>
        </Row>
      </Form>
      <Row justify={"center"} style={{ paddingBottom: "2rem" }}>
        <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
          <CheckCircleOutlined onClick={() => form.submit()} className="icon" />
        </Tooltip>
      </Row>
    </div>
  );
}

export default AddAndUpdateTeam;
