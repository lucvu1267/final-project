import {  Tabs } from "antd";
import { ROLE_MEMBER } from "constants/event";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ListMemberEvent from "./listMemberEvent/ListMemberEvent";
import ListTeam from "./listTeam/ListTeam";

function Index(props: any) {
  const navigate = useNavigate();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  useEffect(()=>{
    if (roleUser.organizationAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission")
    }
  },[])
  return (
    
    <Tabs
      className="container-ticket"
      defaultActiveKey="1"
      type="card"
      // onChange={onChange}
      items={[
        {
          label: `Danh sách thành viên`,
          key: "1",
          children: <ListMemberEvent />,
        },
        {
          label: `Danh sách nhóm`,
          key: "2",
          children: <ListTeam />,
        },
      ]}
    />
  );
}

export default Index;
