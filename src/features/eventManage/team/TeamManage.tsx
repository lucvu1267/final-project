import { Button, Col, Form, message, Modal, Row, Select, Tooltip } from "antd";
import ListMember from "components/team/ListMember";
import ListPostTeam from "components/team/ListPostTeam";
import React, { useEffect, useState } from "react";
import "./team-manager.scss";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import {
  addMemberIntoTeam,
  getTeamById,
  searchUserToAdd,
} from "../eventManageSlice";
import { CheckCircleOutlined } from "@ant-design/icons";
import MessError from "components/common/MessError";
import useDebounce from "hook/useDebounce";
import OptionItem from "components/common/optionItem/OptionItem";
import { toastCustom } from "config/toast";
import { ROLE_MEMBER } from "constants/event";
import { getMessStatus } from "mixin/generate";

const { Option } = Select;

function TeamManage(props: any) {
  const { idEvent, idTeam } = useParams();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const user = useSelector((state: any) => state.contactSlice.user);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [keySearchUser, setKeySearchUser] = useState(null);
  const [isLoadingDropdown, setIsLoadingDropdown] = useState(false);
  const [listUser, setListUser] = useState([]);
  const [teamDetail, setTeamDetail] = useState<any>([]);
  // const [userId, setUserId] = useState(null);

  const debouncedSearch = useDebounce(keySearchUser, 1000);

  const navigate = useNavigate();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const handleCloseModal = () => {
    setIsOpenModal(false);
    resetData();
  };

  const checkRoleUserAuthen = () => {
    return teamDetail?.members?.find((item: any) => item.member.id === user.id)
      ?.teamRole;
  };

  const handleGetTeam = async () => {
    const data = await getTeamById(idTeam);
    setTeamDetail(data.data);
  };

  const handleDebounceSearchUser = async () => {
    try {
      setIsLoadingDropdown(true);
      const payload = {
        keyword: keySearchUser,
        teamId: idTeam,
      };
      const data = await searchUserToAdd(payload);
      setListUser(data.data);
    } catch (error) {
    } finally {
      setIsLoadingDropdown(false);
    }
  };

  const handleAddMemberIntoEvent = async (value: any) => {
    await addMemberIntoTeam(idTeam, value.idUser);
    handleGetTeam();
    setIsOpenModal(false);
    toastCustom("success", "Thêm thành công");
    resetData();
  };

  const resetData = () => {
    form.resetFields();
    setListUser([]);
    setKeySearchUser(null);
  };

  useEffect(() => {
    if (roleUser.organizationAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission");
    } else {
      handleGetTeam();
    }
  }, []);

  useEffect(() => {
    // search the api
    if (debouncedSearch) handleDebounceSearchUser();
  }, [debouncedSearch]);

  return (
    <>
      {checkRoleUserAuthen() === "LEADER" && (
        <Row className="wrapper-action">
          <Col className="add-action-team-member" span={24}>
            <div
              onClick={() => {
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setIsOpenModal(true);
              }}
            >
              Thêm thành viên{" "}
            </div>
          </Col>
        </Row>
      )}
      <div style={{ width: "100%" }}>
        <Row justify="space-between" className="container-team">
          <Col span={16} className="wrapper-post">
            <ListPostTeam teamDetail={teamDetail} />
          </Col>
          <Col span={7} className="wrapper-list-member">
            <ListMember teamDetail={teamDetail} handleGetTeam={handleGetTeam} />
          </Col>
        </Row>
      </div>
      <Modal
        width={600}
        title={"Thêm thành viên"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <Form
          form={form}
          onFinish={(value) => handleAddMemberIntoEvent(value)}
          autoComplete="off"
          style={{ width: "100%" }}
        >
          <Row justify="center" style={{ padding: "3rem" }}>
            <Col className="wrapper-field-common" span={21}>
              <div className="field-add" style={{ width: "100%" }}>
                <div className="label-field">
                  Thành viên<span style={{ color: "red" }}> *</span>
                </div>
                <Form.Item
                  name="idUser"
                  className="text-field"
                  rules={[
                    {
                      required: true,
                      message: (
                        <MessError
                          message={"Vui lòng chọn người muốn thêm vào nhóm!"}
                        />
                      ),
                    },
                  ]}
                >
                  <Select
                    className="select"
                    showSearch
                    placeholder="Tìm kiếm theo tên tài khoản"
                    notFoundContent="Không có dữ liệu"
                    optionLabelProp="label"
                    // onChange={(e) => {
                    //   onChangeSelectUser(e);
                    // }}
                    onSearch={(value: any) => {
                      setKeySearchUser(value?.trim());
                    }}
                    loading={isLoadingDropdown}
                    filterOption={false}
                  >
                    {listUser &&
                      listUser.map((item: any, idx) => (
                        <Option
                          key={idx}
                          value={item.id}
                          label={item.emailAccount}
                        >
                          <OptionItem
                            mail={item.emailAccount}
                            name={item.firstName}
                            image={item.imagePath}
                          />
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              </div>
            </Col>
            <Col className="center-common" span={3}>
              <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
                <CheckCircleOutlined
                  className="icon"
                  style={{ fontSize: "27px", marginTop: "5px" }}
                  onClick={() => form.submit()}
                />
              </Tooltip>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
}

export default TeamManage;
