import { Avatar, Card, Tooltip, Dropdown, Modal, message } from "antd";
import profileDefault from "assets/images/profileDefault.png";
import "./member-cancel.scss";
import { useState } from "react";
import {
  EllipsisOutlined,
  UserOutlined,
  CloseCircleOutlined,
  RedoOutlined,
} from "@ant-design/icons";
import { getMessStatus, isEmpty } from "mixin/generate";
import { useSelector } from "react-redux";
import AddMemberToEvent from "../../modal/AddMemberToEvent";
import { TYPE_MODAL_MEMBER_EVENT } from "constants/event";
const { Meta } = Card;
function MemberCancel(props: any) {
  const { contact, setIsCallBackApi } = props;
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setTypeModal(TYPE_MODAL_MEMBER_EVENT.VIEW);
            setIsOpenModal(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <UserOutlined
            className="icon"
            style={{ fontSize: "16px", marginRight: "5px" }}
          />{" "}
          Thông tin phân quyền
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            }
            setTypeModal(TYPE_MODAL_MEMBER_EVENT.UPDATE);
            setIsOpenModal(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <RedoOutlined
            className="icon"
            style={{
              fontSize: "16px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Gửi lại lời mời
        </div>
      ),
    },
    {
      key: "3",
      label: (
        <div
          //   onClick={handleLogout}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <CloseCircleOutlined
            className="icon"
            style={{
              fontSize: "16px",
              marginRight: "5px",
              fontWeight: "bolder",
            }}
          />{" "}
          Xóa lời mời
        </div>
      ),
    },
  ];

  // const navigate = useNavigate();
  // const dispatch = useDispatch();
  // const [open, setOpen] = useState(false);
  // const [contactDetail, setContactDetail] = useState<any>({});
  const [typeModal, setTypeModal] = useState<any>(
    TYPE_MODAL_MEMBER_EVENT.UPDATE
  );

  const [isOpenModal, setIsOpenModal] = useState(false);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  // const showDrawer = (idContact: any) => {
  //   setOpen(true);
  //   handleGetContactById(idContact);
  // };

  // const onClose = () => {
  //   setOpen(false);
  // };

  // const clickNavigate = (e: any) => {
  //   navigate("/contact/detail/" + props.id);
  // };

  // const handleGetContactById = async (idContact: any) => {
  //   try {
  //     // dispatch(setLoading(true));
  //     const data = await getContactId(idContact);
  //     setContactDetail(data.data);
  //   } catch (error) {
  //   } finally {
  //     // dispatch(setLoading(false));
  //   }
  // };

  return (
    <>
      <Card
        // onClick={() => showDrawer(contact.id)}
        className="card-item"
        title={contact.member.firstName}
        extra={
          <Dropdown menu={{ items }} placement="top" arrow>
            <EllipsisOutlined
              className="icon"
              style={{ fontSize: "22px" }}
              onClick={(e) => e.stopPropagation()}
            />
          </Dropdown>
        }
        hoverable
      >
        {/* <Skeleton loading={true} avatar active> */}
        <Meta
          avatar={
            <Avatar
              src={
                isEmpty(contact.member.imagePath)
                  ? profileDefault
                  : contact.member.imagePath
              }
            />
          }
          title={<div>{contact.member.emailAccount}</div>}
          description={
            <Tooltip placement="top" title="Số điện thoại" arrowPointAtCenter>
              {/* <span>{contact.phone}</span> */}
            </Tooltip>
          }
        />
        {/* </Skeleton> */}
      </Card>
      <Modal
        width={1000}
        title={"Thông tin phân quyền"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddMemberToEvent
          typeModal={typeModal}
          dataPermission={contact}
          isRequestAgain={typeModal === TYPE_MODAL_MEMBER_EVENT.UPDATE}
          setIsOpenModal={setIsOpenModal}
          setIsCallBackApi={setIsCallBackApi}
        />
      </Modal>
    </>
  );
}

export default MemberCancel;
