import {
  Avatar,
  Card,
  Drawer,
  Row,
  Divider,
  Dropdown,
  Modal,
  message,
} from "antd";
import profileDefault from "assets/images/profileDefault.png";
import { useNavigate, useParams } from "react-router-dom";
import { useState } from "react";
import {
  EllipsisOutlined,
  UserOutlined,
  CloseCircleOutlined,
} from "@ant-design/icons";
import { getMessStatus, isEmpty } from "mixin/generate";
import { useDispatch, useSelector } from "react-redux";
import { getContactId } from "features/contact/contactSlice";
import AddMemberToEvent from "../../modal/AddMemberToEvent";
import { TYPE_MODAL_MEMBER_EVENT } from "constants/event";
import moment from "moment";
import { toastCustom } from "config/toast";
import { deleteMember } from "features/eventManage/eventManageSlice";
import { FORMAT_DATE_FULL } from "constants/index";
const { Meta } = Card;
function MemberRequset(props: any) {
  const { contact, setIsCallBackApi } = props;
  const { idEvent } = useParams();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const handleDeleteEvent = async (value: any) => {
    await deleteMember(idEvent, value.member.id);
    setIsCallBackApi();
    toastCustom("success", `Đã hủy lời mời!`);
  };

  const getItem = (value: any) => {
    return {
      items: [
        {
          key: "1",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setIsOpenModal(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <UserOutlined
                className="icon"
                style={{ fontSize: "16px", marginRight: "5px" }}
              />{" "}
              Thông tin phân quyền
            </div>
          ),
        },
        {
          key: "2",
          label: (
            <div
              onClick={(e) => {
                e.stopPropagation();
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleDeleteEvent(value);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <CloseCircleOutlined
                className="icon"
                style={{
                  fontSize: "16px",
                  marginRight: "5px",
                  fontWeight: "bolder",
                }}
              />{" "}
              Hủy lời mời
            </div>
          ),
        },
      ],
    };
  };

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [contactDetail, setContactDetail] = useState<any>({});
  const [isOpenModal, setIsOpenModal] = useState(false);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const showDrawer = (idContact: any) => {
    setOpen(true);
    // handleGetContactById(idContact);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Card
        onClick={() => showDrawer(contact.id)}
        className="card-item"
        title={contact.member.firstName}
        extra={
          <Dropdown menu={getItem(contact)} placement="top" arrow>
            <EllipsisOutlined
              className="icon"
              style={{ fontSize: "22px" }}
              onClick={(e) => e.stopPropagation()}
            />
          </Dropdown>
        }
        hoverable
      >
        {/* <Skeleton loading={true} avatar active> */}
        <Meta
          avatar={
            <Avatar
              src={
                isEmpty(contact.member.imagePath)
                  ? profileDefault
                  : contact.member.imagePath
              }
            />
          }
          title={<div>{contact.member.emailAccount}</div>}
          description={
            <span>
              <span className="label-grayest">Ngày mời: </span>
              {moment(contact.createDate).format("DD/MM/YYYY HH:mm")}
            </span>
          }
        />
        {/* </Skeleton> */}
      </Card>
      <Drawer
        title={contact.member.firstName}
        placement="right"
        onClose={onClose}
        open={open}
      >
        <div className="drawer-contact-detail">
          <Row justify={"center"} className="wrapper-avatar">
            <Avatar
              className="avatar"
              src={
                isEmpty(contact.member.imagePath)
                  ? profileDefault
                  : contact.member.imagePath
              }
            />
          </Row>
          <div className="wrapper-detail">
            <div className="row-info field">
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{contact.member.firstName}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Email: </span>
            </div>
            <div className="row-info infor">
              <span>{contact.member.emailAccount}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Ngày gửi lời mời: </span>
            </div>
            <div className="row-info infor">
              <span>{moment(contact.createDate).format(FORMAT_DATE_FULL)}</span>
            </div>
          </div>
        </div>
      </Drawer>
      <Modal
        width={1000}
        title={"Thông tin phân quyền"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddMemberToEvent
          typeModal={TYPE_MODAL_MEMBER_EVENT.VIEW}
          dataPermission={contact}
        />
      </Modal>
    </>
  );
}

export default MemberRequset;
