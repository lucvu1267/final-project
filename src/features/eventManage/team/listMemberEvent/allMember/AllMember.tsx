import {
  Avatar,
  Card,
  Drawer,
  Tooltip,
  Popconfirm,
  Row,
  Divider,
  Dropdown,
  Modal,
  message,
} from "antd";
import profileDefault from "assets/images/profileDefault.png";
import { useNavigate, useParams } from "react-router-dom";
import { useState } from "react";
import {
  EllipsisOutlined,
  UserOutlined,
  CloseCircleOutlined,
  UserAddOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import { getMessStatus, getRoleMemberEvent, isEmpty } from "mixin/generate";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { addContact, getContactId } from "features/contact/contactSlice";
import AddMemberToEvent from "../../modal/AddMemberToEvent";
import { ROLE_EVENT, TYPE_MODAL_MEMBER_EVENT } from "constants/event";
import { toastCustom } from "config/toast";
import { deleteMember } from "features/eventManage/eventManageSlice";
const { Meta } = Card;

function AllMember(props: any) {
  const { contact, setIsCallBackApi } = props;
  const { idEvent } = useParams();
  const user = useSelector((state: any) => state.contactSlice.user);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const handleAddContact = async (idUser: any) => {
    await addContact(idUser);
    setIsCallBackApi();
    toastCustom("success", "Đã thêm liên hệ");
  };

  const handleDeleteEvent = async (value: any) => {
    await deleteMember(idEvent, value.member.id);
    setIsCallBackApi();
    toastCustom("success", `Đã xóa ${value.member.firstName} khỏi sự kiện!`);
  };

  const getItem = (value: any) => {
    return {
      items: [
        {
          key: "1",
          label: ((roleUser.eventRole !== ROLE_EVENT.MEMBER &&
            value.eventRole !== ROLE_EVENT.HEADER) ||
            user.id === value.member.id) && (
            <div
              onClick={(e) => {
                e.stopPropagation();
                setIsOpenModal(true);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              <UserOutlined
                className="icon"
                style={{ fontSize: "16px", marginRight: "5px" }}
              />{" "}
              Thông tin phân quyền
            </div>
          ),
        },
        {
          key: "2",
          label: user.id !== value.member.id && (
            <div
              onClick={(e) => {
                e.stopPropagation();
                handleAddContact(value.member.id);
              }}
              style={{ fontWeight: "600", color: "#285158" }}
            >
              {value.member.isAdded ? (
                <CheckCircleOutlined
                  className="icon"
                  style={{ fontSize: "16px", marginRight: "5px" }}
                />
              ) : (
                <UserAddOutlined
                  className="icon"
                  style={{ fontSize: "16px", marginRight: "5px" }}
                />
              )}{" "}
              {value.member.isAdded ? "Đã thêm liên hệ" : "Thêm liên hệ"}
            </div>
          ),
        },
        {
          key: "3",
          label: user.id !== value.member.id &&
            (roleUser.eventRole === "HEADER" ||
              roleUser.eventRole === "SUB_HEADER") &&
            value.eventRole !== ROLE_EVENT.HEADER && (
              <div
                onClick={(e) => {
                  if (
                    roleUser?.event?.status === "DONE" ||
                    roleUser?.event?.status === "CANCEL"
                  ) {
                    return message.open({
                      type: "error",
                      content: getMessStatus(roleUser?.event?.status),
                    });
                  }
                  e.stopPropagation();
                  handleDeleteEvent(value);
                }}
                style={{ fontWeight: "600", color: "#285158" }}
              >
                <CloseCircleOutlined
                  className="icon"
                  style={{
                    fontSize: "16px",
                    marginRight: "5px",
                    fontWeight: "bolder",
                  }}
                />{" "}
                Xóa khỏi sự kiện
              </div>
            ),
        },
      ],
    };
  };

  const [open, setOpen] = useState(false);
  const [contactDetail, setContactDetail] = useState<any>({});
  const [isOpenModal, setIsOpenModal] = useState(false);

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const showDrawer = (idContact: any) => {
    setOpen(true);
    handleGetContactById(idContact);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleGetContactById = async (idContact: any) => {
    try {
      // dispatch(setLoading(true));
      const data = await getContactId(idContact);
      setContactDetail(data.data);
    } catch (error) {
    } finally {
      // dispatch(setLoading(false));
    }
  };

  return (
    <>
      <Card
        onClick={() => showDrawer(contact.member.id)}
        className="card-item"
        title={contact.member.firstName}
        extra={
          (contact.eventRole !== "HEADER" || user.id !== contact.member.id) && (
            <Dropdown menu={getItem(contact)} placement="top" arrow>
              <EllipsisOutlined
                className="icon"
                style={{ fontSize: "22px" }}
                onClick={(e) => e.stopPropagation()}
              />
            </Dropdown>
          )
        }
        hoverable
      >
        {/* <Skeleton loading={true} avatar active> */}
        <Meta
          avatar={
            <Avatar
              src={
                isEmpty(contact.member.imagePath)
                  ? profileDefault
                  : contact.member.imagePath
              }
            />
          }
          title={<div>{contact.member.emailAccount}</div>}
          description={
            <Tooltip placement="top" title="Chức vụ" arrowPointAtCenter>
              <span>{getRoleMemberEvent(contact.eventRole)}</span>
            </Tooltip>
          }
        />
        {/* </Skeleton> */}
      </Card>
      <Drawer
        title={contact.member.firstName}
        placement="right"
        onClose={onClose}
        open={open}
      >
        <div className="drawer-contact-detail">
          <Row justify={"center"} className="wrapper-avatar">
            <Avatar
              className="avatar"
              src={
                isEmpty(contact.member.imagePath)
                  ? profileDefault
                  : contact.member.imagePath
              }
            />
          </Row>
          <div className="wrapper-detail">
            <div className="row-info field">
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.firstName}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Email: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.email}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Số điện thoại: </span>
            </div>
            <div className="row-info infor">
              <span>{contactDetail.phone}</span>
            </div>
            <Divider className="divider" plain></Divider>
          </div>
        </div>
      </Drawer>
      <Modal
        width={1000}
        title={"Thông tin phân quyền"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddMemberToEvent
          typeModal={TYPE_MODAL_MEMBER_EVENT.UPDATE}
          dataPermission={contact}
          setIsOpenModal={setIsOpenModal}
          setIsCallBackApi={setIsCallBackApi}
        />
      </Modal>
    </>
  );
}

export default AllMember;
