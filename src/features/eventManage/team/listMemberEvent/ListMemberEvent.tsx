import { Button, Col, message, Modal, Row, Tooltip } from "antd";
import { setLoading } from "components/common/loading/loadingSlice";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AllMember from "./allMember/AllMember";
import MemberCancel from "./allMember/MemberCancel";
import MemberRequset from "./allMember/MemberRequset";
import { QuestionCircleOutlined } from "@ant-design/icons";
import "./list-member-event.scss";
import { TYPE_FORM } from "constants/index";
import AddMemberToEvent from "../modal/AddMemberToEvent";
import {
  getAllMember,
  getAllMemberCancel,
  getAllMemberRequest,
} from "features/eventManage/eventManageSlice";
import { useParams } from "react-router-dom";
import EmptyData from "components/common/emptyData/EmptyData";
import {
  ROLE_EVENT,
  ROLE_MEMBER,
  TYPE_MODAL_MEMBER_EVENT,
} from "constants/event";
import { getMessStatus } from "mixin/generate";

const TAB = {
  ALL_MEMBER: 1,
  REQUEST_MEMBER: 2,
  CANCEL_MEMBER: 3,
};

function ListMemberEvent(props: any) {
  const { idEvent } = useParams();
  const dispatch = useDispatch();

  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const [tabCurrent, setTabCurrent] = useState(TAB.ALL_MEMBER);
  const [typeForm, setTypeForm] = useState(TYPE_FORM.CREATE);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [listAllMember, setListAllMember] = useState([]);
  const [listMemberRequest, setListMemberRequest] = useState([]);
  const [listMemberCancel, setListMemberCancel] = useState([]);
  const [isCallBackApi, setIsCallBackApi] = useState(false);

  const listTabs =
    roleUser.eventRole === ROLE_EVENT.MEMBER
      ? [{ value: 1, label: "Tất cả thành viên" }]
      : [
          { value: 1, label: "Tất cả thành viên" },
          { value: 2, label: "Lời mời tham gia" },
          { value: 3, label: "Lời mời bị từ chối" },
        ];

  useEffect(() => {
    if (isCallBackApi) {
      setIsCallBackApi(false);
    }
    if (tabCurrent === TAB.ALL_MEMBER) {
      handleGetAllMember();
    } else if (tabCurrent === TAB.REQUEST_MEMBER) {
      handleGetAllMemberRequest();
    } else if (tabCurrent === TAB.CANCEL_MEMBER) {
      handleGetAllMemberCancel();
    }
  }, [tabCurrent, isCallBackApi]);

  const handleGetAllMember = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getAllMember(idEvent);
      setListAllMember(data.data);
    } catch (e) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetAllMemberRequest = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getAllMemberRequest(idEvent);
      setListMemberRequest(data.data);
    } catch (e) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetAllMemberCancel = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getAllMemberCancel(idEvent);
      setListMemberCancel(data.data);
    } catch (e) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  return (
    <>
      <Row className="wrapper-option-view">
        <Col className="add-action-team" span={15}>
          {(roleUser.eventRole === "HEADER" ||
            roleUser.eventRole === "SUB_HEADER") && (
            <div
              onClick={() => {
                if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                setIsOpenModal(true);
              }}
            >
              Thêm thành viên{" "}
              <Tooltip
                placement="top"
                title="Sẽ gửi email lời mời đến người bạn muốn thêm vào sự kiện "
                arrowPointAtCenter
              >
                <QuestionCircleOutlined
                  className="icon"
                  style={{ fontSize: "15px", marginLeft: "5px" }}
                />
              </Tooltip>
            </div>
          )}
        </Col>

        {listTabs.map((item, idx) => (
          <Col
            key={idx}
            className={`option ${item.value === tabCurrent && "option-select"}`}
            span={3}
            onClick={() => {
              setTabCurrent(item.value);
            }}
          >
            <div className={`${item.value === tabCurrent && "selected"}`}>
              {item.label}
            </div>
          </Col>
        ))}
      </Row>
      <Row className="container-list-member">
        {/* TAB_1 */}
        <div hidden={tabCurrent !== TAB.ALL_MEMBER}>
          <div className="wrapper-list-member">
            {listAllMember?.length > 0 &&
              listAllMember.map((item, index) => {
                return (
                  <AllMember
                    key={index}
                    contact={item}
                    setIsCallBackApi={setIsCallBackApi}
                  ></AllMember>
                );
              })}
            {listAllMember.length === 0 && <EmptyData />}
          </div>
        </div>
        {/* TAB_2 */}
        <div hidden={tabCurrent !== TAB.REQUEST_MEMBER}>
          <div className="wrapper-list-member">
            {listMemberRequest?.length > 0 &&
              listMemberRequest.map((item, index) => {
                return (
                  <MemberRequset
                    key={index}
                    contact={item}
                    setIsCallBackApi={setIsCallBackApi}
                  ></MemberRequset>
                );
              })}
          </div>{" "}
          {listMemberRequest.length === 0 && <EmptyData />}
        </div>
        {/* TAB_3 */}

        <div hidden={tabCurrent !== TAB.CANCEL_MEMBER}>
          <div className="wrapper-list-member">
            {listMemberCancel?.length > 0 &&
              listMemberCancel.map((item, index) => {
                return (
                  <MemberCancel
                    key={index}
                    contact={item}
                    setIsCallBackApi={setIsCallBackApi}
                  ></MemberCancel>
                );
              })}
          </div>{" "}
          {listMemberCancel.length === 0 && <EmptyData />}
        </div>
      </Row>
      <Modal
        width={1000}
        title={
          typeForm === TYPE_FORM.CREATE
            ? "Thêm thành viên"
            : "Thông tin phân quyền"
        }
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <AddMemberToEvent
          setIsCallBackApi={setIsCallBackApi}
          setIsOpenModal={setIsOpenModal}
          typeModal={TYPE_MODAL_MEMBER_EVENT.ADD}
        />
      </Modal>
    </>
  );
}

export default ListMemberEvent;
