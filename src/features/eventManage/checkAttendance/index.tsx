import { QrCodeScanner } from "@mui/icons-material";
import React, { useState } from "react";
// import QrReader from "react-web-qr-reader";
// import QrReader from "react-qr-scanner";
import QrReader from "react-qr-reader";

function CheckAttenDance(props: any) {
  const [result, setResult] = useState("no result");
  const [error, setError] = useState(null);

  if (error) {
    return <div className="error">{error}</div>;
  }
  return (
    <div style={{ width: "500px" }}>
      <QrReader
        delay={300}
        onError={(error: any) => {
          setError(error.message);
        }}
        onScan={(data: any) => {
          if (data) {
            setResult(data);
            setError(null);
          }
        }}
        legacyMode={true}
        style={{ width: "100%" }}
      />
      <p>{result}</p>
    </div>
  );
}

export default CheckAttenDance;
