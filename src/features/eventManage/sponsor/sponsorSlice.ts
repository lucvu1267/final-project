import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const createSponsor = async (payload: any) => {
  return await axiosInstance.post(`sponsor/create`, payload);
};

export const getListSponsor = async (payload: any) => {
  return await axiosInstance.post(`sponsor/search`, payload);
};

export const getListSponsorPublic = async (idEvent: any) => {
  return await axiosInstance.get(`sponsor/get-all-public-in-event/${idEvent}`);
};

export const deleteSponsor = async (idEvent: any, idSponsor: any) => {
  return await axiosInstance.post(`sponsor/delete/${idEvent}/${idSponsor}`);
};

export const updateSponsor = async (payload: any) => {
  return await axiosInstance.post(`sponsor/update`, payload);
};

export const sponsorSlice = createSlice({
  name: "loading",
  initialState: {},
  reducers: {
    // setLoading: (state, action) => {
    //   state.isLoading = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = sponsorSlice.actions;

export default sponsorSlice.reducer;
