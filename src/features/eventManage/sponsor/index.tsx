import {
  Col,
  Popconfirm,
  Row,
  Switch,
  Table,
  Tooltip,
  Form,
  Modal,
  Input,
  Select,
  Checkbox,
  Drawer,
  Divider,
  Avatar,
  message,
} from "antd";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import { getMessStatus, isEmpty, uploadImgCloudinary } from "mixin/generate";
import {
  EditOutlined,
  CheckCircleOutlined,
  DeleteOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import moment from "moment";
import profileDefault from "assets/images/profileDefault.png";
import React, { useEffect, useState } from "react";
import "./index.scss";
import MessError from "components/common/MessError";
import TextArea from "antd/lib/input/TextArea";
import UploadImage from "components/common/uploadImage/UploadImage";
import {
  createSponsor,
  deleteSponsor,
  getListSponsor,
  updateSponsor,
} from "./sponsorSlice";
import { toastCustom } from "config/toast";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { ROLE_MEMBER } from "constants/event";

const { Option } = Select;

function Sponsor(props: any) {
  const [form] = Form.useForm();
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isOpenDrawer, setIsOpenDrawer] = useState(false);
  const [typeForm, setTypeForm] = useState(TYPE_FORM.CREATE);
  const [file, setFile] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);
  const [isShowInPost, setIsShowInPost] = useState(true);
  const [listSponsor, setListSponsor] = useState([]);
  const [idSponsorCurrent, setIdSponsorCurrent] = useState(null);
  const [imgUrlInit, setImgUrlInit] = useState(null);
  const [sponsorDetail, setSponsorDetail] = useState<any>({});

  const columnsOrderTicket: any = [
    {
      title: "Tên",
      render: (value: any) => <div className="label-grayest">{value.name}</div>,
    },
    {
      title: "Ngày tạo",
      render: (value: any) => (
        <div>{moment(value?.createDate).format(FORMAT_DATE_FULL)}</div>
      ),
    },
    {
      title: "Tài trợ",
      render: (value: any) => (
        <div className="label-grayest">{value.sponsorItem}</div>
      ),
    },
    {
      title: "Số điện thoại",
      render: (value: any) => <div>{value.phone}</div>,
    },
    {
      title: "Email",
      render: (value: any) => <div>{value.email}</div>,
    },
    {
      title: "Link",
      render: (value: any) => <div>{value.link}</div>,
    },
    {
      title: "Hiển thị trong bài viết",
      align: "center",
      render: (value: any) => (
        <div>
          <Tooltip
            placement="top"
            title="Hiển thị dưới bài viết"
            arrowPointAtCenter
          >
            {isOpenModal === false && (
              <Switch
                disabled={
                  roleUser.sponsorAccess === ROLE_MEMBER.VIEW ||
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                }
                defaultChecked={value.publicSponsor === "ACTIVE"}
                onChange={(isActive) => handleUpdateStatus(value, isActive)}
              />
            )}
          </Tooltip>
        </div>
      ),
    },
    {
      title: "Thao tác",
      align: "center",
      render: (value: any) => (
        <div className="center-common ">
          <Tooltip placement="top" title="Xem chi tiết" arrowPointAtCenter>
            <EyeOutlined
              className="icon-table"
              style={{ marginRight: "10px" }}
              onClick={() => {
                setSponsorDetail(value);
                setIsOpenDrawer(true);
              }}
            />
          </Tooltip>
          <Tooltip placement="top" title="Xóa" arrowPointAtCenter>
            <Popconfirm
              title="Bạn có chắc muốn xóa?"
              onConfirm={() => {
                if (roleUser.sponsorAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleDeleteSponsor(value.id);
              }}
              // onCancel={null}
              okText="Xóa"
              showCancel={false}
            >
              <DeleteOutlined
                className="icon-table"
                style={{ marginRight: "10px" }}
                // onClick={() => getTicketDetail(value.id)}
              />
            </Popconfirm>
          </Tooltip>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              className="icon-table"
              onClick={() => {
                if (roleUser.sponsorAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleClickUpdate(value);
              }}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  const onCloseDrawer = () => {
    setIsOpenDrawer(false);
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
    form.resetFields();
    resetData();
  };

  const handleSubmitForm = async (value: any) => {
    dispatch(setLoading(true));
    let urlImage = null;
    if (file) {
      urlImage = await uploadImgCloudinary(file);
    }
    try {
      const payload = {
        email: value.email,
        eventId: idEvent,
        link: value.link,
        logoPath: urlImage,
        moreInformation: value.description,
        name: value.name,
        phone: value.phone,
        publicSponsor: isShowInPost ? "ACTIVE" : "NOT_ACTIVE",
        sponsorItem: value.sponsor,
      };
      const payloadUpdate = {
        email: value.email,
        eventId: idEvent,
        id: idSponsorCurrent,
        link: value.link,
        logoPath: urlImage ? urlImage : imgUrlInit,
        moreInformation: value.description,
        name: value.name,
        phone: value.phone,
        publicSponsor: isShowInPost ? "ACTIVE" : "NOT_ACTIVE",
        sponsorItem: value.sponsor,
      };
      if (typeForm === TYPE_FORM.CREATE) {
        await createSponsor(payload);
      } else {
        await updateSponsor(payloadUpdate);
      }
      await handleGetListSponsor();
      handleCloseModal();
      toastCustom(
        "success",
        typeForm === TYPE_FORM.CREATE
          ? "Tạo nhà tài trợ thành công"
          : "Cập nhật thành công"
      );
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const resetData = () => {
    setImgUrlInit(null);
    setFile(null);
    setIsShowInPost(true);
    setIdSponsorCurrent(null);
  };

  const handleUpdateStatus = async (value: any, isActive: any) => {
    const payloadUpdate = {
      eventId: idEvent,
      id: value.id,
      name: value.name,
      sponsorItem: value.sponsorItem,
      phone: value.phone,
      email: value.email,
      link: value.link,
      moreInformation: value.moreInformation,
      logoPath: value.logoPath,
      publicSponsor: isActive ? "ACTIVE" : "NOT_ACTIVE",
    };
    await updateSponsor(payloadUpdate);
    handleGetListSponsor();
  };

  const handleGetListSponsor = async () => {
    dispatch(setLoading(true));
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    const data = await getListSponsor(payload);
    setListSponsor(data.data.page.content);
    dispatch(setLoading(false));
  };

  const handleClickUpdate = (value: any) => {
    form.setFieldsValue({
      name: value.name,
      sponsor: value.sponsorItem,
      phone: value.phone,
      email: value.email,
      link: value.link,
      description: value.moreInformation,
    });
    setIsShowInPost(value.publicSponsor === "ACTIVE");
    setImgUrlInit(value.logoPath);
    setIdSponsorCurrent(value.id);
    setIsOpenModal(true);
    setTypeForm(TYPE_FORM.UPDATE);
  };

  const handleDeleteSponsor = async (idSponsor: any) => {
    dispatch(setLoading(true));
    await deleteSponsor(idEvent, idSponsor);
    await handleGetListSponsor();
    toastCustom("success", "Xóa thành công");
  };

  useEffect(() => {
    if (roleUser.sponsorAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission");
    } else {
      handleGetListSponsor();
    }
  }, []);

  return (
    <div>
      <Row className="wrapper-action">
        <Col className="add-action-team-member" span={24}>
          <div
            onClick={() => {
              if (roleUser.sponsorAccess === ROLE_MEMBER.VIEW) {
                return message.error({
                  type: "error",
                  content: "Bạn không có quyền!",
                });
              } else if (
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              ) {
                return message.open({
                  type: "error",
                  content: getMessStatus(roleUser?.event?.status),
                });
              }
              setIsOpenModal(true);
              setTypeForm(TYPE_FORM.CREATE);
            }}
          >
            Thêm nhà tài trợ{" "}
          </div>
        </Col>
      </Row>
      <Table
        className="table-common"
        style={{ width: "100vw" }}
        rowKey={(record: any) => record.id}
        columns={columnsOrderTicket}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>
              <span className="label-grayest">Thông tin thêm: </span>{" "}
              {record.moreInformation}
            </p>
          ),
          rowExpandable: (record) => record.name !== "Not Expandable",
        }}
        dataSource={listSponsor}
        pagination={{ position: ["bottomCenter"] }}
      />
      <Modal
        width={1000}
        title={
          typeForm === TYPE_FORM.CREATE
            ? "Thêm nhà tài trợ"
            : "Chỉnh sửa thông tin nhà tài trợ"
        }
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <div className="container-add-update-ticket">
          {" "}
          <Row justify="center" style={{ marginBottom: "1rem" }}>
            <Tooltip
              placement="top"
              title="Thêm ảnh cho nhóm"
              arrowPointAtCenter
            >
              <div>
                <UploadImage
                  imgUrlInit={imgUrlInit}
                  setFile={setFile}
                  isResetImageUrl={isResetImageUrl}
                  setDefaultReset={() => setIsResetImageUrl(false)}
                />
              </div>
            </Tooltip>
          </Row>{" "}
          <Form
            form={form}
            onFinish={(value) => handleSubmitForm(value)}
            autoComplete="off"
          >
            <Row justify="center">
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Tên<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng nhập tên nhà tài trợ!"}
                          />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={
                              "Tên nhà tài trợ không được quá 255 kí tự!"
                            }
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên nhà tài trợ" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Tài trợ<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="sponsor"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng nhập thông tin tài trợ!"}
                          />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError message={"Không được quá 255 kí tự!"} />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tài trợ" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Số điện thoại<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="phone"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError message={"Vui lòng nhập số điện thoại!"} />
                        ),
                      },
                      {
                        pattern: /^[0-9]+$/,
                        message: (
                          <MessError
                            message={"Số điện thoại phải bao gồm các chữ số"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Số điện thoại" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">
                    Email<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="email"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: <MessError message={"Vui lòng email!"} />,
                      },
                      {
                        type: "email",
                        message: (
                          <MessError message={"Email không đúng định dạng!"} />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Email" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field">Link</div>
                  <Form.Item name="link" className="text-field">
                    <Input className="input" placeholder="Link" />
                  </Form.Item>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={12}
              >
                <div className="field-add">
                  <div className="label-field"></div>
                  <Checkbox
                    defaultChecked={isShowInPost}
                    onChange={(value) => setIsShowInPost(value.target.checked)}
                  >
                    Hiển thị trong bài đăng
                  </Checkbox>
                </div>
              </Col>
              <Col
                className="wrapper-field"
                xs={24}
                sm={24}
                md={24}
                lg={24}
                xl={24}
              >
                <div className="field-add" style={{ width: "90%" }}>
                  <div className="label-field">Thông tin thêm</div>
                  <Form.Item name="description" className="text-field">
                    <TextArea
                      placeholder="Thông tin thêm"
                      className="description"
                      // onChange={getDataPost}
                    ></TextArea>
                  </Form.Item>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
      <Drawer
        title={sponsorDetail.firstName}
        placement="right"
        onClose={onCloseDrawer}
        open={isOpenDrawer}
      >
        <div className="drawer-contact-detail">
          <Row justify={"center"} className="wrapper-avatar">
            <Avatar
              className="avatar"
              src={
                isEmpty(sponsorDetail.logoPath)
                  ? profileDefault
                  : sponsorDetail.logoPath
              }
            />
          </Row>
          <div className="wrapper-detail">
            <div className="row-info field">
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{sponsorDetail.name}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Email: </span>
            </div>
            <div className="row-info infor">
              <span>{sponsorDetail.email}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Số điện thoại: </span>
            </div>
            <div className="row-info infor">
              <span>{sponsorDetail.phone}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field">
              <span>Link: </span>
            </div>
            <div className="row-info infor">
              <span style={{ overflow: "auto" }}>{sponsorDetail.link}</span>
            </div>
          </div>
        </div>
      </Drawer>
    </div>
  );
}

export default Sponsor;
