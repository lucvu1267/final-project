import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const propsEvent = async (idEvent: any) => {
  return await axiosInstance.post(`event-member/load-user-prop/${idEvent}`);
};

export const createEvent = async (payload: any) => {
  return await axiosInstance.post("event/create", payload);
};

export const updateEvent = async (payload: any) => {
  return await axiosInstance.post("event/update", payload);
};

export const getListEventManager = async (payload: any) => {
  return await axiosInstance.post("event/search", payload);
};

export const getListEventJoined = async () => {
  return await axiosInstance.post("event/joined-events");
};

export const createPost = async (payload: any) => {
  return await axiosInstance.post("post/create-post-event", payload);
};

export const getPostEventById = async (postId: any) => {
  return await axiosInstance.get(`post/event/${postId}`);
};

export const updatePost = async (payload: any) => {
  return await axiosInstance.post(`post/update`, payload);
};

export const getEventById = async (id: any) => {
  return await axiosInstance.post(`/event/${id}`);
};

export const getPostById = async (id: any) => {
  return await axiosInstance.get(`/post/event/${id}`);
};

export const createTicketManager = async (payload: any) => {
  return await axiosInstance.post("ticket/create", payload);
};

export const getListPostAuthen = async (payload: any) => {
  return await axiosInstance.post("post/search-post-manage", payload);
};

//api ticket
export const getListTicketsByEvent = async (payload: any) => {
  return await axiosInstance.post("ticket/find-by-event", payload);
};

export const getTicketById = async (id: any) => {
  return await axiosInstance.post(`ticket/${id}`);
};

export const updateTicket = async (payload: any) => {
  return await axiosInstance.post(`ticket/update`, payload);
};

export const getTicketOrder = async (payload: any) => {
  return await axiosInstance.post(`ticket/orders`, payload);
};

export const getTicketOrderDetail = async (id: any) => {
  return await axiosInstance.post(`ticket/order/${id}`);
};

export const updateStatusSaleTicket = async (idEvent: any) => {
  return await axiosInstance.post(
    `ticket/change-status-sale-ticket/${idEvent}`
  );
};

export const getStatusSaleTicket = async (idEvent: any) => {
  return await axiosInstance.get(`ticket/get-sale-ticket/${idEvent}`);
};

//api team
export const getUserInSystem = async (payload: any) => {
  return await axiosInstance.post(`contact/search`, payload);
};

export const addTeam = async (payload: any) => {
  return await axiosInstance.post(`team/create`, payload);
};

export const addMemberIntoTeam = async (idTeam: any, userToAddId: any) => {
  return await axiosInstance.post(`team/${idTeam}/add-user/${userToAddId}`);
};

export const getTeamByEvent = async (id: any) => {
  return await axiosInstance.post(`team/find-by-event/${id}`);
};

export const getTeamById = async (id: any) => {
  return await axiosInstance.post(`team/${id}`);
};

export const searchUserToAdd = async (payload: any) => {
  return await axiosInstance.post(`team/search-user-to-add`, payload);
};

export const addMember = async (payload: any) => {
  return await axiosInstance.post(`event-member/add-member`, payload);
};

export const grantRoleMember = async (idTeam: any, idUser: any) => {
  return await axiosInstance.post(`team/${idTeam}/grant-leader-role/${idUser}`);
};

export const removeMemberFromTeam = async (
  teamId: any,
  userToRemoveId: any
) => {
  return await axiosInstance.post(
    `/team/${teamId}/remove-user/${userToRemoveId}`
  );
};

export const getMemberToAddEvent = async (payload: any) => {
  return await axiosInstance.post(`event-member/search-user-to-add`, payload);
};

export const getAllMember = async (idEvent: any) => {
  return await axiosInstance.post(`event-member/load-all-member/${idEvent}`);
};

export const getAllMemberRequest = async (idEvent: any) => {
  return await axiosInstance.post(
    `event-member/load-all-member-waiting/${idEvent}`
  );
};

export const getAllMemberCancel = async (idEvent: any) => {
  return await axiosInstance.post(
    `event-member/load-all-member-reject/${idEvent}`
  );
};

export const deleteMember = async (eventId: any, memberId: any) => {
  return await axiosInstance.post(
    `event-member/${eventId}/delete-member/${memberId}`
  );
};

export const createPostTeam = async (payload: any) => {
  return await axiosInstance.post(`post/create-post-team`, payload);
};

export const updateTeam = async (payload: any) => {
  return await axiosInstance.post(`team/update`, payload);
};

export const updateRoleMember = async (payload: any) => {
  return await axiosInstance.post(`event-member/update-member`, payload);
};

//api post team
export const createComment = async (payload: any) => {
  return await axiosInstance.post(`post/create-comment`, payload);
};

export const getListComment = async (idPost: any, payload: any) => {
  return await axiosInstance.post(`post/${idPost}/comments`, payload);
};

export const updatePostTeam = async (payload: any) => {
  return await axiosInstance.post(`post/update-post-team`, payload);
};

export const updateCmtTeam = async (payload: any) => {
  return await axiosInstance.post(`post/update-comment`, payload);
};

export const deletePostTeam = async (idPost: any) => {
  return await axiosInstance.post(`post/delete-post-team/${idPost}`);
};

export const deleteComment = async (idComment: any) => {
  return await axiosInstance.post(`post/delete-comment/${idComment}`);
};

export const likePost = async (idPost: any) => {
  return await axiosInstance.post(`post/team/${idPost}/like`);
};

//api task
export const getTaskBySearch = async (payload: any) => {
  return await axiosInstance.post(`task/search`, payload);
};

//api agenda
export const createAgenda = async (payload: any) => {
  return await axiosInstance.post(`agenda/create`, payload);
};

export const getAgenda = async (idEvent: any) => {
  return await axiosInstance.post(`agenda/get-all-in-event/${idEvent}`);
};

export const updateAgenda = async (payload: any) => {
  return await axiosInstance.post(`agenda/update`, payload);
};

export const deleteAgenda = async (eventId: any, id: any) => {
  return await axiosInstance.post(`agenda/delete/${eventId}/${id}`);
};

//api config mail
export const configMailBuyTicket = async (payload: any) => {
  return await axiosInstance.post(`event/update-content-buy-ticket`, payload);
};

export const configMailInviteParticipant = async (payload: any) => {
  return await axiosInstance.post(
    `event/update-content-invite-participant`,
    payload
  );
};

export const configMailTicket = async (payload: any) => {
  return await axiosInstance.post(`event/update-content-ticket-mail`, payload);
};

//change status event
export const changeStatusEvent = async (payload: any) => {
  return await axiosInstance.post(`event/change-event-status`, payload);
};

export const getLogEvent = async (eventId: any) => {
  return await axiosInstance.post(`activity-log/event/${eventId}`);
};

export const eventManageSlice = createSlice({
  name: "loading",
  initialState: {
    userEventPops: {},
  },
  reducers: {
    setUserEventPops: (state, action) => {
      state.userEventPops = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUserEventPops } = eventManageSlice.actions;

export default eventManageSlice.reducer;
