import {
  Button,
  Cascader,
  Modal,
  Row,
  Tooltip,
  Select,
  Table,
  Switch,
  message,
} from "antd";
import type { ColumnsType } from "antd/es/table";
import React, { useEffect, useState } from "react";
import Search from "antd/lib/input/Search";
import "./posts-by-event.scss";
import { Link, useNavigate, useParams } from "react-router-dom";
import {
  PlusOutlined,
  EyeOutlined,
  EditOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { getListPost } from "features/home/homeSlice";
import { ROLE_MEMBER, TYPE_POST } from "constants/event";
import moment from "moment";
import { FORMAT_DATE_FULL } from "constants/index";
import { getListPostAuthen, updatePost } from "../eventManageSlice";
import { getMessStatus } from "mixin/generate";
const { Option } = Select;

const optionLists: any[] = [
  {
    value: "all",
    label: "Tất cả",
    isLeaf: true,
  },
  {
    value: "F-Camp",
    label: "F-Camp",
    isLeaf: false,
  },
  {
    value: "Tết Dân Gian",
    label: "Tết Dân Gian",
    isLeaf: false,
  },
];

function PostsByEvent(props: any) {
  const { idEvent } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [listPosts, setListPost] = useState<any[]>([]);
  const [options, setOptions] = React.useState<any[]>(optionLists);
  const [keySearchPost, setKeySearchPost] = useState(null);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [postDetail, setPostDetaul] = useState<any>({});
  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  const columns: ColumnsType<any> = [
    { title: "Tiêu đề", dataIndex: "subject", key: "subject" },
    {
      title: "Ngày tạo",

      render: (value: any) => (
        <span>{moment(value.createDate).format(FORMAT_DATE_FULL)}</span>
      ),
    },
    // { title: "Tags", dataIndex: "tags", key: "tags" },
    {
      title: "Người tạo",
      render: (value: any) => (
        <span className="label-grayest">{value.creator.email}</span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "",
      key: "y",
      render: (value) => (
        <div>
          <Tooltip
            placement="top"
            title={"Công khai bài viết"}
            arrowPointAtCenter
          >
            <Switch
              disabled={
                roleUser.postAccess === ROLE_MEMBER.VIEW ||
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              }
              defaultChecked={value.status === "ACTIVE"}
              onChange={(isActive) => handleUpdateStatus(value, isActive)}
            />
          </Tooltip>
        </div>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div className="operator-wrapper">
            <Tooltip placement="top" title="Xem bài đăng" arrowPointAtCenter>
              <EyeOutlined
                className="icon-table"
                onClick={() => {
                  setPostDetaul(value);
                  setIsOpenModal(true);
                }}
              />
            </Tooltip>
            <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
              <EditOutlined
                className="icon-table"
                onClick={() => {
                  if (roleUser.postAccess === ROLE_MEMBER.VIEW) {
                    return message.error({
                      type: "error",
                      content: "Bạn không có quyền!",
                    });
                  } else if (
                    roleUser?.event?.status === "DONE" ||
                    roleUser?.event?.status === "CANCEL"
                  ) {
                    return message.open({
                      type: "error",
                      content: getMessStatus(roleUser?.event?.status),
                    });
                  }
                  navigate(`/event-manage/${idEvent}/post/${value.id}/update`);
                }}
              />
            </Tooltip>
          </div>
        );
      },
    },
  ];

  const handleUpdateStatus = async (value: any, isActive: any) => {
    const payloadUpdate = {
      content: value.content,
      eventId: parseInt(idEvent + ""),
      id: value.id,
      overviewDescription: value.overviewDescription,
      overviewImagePath: value.overviewImagePath,
      subject: value.subject,
      status: isActive ? "ACTIVE" : "NOT_ACTIVE",
    };
    await updatePost(payloadUpdate);
  };

  const handleGetListPost = async () => {
    try {
      dispatch(setLoading(true));
      const payload = {
        eventId: parseInt(idEvent + ""),
        orders: [],
        page: 0,
        searchKey: keySearchPost,
        size: 100,
        postType: TYPE_POST.EVENT,
        teamId: null,
      };
      const data = await getListPostAuthen(payload);
      setListPost(data.data.postPage.content);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  useEffect(() => {
    if (roleUser.postAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission");
    } else {
      handleGetListPost();
    }
  }, []);

  return (
    <>
      <Row className="wrapper-action-post">
        <Search
          className="search search-40px"
          placeholder="Tìm kiếm theo tên"
          enterButton
          size="large"
          onChange={(e: any) => setKeySearchPost(e.target.value)}
          onSearch={handleGetListPost}
        />{" "}
        <Tooltip placement="top" title="Đăng bài viết" arrowPointAtCenter>
          <Button
            type="primary"
            className="add-contact btn-primary"
            icon={<PlusOutlined />}
            onClick={() => {
              if (roleUser.postAccess === ROLE_MEMBER.VIEW) {
                return message.error({
                  type: "error",
                  content: "Bạn không có quyền!",
                });
              } else if (
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              ) {
                return message.open({
                  type: "error",
                  content: getMessStatus(roleUser?.event?.status),
                });
              }
              navigate(`/event-manage/${idEvent}/post/add`);
            }}
          ></Button>
        </Tooltip>
      </Row>
      <Row className="container-post">
        <Table
          className="table-post"
          style={{ width: "80vw" }}
          rowKey={(value) => value.id}
          columns={columns}
          dataSource={listPosts}
          pagination={{ position: ["bottomCenter"] }}
        />
      </Row>
      <Modal
        width={1000}
        // title={"Mời vào sự kiện"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
        closeIcon={
          <CloseOutlined className="icon" style={{ fontSize: "22px" }} />
        }
      >
        <div
          style={{ padding: "3rem 2rem" }}
          dangerouslySetInnerHTML={{
            __html: postDetail?.content,
          }}
        />
      </Modal>
    </>
  );
}
export default PostsByEvent;
