import React, { useState, useRef, useEffect } from "react";
import JoditEditor from "jodit-react";
import { Button, message, Steps } from "antd";
import "./add-post.scss";
import StepOne from "components/event/addPost/StepOne";
import HeaderFeature from "components/common/HeaderFeature";
import { isEmpty } from "mixin/generate";
import { toastCustom } from "config/toast";
import { CheckCircleOutlined } from "@ant-design/icons";
import { useNavigate, useParams } from "react-router-dom";
import { getPostEventById } from "features/eventManage/eventManageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { ROLE_MEMBER } from "constants/event";
const config: any = {
  readonly: false,
  allowResizeX: true,
  allowResizeY: true,
  height: 1000,
  i18n: "vi",
  placeholder: "Nội dung bài viết",
  buttons: [
    "bold",
    "strikethrough",
    "underline",
    "italic",
    "eraser",
    "|",
    "ul",
    "ol",
    "|",
    "outdent",
    "indent",
    "|",
    "font",
    "fontsize",
    "brush",
    "paragraph",
    "|",
    "image",
    "table",
    "link",
    "|",
    "align",
    "undo",
    "redo",
    "|",
    "hr",
    "|",
    "fullsize",
    "|",
    "source",
    "preview",
  ],
  uploader: {
    url: "https://api.cloudinary.com/v1_1/dcgjui0yd/image/upload",
    imagesExtensions: ["jpg", "png", "jpeg", "gif"],
    //headers: {"token":`${db.token}`},
    filesVariableName: function (t: any) {
      return "file";
    }, //"files",
    // withCredentials: false,
    // pathVariableName: "path",
    format: "json",
    method: "POST",
    prepareData: function (formdata: any) {
      formdata.append("upload_preset", "ems-fpt");
      return formdata;
    },
    isSuccess: function (this: any, e: any) {
      console.log(e);
      const j = this;
      const tagName = "img";
      const elm = j.jodit.createInside.element(tagName);
      elm.setAttribute("src", e.url);

      j.jodit.s.insertImage(
        elm as HTMLImageElement,
        null,
        j.jodit.o.imageDefaultWidth
      );
      return e.success;
    },
    getMessage: function (e: any) {
      return void 0 !== e.data?.messages && Array.isArray(e.data?.messages)
        ? e.data?.messages.join("")
        : "";
    },
    process: function (resp: any) {
      //success callback transfrom data to defaultHandlerSuccess use.it's up to you.
      let files = [];
      files.unshift(resp.data);
      return {
        files: resp.data,
        error: resp.msg,
        msg: resp.msg,
      };
    },
    // error: function (this: any, e: Error) {
    //   this.j.e.fire("errorMessage", e?.message, "error", 4000);
    // },

    defaultHandlerError: function (this: any, e: any) {
      this.j.e.fire("errorMessage", e.message);
    },
  },
};

const steps = [
  {
    title: "Tổng quan bài viết",
  },
  {
    title: "Chi tiết bài viết",
  },
  {
    title: "Hoàn thành",
  },
];

function AddPost(props: any) {
  const { idEvent, idPost } = useParams();
  const editor = useRef(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [content, setContent] = useState("");

  const [current, setCurrent] = useState(0);
  const [isCheckNextStep, setIsCheckNextStep] = useState(false);
  const [isCreatePost, setIsCreatePost] = useState(false);
  const [postDetail, setPostDetail] = useState<any>({});
  const [idPostAfterAddOrUpdate, setidPostAfterAddOrUpdate] =
    useState<any>(null);

  const handleNextStep = () => {
    if (current === 1) {
      if (isEmpty(content) || content === "<p><br></p>") {
        toastCustom("error", "Vui lòng nhập nội dung bài viết!");
        setIsCreatePost(false);
        return;
      } else if (content.length < 255) {
        toastCustom("error", "Nội dung bài viết quá ngắn!");
        setIsCreatePost(false);
        return;
      }
      setIsCreatePost(true);
    } else {
      setCurrent(current + 1);
    }
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const items = steps.map((item) => ({ key: item.title, title: item.title }));

  const handleGetPostById = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getPostEventById(idPost);
      setPostDetail(data.data);
      setContent(data.data.content);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  
  useEffect(()=>{
    if (roleUser.postAccess === ROLE_MEMBER.NONE) {
      navigate("/not-permission")
    }else{
      if (idPost) {
        handleGetPostById();
      }
    }
  },[])


  return (
    <>
      <Steps className="step" current={current} items={items} />

      <div hidden={current !== 0}>
        {" "}
        <StepOne
          postDetail={postDetail}
          handleNextStep={() => handleNextStep()}
          isCheckNextStep={isCheckNextStep}
          setIsCheckNextStep={setIsCheckNextStep}
          isCreatePost={isCreatePost}
          content={content}
          setCurrent={setCurrent}
          setidPostAfterAddOrUpdate={setidPostAfterAddOrUpdate}
        ></StepOne>
      </div>
      <div hidden={current !== 1} className="steps-content">
        <HeaderFeature content={"Chi tiết bài viết"} />
        <JoditEditor
          ref={editor}
          value={content}
          config={config}
          // tabIndex={1} // tabIndex of textarea
          onBlur={(newContent: any) => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
          onChange={(newContent: any) => {}}
        />
      </div>
      <div hidden={current !== 2} className="wrapper-step-three">
        <CheckCircleOutlined className="icon-success" size={200} />
        <div className="mess-success">
          <p
            style={{
              fontSize: "17px",
              fontWeight: "bold",
              color: "#285158",
            }}
          >
            {idPost
              ? "Cập nhật bài viết thành công!"
              : "Tạo bài đăng thành công!"}
          </p>
          <p className="description-gray">
            {" "}
            Bài viết của bạn sẽ được công khai trên trang chủ của hệ thống.
          </p>
          <span
            onClick={() => navigate(`/post-detail/${idPostAfterAddOrUpdate}`)}
            className="btn-view-post"
          >
            Xem bài viết
          </span>
        </div>
      </div>
      <div hidden={current === 2} className="steps-action">
        {current < steps.length - 1 && (
          <Button
            className="btn"
            type="primary"
            onClick={() => {
              setIsCheckNextStep(true);
            }}
          >
            Tiếp tục
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button
            className="btn"
            type="primary"
            onClick={() => message.success("Processing complete!")}
          >
            Hoàn thành
          </Button>
        )}
        {current > 0 && (
          <Button
            className="btn btn-no-backgroud"
            style={{ margin: "0 8px" }}
            onClick={() => prev()}
          >
            Quay lại
          </Button>
        )}
      </div>
    </>
  );
}

export default AddPost;
