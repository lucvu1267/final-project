import {
  Button,
  Col,
  Divider,
  Input,
  InputNumber,
  Modal,
  Popover,
  Row,
  Tooltip,
  Form,
  Select,
  Avatar,
  Card,
} from "antd";
import { setLoading } from "components/common/loading/loadingSlice";
import {
  getPostById,
  getStatusSaleTicket,
} from "features/eventManage/eventManageSlice";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  CheckCircleOutlined,
  InfoCircleOutlined,
  LeftCircleOutlined,
  RightCircleOutlined,
  ClockCircleOutlined,
  TagsOutlined,
  FrownOutlined,
  ShopOutlined,
} from "@ant-design/icons";
import "./post-detail.scss";
import MessError from "components/common/MessError";
import {
  LIST_FEATURE_EVENT,
  MAX_LENGTH_DESCRIPTION,
  MAX_LENGTH_TITLE,
  TYPE_POST,
} from "constants/event";
import { FORMAT_DATE_FULL } from "constants/index";
import moment from "moment";
import { numberFormat } from "mixin/generate";
import {
  getListPost,
  getTicketPublic,
  orderTicket,
} from "features/home/homeSlice";
import { toastCustom } from "config/toast";
import axios from "axios";
import { getListSponsorPublic } from "features/eventManage/sponsor/sponsorSlice";
import logo from "assets/images/logo.png";
import { getStatusRenBooth } from "features/eventManage/boothSlice";
const { Option } = Select;
const { Meta } = Card;

const STATUS_TICKET = {
  WAITING: "WAITING",
  ENABLE: "ENABLE",
};

interface PriceQuantityInterface {
  quantity: number;
  price: number;
  idTicket: number;
  dataTicket: any[];
}
function PostDetailHomePage(props: any) {
  const { idPost } = useParams();
  const [form] = Form.useForm();
  const [formCustomer] = Form.useForm();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [eventId, setEventId] = useState(null);
  const [postDetail, setPostDetail] = useState<any>({});
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isEnableBtnNext, setIsEnableBtnNext] = useState(false);
  const [current, setCurrent] = useState(0);
  const [totalTicket, setTotalTicket] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [listTicket, setListTicket] = useState([]);
  const [listSponsor, setListSponsor] = useState([]);
  const [listOrderTicket, setListOrderTicket] = useState<any>([]);
  const [listStatusTicket, setListStatusTicket] = useState([]);
  const [listProvince, setListProvince] = useState<any>([]);
  const [listDistrict, setListDistrict] = useState<any>([]);
  const [listStreet, setListStreet] = useState<any>([]);
  const [dataTicketOrder, setDataTicketOrder] = useState<
    PriceQuantityInterface[]
  >([]);
  const [payloadOrderDetail, setPayloadOrderDetail] = useState<any[]>([]);
  const [listPostByEvent, setListPostByEvent] = useState<any>([]);
  const [statusSaleTicket, setStatusSaleTicket] = useState(false);
  const [isOpenRenBooth, setIsOpenRenBooth] = useState(false);
  const handleCloseModal = () => {
    setIsOpenModal(false);
    setTimeout(() => {
      setCurrent(0);
    }, 100);
  };

  const onChangeProvine = async (value: string) => {
    const data = await axios.get(
      `https://provinces.open-api.vn/api/p/${value}?depth=2`
    );
    setListDistrict(data.data.districts);
    formCustomer.resetFields(["commune", "district"]);
  };

  const onChangeDistrict = async (value: string) => {
    const data = await axios.get(
      `https://provinces.open-api.vn/api/d/${value}?depth=2`
    );
    setListStreet(data.data.wards);
    formCustomer.resetFields(["commune"]);
  };

  const handleSetListPriceQuantity = async () => {
    const data = await getTicketPublic(postDetail.event.id);
    setListTicket(data.data);
    const listTemp = data.data.map((item: any) => {
      return {
        quantity: 0,
        price: item.price,
        idTicket: item.id,
        dataTicket: [],
      };
    });
    const listStatusTemp = data.data.map((item: any) => {
      console.log(moment(item.saleBeginDate).isAfter(new Date()));
      // console.log("BB",moment(new Date(), "DD/MM/YYYY HH:mm"))
      return moment(item.saleBeginDate).isAfter(new Date())
        ? STATUS_TICKET.WAITING
        : STATUS_TICKET.ENABLE;
    });
    setListStatusTicket(listStatusTemp);
    setDataTicketOrder(listTemp);
    setIsOpenModal(true);
  };

  const handleGetStatusRenBooth = async (idEvent: any) => {
    const data = await getStatusRenBooth(idEvent);
    setIsOpenRenBooth(data.data === "ENABLE");
  };

  const handleGetStatusSaleTicket = async (idEvent: any) => {
    const data: any = await getStatusSaleTicket(idEvent);
    setStatusSaleTicket(data.data === "ENABLE");
  };

  const handleSetTotalAmount = (list: any) => {
    setTotalPrice(
      list.reduce((total: any, currentValue: any) => {
        return (total += currentValue.quantity * currentValue.price);
      }, 0)
    );
    const totalTicket = list.reduce((total: any, currentValue: any) => {
      return (total += currentValue.quantity);
    }, 0);
    setTotalTicket(totalTicket);
    setIsEnableBtnNext(totalTicket !== 0);
  };

  const handleGetPostById = async () => {
    try {
      dispatch(setLoading(true));
      const data = await getPostById(idPost);
      await handleGetListSponsor(data.data.event.id);
      await handleGetListPostByEvent(data.data.event.id);
      await handleGetStatusSaleTicket(data.data.event.id);
      await handleGetStatusRenBooth(data.data.event.id);
      setPostDetail(data.data);
      setEventId(data.data.event.id);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const calulatorAmount = (value: any, idx: any, dataTicket: any) => {
    const listTemp =
      dataTicketOrder &&
      dataTicketOrder.map((item: any, elm) => {
        if (elm === idx) {
          return {
            quantity: value,
            price: item.price,
            idTicket: item.idTicket,
            dataTicket: value > 0 ? new Array(value).fill(dataTicket) : [],
          };
        } else {
          return item;
        }
      });
    setDataTicketOrder(listTemp);

    const listTicketOrder = listTemp.filter((item, idx) => item.quantity > 0);
    let orderDetailRequests: any = [];
    listTicketOrder.map((item, idx) => {
      if (item.quantity > 0) {
        orderDetailRequests = [
          ...orderDetailRequests,
          ...item.dataTicket.map((value: any, elm: any) => ({
            ...value,
            nameTicketIdx: `${value.name} #${elm + 1}`,
          })),
        ];
      }
    });
    setListOrderTicket(orderDetailRequests);
  };

  const handleNextStepThree = () => {
    form.submit();
  };

  const handleOpenModalTicket = () => {
    handleSetListPriceQuantity();
  };

  const handleSubmitStepTwo = (value: any) => {
    const payload = listOrderTicket.map((item: any, idx: any) => {
      return {
        email: value[`email_${idx}`],
        name: value[`name_${idx}`],
        phone: value[`phone_${idx}`],
        ticketId: item.id,
      };
    });
    setPayloadOrderDetail(payload);
    setCurrent(2);
  };

  const handleSubmitStepThree = async (value: any) => {
    dispatch(setLoading(true));
    try {
      const payload = {
        addressRequest: {
          commune: listStreet.find((item: any) => item.code === value.commune)
            ?.name,
          district: listDistrict.find(
            (item: any) => item.code === value.district
          )?.name,
          province: listProvince.find(
            (item: any) => item.code === value.province
          )?.name,
          street1: value.street1,
          street2: value.street2,
        },
        amount: totalPrice,
        email: value.email,
        eventId: postDetail.event.id,
        name: value.name,
        orderDetailRequests: [...payloadOrderDetail],
        phone: value.phone,
      };
      const data: any = await orderTicket(payload);
      if (data.errorCode === null) {
        setCurrent(3);
      } else {
        setCurrent(4);
      }
    } catch (e) {
      toastCustom("error", "Mua vé thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  const getListCityProvince = async () => {
    const data = await axios.get("https://provinces.open-api.vn/api/");
    setListProvince(data.data);
  };

  const handleGetListSponsor = async (idEvent: any) => {
    const data = await getListSponsorPublic(idEvent);
    setListSponsor(data.data);
  };

  const handleGetListPostByEvent = async (idEvent: any) => {
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      searchKey: null,
      size: 100,
      postType: TYPE_POST.EVENT,
    };
    const data = await getListPost(payload);
    setListPostByEvent(data.data.postPage.content);
  };

  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  useEffect(() => {
    handleGetPostById();
    getListCityProvince();
  }, [idPost]);

  useEffect(() => {
    if (dataTicketOrder) {
      handleSetTotalAmount(dataTicketOrder);
    }
  }, [dataTicketOrder]);

  return (
    <>
      <Row className="container-event">
        <Col className="content-post" span={16}>
          <div className="post-detail">
            <div
              dangerouslySetInnerHTML={{
                __html: postDetail?.content,
              }}
            />
            <Col
              style={{ margin: "2rem 0" }}
              className="add-action-team-member"
              span={24}
            >
              {isOpenRenBooth && (
                <div
                  onClick={() => navigate(`/booth/${eventId}`)}
                  style={{ marginRight: "10px" }}
                >
                  <ShopOutlined
                    style={{ fontSize: "18px", marginRight: "5px" }}
                  />{" "}
                  Đăng ký gian hàng
                </div>
              )}
              {statusSaleTicket && (
                <div onClick={handleOpenModalTicket}>
                  <TagsOutlined
                    style={{ fontSize: "18px", marginRight: "5px" }}
                  />{" "}
                  Mua vé
                </div>
              )}
            </Col>
          </div>

          <Col className="list-sponsor" span={24}>
            {listSponsor.map((item: any, idx: any) => (
              <Card
                key={idx}
                hoverable
                style={{ maxWidth: "170px" }}
                cover={
                  <div className="center-common">
                    <img
                      alt="example"
                      src={item.logoPath}
                      style={{
                        marginTop: "1rem",
                        width: "60px",
                        height: "60px",
                        objectFit: "cover",
                        borderRadius: "5px",
                      }}
                    />
                  </div>
                }
              >
                <Meta
                  title={<div style={{ fontSize: "12px" }}>{item.name}</div>}
                  description={
                    <div style={{ fontSize: "10px" }}>{item.link}</div>
                  }
                />
              </Card>
            ))}
          </Col>
        </Col>
        <Col className="container-post-most-view" span={7}>
          <div className="wrapper-title-list-post">
            <div className="title" style={{ fontSize: "19px" }}>
              Bài viết liên quan
            </div>
          </div>
          <div className="wrapper-list-new-post">
            <div className="post-over-view">
              {listPostByEvent
                ?.slice(
                  0,
                  listPostByEvent.length > 4 ? 4 : listPostByEvent.length
                )
                .map((item: any, idx: any) => (
                  <div
                    key={idx}
                    className="wrapper-animation-team"
                    // onClick={() => navigate(`/event-manage/${1}/team/${1}`)}
                  >
                    <Row justify={"space-between"} className="team-item">
                      <Col className="avatar-team" span={5}>
                        <Avatar
                          style={{ width: "100px", height: "100px" }}
                          shape="square"
                          size="large"
                          src={
                            item.overviewImagePath
                              ? item.overviewImagePath
                              : logo
                          }
                        />
                      </Col>
                      <Col className="info-team" span={18}>
                        <div
                          className="title"
                          onClick={() => {
                            navigate(`/post-detail/${item.id}`);
                          }}
                        >
                          {" "}
                          {subTitle(item.subject, MAX_LENGTH_TITLE)}
                        </div>
                        <p className="description">
                          {subTitle(
                            item.overviewDescription,
                            MAX_LENGTH_DESCRIPTION
                          )}
                        </p>
                        <div
                          className="description"
                          style={{ marginBottom: "12px", fontSize: "12px" }}
                        >
                          <span>
                            <ClockCircleOutlined />{" "}
                            {moment(item.createDate).format(FORMAT_DATE_FULL)}
                          </span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                ))}
            </div>
          </div>
        </Col>
      </Row>
      <Modal
        width={current === 3 ? 600 : 1300}
        title={"Mua vé"}
        open={isOpenModal}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModal}
        footer={null}
      >
        <>
          {listTicket.length > 0 && (
            <div>
              <div
                hidden={current !== 0}
                className="container-by-ticket-step-one"
              >
                <Divider className="divider" plain>
                  <span className="label-grayest">Các loại vé</span>
                </Divider>
                <div className="wrapper-list-ticket">
                  {listTicket.length > 0 &&
                    listTicket.map((item: any, idx) => (
                      <Row
                        className="wrapper-ticket-item"
                        key={idx}
                        style={{
                          borderTop:
                            idx % 2 !== 0 ? "none" : "1px solid #c5cccc",
                          borderBottom:
                            idx % 2 !== 0 ? "none" : "1px solid #c5cccc",
                        }}
                      >
                        <Col span={1} className="col-info">
                          <Tooltip
                            placement="topRight"
                            title={item.description}
                            arrowPointAtCenter
                          >
                            <InfoCircleOutlined
                              className="icon"
                              style={{ fontSize: "15px", width: "100%" }}
                              onClick={(e) => e.stopPropagation()}
                            />
                          </Tooltip>
                        </Col>
                        <Col span={10} className="col-info">
                          <Row className="label-grayest">
                            {item.name}
                            <TagsOutlined
                              className="icon"
                              style={{ fontSize: "16px", marginLeft: "6px" }}
                            />
                          </Row>
                          <Row
                            className="description-gray"
                            style={{
                              marginTop: "0.5rem",
                              // marginLeft: "1.4rem",
                            }}
                          >
                            {listStatusTicket[idx] === STATUS_TICKET.ENABLE && (
                              <span>
                                <span
                                  style={{
                                    color: "#285158",
                                    fontWeight: "600",
                                  }}
                                >
                                  {" "}
                                  Thời gian kết thúc bán vé:
                                </span>{" "}
                                {moment(item.saleEndDate).format(
                                  FORMAT_DATE_FULL
                                )}
                              </span>
                            )}
                            {listStatusTicket[idx] ===
                              STATUS_TICKET.WAITING && (
                              <span>
                                <span
                                  style={{
                                    color: "#285158",
                                    fontWeight: "600",
                                  }}
                                >
                                  Thời gian mở bán vé:
                                </span>{" "}
                                {moment(item.saleBeginDate).format(
                                  FORMAT_DATE_FULL
                                )}
                              </span>
                            )}
                          </Row>
                        </Col>
                        <Col span={4} className="col-info">
                          <div
                            className={`label--${
                              listStatusTicket[idx] === STATUS_TICKET.ENABLE
                                ? "accept"
                                : "absent"
                            }`}
                          >
                            {listStatusTicket[idx] === STATUS_TICKET.ENABLE
                              ? "Đang mở bán"
                              : "Chưa mở bán"}
                          </div>
                        </Col>
                        <Col span={4} className="col-info">
                          <span className="description-gray">
                            Số vé còn lại:{" "}
                            <span className="cost">
                              {item.remainingQuantity}
                            </span>
                          </span>
                        </Col>

                        <Col
                          span={3}
                          className="col-info cost description-gray"
                        >
                          {numberFormat(item.price)} vnđ
                        </Col>
                        <Col span={2} className="col-info">
                          <InputNumber
                            style={{ width: "50px" }}
                            size="small"
                            min={0}
                            max={100000}
                            onChange={(e: any) => calulatorAmount(e, idx, item)}
                            defaultValue={0}
                            disabled={
                              listStatusTicket[idx] === STATUS_TICKET.WAITING
                            }
                          />
                        </Col>
                      </Row>
                    ))}
                </div>
                <Col className="detail-order">
                  <Row style={{ marginBottom: "0.5rem" }}>
                    <span className="label-grayest">
                      Tổng số vé:{" "}
                      <span className="info-order">{totalTicket}</span>
                    </span>
                  </Row>
                  <Row>
                    <span className="label-grayest">
                      Tổng tiền:{" "}
                      <span className="info-order">
                        {numberFormat(totalPrice)} vnđ
                      </span>
                    </span>
                  </Row>
                </Col>
                <Row justify="center" style={{ width: "100%" }}>
                  <Col
                    style={{ marginTop: "2rem" }}
                    span={1}
                    className="action-step-two"
                  >
                    <Tooltip
                      placement="top"
                      title="Tiếp tục"
                      arrowPointAtCenter
                    >
                      <RightCircleOutlined
                        onClick={() => isEnableBtnNext && setCurrent(1)}
                        className={isEnableBtnNext ? "icon" : "icon-disable"}
                      />
                    </Tooltip>
                  </Col>
                </Row>
              </div>
              <div
                className="container-by-ticket-step-two"
                hidden={current !== 1}
              >
                <Form
                  form={form}
                  onFinish={(value) => handleSubmitStepTwo(value)}
                  autoComplete="off"
                  style={{ width: "100%" }}
                >
                  {listOrderTicket &&
                    listOrderTicket.map((dataTicket: any, idx: any) => (
                      <div style={{ marginBottom: "2rem" }} key={idx}>
                        <Divider className="divider" plain>
                          <span className="label-grayest">
                            Đăng ký - {dataTicket.nameTicketIdx}
                          </span>
                        </Divider>
                        <div className="ticket-item">
                          <Row justify="center">
                            {/* <Row justify="space-between"> */}
                            <Col
                              className="wrapper-field"
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={12}
                            >
                              <div className="field-add">
                                <div className="label-field">
                                  Tên<span style={{ color: "red" }}> *</span>
                                </div>
                                <Form.Item
                                  name={`name_${idx}`}
                                  className="text-field"
                                  rules={[
                                    {
                                      required: true,
                                      message: (
                                        <MessError
                                          message={"Vui lòng nhập tên!"}
                                        />
                                      ),
                                    },
                                    {
                                      max: 255,
                                      message: (
                                        <MessError
                                          message={
                                            "Tên không được quá 255 kí tự!"
                                          }
                                        />
                                      ),
                                    },
                                  ]}
                                >
                                  <Input className="input" placeholder="Tên" />
                                </Form.Item>
                              </div>
                            </Col>

                            <Col
                              className="wrapper-field"
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={12}
                            >
                              <div className="field-add">
                                <div className="label-field">
                                  Email
                                  <span style={{ color: "red" }}> *</span>
                                </div>
                                <Form.Item
                                  name={`email_${idx}`}
                                  className="text-field"
                                  rules={[
                                    {
                                      required: true,
                                      message: (
                                        <MessError
                                          message={"Vui lòng nhập email!"}
                                        />
                                      ),
                                    },
                                    {
                                      type: "email",
                                      message: (
                                        <MessError
                                          message={
                                            "Email không đúng định dạng!"
                                          }
                                        />
                                      ),
                                    },
                                  ]}
                                >
                                  <Input
                                    className="input"
                                    placeholder="Email"
                                  />
                                </Form.Item>
                              </div>
                            </Col>
                            {/* </Row> */}
                            {/* <Row justify="space-between"> */}
                            <Col
                              className="wrapper-field"
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={12}
                            >
                              <div className="field-add">
                                <div className="label-field">
                                  Số điện thoại
                                  <span style={{ color: "red" }}> *</span>
                                </div>
                                <Form.Item
                                  name={`phone_${idx}`}
                                  className="text-field"
                                  rules={[
                                    {
                                      required: true,
                                      message: (
                                        <MessError
                                          message={
                                            "Vui lòng nhập số điện thoại!"
                                          }
                                        />
                                      ),
                                    },
                                    {
                                      min: 9,
                                      message: (
                                        <MessError
                                          message={
                                            "Số điện thoại phải từ 9 đến 11 số!"
                                          }
                                        />
                                      ),
                                    },
                                    {
                                      max: 11,
                                      message: (
                                        <MessError
                                          message={
                                            "Số điện thoại phải từ 9 đến 11 số"
                                          }
                                        />
                                      ),
                                    },
                                    {
                                      pattern:
                                        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                                      message: (
                                        <MessError
                                          message={
                                            "Số điện thoại phải bao gồm các chữ số!"
                                          }
                                        />
                                      ),
                                    },
                                  ]}
                                >
                                  <Input
                                    className="input"
                                    placeholder="Số điện thoại:"
                                  />
                                </Form.Item>
                              </div>
                            </Col>

                            <Col
                              className="wrapper-field"
                              xs={24}
                              sm={24}
                              md={12}
                              lg={12}
                              xl={12}
                            >
                              <div className="field-add">
                                {/* <div className="label-field">
                                Câu hỏi của bạn dành cho sự kiện
                              </div>
                              <Form.Item
                                name={`question_${idx}`}
                                className="text-field"
                              >
                                <Input
                                  className="input"
                                  placeholder="Câu hỏi"
                                />
                              </Form.Item> */}
                              </div>
                            </Col>
                            {/* </Row> */}
                          </Row>
                        </div>
                      </div>
                    ))}
                </Form>
                <Row style={{ width: "100%", justifyContent: "space-evenly" }}>
                  <Col
                    style={{ marginTop: "2rem", display: "contents" }}
                    span={2}
                    className="action-step-two"
                  >
                    <Tooltip
                      placement="top"
                      title="Quay lại"
                      arrowPointAtCenter
                    >
                      <LeftCircleOutlined
                        className="icon"
                        onClick={() => setCurrent(0)}
                      />
                    </Tooltip>
                  </Col>
                  <Col
                    style={{ marginTop: "2rem", display: "contents" }}
                    span={2}
                    className="action-step-two"
                  >
                    <Tooltip
                      placement="top"
                      title="Tiếp tục"
                      arrowPointAtCenter
                    >
                      <RightCircleOutlined
                        className="icon"
                        onClick={() => handleNextStepThree()}
                      />
                    </Tooltip>
                  </Col>
                </Row>
              </div>
              <div
                className="container-by-ticket-step-three"
                hidden={current !== 2}
              >
                <Divider className="divider" plain>
                  <span className="label-grayest">Thông tin của bạn</span>
                </Divider>
                <div className="info-customer">
                  <Form
                    form={formCustomer}
                    onFinish={(value) => handleSubmitStepThree(value)}
                    autoComplete="off"
                    style={{ width: "100%" }}
                  >
                    <Row justify="center">
                      {/* <Row justify="space-between"> */}
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Tên<span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="name"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError message={"Vui lòng nhập tên!"} />
                                ),
                              },
                              {
                                max: 255,
                                message: (
                                  <MessError
                                    message={"Tên không được quá 255 kí tự!"}
                                  />
                                ),
                              },
                            ]}
                          >
                            <Input className="input" placeholder="Tên" />
                          </Form.Item>
                        </div>
                      </Col>

                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Email<span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="email"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError message={"Vui lòng nhập email!"} />
                                ),
                              },
                              {
                                max: 255,
                                message: (
                                  <MessError
                                    message={"Email không được quá 255 kí tự!"}
                                  />
                                ),
                              },
                            ]}
                          >
                            <Input className="input" placeholder="Email" />
                          </Form.Item>
                        </div>
                      </Col>
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Số điện thoại
                            <span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="phone"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError
                                    message={"Vui lòng nhập số điện thoại!"}
                                  />
                                ),
                              },
                              {
                                min: 9,
                                message: (
                                  <MessError
                                    message={
                                      "Số điện thoại phải từ 9 đến 11 số!"
                                    }
                                  />
                                ),
                              },
                              {
                                max: 11,
                                message: (
                                  <MessError
                                    message={
                                      "Số điện thoại phải từ 9 đến 11 số"
                                    }
                                  />
                                ),
                              },
                              {
                                pattern:
                                  /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
                                message: (
                                  <MessError
                                    message={
                                      "Số điện thoại phải bao gồm các chữ số!"
                                    }
                                  />
                                ),
                              },
                            ]}
                          >
                            <Input
                              className="input"
                              placeholder="Số điện thoại:"
                            />
                          </Form.Item>
                        </div>
                      </Col>

                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Tỉnh/Thành phố{" "}
                            <span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="province"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError
                                    message={"Vui lòng nhập tỉnh/thành phố!"}
                                  />
                                ),
                              },
                            ]}
                          >
                            <Select
                              className="select"
                              placeholder="Chọn Tỉnh/Thành phố"
                              showSearch
                              optionFilterProp="children"
                              onChange={onChangeProvine}
                              filterOption={(input, option: any) =>
                                (option?.children ?? "")
                                  ?.toLowerCase()
                                  .includes(input.toLowerCase())
                              }
                              // defaultValue={[1, 2]}
                              // value={["1", "2"]}
                              // onChange={handleChangeFeature}
                            >
                              {listProvince.map((value: any, idx: any) => (
                                <Option key={idx} value={value.code}>
                                  {value.name}
                                </Option>
                              ))}
                            </Select>
                          </Form.Item>
                        </div>
                      </Col>
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Quận/Huyện
                            <span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="district"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError
                                    message={"Vui lòng nhập quận/huyện!"}
                                  />
                                ),
                              },
                            ]}
                          >
                            <Select
                              className="select"
                              placeholder="Chọn Quận/Huyện"
                              showSearch
                              optionFilterProp="children"
                              onChange={onChangeDistrict}
                              filterOption={(input, option: any) =>
                                (option?.children ?? "")
                                  ?.toLowerCase()
                                  .includes(input.toLowerCase())
                              }
                            >
                              {listDistrict.map((value: any, idx: any) => (
                                <Option key={idx} value={value.code}>
                                  {value.name}
                                </Option>
                              ))}
                            </Select>
                          </Form.Item>
                        </div>
                      </Col>
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">
                            Xã/Phường
                            <span style={{ color: "red" }}> *</span>
                          </div>
                          <Form.Item
                            name="commune"
                            className="text-field"
                            rules={[
                              {
                                required: true,
                                message: (
                                  <MessError
                                    message={"Vui lòng nhập xã/phường!"}
                                  />
                                ),
                              },
                            ]}
                          >
                            <Select
                              className="select"
                              placeholder="Chọn Xã/Phường"
                              showSearch
                              optionFilterProp="children"
                              filterOption={(input, option: any) =>
                                (option?.children ?? "")
                                  ?.toLowerCase()
                                  .includes(input.toLowerCase())
                              }
                            >
                              {listStreet.map((value: any, idx: any) => (
                                <Option key={idx} value={value.code}>
                                  {value.name}
                                </Option>
                              ))}
                            </Select>
                          </Form.Item>
                        </div>
                      </Col>
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">Số nhà</div>
                          <Form.Item name="street2" className="text-field">
                            <Input className="input" placeholder="Số nhà" />
                          </Form.Item>
                        </div>
                      </Col>
                      <Col
                        className="wrapper-field"
                        xs={24}
                        sm={24}
                        md={12}
                        lg={12}
                        xl={12}
                      >
                        <div className="field-add" key={1}>
                          <div className="label-field">Thôn/Xóm</div>
                          <Form.Item name="street1" className="text-field">
                            <Input className="input" placeholder="Thôn/Xóm" />
                          </Form.Item>
                        </div>
                      </Col>
                      {/* </Row> */}
                    </Row>
                  </Form>
                </div>
                <Row
                  style={{
                    width: "100%",
                    justifyContent: "space-evenly",
                    marginTop: "2rem",
                  }}
                >
                  <Col
                    style={{ marginTop: "2rem", display: "contents" }}
                    span={2}
                    className="action-step-two"
                  >
                    <Tooltip
                      placement="top"
                      title="Quay lại"
                      arrowPointAtCenter
                    >
                      <LeftCircleOutlined
                        className="icon"
                        onClick={() => setCurrent(1)}
                      />
                    </Tooltip>
                  </Col>
                  <Col
                    style={{ marginTop: "2rem", display: "contents" }}
                    span={2}
                    className="action-step-two"
                  >
                    <Tooltip
                      placement="top"
                      title="Hoàn thành"
                      arrowPointAtCenter
                    >
                      <CheckCircleOutlined
                        className="icon"
                        onClick={() => formCustomer.submit()}
                      />
                    </Tooltip>
                  </Col>
                </Row>
              </div>
              <div
                hidden={current !== 3}
                className="container-buy-ticket-success"
              >
                <CheckCircleOutlined className="icon-success" size={200} />
                <div className="mess-success">
                  <p
                    style={{
                      fontSize: "17px",
                      fontWeight: "bold",
                      color: "#285158",
                    }}
                  >
                    Mua vé thành công!
                  </p>
                  <p className="description-gray">
                    {" "}
                    Chúng tôi sẽ gửi thông tin vé về email bạn đã đăng ký.
                  </p>
                </div>
              </div>
              <div
                hidden={current !== 4}
                className="container-buy-ticket-success"
              >
                <FrownOutlined className="icon-success" size={200} />
                <div className="mess-success">
                  <p
                    style={{
                      fontSize: "17px",
                      fontWeight: "bold",
                      color: "#285158",
                    }}
                  >
                    Mua vé không thành công!
                  </p>
                  <p className="description-gray">
                    {" "}
                    Số lượng vé hiện tại không đủ với.
                  </p>
                </div>
              </div>
            </div>
          )}
          {listTicket.length === 0 && (
            <div className="container-empty-data">
              <h2>Chưa mở bán vé</h2>
            </div>
          )}
        </>
      </Modal>
    </>
  );
}

export default PostDetailHomePage;
