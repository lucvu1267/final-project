import { message, Modal, Popconfirm, Tooltip } from "antd";
import React, { useEffect } from "react";
import "./detail-agenda.scss";
import {
  ClockCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { FORMAT_DATE_FULL } from "constants/index";
import { useDispatch, useSelector } from "react-redux";
import { ROLE_MEMBER } from "constants/event";
import { deleteAgenda } from "../eventManageSlice";
import { useParams } from "react-router-dom";
import { setLoading } from "components/common/loading/loadingSlice";
import { getMessStatus } from "mixin/generate";

function DetailAgenda(props: any) {
  const {
    event,
    fields,
    setAgendaDetail,
    setIsOpenModalUpdate,
    setIsCallbackApi,
  } = props;
  const dispatch = useDispatch();
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const { idEvent } = useParams();
  const handleCloseModal = () => {
    document.getElementById("btn")?.click();
  };

  const handleOpenUpdateAgenda = () => {
    setIsOpenModalUpdate(true);
    handleCloseModal();
  };

  const handleDeleteAgenda = async (idAgenda: any) => {
    dispatch(setLoading(true));
    await deleteAgenda(idEvent, idAgenda);
    setIsCallbackApi(true);
    handleCloseModal();
    dispatch(setLoading(false));
  };

  useEffect(() => {
    const elm = document.querySelectorAll('[data-testid="ClearRoundedIcon"]');
    elm[0]?.parentElement?.setAttribute("id", "btn");
    setAgendaDetail(event);
  }, []);

  return (
    <Modal
      width={600}
      title={"Chi tiết lịch trình"}
      open={true}
      destroyOnClose={true}
      centered={true}
      onCancel={handleCloseModal}
      footer={null}
    >
      <div className="wrapper-agenda-detail">
        <div className="label-grayest" style={{ fontSize: "18px" }}>
          {event.title}
        </div>
        <div
          className="description-gray"
          style={{ marginBottom: "12px", fontSize: "12px" }}
        >
          <span>
            <ClockCircleOutlined />{" "}
            {moment(event.createDate).format(FORMAT_DATE_FULL)}
          </span>
        </div>
        <div className="description-gray">{event.description}</div>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "end",
            marginTop: "10px",
          }}
        >
          {" "}
          <Popconfirm
            title="Bạn có chắc muốn xóa?"
            onConfirm={() => {
              if (roleUser.agendaAccess === ROLE_MEMBER.VIEW) {
                return message.error({
                  type: "error",
                  content: "Bạn không có quyền!",
                });
              } else if (
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              ) {
                return message.open({
                  type: "error",
                  content: getMessStatus(roleUser?.event?.status),
                });
              } else {
                handleDeleteAgenda(event.event_id);
              }
            }}
            okText="Xóa"
            showCancel={false}
          >
            <Tooltip placement="top" title="Xóa" arrowPointAtCenter>
              <DeleteOutlined
                className="icon-table"
                // onClick={() => handleOpenUpdateAgenda()}
              />
            </Tooltip>
          </Popconfirm>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              style={{
                marginLeft: "10px",
              }}
              className="icon-table"
              onClick={() => {
                if (roleUser.agendaAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                handleOpenUpdateAgenda();
              }}
            />
          </Tooltip>
        </div>
      </div>
    </Modal>
  );
}

export default DetailAgenda;
