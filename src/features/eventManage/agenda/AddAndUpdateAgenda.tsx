import { Col, Form, Input, Modal, Row, Select, Tooltip } from "antd";
import { useState } from "react";
import { DatePicker, Space } from "antd";
import React, { useEffect } from "react";
import locale from "antd/es/locale/vi_VN";
import { CheckCircleOutlined } from "@ant-design/icons";
import ConfigProvider from "antd/es/config-provider";
import moment from "moment";
import MessError from "components/common/MessError";
import {
  createAgenda,
  getAllMember,
  getListEventJoined,
  getTaskBySearch,
  updateAgenda,
} from "features/eventManage/eventManageSlice";
import { createTask } from "features/task/taskSlice";
import { toastCustom } from "config/toast";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { useParams } from "react-router-dom";
const { RangePicker } = DatePicker;
const { TextArea } = Input;
function AddAndUpdateAgenda(props: any) {
  const {
    scheduler,
    setIsCallbackApi,
    setIsOpenModalUpdate,
    agendaDetail,
    isModalUpdate = false,
  } = props;
  const { idEvent } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [idEventSelected, setIdEventSelected] = useState(null);

  const handleCloseModal = () => {
    setIsOpenModalUpdate && setIsOpenModalUpdate(false);
    scheduler && scheduler.close();
  };

  const handlecCreateTask = async (value: any) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        description: value.description,
        startDate: value.deadline[0]._d,
        endDate: value.deadline[1]._d,
        eventId: idEvent,
        name: value.name,
      };

      const payloadUpdate = {
        description: value.description,
        startDate: value.deadline[0]._d,
        endDate: value.deadline[1]._d,
        id: agendaDetail?.event_id,
        eventId: idEvent,
        name: value.name,
      };
      if (isModalUpdate) {
        await updateAgenda(payloadUpdate);
      } else {
        await createAgenda(payload);
      }
      setIsCallbackApi(true);
      handleCloseModal();
      setIsCallbackApi(true);
      toastCustom(
        "success",
        isModalUpdate
          ? "Cập nhật lịch trình thành công"
          : "Tạo lịch trình thành công"
      );
    } catch (error) {
      // toastCustom("error", "Tạo lịch trình thất bại!");
    } finally {
      dispatch(setLoading(false));
    }
  };

  useEffect(() => {
    if (isModalUpdate) {
      form.setFieldsValue({
        name: agendaDetail.title,
        deadline: [
          moment(agendaDetail.startDate),
          moment(agendaDetail.endDate),
        ],
        description: agendaDetail.description,
      });
    }
  }, []);

  return (
    <div>
      <Modal
        width={1300}
        title={scheduler ? "Tạo mới lịch trình" : "Cập nhật lịch trình"}
        open={true}
        onCancel={handleCloseModal}
        centered={true}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => form.submit()}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        {" "}
        <Form
          form={form}
          onFinish={(value) => handlecCreateTask(value)}
          autoComplete="off"
          style={{ width: "100%" }}
          initialValues={
            scheduler
              ? {
                  deadline: [
                    moment(scheduler.state.start.value),
                    moment(scheduler.state.end.value),
                  ],
                }
              : {}
          }
        >
          <div className="container-add-task">
            <Row className="wrapper-top">
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Tên lịch trình<span style={{ color: "red" }}> *</span>
                  </div>
                  <Form.Item
                    name="name"
                    className="text-field"
                    rules={[
                      {
                        required: true,
                        message: (
                          <MessError
                            message={"Vui lòng nhập tên lịch trình!"}
                          />
                        ),
                      },
                      {
                        max: 255,
                        message: (
                          <MessError
                            message={"Tên lịch trình không được quá 255 kí tự!"}
                          />
                        ),
                      },
                    ]}
                  >
                    <Input className="input" placeholder="Tên lịch trình" />
                  </Form.Item>
                </div>
              </Col>
              <Col span={12}>
                <div className="field-add">
                  <div className="label-field">
                    Thời gian hoàn thành<span style={{ color: "red" }}> *</span>
                  </div>
                  <Space direction="vertical" size={12}>
                    <ConfigProvider locale={locale}>
                      <Form.Item
                        name="deadline"
                        className="text-field"
                        rules={[
                          {
                            required: true,
                            message: (
                              <MessError
                                message={"Vui lòng chọn thời gian hoàn thành!"}
                              />
                            ),
                          },
                        ]}
                      >
                        <RangePicker
                          showTime={{ format: "HH:mm" }}
                          format="DD/MM/YYYY HH:mm"
                        />
                      </Form.Item>
                    </ConfigProvider>
                  </Space>
                </div>
              </Col>
            </Row>
            <Row className="wrapper-bottom" style={{ width: "100%" }}>
              <div className="label-field">
                Mô tả<span style={{ color: "red" }}> *</span>
              </div>
              <Form.Item
                name="description"
                className="text-field"
                rules={[
                  {
                    required: true,
                    message: (
                      <MessError message={"Vui lòng chi tiết lịch trình!"} />
                    ),
                  },
                  {
                    max: 500,
                    message: (
                      <MessError
                        message={
                          "Chi tiết lịch trình không được quá 500 kí tự!"
                        }
                      />
                    ),
                  },
                ]}
              >
                <TextArea
                  rows={8}
                  size="large"
                  placeholder="Chi tiết lịch trình"
                />
              </Form.Item>{" "}
            </Row>
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default AddAndUpdateAgenda;
