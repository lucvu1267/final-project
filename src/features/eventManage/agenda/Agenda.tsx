import { Scheduler } from "@aldabil/react-scheduler";
import { EVENTS, RESOURCES } from "features/task/events";
import { vi } from "date-fns/esm/locale";
import { Divider, Modal, Row } from "antd";
import "./agenda.scss";
import AddAndUpdateAgenda from "./AddAndUpdateAgenda";
import React from "react";
import { useState, useEffect } from "react";
import DetailAgenda from "./DetailAgenda";
import { ROLE_MEMBER } from "constants/event";
import { useSelector } from "react-redux";

function Agenda(props: any) {
  const { handleGetListAgenda, listAgenda } = props;
  const [isOpenModalUpdate, setIsOpenModalUpdate] = useState(false);
  const [isCallbackApi, setIsCallbackApi] = useState(false);
  const [agendaDetail, setAgendaDetail] = useState(null);
  const roleUser = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );
  const handleCloseModal = () => {
    setIsOpenModalUpdate(false);
  };

  useEffect(() => {
    if (isCallbackApi) {
      handleGetListAgenda();
    }
    setIsCallbackApi(false);
  }, [isCallbackApi]);

  return (
    <>
      <Row className="wrapper-title-agenda">
        <div className="title">Lịch trình sự kiện</div>
      </Row>
      <div className="wrapper-table-agenda">
        {/* <HeaderFeature  content={"Lịch trình sự kiện"}/> */}
        <Scheduler
          deletable={false}
          locale={vi}
          resourceViewMode="default"
          events={listAgenda}
          view={"month"}
          day={
            {
              startHour: 0, 
              endHour: 23, 
              step: 60,
              // cellRenderer?:(props: CellProps) => JSX.Element,
              navigation: true
              }
          }
          customEditor={
            roleUser.agendaAccess === ROLE_MEMBER.EDIT &&
            roleUser?.event?.status === "INPROGRESS"
              ? (scheduler) => (
                  <AddAndUpdateAgenda
                    scheduler={scheduler}
                    setIsCallbackApi={setIsCallbackApi}
                    handleGetListAgenda={handleGetListAgenda}
                  />
                )
              : (scheduler) => <div></div>
          }
          onConfirm={async (event, action) => {
            return new Promise((res, rej) => {
              setTimeout(() => {}, 1000);
            });
          }}
          dialogMaxWidth="lg"
          viewerExtraComponent={(fields: any, event: any) => (
            <DetailAgenda
              event={event}
              fields={fields}
              setIsOpenModalUpdate={setIsOpenModalUpdate}
              setAgendaDetail={setAgendaDetail}
              setIsCallbackApi={setIsCallbackApi}
            />
          )}
          onDelete={async (id) => {
            await new Promise((res, rej) => {
              console.log("onDelete");
              setTimeout(() => {
                // setEvents((prev) => {
                //   return prev.filter((p) => p.event_id !== id);
                // });
                res("");
              }, 1000);
            });
          }}
          translations={{
            navigation: {
              month: "Tháng",
              week: "Tuần",
              day: "Ngày",
              today: "Hôm nay",
            },
            form: {
              addTitle: "Novo Evento",
              editTitle: "Editar Evento",
              confirm: "Confirmar",
              delete: "Excluir",
              cancel: "Cancelar",
            },
            event: {
              title: "Título",
              start: "Início",
              end: "Fim",
              allDay: "",
            },
            moreEvents: "Xem thêm...",
          }}
        />
        {/* <Scheduler events={EVENTS} /> */}
      </div>
      {/* modal update */}
      {isOpenModalUpdate && (
        <AddAndUpdateAgenda
          setIsCallbackApi={setIsCallbackApi}
          setIsOpenModalUpdate={setIsOpenModalUpdate}
          handleGetListAgenda={handleGetListAgenda}
          agendaDetail={agendaDetail}
          isModalUpdate={true}
        />
      )}
    </>
  );
}

export default Agenda;
