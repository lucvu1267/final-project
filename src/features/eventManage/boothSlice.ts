import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const addLocation = async (payload: any) => {
  return await axiosInstance.post(`booth/location/create`, payload);
};

export const updateLocation = async (payload: any) => {
  return await axiosInstance.post(`booth/location/update`, payload);
};

export const getListLocations = async (payload: any) => {
  return await axiosInstance.post(`booth/location/search`, payload);
};

export const deleteLocationById = async (idEvent: any, idLocation: any) => {
  return await axiosInstance.post(
    `booth/location/delete/${idEvent}/${idLocation}`
  );
};

export const getListBooths = async (payload: any) => {
  return await axiosInstance.post(`booth/search`, payload);
};

export const getListBoothPublic = async (payload: any) => {
  return await axiosInstance.post(`booth/get-all-in-event`, payload);
};

export const createBooth = async (payload: any) => {
  return await axiosInstance.post(`booth/create`, payload);
};

export const updateBooth = async (payload: any) => {
  return await axiosInstance.post(`booth/update`, payload);
};

export const deleteBooth = async (idEvent: any, idBooth: any) => {
  return await axiosInstance.post(`booth/delete/${idEvent}/${idBooth}`);
};

export const getMap = async (idEvent: any) => {
  return await axiosInstance.get(`booth/get-map-path/${idEvent}`);
};

export const uploadMap = async (payload: any) => {
  return await axiosInstance.post(`booth/save-map-path`, payload);
};

export const registerBooth = async (payload: any) => {
  return await axiosInstance.post(`order/booth`, payload);
};

export const getBoothOrder = async (payload: any) => {
  return await axiosInstance.post(`booth/orders`, payload);
};

export const getStatusRenBooth = async (eventId: any) => {
  return await axiosInstance.get(`booth/get-rent-booth/${eventId}`);
};

export const updateRenBoothStatus = async (eventId: any) => {
  return await axiosInstance.post(`booth/change-status-rent-booth/${eventId}`);
};

export const acceptBoothOrder = async (eventId: any, id: any) => {
  return await axiosInstance.post(`booth/order/accept/${eventId}/${id}`);
};

export const rejectBoothOrder = async (eventId: any, id: any) => {
  return await axiosInstance.post(`booth/order/reject/${eventId}/${id}`);
};

export const boothSlice = createSlice({
  name: "loading",
  initialState: {},
  reducers: {
    // setLoading: (state, action) => {
    //   state.isLoading = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = boothSlice.actions;

export default boothSlice.reducer;
