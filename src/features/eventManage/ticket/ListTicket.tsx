import React, { useState, useEffect } from "react";
import {
  Button,
  Dropdown,
  message,
  Modal,
  Row,
  Switch,
  Table,
  Tabs,
  Tooltip,
} from "antd";
import {
  PlusOutlined,
  EyeOutlined,
  EditOutlined,
  PlusCircleFilled,
  SettingFilled,
  TagsOutlined
} from "@ant-design/icons";
import "./list-ticket.scss";
import DrawerTicketOrder from "./drawer/DrawerTicketOrder";
import CreateAndUpdateTicket from "components/event/ticket/CreateAndUpdateTicket";
import { CheckCircleOutlined } from "@ant-design/icons";
import DrawerTicketDetail from "./drawer/DrawerTicketDetail";
import { FORMAT_DATE_FULL, TYPE_FORM } from "constants/index";
import { useNavigate, useParams } from "react-router-dom";
import {
  getListTicketsByEvent,
  getStatusSaleTicket,
  getTicketById,
  getTicketOrder,
  getTicketOrderDetail,
  updateStatusSaleTicket,
  updateTicket,
} from "../eventManageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import moment from "moment";
import { getMessStatus, numberFormat } from "mixin/generate";
import Search from "antd/lib/input/Search";
import { ROLE_MEMBER } from "constants/event";

const TAB = {
  TICKET: "1",
  TICKET_ORDER: "2",
};

function ListTicket(props: any) {
  const { idEvent } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const roleUser: any = useSelector(
    (state: any) => state.eventManageSlice.userEventPops
  );

  const [typeModal, setTypeModal] = useState(TYPE_FORM.CREATE);
  const [statusSaleTicket, setStatusSaleTicket] = useState(false);
  const [openDrawerTicketOrderDetail, setPpenDrawerTicketOrderDetail] =
    useState(false);
  const [openDrawerTicketDetail, setOpenDrawerTicketDetail] = useState(false);
  const [openModalAddTicket, setOpenModalAddTicket] = useState(false);
  const [isCreateUpdateTicket, setIsCreateUpdateTicket] = useState(false);
  const [isUpdateDataTable, setIsUpdateDataTable] = useState(true);
  const [codeOrder, setCodeOrder] = useState(null);
  const [tab, setTab] = useState(TAB.TICKET);
  const [listTickets, setListTickets] = useState<any[]>([]);
  const [listTicketsOrder, setListTicketsOrder] = useState<any[]>([]);
  const [ticketDetail, setTicketDetail] = useState({});
  const [tickeOrdertDetail, setTickeOrdertDetail] = useState<any[]>([]);

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            if (roleUser.ticketAccess === ROLE_MEMBER.VIEW) {
              return message.error({
                type: "error",
                content: "Bạn không có quyền!",
              });
            } else if (
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            ) {
              return message.open({
                type: "error",
                content: getMessStatus(roleUser?.event?.status),
              });
            }
            setOpenModalAddTicket(true);
            setTypeModal(TYPE_FORM.CREATE);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm vé sự kiện
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div
          style={{
            fontWeight: "600",
            color: "#285158",
            display: "flex",
            alignItems: "center",
          }}
        >
          Mở bán vé:{" "}
          <Switch
            onChange={() => handleUpdateSaleTicket()}
            disabled={
              roleUser.ticketAccess === ROLE_MEMBER.VIEW ||
              roleUser?.event?.status === "DONE" ||
              roleUser?.event?.status === "CANCEL"
            }
            style={{ marginLeft: "7px" }}
            size="small"
            defaultChecked={statusSaleTicket}
          />
        </div>
      ),
    },
  ];

  const handleUpdateSaleTicket = async () => {
    await updateStatusSaleTicket(idEvent);
  };

  const handleUpdateStatus = async (value: any, isActive: any) => {
    const payloadUpdate = {
      eventId: idEvent,
      id: value.id,
      description: value.description,
      limited: value.limited,
      name: value.name,
      price: parseInt((value.price + "").split(".").join("")),
      saleBeginDate: value.saleBeginDate,
      saleEndDate: value.saleEndDate,
      status: isActive ? "ACTIVE" : "NOT_ACTIVE",
    };
    await updateTicket(payloadUpdate);
  };

  const handleGetStatusSaleTicket = async () => {
    const data: any = await getStatusSaleTicket(idEvent);
    setStatusSaleTicket(data.data === "ENABLE");
  };

  const columnsTicket: any = [
    { title: "Tên vé", 
    render: (value: any) => (
      <>
        <TagsOutlined style={{ fontSize: "18px", marginRight: "10px" }} />
        <span className="label-grayest">{value.name}</span>
      </>
    ),
  },
    {
      title: "Người tạo",
      key: "creatorContact.account.email",
      render: (value: any) => <div>{value.creator.account.email}</div>,
    },
    {
      title: "Ngày tạo",
      key: "createDate",
      render: (value: any) => (
        <div>{moment(value.createDate).format("DD/MM/YYYY HH:mm")}</div>
      ),
    },
    { title: "Số lượng vé giới hạn", dataIndex: "limited", key: "limited" },
    {
      title: "Giá",
      render: (value: any) => <div className="label-grayest">{numberFormat(value.price)} vnđ</div>,
    },
    {
      title: "Vé đã bán",
      dataIndex: "soldQuantity",
      key: "soldQuantity",
    },
    {
      title: "Vé còn lại",
      dataIndex: "remainingQuantity",
      key: "remainingQuantity",
    },
    {
      title: "Thời gian bán vé",
      render: (value: any) => (
        <div>
          {moment(value.saleBeginDate).format("DD/MM/YYYY HH:mm")} -{" "}
          {moment(value.saleEndDate).format("DD/MM/YYYY HH:mm")}
        </div>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "",
      key: "y",
      render: (value: any) => (
        <div>
          <Tooltip placement="top" title="Vé hoạt động" arrowPointAtCenter>
            <Switch
              disabled={
                roleUser.ticketAccess === ROLE_MEMBER.VIEW ||
                roleUser?.event?.status === "DONE" ||
                roleUser?.event?.status === "CANCEL"
              }
              defaultChecked={value.status === "ACTIVE"}
              onChange={(isActive) => handleUpdateStatus(value, isActive)}
            />
          </Tooltip>
        </div>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value: any) => (
        <div className="operator-wrapper">
          <Tooltip placement="top" title="Xem chi tiết" arrowPointAtCenter>
            <EyeOutlined
              className="icon-table"
              onClick={() => {
                getTicketDetail(value.id, 1);
              }}
            />
          </Tooltip>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              className="icon-table"
              onClick={() => {
                if (roleUser.ticketAccess === ROLE_MEMBER.VIEW) {
                  return message.error({
                    type: "error",
                    content: "Bạn không có quyền!",
                  });
                } else if (
                  roleUser?.event?.status === "DONE" ||
                  roleUser?.event?.status === "CANCEL"
                ) {
                  return message.open({
                    type: "error",
                    content: getMessStatus(roleUser?.event?.status),
                  });
                }
                getTicketDetail(value.id, 2);
              }}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  const columnsOrderTicket: any = [
    { title: "Mã đơn hàng", render: (value: any) => <div className="label-grayest">[{value.code}]</div> },
    {
      title: "Ngày mua",
      render: (value: any) => (
        <div> {moment(value.createDate).format(FORMAT_DATE_FULL)}</div>
      ),
    },
    { title: "Số lượng", dataIndex: "quantity", key: "quantity" },
    {
      title: "Tổng tiền",
      render: (value: any) => <div className="label-grayest">{numberFormat(value.amount)} vnđ</div>,
    },
    { title: "Số điện thoại", dataIndex: "phone", key: "phone" },
    { title: "Email khách hàng", dataIndex: "email", key: "email" },
    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value: any) => (
        <div className="operator-wrapper">
          <Tooltip placement="top" title="Xem chi tiết" arrowPointAtCenter>
            <EyeOutlined
              className="icon-table"
              onClick={() => {
                setCodeOrder(value.code);
                handleGetTicketOrderDetail(value.id);
              }}
            />
          </Tooltip>
          {/* <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined className="icon-table" />
          </Tooltip> */}
        </div>
      ),
    },
  ];

  const handleGetListTicket = async () => {
    const payload = {
      eventId: idEvent,
      orders: [],
      page: 0,
      size: 100,
    };
    return await getListTicketsByEvent(payload);
  };

  const handleGetListTicketOrder = async () => {
    const payload = {
      eventId: idEvent,
      keySearch: null,
      orders: [],
      page: 0,
      size: 100,
    };
    return await getTicketOrder(payload);
  };

  const handleGetListTicketOrderSearch = async (
    keySearchTicketOrder = null
  ) => {
    dispatch(setLoading(true));
    const payload = {
      eventId: idEvent,
      keySearch: keySearchTicketOrder,
      orders: [],
      page: 0,
      size: 100,
    };
    const data = await getTicketOrder(payload);
    setListTicketsOrder(data.data.page.content);
    dispatch(setLoading(false));
  };

  const getTicketDetail = async (id: any, type: any) => {
    try {
      const data = await getTicketById(id);
      if (data) {
        setTicketDetail(data.data);
      }
      if (type === 1) {
        showDrawerTicket();
      } else {
        setOpenModalAddTicket(true);
        setTypeModal(TYPE_FORM.UPDATE);
      }
    } catch (error) {}
  };

  const handleGetTicketOrderDetail = async (id: any) => {
    try {
      const data = await getTicketOrderDetail(id);
      if (data) {
        setTickeOrdertDetail(data.data);
      }
      showDrawerTicketOrder();
    } catch (error) {}
  };

  const onChange = (key: string) => {
    setTab(key);
  };

  const showDrawerTicketOrder = () => {
    setPpenDrawerTicketOrderDetail(true);
  };

  const showDrawerTicket = () => {
    setOpenDrawerTicketDetail(true);
  };

  const onCloseDrawerTicketOrder = () => {
    setPpenDrawerTicketOrderDetail(false);
  };

  const onCloseDrawerTicket = () => {
    setOpenDrawerTicketDetail(false);
  };

  let locale = {
    emptyText: <h2> Không có dữ liệu</h2>,
  };

  useEffect(() => {
    if (roleUser.ticketAccess === ROLE_MEMBER.NONE) {
      return navigate("/not-permission");
    }
    if (isUpdateDataTable) {
      dispatch(setLoading(true));
      Promise.all([
        handleGetListTicket(),
        handleGetListTicketOrder(),
        handleGetStatusSaleTicket(),
      ])
        .then((value) => {
          setListTickets(value[0].data.ticketPage.content);
          setListTicketsOrder(value[1].data.page.content);
        })
        .finally(() => dispatch(setLoading(false)));
      handleGetListTicket();
      setIsUpdateDataTable(false);
    }
  }, [isUpdateDataTable]);

  return (
    <>
      {tab === TAB.TICKET && (
        <Dropdown menu={{ items }} placement="bottomLeft" arrow>
          <SettingFilled className="icon" style={{ fontSize: "22px" }} />
        </Dropdown>
      )}
      {tab === TAB.TICKET_ORDER && (
        <Row className="wrapper-action" style={{ marginBottom: "0px" }}>
          <Search
            className="search search-40px"
            placeholder="Mã đơn hàng, email"
            enterButton
            size="large"
            onSearch={(value: any) => handleGetListTicketOrderSearch(value)}
          />
        </Row>
      )}
      {listTickets && (
        <Tabs
          className="container-ticket"
          defaultActiveKey="1"
          type="card"
          onChange={onChange}
          items={[
            {
              label: `Danh sách vé`,
              key: "1",
              children: (
                <Row className="container-post">
                  <Table
                    locale={locale}
                    className="table-post"
                    style={{ width: "100vw" }}
                    columns={columnsTicket}
                    rowKey={(record: any) => record.id}
                    expandable={{
                      expandedRowRender: (record) => (
                        <p key={record.id} style={{ margin: 0 }}>
                          <span className="label-grayest">Mô tả vé: </span>{" "}
                          {record.description}
                        </p>
                      ),
                      rowExpandable: (record) =>
                        record.name !== "Not Expandable",
                    }}
                    dataSource={listTickets}
                    pagination={{ position: ["bottomCenter"] }}
                  />
                </Row>
              ),
            },
            {
              label: `Danh sách đặt vé`,
              key: "2",
              children: (
                <Row className="container-post">
                  <Table
                    className="table-post"
                    style={{ width: "100vw" }}
                    rowKey={(record: any) => record.id}
                    columns={columnsOrderTicket}
                    expandable={{
                      expandedRowRender: (record) => (
                        <p style={{ margin: 0 }}>
                          <span className="label-grayest">Địa chỉ: </span>{" "}
                          {`${
                            record.address.street1
                              ? record.address.street1 + ", "
                              : ""
                          }${
                            record.address.street2
                              ? record.address.street2 + ", "
                              : ""
                          }${
                            record.address.commune +
                            ", " +
                            record.address.district +
                            ", " +
                            record.address.province
                          }`}
                        </p>
                      ),
                      rowExpandable: (record) =>
                        record.name !== "Not Expandable",
                    }}
                    dataSource={listTicketsOrder}
                    pagination={{ position: ["bottomCenter"] }}
                  />
                </Row>
              ),
            },
          ]}
        />
      )}
      <DrawerTicketOrder
        open={openDrawerTicketOrderDetail}
        onClose={onCloseDrawerTicketOrder}
        dataDetal={tickeOrdertDetail}
        codeOrder={codeOrder}
      />
      <DrawerTicketDetail
        open={openDrawerTicketDetail}
        onClose={onCloseDrawerTicket}
        dataDetal={ticketDetail}
      />
      <Modal
        width={1000}
        title={
          typeModal === TYPE_FORM.CREATE
            ? "Tạo mới vé"
            : "Cập nhật thông tin vé"
        }
        open={openModalAddTicket}
        destroyOnClose={true}
        centered={true}
        onCancel={() => setOpenModalAddTicket(false)}
        footer={[
          <Tooltip placement="top" title="Hoàn thành" arrowPointAtCenter>
            <CheckCircleOutlined
              onClick={() => setIsCreateUpdateTicket(true)}
              className="icon"
            />
          </Tooltip>,
        ]}
      >
        <CreateAndUpdateTicket
          dataDetal={ticketDetail}
          isCreateUpdateTicket={isCreateUpdateTicket}
          setIsCreateUpdateTicket={setIsCreateUpdateTicket}
          setIsUpdateDataTable={setIsUpdateDataTable}
          setOpenModalAddTicket={setOpenModalAddTicket}
          typeModal={typeModal}
        />
      </Modal>
    </>
  );
}

export default ListTicket;
