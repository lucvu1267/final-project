import { Divider, Drawer } from "antd";

function DrawerTicketOrder(props: any) {
  const { open, onClose, dataDetal, codeOrder } = props;
  return (
    <Drawer
      title={`Thông tin hóa đơn ${codeOrder}`}
      placement="right"
      onClose={onClose}
      open={open}
      size={"large"}
    >
      <div className="drawer-contact-detail">
        {dataDetal &&
          dataDetal.map((item: any, idx: any) => (
            <div key={idx}>
              <Divider className="divider" plain>
                <span className="label-grayest">{item.ticketName}</span>
              </Divider>
              <div className="wrapper-detail">
                <div className="row-info field">
                  <span>Tên: </span>
                </div>
                <div className="row-info infor">
                  <span>{item.name}</span>
                </div>
                <div className="row-info field">
                  <span>Email: </span>
                </div>
                <div className="row-info infor">
                  <span>{item.email}</span>
                </div>
                <div className="row-info field">
                  <span>Số điện thoại: </span>
                </div>
                <div className="row-info infor">
                  <span>{item.phone}</span>
                </div>
              </div>
            </div>
          ))}
      </div>
    </Drawer>
  );
}

export default DrawerTicketOrder;
