import { Divider, Drawer } from "antd";
import { FORMAT_DATE_FULL } from "constants/index";
import moment from "moment";
import { numberFormat } from "mixin/generate";

function DrawerTicketDetail(props: any) {
  const { open, onClose, dataDetal } = props;
  return (
    <Drawer
      title={"Thông tin Vé Vip"}
      placement="right"
      onClose={onClose}
      open={open}
      size={"default"}
    >
      <div className="drawer-contact-detail">
        <div>
          {/* <Divider className="divider" plain>
            <span className="label-grayest">{item.typeTicket}</span>
          </Divider> */}
          <div className="wrapper-detail">
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Tên: </span>
            </div>
            <div className="row-info infor">
              <span>{dataDetal?.name}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Người tạo: </span>
            </div>
            <div className="row-info infor">
              <span>{dataDetal?.creator?.email}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Ngày tạo: </span>
            </div>
            <div className="row-info infor">
              <span>
                {moment(dataDetal?.creator?.createDate).format(
                  FORMAT_DATE_FULL
                )}
              </span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Giá vé: </span>
            </div>
            <div className="row-info infor">
              <span>{numberFormat(dataDetal?.price)} vnđ</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Số lượng vé giới hạn: </span>
            </div>
            <div className="row-info infor">
              <span>{dataDetal?.limited}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field" style={{ marginBottom: "0.5rem" }}>
              <span>Vé đã bán: </span>
            </div>
            <div className="row-info infor">
              <span>{dataDetal?.soldQuantity}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field " style={{ marginBottom: "0.5rem" }}>
              <span>Vé còn lại: </span>
            </div>
            <div className="row-info infor">
              <span>{dataDetal?.remainingQuantity}</span>
            </div>
            <Divider className="divider" plain></Divider>
            <div className="row-info field " style={{ marginBottom: "0.5rem" }}>
              <span>Thời gian mở bán vé: </span>
            </div>
            <div className="row-info infor">
              <span>
                từ {moment(dataDetal?.saleBeginDate).format("DD/MM/YYYY HH:mm")}{" "}
                đến {moment(dataDetal?.saleEndDate).format("DD/MM/YYYY HH:mm")}
              </span>
            </div>
          </div>
        </div>
      </div>
    </Drawer>
  );
}

export default DrawerTicketDetail;
