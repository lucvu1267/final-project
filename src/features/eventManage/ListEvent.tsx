import { Button, Cascader, Dropdown, Modal, Row, Tooltip } from "antd";
import "./list-event.scss";
import React, { useEffect, useState } from "react";
import Search from "antd/lib/input/Search";
import { Select } from "antd";
import EventItem from "components/event/EventItem";
import { SettingFilled, FilterOutlined, PlusCircleFilled } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getListEventManager } from "./eventManageSlice";
import { setLoading } from "components/common/loading/loadingSlice";
import EmptyData from "components/common/emptyData/EmptyData";

const { Option } = Select;

export function ListEvent(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [listEvent, setListEvent] = useState([]);

  const handleResetFilter = async () => {
    dispatch(setLoading(true));
    setKeySearchEvents(null);
    setValueSatus(null);
    await handleGetListEvent();

    dispatch(setLoading(false));
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            handleResetFilter();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <FilterOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Xóa bộ lọc
        </div>
      ),
    },

    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            navigate("/event-manage/add")
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm sự kiện
        </div>
      ),
    },
  ];

  useEffect(() => {
    handleGetListEvent();
  }, []);

  const handleGetListEvent = async (keySearchEvents = null, status = null) => {
    dispatch(setLoading(true));
    const dataPayload = {
      orders: [],
      page: 0,
      searchKey: keySearchEvents,
      size: 1000,
      status: status,
    };
    try {
      const data = await getListEventManager(dataPayload);
      setListEvent(data.data.eventPage.content);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };
  const [keySearchEvents, setKeySearchEvents] = useState<any>(null);
  const [valueSatus, setValueSatus] = useState(null);

  return (
    <>
      <Row className="wrapper-action-event">
        <Search
          className="search search-40px"
          placeholder="Tìm kiếm theo tên"
          enterButton
          size="large"
          value={keySearchEvents}
          // value={keySearchEvents}
          onChange={(e: any) => setKeySearchEvents(e.target.value)}
          onSearch={() => handleGetListEvent(keySearchEvents, valueSatus)}
        />{" "}
        <Select
          className="select"
          showSearch
          placeholder="Trạng thái"
          optionFilterProp="children"
          value={valueSatus}
          onChange={(value) => {
            setValueSatus(value);
            handleGetListEvent(keySearchEvents, value);
          }}
          filterOption={(input, option) =>
            (option!.children as unknown as string)
              .toLowerCase()
              .includes(input.toLowerCase())
          }
        >
          <Option value="INPROGRESS">Đang hoạt động</Option>
          <Option value="DONE">Đã hoàn thành</Option>
          <Option value="CANCEL">Đã hủy</Option>
        </Select>
        <Dropdown menu={{ items }} placement="bottomLeft" arrow>
            <SettingFilled className="icon" style={{ fontSize: "22px" }} />
          </Dropdown>
      </Row>
      <Row className="container-event">
        {listEvent.length === 0 && <EmptyData />}
        <div className="wrapper-event">
          {listEvent?.length > 0 &&
            listEvent.map((item, index) => {
              return <EventItem key={index} eventDetail={item}></EventItem>;
            })}
        </div>
      </Row>
    </>
  );
}
