import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const getInfoUser = async () => {
  return await axiosInstance.post("contact/load-profile");
};

export const createContact = async (payload: any) => {
  return await axiosInstance.post("contact/create", payload);
};

export const updateContact = async (payload: any) => {
  return await axiosInstance.post("contact/update", payload);
};

export const deleteContact = async (idContact: any) => {
  return await axiosInstance.post(`contact/remove-other-user/${idContact}`);
};

export const updateProfile = async (payload: any) => {
  return await axiosInstance.post("contact/update-profile", payload);
};

export const getContactId = async (id: any) => {
  return await axiosInstance.post(`contact/${id}`);
};

export const addContact = async (id: any) => {
  return await axiosInstance.post(`contact/add-other-user/${id}`);
};

export const contactSlice = createSlice({
  name: "contact",
  initialState: {
    user: {},
  },
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUser } = contactSlice.actions;

export default contactSlice.reducer;
