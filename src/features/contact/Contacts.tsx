import {
  Button,
  Cascader,
  Dropdown,
  Form,
  Modal,
  Row,
  Select,
  Spin,
  Tooltip,
} from "antd";
import Contact from "components/contact/Contact";
import "./contacts.scss";
import React, { useEffect, useState } from "react";
import Search from "antd/lib/input/Search";
import {
  PlusOutlined,
  PlusCircleFilled,
  FilterOutlined,
  SettingFilled,
} from "@ant-design/icons";
import AddContact from "components/contact/addContact/AddContact";
import { setLoading } from "components/common/loading/loadingSlice";
import { useDispatch } from "react-redux";
import EmptyData from "components/common/emptyData/EmptyData";
import {
  getAllMember,
  getListEventJoined,
  getTeamByEvent,
  getUserInSystem,
} from "features/eventManage/eventManageSlice";
import OptionEventItem from "components/common/optionItem/OptionEventItem";

interface OptionInterface {
  value: number;
  label: any;
  children?: OptionInterface[];
  isLeaf?: boolean;
  loading?: boolean;
}

const { Option } = Select;

export function Contacts(props: any) {
  const dispatch = useDispatch();

  const [form] = Form.useForm();
  const [isOpenModalAddContact, setIsOpenModalAddContact] = useState(false);
  const [isCallBackApi, setIsCallBackApi] = useState(false);
  const [listContact, setListContact] = useState([]);
  const [listEvent, setListEvent] = useState<any>([]);
  const [listTeam, setListTeam] = useState<any>([]);
  const [keySearchContact, setKeySearchContact] = useState(null);
  const [eventSelected, setEventSelected] = useState(null);
  const [teamSelected, setTeamSelected] = useState(null);

  const handleGetListEventJoined = async () => {
    const data = await getListEventJoined();
    setListEvent(data.data);
  };

  useEffect(() => {
    handleGetListEventJoined();
  }, []);

  useEffect(() => {
    setListTeam([]);
    setKeySearchContact(null);
    setEventSelected(null);
    setTeamSelected(null);
    handleGetListContact();
    setIsCallBackApi(false);
  }, [isCallBackApi]);

  const handleGetListContact = async (
    eventSelected = null,
    teamSelected = null,
    keySearchContact = null
  ) => {
    try {
      dispatch(setLoading(true));
      const payload = {
        eventId: eventSelected,
        // isUser: true,
        keyword: keySearchContact,
        findAddedUser: true,
        orders: [],
        page: 0,
        size: 100,
        teamId: teamSelected,
      };
      const data = await getUserInSystem(payload);
      if (data) {
        setListContact(data.data.contactPage.content);
      }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleCloseModalAddConatct = () => {
    setIsOpenModalAddContact(false);
  };

  const handleGetListTeamByEvent = async (eventId: any) => {
    try {
      setEventSelected(eventId);
      setTeamSelected(null);
      dispatch(setLoading(true));
      await Promise.all([
        getTeamByEvent(eventId),
        handleGetListContact(eventId, null, keySearchContact),
      ])
        .then((value: any) => {
          setListTeam(value[0].data);
        })
        .finally(() => dispatch(setLoading(false)));
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetContatcByTeam = async (teamId: any) => {
    try {
      dispatch(setLoading(true));
      setTeamSelected(teamId);
      handleGetListContact(eventSelected, teamId, keySearchContact);
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleSearchKey = async (value: any) => {
    dispatch(setLoading(true));
    await handleGetListContact(eventSelected, teamSelected, keySearchContact);
    dispatch(setLoading(false));
  };

  const handleResetFilter = async () => {
    form.resetFields();
    dispatch(setLoading(true));
    setListTeam([]);
    setKeySearchContact(null);
    setEventSelected(null);
    setTeamSelected(null);
    await handleGetListContact();

    dispatch(setLoading(false));
  };

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            handleResetFilter();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <FilterOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Xóa bộ lọc
        </div>
      ),
    },

    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setIsOpenModalAddContact(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm liên hệ
        </div>
      ),
    },
  ];

  return (
    <>
      <Form
        className="wrapper-action"
        form={form}
        // onFinish={(value) => handleInviteParticipant(value)}
        autoComplete="off"
      >
        <Form.Item name="name">
          <Search
            className="search search-40px"
            placeholder="Tìm kiếm theo tên"
            enterButton
            size="large"
            onChange={(e: any) => setKeySearchContact(e.target.value)}
            onSearch={handleSearchKey}
          />
        </Form.Item>
        <Form.Item name="event">
          <Select
            className="select"
            placeholder="Chọn sự kiện"
            notFoundContent="Không có dữ liệu"
            onChange={(value) => {
              handleGetListTeamByEvent(value);
            }}
            optionFilterProp="label"
            optionLabelProp="label"
            value={eventSelected}
          >
            {listEvent &&
              listEvent.map((item: any, idx: any) => (
                <Option key={idx} value={item.id} label={item.code}>
                  <OptionEventItem
                    code={item.code}
                    image={item.logoPath}
                    name={item.name}
                  />
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item name="team">
          <Select
            className="select"
            placeholder="Chọn nhóm"
            notFoundContent="Không có dữ liệu"
            optionFilterProp="label"
            optionLabelProp="label"
            onChange={(value) => {
              handleGetContatcByTeam(value);
            }}
            value={teamSelected}
          >
            {listTeam &&
              listTeam.map((item: any, idx: any) => (
                <Option key={idx} value={item.id} label={item.name}>
                  <OptionEventItem
                    code={item.name}
                    image={item.imagePath}
                    isShowAvt={false}
                  />
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Dropdown menu={{ items }} placement="bottomLeft" arrow>
            <SettingFilled className="icon" style={{ fontSize: "22px" }} />
          </Dropdown>
        </Form.Item>
      </Form>
      <Row className="container-contacts">
        {listContact && (
          <div className="wrapper-contacts">
            {listContact?.length > 0 &&
              listContact.map((item, index) => {
                return (
                  <Contact
                    key={index}
                    contact={item}
                    setIsCallBackApi={setIsCallBackApi}
                  ></Contact>
                );
              })}
          </div>
        )}
        {listContact?.length === 0 && <EmptyData />}
      </Row>
      <Modal
        width={1000}
        title="Thêm liên hệ"
        open={isOpenModalAddContact}
        destroyOnClose={true}
        centered={true}
        onCancel={handleCloseModalAddConatct}
        footer={null}
      >
        <AddContact
          setIsCallBackApi={setIsCallBackApi}
          setIsOpenModalAddContact={setIsOpenModalAddContact}
        />
      </Modal>
    </>
  );
}
