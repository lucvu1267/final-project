import { useEffect } from "react";
import { Avatar, Button, Card, Descriptions, Divider } from "antd";
import luc_vu from "../../../assets/images/luc_vu.jpeg";
import { useParams } from "react-router-dom";
import "./detail-contact.scss";
function ContactDetailed() {
  const param = useParams();
  const contactId = param.contactId;
  //call api after this by useEffect
  useEffect(() => {});

  const contactInfo = {
    contactId: contactId,
    username: "Tên người dùng " + contactId,
    contactParentId: "",
    contactType: "Person",
    email: "chenlonghfu@gmail.com",
    phone: "",
    mobile: "0869505520",
    websiteLink: "https://www.google.com",
    titledId: "",
    image: luc_vu,
    firstName: "Vũ",
    middleName: "Xuân",
    lastName: "Lực",
  };

  return (
    <div className="detail-contact-container">
      <div className="detail-contact-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className="detail-contact-to-webview"
          onClick={() => window.open(contactInfo.websiteLink, "_blank")}
        >
          Trang sự kiện
        </Button>
      </div>
      <Divider />
      <div className="detail-contact-view">
        <div className="detail-contact-common">
          <Card
            title={
              <Avatar
                size={{
                  xs: 24,
                  sm: 32,
                  md: 40,
                  lg: 64,
                  xl: 80,
                  xxl: 100,
                }}
                src={contactInfo.image}
              />
            }
            bordered={false}
            className="common-info"
          >
            <p>{contactInfo.username}</p>
            {/* <p>
              <Tag color="green">Hoạt động</Tag>
            </p> */}
          </Card>
        </div>
        <div className="detail-contact-detail">
          <div>
            <Descriptions
              title={"Thông tin"}
              labelStyle={{ fontStyle: "italic", fontWeight: "bold" }}
              layout={"vertical"}
            >
              <Descriptions.Item label="Họ">
                {contactInfo.firstName}
              </Descriptions.Item>
              {/* <Descriptions.Item label="Tên đệm">Thành</Descriptions.Item> */}
              <Descriptions.Item label="Tên">
                {contactInfo.lastName}
              </Descriptions.Item>
              {contactInfo.contactType !== "Person" ? (
                <Descriptions.Item label="Liên quan">
                  <a href="/">
                    {contactInfo.contactParentId} tên sự kiện trong manager
                  </a>
                </Descriptions.Item>
              ) : null}
            </Descriptions>
          </div>
          <Divider />
          <div>
            <Descriptions
              title="Liên lạc"
              labelStyle={{ fontStyle: "italic", fontWeight: "bold" }}
              layout={"vertical"}
            >
              {/* <Descriptions.Item label="Tên tài khoản">
                chenlonghfu0802
              </Descriptions.Item> */}
              <Descriptions.Item label="Email">
                {contactInfo.email}
              </Descriptions.Item>
              {/* <Descriptions.Item label="Điện thoại cố định">
                113
              </Descriptions.Item> */}
              <Descriptions.Item label="Điện thoại di động">
                {contactInfo.mobile}
              </Descriptions.Item>
              {contactInfo.phone !== "" ? (
                <Descriptions.Item label="Điện thoại cố định">
                  {contactInfo.phone}
                </Descriptions.Item>
              ) : null}
            </Descriptions>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactDetailed;
