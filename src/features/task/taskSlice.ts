import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const createTask = async (payload: any) => {
  return await axiosInstance.post("task/create", payload);
};

export const getListTask = async (payload: any) => {
  return await axiosInstance.post("task/search", payload);
};

export const getListTaskById = async (taskId: any) => {
  return await axiosInstance.post(`task/${taskId}`);
};

export const updateTask = async (payload: any) => {
  return await axiosInstance.post(`task/update`, payload);
};

export const getActivitiLog = async (taskId: any) => {
  return await axiosInstance.post(`activity-log/task/${taskId}`);
};

export const taskSlice = createSlice({
  name: "loading",
  initialState: {},
  reducers: {
    // setLoading: (state, action) => {
    //   state.isLoading = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = taskSlice.actions;

export default taskSlice.reducer;
