import React, { useEffect } from "react";
import { useState } from "react";
import { Scheduler } from "@aldabil/react-scheduler";
import { EVENTS } from "./events";
import "./tasks.scss";
import TaskDetail from "components/task/TaskDetail";
import {
  Button,
  Cascader,
  Col,
  Dropdown,
  Form,
  Progress,
  Row,
  Select,
  Table,
  Tooltip,
} from "antd";
import AddTaskModal from "components/task/AddTaskModal";
import { vi } from "date-fns/esm/locale";
import {
  TableOutlined,
  BarChartOutlined,
  EditOutlined,
  EyeOutlined,
  PlusCircleFilled,
  FilterOutlined,
  SettingFilled,
} from "@ant-design/icons";
import moment from "moment";
import UpdateTaskModal from "components/task/UpdateTaskModal";
import Search from "antd/lib/input/Search";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import { getListTask, getListTaskById } from "./taskSlice";
import { FORMAT_DATE_FULL } from "constants/index";
import { parseISO } from "date-fns";
import {
  getAllMember,
  getListEventJoined,
  getListEventManager,
  getTaskBySearch,
  getTeamByEvent,
  getTeamById,
} from "features/eventManage/eventManageSlice";
import OptionEventItem from "components/common/optionItem/OptionEventItem";
import OptionItem from "components/common/optionItem/OptionItem";

const TYPE_VIEW = {
  TABLE: 1,
  SCHEDULE: 2,
};

const LIST_STATUS = [
  { value: "OPEN", label: "Đang mở" },
  { value: "INPROGRESS", label: "Đang thực hiện" },
  { value: "DONE", label: "Hoàn thành" },
  { value: "CANCEL", label: "Đã đóng" },
];

const { Option } = Select;

function Tasks(props: any) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [typeView, setTypeView] = useState(TYPE_VIEW.TABLE);
  const [isOpenModalView, setIsOpenModalView] = useState(false);
  const [isOpenModalAdd, setIsOpenModalAdd] = useState(false);
  const [isOpenModalUpdate, setIsOpenModalUpdate] = useState(false);
  const [isCallbackApi, setIsCallbackApi] = useState(false);
  const [taskDetail, setTaskDetail] = useState<any>({});
  const [listTask, setListTask] = useState([]);
  const [listTaskToUpdate, setListTaskToUpdate] = useState([]);
  const [listTaskByEvent, setListTaskByEvent] = useState([]);
  const [listMember, setListMember] = useState([]);
  const [listEvent, setListEvent] = useState<any>([]);
  const [listTeam, setListTeam] = useState<any>([]);
  const [eventSelected, setEventSelected] = useState(null);
  const [teamSelected, setTeamSelected] = useState(null);
  const [assigneeSelected, setAssigneeSelected] = useState(null);
  const [taskId, setTaskId] = useState(null);
  const [listEventHeaderShedule, setListEventHeaderShedule] = useState<any[]>(
    []
  );
  const [listEventSchudule, setListEventSchudule] = useState<any[]>([]);
  const [keySearchTask, setKeySearchTask] = useState(null);
  const [stastusData, setStatusData] = useState(null);

  const items: any = [
    {
      key: "1",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            handleResetFilter();
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <FilterOutlined
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Xóa bộ lọc
        </div>
      ),
    },

    {
      key: "2",
      label: (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setIsOpenModalAdd(true);
          }}
          style={{ fontWeight: "600", color: "#285158" }}
        >
          <PlusCircleFilled
            className="icon"
            style={{ fontSize: "14px", marginRight: "5px" }}
          />{" "}
          Thêm công việc
        </div>
      ),
    },
  ];

  const columnsOrderTicket: any = [
    {
      title: "Tên công việc",
      key: "title",
      render: (value: any) => (
        <span>
          <span className="label-grayest">{`[${value.code}]`} </span>
          {value.name}
        </span>
      ),
      width: "33%",
    },
    {
      title: "Người được giao",
      key: "assignee",
      render: (value: any) => (
        <span className="label-grayest">{value.assignee.firstName}</span>
      ),
    },
    {
      title: "Thời gian bắt đầu",
      key: "startDate",
      render: (value: any) => (
        <span>{moment(value.startDate).format(FORMAT_DATE_FULL)}</span>
      ),
      sorter: (a: any, b: any) =>
        moment(a.startDate).unix() - moment(b.startDate).unix(),
    },
    {
      title: "Thời gian kết thúc",
      key: "endDate",
      render: (value: any) => (
        <span>{moment(value.endDate).format(FORMAT_DATE_FULL)}</span>
      ),
      sorter: (a: any, b: any) =>
        moment(a.endDate).unix() - moment(b.endDate).unix(),
    },
    {
      title: "Trạng thái",
      key: "status",
      render: (value: any) => <span>{handleGetNameStatus(value.status)}</span>,
      // defaultSortOrder: "ascend",
      // sorter: (a: any, b: any) => a.progress - b.progress,
    },
    {
      title: "Tiến độ",
      key: "progress",
      render: (value: any) => (
        <Progress
          style={{ width: "7rem" }}
          percent={value.progress}
          size="small"
        />
      ),
      defaultSortOrder: "ascend",
      sorter: (a: any, b: any) => a.progress - b.progress,
    },
    {
      title: "Thao tác",
      dataIndex: "",
      key: "x",
      render: (value: any) => (
        <div className="operator-wrapper">
          <Tooltip placement="top" title="Xem chi tiết" arrowPointAtCenter>
            <EyeOutlined
              className="icon-table"
              onClick={() => {
                setTaskId(value.id);
                setIsOpenModalView(true);
              }}
            />
          </Tooltip>
          <Tooltip placement="top" title="Chỉnh sửa" arrowPointAtCenter>
            <EditOutlined
              className="icon-table"
              onClick={() => handleOpenModalUpdateTask(value.id)}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  const handleGetListEvent = async () => {
    // dispatch(setLoading(true));
    const dataPayload = {
      orders: [],
      page: 0,
      searchKey: "",
      size: 1000,
    };
    try {
      const data = await getListEventManager(dataPayload);
      setListEventHeaderShedule(
        data.data.eventPage.content.map((item: any) => ({
          admin_id: item.id,
          title: item.name,
          // mobile: item.event.emailAccount,
          avatar: item.logoPath,
        }))
      );
    } catch (error) {
    } finally {
      // dispatch(setLoading(false));
    }
  };

  const handleGetListTask = async (
    assigneeSelected = null,
    eventSelected = null,
    teamSelected = null,
    keySearchTask = null,
    statusSelected = null
  ) => {
    try {
      const payload = {
        assigneeId: assigneeSelected,
        eventId: eventSelected,
        orders: [],
        page: 0,
        searchKey: keySearchTask,
        size: 100,
        teamId: teamSelected,
        status: statusSelected,
      };
      const data = await getListTask(payload);
      setListEventSchudule(
        data.data.taskPage.content.map((item: any) => ({
          event_id: item.id,
          title: item.name,
          start: parseISO(item.startDate),
          end: parseISO(item.endDate),
          admin_id: item.event.id,
          editable: true,
          deletable: true,
          color: "rgb(174 235 187)",
        }))
      );
      setListTask(data.data.taskPage.content);
    } catch (error) {
    } finally {
    }
  };

  const handleOpenModalUpdateTask = async (taskId: any) => {
    dispatch(setLoading(true));
    try {
      const data = await getListTaskById(taskId);
      Promise.all([
        getTaskByEvent(data.data.event.id),
        getMemberByEvent(data.data.event.id),
        getTaskParentToUpdate(data.data.event.id, taskId),
      ]);
      setTaskDetail(data.data);
      setIsOpenModalUpdate(true);
    } catch (error) {
    } finally {
      setTimeout(() => {
        dispatch(setLoading(false));
      }, 300);
    }
  };

  const getTaskByEvent = async (idEvent: any) => {
    const payload = {
      assigneeId: null,
      eventId: idEvent,
      orders: [],
      page: 0,
      searchKey: null,
      size: 100,
      teamId: null,
    };
    const data = await getTaskBySearch(payload);
    setListTaskByEvent(data.data.taskPage.content);
  };

  const getTaskParentToUpdate = async (idEvent: any, currentTaskId: any) => {
    const payload = {
      currentTaskId: currentTaskId,
      assigneeId: null,
      eventId: idEvent,
      orders: [],
      page: 0,
      searchKey: null,
      size: 100,
      teamId: null,
    };
    const data = await getTaskBySearch(payload);
    setListTaskToUpdate(data.data.taskPage.content);
  };

  const getMemberByEvent = async (idEvent: any) => {
    const data = await getAllMember(idEvent);
    setListMember(data.data);
  };

  const handleGetListTeamByEvent = async (eventId: any) => {
    try {
      setEventSelected(eventId);
      setTeamSelected(null);
      setAssigneeSelected(null);
      dispatch(setLoading(true));
      await Promise.all([
        getTeamByEvent(eventId),
        getAllMember(eventId),
        handleGetListTask(null, eventId, null, keySearchTask),
      ])
        .then((value: any) => {
          setListTeam(value[0].data);
          setListMember(value[1].data);
        })
        .finally(() => dispatch(setLoading(false)));
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const handleGetListAssignee = async (teamId: any) => {
    setTeamSelected(teamId);
    setAssigneeSelected(null);
    dispatch(setLoading(true));
    await Promise.all([
      getTeamById(teamId),
      handleGetListTask(null, eventSelected, teamId, keySearchTask),
    ])
      .then((value: any) => {
        setListMember(value[0].data.members);
      })
      .finally(() => dispatch(setLoading(false)));
  };

  const handleSearchByAssignee = async (assigneeId: any) => {
    setAssigneeSelected(assigneeId);
    dispatch(setLoading(true));
    await handleGetListTask(
      assigneeId,
      eventSelected,
      teamSelected,
      keySearchTask
    );
    dispatch(setLoading(false));
  };

  const handleGetListEventJoined = async () => {
    const data = await getListEventJoined();
    setListEvent(data.data);
  };

  const handleGetNameStatus = (status: any) => {
    switch (status) {
      case "OPEN":
        return "Đang mở";
      case "INPROGRESS":
        return "Đang thực hiện";
      case "DONE":
        return "Hoàn thành";
      case "CANCEL":
        return "Đã đóng";
      default:
        break;
    }
  };

  useEffect(() => {
    dispatch(setLoading(true));
    // setDefaultFilter();
    Promise.all([
      handleGetListEvent(),
      handleGetListTask(
        assigneeSelected,
        eventSelected,
        teamSelected,
        keySearchTask
      ),
    ]).then(() => dispatch(setLoading(false)));
    setIsCallbackApi(false);
  }, [isCallbackApi]);

  const handleSearchKey = async (value: any) => {
    dispatch(setLoading(true));
    await handleGetListTask(
      assigneeSelected,
      eventSelected,
      teamSelected,
      keySearchTask
    );
    dispatch(setLoading(false));
  };

  const handleResetFilter = async () => {
    form.resetFields();
    dispatch(setLoading(true));
    setDefaultFilter();
    await handleGetListTask();
    dispatch(setLoading(false));
  };

  const setDefaultFilter = () => {
    setAssigneeSelected(null);
    setTeamSelected(null);
    setEventSelected(null);
    setListTeam([]);
    setListMember([]);
  };

  useEffect(() => {
    handleGetListEventJoined();
  }, []);

  return (
    <>
      <Form
        form={form}
        className="wrapper-action"
        style={{ marginBottom: "0" }}
      >
        <Form.Item name="name">
          <Search
            className="search search-40px"
            placeholder="Tìm kiếm theo tên"
            enterButton
            // allowClear
            size="large"
            style={{ width: "16vw" }}
            onChange={(e: any) => setKeySearchTask(e.target.value)}
            onSearch={handleSearchKey}
          />
        </Form.Item>
        <Form.Item name="status">
          <Select
            className="select"
            placeholder="Tìm kiếm theo trạng thái"
            notFoundContent="Không có dữ liệu"
            optionFilterProp="label"
            optionLabelProp="label"
            onChange={(e) => {
              setStatusData(e);
              handleGetListTask(
                assigneeSelected,
                eventSelected,
                teamSelected,
                keySearchTask,
                e
              );
            }}
          >
            {LIST_STATUS.map((item: any, idx: any) => (
              <Option key={idx} value={item.value} label={item.label}>
                {item.label}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="name2">
          <Select
            className="select"
            placeholder="Chọn sự kiện"
            notFoundContent="Không có dữ liệu"
            onChange={(value) => {
              handleGetListTeamByEvent(value);
            }}
            optionFilterProp="label"
            optionLabelProp="label"
            style={{ width: "16vw" }}
            value={eventSelected}
          >
            {listEvent &&
              listEvent.map((item: any, idx: any) => (
                <Option key={idx} value={item.id} label={item.code}>
                  <OptionEventItem
                    code={item.code}
                    image={item.logoPath}
                    name={item.name}
                  />
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Dropdown menu={{ items }} placement="bottomLeft" arrow>
            <SettingFilled className="icon" style={{ fontSize: "22px" }} />
          </Dropdown>
        </Form.Item>
      </Form>
      {eventSelected && (
        <Row className="wrapper-action">
          <Select
            className="select"
            placeholder="Chọn nhóm"
            notFoundContent="Không có dữ liệu"
            onChange={(value) => {
              handleGetListAssignee(value);
            }}
            optionFilterProp="label"
            optionLabelProp="label"
            style={{ width: "16vw" }}
            value={teamSelected}
          >
            {listTeam &&
              listTeam.map((item: any, idx: any) => (
                <Option key={idx} value={item.id} label={item.name}>
                  <OptionEventItem
                    code={item.name}
                    image={item.imagePath}
                    isShowAvt={false}
                  />
                </Option>
              ))}
          </Select>
          <Select
            className="select"
            placeholder="Chọn thành viên"
            notFoundContent="Không có dữ liệu"
            onChange={(value) => {
              handleSearchByAssignee(value);
            }}
            optionFilterProp="label"
            optionLabelProp="label"
            style={{ width: "16vw" }}
            value={assigneeSelected}
          >
            {listMember &&
              listMember.map((item: any, idx: any) => (
                <Option
                  key={idx}
                  value={item.member.id}
                  label={item.member.emailAccount}
                >
                  <OptionItem
                    mail={item.member.emailAccount}
                    name={item.member.firstName}
                    image={item.member.imagePath}
                  />
                </Option>
              ))}
          </Select>
          <SettingFilled
            className="icon"
            style={{ fontSize: "22px", opacity: 0 }}
          />
        </Row>
      )}
      <Row justify={"end"} className="wrapper-option-view">
        {/* <div onClick={() => setIsOpenModalAdd(true)} className="add-task">
          Thêm công việc
        </div>*/}
        <div style={{ display: "flex", justifyContent: "end", width: "100%" }}>
          <Tooltip placement="top" title="Xem bằng bẳng" arrowPointAtCenter>
            <TableOutlined
              onClick={() => setTypeView(TYPE_VIEW.TABLE)}
              className="icon"
              style={
                typeView === TYPE_VIEW.TABLE
                  ? {
                      fontSize: "20px",
                      marginRight: "10px",
                      borderBottom: "2px solid #BE640B",
                    }
                  : { fontSize: "20px", marginRight: "10px" }
              }
            />
          </Tooltip>
          <Tooltip placement="top" title="Xem bằng lịch" arrowPointAtCenter>
            <BarChartOutlined
              onClick={() => setTypeView(TYPE_VIEW.SCHEDULE)}
              className="icon"
              style={
                typeView === TYPE_VIEW.SCHEDULE
                  ? { fontSize: "20px", borderBottom: "2px solid #BE640B" }
                  : { fontSize: "20px" }
              }
            />
          </Tooltip>
        </div>
      </Row>
      {/* Table mode */}
      <div
        className="wrapper-table-task"
        hidden={typeView === TYPE_VIEW.SCHEDULE}
      >
        <Table
          className="table-task"
          style={{ width: "100vw" }}
          locale={{
            triggerDesc: "Sắp sắp xếp giảm dần",
            triggerAsc: "Sắp sắp xếp tăng dần",
            cancelSort: "Bỏ sắp xếp",
            emptyText: <h2> Không có dữ liệu</h2>,
          }}
          rowKey={(record: any) => record.id}
          columns={columnsOrderTicket}
          expandable={{
            expandedRowRender: (record) => (
              <p key={record.id} style={{ margin: 0 }}>
                <span className="label-grayest">Sự kiện: </span>{" "}
                {`[${record.event.code}]`} {record.event.name}
              </p>
            ),
            rowExpandable: (record) => record.name !== "Not Expandable",
          }}
          dataSource={listTask}
          pagination={{ position: ["bottomCenter"] }}
        />
      </div>
      {/* Schedule mode */}

      <Row hidden={typeView === TYPE_VIEW.TABLE}>
        <Scheduler
          deletable={true}
          locale={vi}
          resourceViewMode="tabs"
          // eventRenderer={(event) => <>AAAAAAAAAA</>}
          draggable={false}
          // resourceViewMode="default"
          // view={"month"}
          events={listEventSchudule}
          resources={listEventHeaderShedule}
          resourceFields={{
            idField: "admin_id",
            textField: "title",
            subTextField: "mobile",
            avatarField: "avatar",
            colorField: "color",
          }}
          day={{
            startHour: 0,
            endHour: 23,
            step: 60,
            // cellRenderer?:(props: CellProps) => JSX.Element,
            navigation: true,
          }}
          // day={{
          //   startHour: 0,
          //   endHour: 23,
          //   step: 60,
          //   // cellRenderer:(props: any) => <div>hihih</div>,
          // }}
          // week={{
          //   weekDays: [0, 1, 2, 3, 4, 5, 6],
          //   weekStartOn: 0,
          //   startHour: 10,
          //   endHour: 10,
          //   step: 60,
          //   // cellRenderer: (value) => {
          //   //  return value.onClick((value)=><div></div>)
          //   // },
          // }}
          week={{
            weekDays: [0, 1, 2, 3, 4, 5, 6],
            weekStartOn: 6,
            startHour: 1,
            endHour: 23,
            step: 60,
            // cellRenderer?:(props: CellProps) => JSX.Element,
            navigation: true,
          }}
          // onConfirm={true}
          customEditor={(scheduler) => (
            <AddTaskModal
              scheduler={scheduler}
              setIsOpenModalAdd={setIsOpenModalAdd}
              setIsCallbackApi={setIsCallbackApi}
            />
          )}
          onConfirm={async (event, action) => {
            console.log(action);
            return new Promise((res, rej) => {
              setTimeout(() => {
                // res({
                //   ...event,
                //   event_id: event.event_id || Math.random(),
                //   // title: "From Custom",
                //   // start: new Date(new Date().setHours(11)),
                //   // end: new Date(new Date().setHours(18)),
                // });
              }, 1000);
            });
          }}
          dialogMaxWidth="lg"
          // remoteEvents={()=>{console.log("AAAAAAAAA")}}
          // recourseHeaderComponent={() => <Contact></Contact>}
          viewerExtraComponent={(fields: any, event: any) => (
            <TaskDetail
              event={event}
              fields={fields}
              setIsOpenModalView={setIsOpenModalView}
              handleOpenModalUpdateTask={handleOpenModalUpdateTask}
            />
          )}
          translations={{
            navigation: {
              month: "Tháng",
              week: "Tuần",
              day: "Ngày",
              today: "Hôm nay",
            },
            form: {
              addTitle: "Novo Evento",
              editTitle: "Editar Evento",
              confirm: "Confirmar",
              delete: "Excluir",
              cancel: "Cancelar",
            },
            event: {
              title: "Título",
              start: "Início",
              end: "Fim",
              allDay: "",
            },
            moreEvents: "Xem thêm...",
          }}
        />
        {/* <Scheduler
          view="week"
          events={EVENTS}
          day={{
            startHour: 0,
            endHour: 23,
            step: 60,
            // cellRenderer:(props: any) => <div>hihih</div>,
          }}
          week={{
            weekDays: [0, 1, 2, 3, 4, 5, 6],
            weekStartOn: 0,
            startHour: 0,
            endHour: 23,
            step: 60,
            // cellRenderer: (value) => {
            //  return value.onClick((value)=><div></div>)
            // },
          }}
          selectedDate={new Date(2021, 4, 5)}
          resourceViewMode="tabs"
        /> */}
      </Row>
      {isOpenModalView && (
        <TaskDetail
          taskId={taskId}
          setIsOpenModalView={setIsOpenModalView}
          handleOpenModalUpdateTask={handleOpenModalUpdateTask}
        />
      )}
      {isOpenModalAdd && (
        <AddTaskModal
          setIsOpenModalAdd={setIsOpenModalAdd}
          setIsCallbackApi={setIsCallbackApi}
        />
      )}
      {isOpenModalUpdate && (
        <UpdateTaskModal
          taskDetail={taskDetail}
          listMember={listMember}
          listTaskByEvent={listTaskToUpdate}
          setIsOpenModalUpdate={setIsOpenModalUpdate}
          setIsCallbackApi={setIsCallbackApi}
          isOpenModal={isOpenModalUpdate}
        />
      )}
    </>
  );
}

export default Tasks;
