import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const login = async (payload: any) => {
  return await axiosInstance.post("auth/login", payload);
};
export const loginSlice = createSlice({
  name: "loading",
  initialState: {
    isLoading: false,
  },
  reducers: {
    // setLoading: (state, action) => {
    //   state.isLoading = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = loginSlice.actions;

export default loginSlice.reducer;
