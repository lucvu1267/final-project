import React, { useState, useEffect } from "react";
import { Button, Form, Input, message } from "antd";
import "./login.scss";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import MessError from "components/common/MessError";
import { login } from "./loginSlice";
import { JWT_AUTH } from "constants/index";
import { toastCustom } from "config/toast";
import { setIsAuth } from "redux/slice";
import { getInfoUser, setUser } from "features/contact/contactSlice";

function Login(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [isCheckValidate, setIsCheckValidate] = useState(false);

  const handleLogin = async (value: any) => {
    setIsCheckValidate(true);
    //dngbi97@gmail.com
    //123456789
    if (value.username) {
      const databody = {
        deviceName: "",
        deviceToken: "",
        locationInfo: "",
        email: value.username,
        password: value.password,
        rememberMe: true,
      };
      dispatch(setLoading(true));
      try {
        const responseData: any = await login(databody);
        if (responseData.errorCode === null) {
          localStorage.setItem(JWT_AUTH, responseData.data.jwtToken);
          dispatch(setIsAuth(true));
          handleGetInfoUser();
          setTimeout(() => {
            dispatch(setLoading(false));
          }, 300);
          navigate("/");
          toastCustom("success", "Đăng nhập thành công");
        } else if (responseData.errorCode === "WRONG_USERNAME_OR_PASSWORD") {
          message.open({
            type: "error",
            content: "Tài khoản hoặc mật khẩu không chính xác!",
          });
        } else if (responseData.errorCode === "ACCOUNT_NOT_ACTIVE") {
          message.open({
            type: "error",
            content: "Tài khoản chưa được kích hoạt, vui lòng kiểm tra email!",
          });
        }
        dispatch(setLoading(false));
      } catch (error) {
        console.log(error);
        toastCustom("error", "Tài khoản hoặc mật khẩu không chính xác!");
        dispatch(setLoading(false));
      }
    }
  };

  const handleGetInfoUser = async () => {
    try {
      const data = await getInfoUser();
      dispatch(setUser(data.data));
    } catch (error) {
    } finally {
    }
  };

  return (
    <Form
      name="basic"
      className="container-login"
      onFinish={(value) => handleLogin(value)}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        name="username"
        className="text-field"
        // dependencies={[]}
        // validateTrigger: 'onSubmit'
        rules={
          isCheckValidate
            ? [
                {
                  required: true,
                  message: <MessError message={"Vui lòng nhập tài khoản!"} />,
                },
                {
                  type: "email",
                  message: (
                    <MessError
                      message={"Tài khoản phải đúng định dạng mail!"}
                    />
                  ),
                },
                // ({ getFieldValue }) => ({
                //   validator(rule, value, callback) {
                //     if (value) {
                //       return Promise.resolve();
                //     }
                //     return Promise.reject("dsd");
                //   },
                // }),
                // {
                //   validator(rule, value, callback) {
                //     value !== null ? Promise.resolve(): Promise.reject()
                //   },
                // }
              ]
            : []
        }
      >
        <Input className="input input-normail" placeholder="Nhập email" />
      </Form.Item>

      <Form.Item
        name="password"
        className="text-field"
        rules={
          isCheckValidate
            ? [
                {
                  required: true,
                  message: <MessError message={"Vui lòng nhập mật khẩu!"} />,
                },
              ]
            : []
        }
      >
        <Input.Password
          className="input input-password"
          placeholder="Nhập mật khẩu"
        />
      </Form.Item>
      <div className="wrap-action">
        <Link className="link" to={"/forgot-password"}>
          Quên mật khẩu{" "}
        </Link>

        <Button
          type="primary"
          htmlType="submit"
          className="btn-primary"
          onClick={(e) => handleLogin(e)}
        >
          Đăng nhập
        </Button>
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className=" btn-border-primary"
          onClick={() => navigate("/register")}
        >
          Đăng ký
        </Button>
      </div>
    </Form>
  );
}

export default Login;
