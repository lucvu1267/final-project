import React, { useState } from "react";
import { Button, Form, Input, message, Row, Tooltip } from "antd";
import "./register.scss";
import { useLocation, useNavigate } from "react-router-dom";
import { setLoading } from "../../../components/common/loading/loadingSlice";
import { useDispatch } from "react-redux";
import { login } from "../login/loginSlice";
import { JWT_AUTH } from "../../../constants";
import { setIsAuth } from "../../../redux/slice";
import { toastCustom } from "../../../config/toast";
import { register } from "./registerSlice";
import { isEmpty, uploadImgCloudinary } from "../../../mixin/generate";
import UploadImage from "../../../components/common/uploadImage/UploadImage";
import MessError from "../../../components/common/MessError";
function Register(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [file, setFile] = useState(null);
  const [isResetImageUrl, setIsResetImageUrl] = useState(false);

  const onFinish = async (values: any) => {
    try {
      dispatch(setLoading(true));
      let fileUpload;
      if (file) {
        fileUpload = await uploadImgCloudinary(file);
      }
      const databody = {
        createContactRequest: {
          email: values.email,
          firstName: values.name ?? "",
          imagePath:
            "https://res.cloudinary.com/dcgjui0yd/image/upload/v1671031848/profileDefault_zhcqph.png",
          mobile: "",
          phone: "",
          websiteLink: "",
        },
        email: values.email,
        password: values.password,
      };
      const data: any = await register(databody);
      dispatch(setLoading(false));
      if (data.errorCode === null) {
        message.open({
          type: "success",
          content:
            "Đăng ký thành công, vui lòng kiểm tra email để kích hoạt tài khoản.",
        });
        navigate("/login");
      } else if (data.errorCode === "ACCOUNT_EXISTED") {
        message.open({
          type: "error",
          content: "Tài khoản đã tồn tại",
        });
      } else {
        message.open({
          type: "success",
          content: "Đăng ký thất bại.",
        });
      }
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  // const onFinishFailed = () => {
  //
  // }

  return (
    <Form
      name="basic"
      className="container-register"
      onFinish={(values) => onFinish(values)}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        name="email"
        className="text-field"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập email",
          },
          {
            type: "email",
            message: (
              <MessError message={"Tài khoản phải đúng định dạng email"} />
            ),
          },
        ]}
      >
        <Input
          autoComplete="off"
          className="input input-normail"
          placeholder="Nhập email"
        />
      </Form.Item>
      <Form.Item
        name="name"
        className="text-field"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập tên của bạn!",
          },
          {
            max: 255,
            message: <MessError message={"Tên không được quá 255 ký tự !"} />,
          },
        ]}
      >
        <Input className="input input-normail" placeholder="Nhập tên" />
      </Form.Item>
      <Form.Item
        name="password"
        className="text-field"
        rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        hasFeedback
      >
        <Input.Password
          autoComplete="off"
          className="input input-password"
          placeholder="Nhập mật khẩu"
        />
      </Form.Item>

      <Form.Item
        // label="Xác nhận mật khẩu"
        name="confirmPassword"
        className="text-field"
        rules={[
          { required: true, message: "Vui lòng xác nhận mật khẩu" },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }

              return Promise.reject(
                new Error("Mật khẩu xác nhận không trùng khớp")
              );
            },
          }),
        ]}
        dependencies={["password"]}
        hasFeedback
      >
        <Input.Password
          className="input input-confirm-password"
          placeholder="Nhập xác nhận mật khẩu"
        />
      </Form.Item>

      {/* <Row justify="center" style={{ marginBottom: "1rem" }}>
        <Tooltip placement="top" title="Ảnh của bạn" arrowPointAtCenter>
          <div>
            <UploadImage
              // imgUrlInit={eventDetail?.logoPath}
              setFile={setFile}
              isResetImageUrl={isResetImageUrl}
              setIsResetImageUrl={setIsResetImageUrl}
            />
          </div>
        </Tooltip>
      </Row> */}
      <div className="wrap-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          htmlType="submit"
          className="btn-border-primary"
        >
          Đăng ký
        </Button>
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className=" btn-border-primary"
          onClick={() => navigate("/login")}
        >
          Đăng nhập
        </Button>
      </div>
    </Form>
  );
}

export default Register;
