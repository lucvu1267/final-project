import { CheckCircleOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import { useNavigate } from "react-router-dom";
import "./active-confirm.scss";
function ActiveConfirm(props: any) {
  const navigate = useNavigate();

  return (
    <div className="active-confirm-container">
      <div className="container-buy-ticket-success">
        <CheckCircleOutlined className="icon-success" size={200} />
        <div className="mess-success">
          <p
            style={{
              fontSize: "17px",
              fontWeight: "bold",
              color: "#285158",
            }}
          >
            Kích hoạt tài khoản thành công!
          </p>
          <p className="description-gray">
            {" "}
            Cảm ơn bạn đã đăng ký tài khoản. Hãy đăng nhập và trải nghiệm nào.
          </p>
        </div>
      </div>
      <div className="center-common">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          className=" btn-border-primary"
          onClick={() => navigate("/login")}
        >
          Đăng nhập
        </Button>
      </div>
    </div>
  );
}

export default ActiveConfirm;
