import { useState } from "react";
import { Button, Form, Input } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
import "./forgot-password.scss";

function ForgotPassword(props: any) {
  const [email, setEmail] = useState("");
  const [verifyCode, setVerifyCode] = useState("");
  const location = useLocation();
  const navigate = useNavigate();

  const sendEmail = () => {
    // Call api send email
    // BE send code to FE as response to set state
    setVerifyCode("123");
  };

  const onFinish = (values: any) => {
    let inputVerifyCode = values.verifyCode;

    if (inputVerifyCode !== verifyCode) {
      alert("Wrong verification code!");
      return;
    }

    navigate("/reset-password");
  };

  return (
    <Form
      name="basic"
      className="container-register"
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        className="text-field"
        rules={[{ required: true, message: "Please input your email!" }]}
      >
        <Input.Group style={{ display: "flex", flexDirection: "row" }} compact>
          <Input
            className="input input-normal"
            id="email"
            placeholder="Nhập email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
          <Button
            ant-click-animating-without-extra-node="true"
            type="primary"
            className="btn-border-primary send-email-btn"
            onClick={sendEmail}
          >
            Send Email
          </Button>
        </Input.Group>
      </Form.Item>
      <Form.Item
        className="text-field"
        name="verifyCode"
        rules={[{ required: true, message: "Please input your confirm code!" }]}
      >
        <Input className="input input-normal" placeholder="Nhập mã xác nhận" />
      </Form.Item>
      <Form.Item>
        <div className="wrap-action">
          <Button
            ant-click-animating-without-extra-node="true"
            type="primary"
            htmlType="submit"
            className="btn-border-primary"
          >
            Tiếp tục
          </Button>
          <Button
            ant-click-animating-without-extra-node="true"
            type="primary"
            className=" btn-border-primary"
            onClick={() => navigate("/login")}
          >
            Quay lại
          </Button>
        </div>
      </Form.Item>
    </Form>
  );
}

export default ForgotPassword;
