import { Button, Form, Input } from "antd";
import "./forgot-password.scss";
import { useNavigate } from "react-router-dom";
function ResetPassword(props: any) {
  const navigate = useNavigate();

  const onFinish = (values: any) => {
    let newPassword = values.newPassword;
    let confirmPassword = values.confirmPassword;

    if (newPassword !== confirmPassword) {
      alert("Password confirmation wrong!");
      return;
    }

    navigate("/login");
  };

  return (
    <Form
      name="basic"
      className="container-register"
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        className="text-field"
        name="newPassword"
        rules={[{ required: true, message: "Hãy nhập mật khẩu mới!" }]}
      >
        <Input.Password
          className="input input-password"
          placeholder="Nhập mật khẩu mới"
        />
      </Form.Item>

      <Form.Item
        className="text-field"
        name="confirmPassword"
        rules={[
          { required: false, message: "Hãy nhập xác nhận mật khẩu mới!" },
        ]}
      >
        <Input.Password
          className="input input-password"
          placeholder="Xác nhận mật khẩu mới"
        />
      </Form.Item>
      <div className="wrap-action">
        <Button
          ant-click-animating-without-extra-node="true"
          type="primary"
          htmlType="submit"
          className="btn-primary"
        >
          Đổi mật khẩu
        </Button>
      </div>
    </Form>
  );
}

export default ResetPassword;
