import { createSlice } from "@reduxjs/toolkit";
import axiosInstance from "config/axios";

//function call api
export const getListPost = async (payload: any) => {
  return await axiosInstance.post("post/search", payload);
};

export const getTicketPublic = async (eventId: any) => {
  return await axiosInstance.get(`ticket/find-all-by-event/${eventId}`);
};

export const orderTicket = async (payload: any) => {
  return await axiosInstance.post(`order/create`, payload);
};

export const homeSlice = createSlice({
  name: "home",
  initialState: {
    user: {},
  },
  reducers: {
    // setUser: (state, action) => {
    //   state.user = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
export const {} = homeSlice.actions;

export default homeSlice.reducer;
