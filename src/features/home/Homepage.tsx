import { Avatar, Carousel, Col, Popover, Row, Tooltip } from "antd";
import { useEffect, useState } from "react";
import { getListPost } from "./homeSlice";
import PostOverView from "components/post/PostOverView";
import { useDispatch } from "react-redux";
import { setLoading } from "components/common/loading/loadingSlice";
import {
  MAX_LENGTH_DESCRIPTION,
  MAX_LENGTH_TITLE,
  TYPE_POST,
} from "constants/event";
import "./home-page.scss";
import { ClockCircleOutlined } from "@ant-design/icons";
import PostCarousel from "components/post/PostCarousel";
import logo from "assets/images/logo.png";
import moment from "moment";
import { FORMAT_DATE_FULL } from "constants/index";
import { useNavigate, useParams } from "react-router";
import Search from "antd/lib/input/Search";
import { isEmpty } from "mixin/generate";
type Props = {};

export function Homepage(props: Props) {
  const {idEventWeb} = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const onChange = (currentSlide: number) => {};
  const [listPosts, setListPost] = useState<any[]>([]);
  const [listPostsMostView, setListPostMostView] = useState<any[]>([]);
  const [searchKeyPost, setSearchKeyPost] = useState(null);


  useEffect(() => {
    handleGetListPost();
  }, []);

  const handleGetListPost = async () => {
    try {
      dispatch(setLoading(true));
      const payload = {
        eventId: isEmpty(idEventWeb)?null:idEventWeb,
        orders: [
          {
            direction: "DESC",
            ignoreCase: false,
            nullHandling: "NATIVE",
            property: "createDate", //
          },
        ],
        page: 0,
        searchKey: searchKeyPost,
        size: 100,
        postType: TYPE_POST.EVENT,
        // teamId: 10,
      };
      const payloadByView = {
        eventId: isEmpty(idEventWeb)?null:idEventWeb,
        orders: [
          {
            direction: "DESC",
            ignoreCase: false,
            nullHandling: "NATIVE",
            property: "views", //createDate
          },
        ],
        page: 0,
        searchKey: null,
        size: 100,
        postType: TYPE_POST.EVENT,
        // teamId: 10,
      };
      await Promise.all([
        getListPost(payload),
        getListPost(payloadByView),
      ]).then((value: any) => {
        setListPost(value[0].data.postPage.content);
        setListPostMostView(value[1].data.postPage.content);
      });
    } catch (error) {
    } finally {
      dispatch(setLoading(false));
    }
  };

  const subTitle = (textValue: String, maxLength: number) => {
    if (textValue?.length > maxLength) {
      return textValue.substring(0, maxLength).trimEnd() + "...";
    }
    return textValue;
  };

  return (
    <div>
      {" "}
      <Row className="container-event">
        <Col span={16} style={{ paddingRight: "1rem" }}>
          <div className="container-carousel">
            <div className="wrap-carousel">
              <Carousel className="carousel" afterChange={onChange} autoplay>
                {listPosts.length > 0 &&
                  listPosts
                    .slice(0, listPosts.length > 5 ? 5 : listPosts.length)
                    .map((item, index) => (
                      <PostCarousel
                        key={index}
                        postOverView={item}
                      ></PostCarousel>
                    ))}
              </Carousel>
            </div>
          </div>
          <div className="wrapper-title-list-post">
            <div className="title">Danh sách bài viết</div>
            <Search
            className="search search-40px btn-search-homepage"
            placeholder="Tìm kiếm..."
            enterButton
            size="large"
            style={{width: "200px", marginBottom: "1rem"}}
            onChange={(e: any) => setSearchKeyPost(e.target.value)}
            onSearch={handleGetListPost}
          />
          </div>
          <div className="wrapper-post">
            {listPosts.length > 0 &&
              listPosts.map((item, index) => (
                <PostOverView key={index} postOverView={item}></PostOverView>
              ))}
          </div>
        </Col>
        <Col className="container-post-most-view" span={7}>
          <div className="wrapper-title-list-post">
            <div className="title" style={{ fontSize: "19px" }}>
              Bài viết nhiều lượt xem nhất
            </div>
          </div>
          <div className="wrapper-list-new-post">
            <div className="post-over-view">
              {listPostsMostView
                ?.slice(0, listPostsMostView.length > 4 ? 4 : listPostsMostView.length)
                .map((item, idx) => (
                  <div
                    key={idx}
                    className="wrapper-animation-team"
                  >
                    <Row justify={"space-between"} className="team-item">
                      <Col className="avatar-team" span={5}>
                        <Avatar
                          style={{ width: "100px", height: "100px" }}
                          shape="square"
                          size="large"
                          src={
                            item.overviewImagePath
                              ? item.overviewImagePath
                              : logo
                          }
                        />
                      </Col>
                      <Col className="info-team" span={18}>
                        <div className="title" 
                         onClick={() => {
                          navigate(`/post-detail/${item.id}`);
                        }}
                        >
                          {subTitle(item.subject, MAX_LENGTH_TITLE)}
                        </div>
                        <Tooltip
                          placement="top"
                          title={item.overviewDescription}
                          arrowPointAtCenter
                        >
                          <p className="description">
                            {subTitle(
                              item.overviewDescription,
                              MAX_LENGTH_DESCRIPTION
                            )}
                          </p>
                        </Tooltip>
                        <div
                          className="description"
                          style={{ marginBottom: "12px", fontSize: "12px" }}
                        >
                          <span>
                            <ClockCircleOutlined />{" "}
                            {moment(item.createDate).format(FORMAT_DATE_FULL)}
                          </span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                ))}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}
