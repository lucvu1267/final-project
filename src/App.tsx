import { JWT_AUTH } from "constants/index";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { setIsAuth } from "redux/slice";
import RouterList from "routers";
import { authRoutes } from "routers/routes";
function App() {
  const dispatch = useDispatch();
  dispatch(setIsAuth(localStorage.getItem(JWT_AUTH) ? true : false));
  // const isLoading = useSelector((state: any) => state.loadingSlice.isLoading);
 
  return (
    <BrowserRouter>
      {/* <div className={`app ${isLoading && "loading"}`}> */}
      <div className={`app`}>
        <RouterList listRoute={authRoutes} />
        {/* <RouterList listRoute={publicRoutes} /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
