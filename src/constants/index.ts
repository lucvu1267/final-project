export const JWT_AUTH = "jwt-authentication";

export const TYPE_FORM = {
  CREATE: "create",
  UPDATE: "update",
};

export const FORMAT_DATE = "DD/MM/YYYY";

export const FORMAT_DATE_FULL = "DD/MM/YYYY HH:mm";

export const routes = [
  {
    path: "",
    breadcrumbName: "Trang chủ",
  },
  {
    path: "event-manage",
    breadcrumbName: "Quản lý sự kiện",
  },
  {
    path: "add",
    breadcrumbName: "Thêm mới",
  },
  {
    path: "task",
    breadcrumbName: "Quản lý công việc",
  },
  {
    path: "contact",
    breadcrumbName: "Danh bạ",
  },
  {
    path: "profile",
    breadcrumbName: "Thông tin cá nhân",
  },
  {
    path: "edit",
    breadcrumbName: "Chỉnh sửa",
  },
  {
    path: "post",
    breadcrumbName: "Danh sách bài đăng",
  },
  {
    path: "detail",
    breadcrumbName: "Thông tin chi tiết",
  },
  {
    path: "ticket",
    breadcrumbName: "Vé sự kiện",
  },
  {
    path: "update",
    breadcrumbName: "Chỉnh sửa",
  },
  {
    path: "teams",
    breadcrumbName: "Ban tổ chức",
  },
  {
    path: "team",
    breadcrumbName: "Nhóm",
  },
  {
    path: "participant",
    breadcrumbName: "Danh sách người tham dự",
  },
  {
    path: "booths",
    breadcrumbName: "Quản lý gian hàng",
  },
  {
    path: "sponsors",
    breadcrumbName: "Danh sách nhà tài trợ",
  },
  {
    path: "post-detail",
    breadcrumbName: "Chi tiết bài viết",
  },
];
