export const MAX_LENGTH_TITLE = 100;
export const MAX_LENGTH_DESCRIPTION = 155;

export const TYPE_FEATURE_EVENT = {
  BOOTH: 1,
  SPONSER: 2,
  POST: 3,
  TICKET: 4,
};

export const LIST_FEATURE_EVENT = [
  { value: TYPE_FEATURE_EVENT.BOOTH, name: "Gian hàng" },
  { value: TYPE_FEATURE_EVENT.SPONSER, name: "Nhà tài trợ" },
  { value: TYPE_FEATURE_EVENT.POST, name: "Bài viết" },
  { value: TYPE_FEATURE_EVENT.TICKET, name: "Vé tham dự" },
];

export const TYPE_POST = {
  EVENT: "EVENT",
  TEAM: "TEAM",
};

export const TYPE_MODAL_MEMBER_EVENT = {
  ADD: "ADD",
  VIEW: "VIEW",
  UPDATE: "UPDATE",
  ADD_CONTACT: "ADD_CONTACT",
};

export const ROLE_EVENT = {
  HEADER: "HEADER",
  SUB_HEADER: "SUB_HEADER",
  MEMBER: "MEMBER",
};

export const ROLE_MEMBER = {
  NONE: "NONE",
  VIEW: "VIEW",
  EDIT: "EDIT",
};
