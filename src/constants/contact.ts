export const TYPE_ADD_CONTACT = {
  BY_USER: "INDIVIDUAL",
  OBJECT: "TEAM",
};
export const OPTION_ADD = [
  {
    value: TYPE_ADD_CONTACT.BY_USER,
    label: "Tìm kiếm theo tên người dùng",
  },
  {
    value: TYPE_ADD_CONTACT.OBJECT,
    label: "Tự tạo danh bạ",
  },
];
